<?php
session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set("Europe/Stockholm");

// function to convert character encoding in the old system
function utf8_converter($array) {
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
        }
    });
 
    return $array;
}

// hack to overcome nasty bug when sending PDF files via outlook.. :s
// seems to clear sessions so reset with ajax call for those internal-pages
if(isset($_GET['reloadSession']))
{
    $_SESSION['inlogged'] = true;
    $_SESSION['admin'] = true;
    exit;
}

	// include the file with paths and mysql info
	include("setup.php");
	// include the file with functions which is used in this and other files
	include("common.php");
	// include the file with security related functions
	include("security.php");

	// set "no cache" http headers
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");    			// date in the past
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");	// always modified
	header("Cache-Control: no-store, no-cache, must-revalidate");	// HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");                          			// HTTP/1.0

	// make a db connection to the MySQL server
//	mysql_select_db($mysql['db'], mysql_pconnect($mysql['host'], $mysql['user'], $mysql['password']));
@mysql_connect("127.0.0.1", "root", "mysqlpass") or die("Could not connect: " . mysql_error());
mysql_select_db("terab") or die("Could not connect: " . mysql_error());
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
	// if it's a login or a logout request
	if(isset($_REQUEST['function_file']) && $_REQUEST['function_file']=="login")
	{
		$redirect = run_function($_REQUEST);
		
		header("Location: ".$redirect);
		exit();
	}
	
	// user must be inlogged
	if(isset($_SESSION['inlogged']))
	{
                if (!isset($_SERVER['HTTP_REFERER']))
                    $_SERVER['HTTP_REFERER'] = "";

		// if the request is from outside the system, logout the user
		// (can't check if HTTP_REFERER is the same as the system URL because javascripts doesn't send HTTP_REFERER correctly)
		//if(substr($_SERVER['HTTP_REFERER'], 0, strlen($GLOBALS['system_url'])-1)!=substr($GLOBALS['system_url'], 0, strlen($GLOBALS['system_url'])-1) && $_SERVER['HTTP_REFERER']!="")
		//{
//			logger("the request (by ".$_SESSION['user_name'].") came from outside the system (".$_SERVER['HTTP_REFERER'].")");
			//header("Location: system.php?function_file=login&function=logout");
			//exit();
		//}

// TMP!
  //  $_SESSION['admin'] = true;
		if (!isset($_GET['page']))
		    $_GET['page'] = "start";

		// if it's a function request
		if(isset($_REQUEST['function_file']))
		{
		    $_REQUEST = utf8_converter($_REQUEST);
			
			$redirect = run_function($_REQUEST);
			
			header("Location: ".$redirect);
			exit();
		}
		// or if it's a page request
		else
		{
			display_page($_GET['page']);
		}
	}
	// if user isn't inlogged, show only login pages

	else
	{
		// if it's a login page
		if(isset($_GET['page']) && in_array($_GET['page'], $GLOBALS['login_pages']))
		{
			display_page($_GET['page']);
		}
		// or else, just show the login form
		else
		{
			display_page("login_form");
		}
	}
?>