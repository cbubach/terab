<?php
function public_createOrder($args)
{
	$sql = "select id as period_id from periods where current_period = 'Y'";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	$period_id = $row['period_id'];
	
	$sql = "insert into orders(user_id, customer_id, att, waybill_no, extra_waybills, total_items, order_date, create_date_time, period_id, notes, reports_text,person_nr,bank_nr) values('".$_SESSION['user_id']."','".$args['customer_id']."','".$args['att']."','".$args['waybill_no']."','".$args['extra_waybills']."','".$args['total_items']."','".$args['order_date']."',now(),'".$period_id."','".$args['notes']."','','".$args['person_nr']."','".$args['bank_nr']."')";
	mysql_query($sql);
	if(mysql_errno()!=0)
	{
		logger($sql."  |  ".mysql_error());
		return "page=error";
	}
	
	return "page=start";
}

function public_checkIfWaybillNoExistsJSON($args)
{
	$sql = "select count(id) as counter from orders where waybill_no = '".$args['waybill_no']."'";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	if($row['counter']!=0)
	{
		print '{"exists": "true", "counter": '.$row['counter'].'}';
		
		exit();
	}
	
	print '{"exists": "false", "counter": 0}';
	exit();
}

function public_sortOrder($args)
{
	if ($args['submit_type'] == "add_items")
	{
		if (!isset($_SESSION['sort_orders'][$args['order_id']]['products']))
		{
			$_SESSION['sort_orders'][$args['order_id']]['products'] = array();
			$_SESSION['sort_orders'][$args['order_id']]['price'] = array();
		}
		
		// update changed nr of items fields
		if (isset($args['items']))
		{
			$_SESSION['sort_orders'][$args['order_id']]['products'] = $args['items'][$args['order_id']]['products'];
			$_SESSION['sort_orders'][$args['order_id']]['price'] = $args['items'][$args['order_id']]['price'];
		}
		
		$_SESSION['sort_orders'][$args['order_id']]['att'] = $args['att'];
		$_SESSION['sort_orders'][$args['order_id']]['waybill_no'] = $args['waybill_no'];
		$_SESSION['sort_orders'][$args['order_id']]['extra_waybills'] = $args['extra_waybills'];
		$_SESSION['sort_orders'][$args['order_id']]['sort_date'] = $args['sort_date'];
		$_SESSION['sort_orders'][$args['order_id']]['notes'] = $args['notes'];
		$_SESSION['sort_orders'][$args['order_id']]['total_items'] = $args['total_items'];
			
		foreach($args['add_items'][$args['order_id']]['products'] as $product_id=>$nr_of_items)
		{
			if($nr_of_items!='' && intval($nr_of_items)>0)
			{
				$_SESSION['sort_orders'][$args['order_id']]['products'][$product_id] += $nr_of_items; 
			}
		}
		
		return "page=order_sort_form&order_id=".$args['order_id'];
	}
	elseif ($args['submit_type'] == "delete_items")
	{
		// update changed nr of items fields
		if (isset($args['items']))
		{
			$_SESSION['sort_orders'][$args['order_id']]['products'] = $args['items'][$args['order_id']]['products'];
			$_SESSION['sort_orders'][$args['order_id']]['price'] = $args['items'][$args['order_id']]['price'];
		}
		
		$_SESSION['sort_orders'][$args['order_id']]['att'] = $args['att'];
		$_SESSION['sort_orders'][$args['order_id']]['waybill_no'] = $args['waybill_no'];
		$_SESSION['sort_orders'][$args['order_id']]['extra_waybills'] = $args['extra_waybills'];
		$_SESSION['sort_orders'][$args['order_id']]['sort_date'] = $args['sort_date'];
		$_SESSION['sort_orders'][$args['order_id']]['notes'] = $args['notes'];
		$_SESSION['sort_orders'][$args['order_id']]['total_items'] = $args['total_items'];
		
		unset($_SESSION['sort_orders'][$args['order_id']]['products'][$args['delete_product_id']]);
				
		return "page=order_sort_form&order_id=".$args['order_id'];
	}
	elseif ($args['submit_type'] == "sort_finished")
	{
		if (!isset($_SESSION['sort_orders'][$args['order_id']]['products']))
		{
			$_SESSION['sort_orders'][$args['order_id']]['product_id'] = array();
		}
		
		// update changed nr of items fields
		if (isset($args['items']))
		{
			$_SESSION['sort_orders'][$args['order_id']]['products'] = $args['items'][$args['order_id']]['products'];
			$_SESSION['sort_orders'][$args['order_id']]['price'] = $args['items'][$args['order_id']]['price'];
		}
		
		foreach($_SESSION['sort_orders'][$args['order_id']]['products'] as $product_id=>$no_of_items)
		{
			// the price is specific to the customer
			$price_result = mysql_query("select price from prices where customer_id='".$args['customer_id']."' and product_id='".$product_id."'");
			
			if(isset($_SESSION['sort_orders'][$args['order_id']]['price'][$product_id]))
			{
				$price = $_SESSION['sort_orders'][$args['order_id']]['price'][$product_id];
				$price_origin = 'ORDER';
			}
			else if(mysql_num_rows($price_result)>0)
			{
				$price_row = mysql_fetch_assoc($price_result);
				$price = $price_row['price'];
				$price_origin = 'CUSTOMER';
			}
			else
			{
				// the price can be retrieved from the default customer
				$price_result = mysql_query("select price from prices where customer_id='0' and product_id='".$product_id."'");
				if(mysql_num_rows($price_result)>0)
				{
					$price_row = mysql_fetch_assoc($price_result);
					$price = $price_row['price'];
					$price_origin = 'DEFAULT';
				}
				else
				{
					// no price is specified
					$price = 0;
					$price_origin = 'NOT_SET';
				}
			}
			
			$sql = "insert into order_data(order_id, product_id, value, price, price_origin) values ('".$args['order_id']."', '".$product_id."', '".$no_of_items."','".$price."','".$price_origin."')";
			mysql_query($sql);
			if(mysql_errno()!=0)
			{
				logger($sql."  |  ".mysql_error());
				return "page=error";
			}
		}
		
		$period_result = mysql_query("select id from periods where current_period = 'Y'");
		$period_row = mysql_fetch_assoc($period_result);
		
		$sql = "update orders set att='".$args['att']."', period_id='".$period_row['id']."', waybill_no='".$args['waybill_no']."', extra_waybills='".$args['extra_waybills']."', total_items='".$args['total_items']."', notes='".$args['notes']."', sort_date='".$args['sort_date']."' where id='".$args['order_id']."'";
		mysql_query($sql);
		if(mysql_errno()!=0)
		{
			logger($sql."  |  ".mysql_error());
			return "page=error";
		}
		
		unset($_SESSION['sort_orders'][$args['order_id']]);

		return "page=order_sort_list";
	}
	else
	{
		logger("sort_order function called without a correct submit type set");
		
		return "page=error";
	}
}

function public_editOrder($args)
{
	if ($args['submit_type'] == "add_items")
	{
		if (!isset($_SESSION['edit_orders'][$args['order_id']]['products']))
		{
			$_SESSION['edit_orders'][$args['order_id']]['products'] = array();
		}
		
		// update changed values in the form
		if (isset($args['items']))
		{
			$_SESSION['edit_orders'][$args['order_id']]['products'] = $args['items'][$args['order_id']]['products'];
			$_SESSION['edit_orders'][$args['order_id']]['price'] = $args['items'][$args['order_id']]['price'];
		}
		
		$_SESSION['edit_orders'][$args['order_id']]['att'] = $args['att'];
		$_SESSION['edit_orders'][$args['order_id']]['waybill_no'] = $args['waybill_no'];
		$_SESSION['edit_orders'][$args['order_id']]['extra_waybills'] = $args['extra_waybills'];
		$_SESSION['edit_orders'][$args['order_id']]['notes'] = $args['notes'];
		$_SESSION['edit_orders'][$args['order_id']]['reports_text'] = $args['reports_text'];
		$_SESSION['edit_orders'][$args['order_id']]['total_items'] = $args['total_items'];
			
		foreach($args['add_items'][$args['order_id']]['products'] as $product_id=>$nr_of_items)
		{
			if($nr_of_items!='' && intval($nr_of_items)>0)
			{
				$_SESSION['edit_orders'][$args['order_id']]['products'][$product_id] += $nr_of_items; 
			}
		}
		
		return "page=order_edit_form&order_id=".$args['order_id'];
	}
	elseif ($args['submit_type'] == "delete_items")
	{
		// update changed values in the form
		if (isset($args['items']))
		{
			$_SESSION['edit_orders'][$args['order_id']]['products'] = $args['items'][$args['order_id']]['products'];
			$_SESSION['edit_orders'][$args['order_id']]['price'] = $args['items'][$args['order_id']]['price'];
		}
		
		$_SESSION['edit_orders'][$args['order_id']]['att'] = $args['att'];
		$_SESSION['edit_orders'][$args['order_id']]['waybill_no'] = $args['waybill_no'];
		$_SESSION['edit_orders'][$args['order_id']]['extra_waybills'] = $args['extra_waybills'];
		$_SESSION['edit_orders'][$args['order_id']]['notes'] = $args['notes'];
		$_SESSION['edit_orders'][$args['order_id']]['reports_text'] = $args['reports_text'];
		$_SESSION['edit_orders'][$args['order_id']]['total_items'] = $args['total_items'];
		
		unset($_SESSION['edit_orders'][$args['order_id']]['products'][$args['delete_product_id']]);
				
		return "page=order_edit_form&order_id=".$args['order_id'];
	}
	elseif ($args['submit_type'] == "edit_finished")
	{
		if(!isset($_SESSION['edit_orders'][$args['order_id']]['products']))
		{
			$_SESSION['edit_orders'][$args['order_id']]['product_id'] = array();
		}
		
		// update changed values in the form
		if(isset($args['items']))
		{
			$_SESSION['edit_orders'][$args['order_id']]['products'] = $args['items'][$args['order_id']]['products'];
			$_SESSION['edit_orders'][$args['order_id']]['price'] = $args['items'][$args['order_id']]['price'];
		}
		
		$sql = "delete from order_data where order_id = '".$args['order_id']."'";
		mysql_query($sql);
		if(mysql_errno()!=0)
		{
			logger($sql."  |  ".mysql_error());
			return "page=error";
		}
		
		foreach($_SESSION['edit_orders'][$args['order_id']]['products'] as $product_id=>$no_of_items)
		{
			// some products price (specified by field edit_order_set_price) can be set in the edit form
			if(isset($_SESSION['edit_orders'][$args['order_id']]['price'][$product_id]))
			{
				$price = $_SESSION['edit_orders'][$args['order_id']]['price'][$product_id];
				$price_origin = 'ORDER';
			}
			else
			{
				// the price is specific to the customer
				$price_result = mysql_query("select price from prices where customer_id='".$args['customer_id']."' and product_id='".$product_id."'");
				if(mysql_num_rows($price_result)>0)
				{
					$price_row = mysql_fetch_assoc($price_result);
					$price = $price_row['price'];
					$price_origin = 'CUSTOMER';
				}
				else
				{
					// the price can be retrieved from the default customer
					$price_result = mysql_query("select price from prices where customer_id='0' and product_id='".$product_id."'");
					if(mysql_num_rows($price_result)>0)
					{
						$price_row = mysql_fetch_assoc($price_result);
						$price = $price_row['price'];
						$price_origin = 'DEFAULT';
					}
					else
					{
						// no price is specified
						$price = 0;
						$price_origin = 'NOT_SET';
					}
				}
			}
			
			$sql = "insert into order_data(order_id, product_id, value, price, price_origin) values('".$args['order_id']."', '".$product_id."', '".$no_of_items."','".$price."','".$price_origin."')";
			mysql_query($sql);
			if(mysql_errno()!=0)
			{
				logger($sql."  |  ".mysql_error());
				return "page=error";
			}
		}
		
		$sql = "update orders set att='".$args['att']."', person_nr='".$args['person_nr']."', bank_nr='".$args['bank_nr']."', waybill_no='".$args['waybill_no']."', extra_waybills='".$args['extra_waybills']."', total_items='".$args['total_items']."', notes='".$args['notes']."', reports_text='".$args['reports_text']."', sort_date='".$args['sort_date']."' where id='".$args['order_id']."'";
		mysql_query($sql);
		if(mysql_errno()!=0)
		{
			logger($sql."  |  ".mysql_error());
			return "page=error";
		}
		
		unset($_SESSION['edit_orders'][$args['order_id']]);

		return "page=order_edit_list";
	}
	else
	{
		logger("edit_order function called without a correct submit type set");
		
		return "page=error";
	}
}

function public_deleteOrder($args)
{
	$sql = "delete from orders where id = '".$args['order_id']."'";
	mysql_query($sql);
	if(mysql_errno()!=0)
	{
		logger($sql."  |  ".mysql_error());
		return "page=error";
	}
	
	logger("User (".$_SESSION['user_id'].") deleted order (".$args['order_id'].")");
	
	return "page=order_edit_list";
}
?>