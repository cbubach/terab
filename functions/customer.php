<?php
function public_createCustomer($args)
{	
	$sql = "insert into customers(name, address, zip, city, contact_person, contact_phone, contact_email, contact_fax, vat_reg_no, bankgiro_no, default_report_type) values('".$args['name']."','".$args['address']."','".$args['zip']."','".$args['city']."','".$args['contact_person']."','".$args['contact_phone']."','".$args['contact_email']."','".$args['contact_fax']."','".$args['vat_reg_no']."','".$args['bankgiro_no']."', 'customer_letter')";
	mysql_query($sql);
	if(mysql_errno()!=0)
	{
		logger($sql."  |  ".mysql_error());
		return "page=error";
	}
	
	return "page=manage_customers";
}

function public_editCustomer($args)
{
        // get cust ID for other table here by comparing all old values before any update (id not same)
        //
        //     SELECT t1.id FROM `cust` AS t1, customers AS t2 WHERE t1.name = t2.name..... and so on...
        
        $sql_query =  "SELECT t1.id FROM `cust` AS t1, `customers` AS t2";
        $sql_query .= " WHERE t1.name = t2.name";
        $sql_query .= " AND t1.del_address = t2.address";
        $sql_query .= " AND t1.del_zip = t2.zip";
        $sql_query .= " AND t1.del_city = t2.city";
        $sql_query .= " AND t1.contact = t2.contact_person";
        $sql_query .= " AND t1.phone = t2.contact_phone";
        $sql_query .= " AND t1.contact_email = t2.contact_email";
        $sql_query .= " AND t1.fax = t2.contact_fax";
        $sql_query .= " AND t1.vat_reg_no = t2.vat_reg_no";
        $sql_query .= " AND t1.giro_no = t2.bankgiro_no";
        $sql_query .= " AND t2.id = '".$args['customer_id']."'";
        
        $old_id = mysql_result(mysql_query($sql_query), 0);


	$sql = "update customers set name='".$args['name']."', address='".$args['address']."', zip='".$args['zip']."', city='".$args['city']."', contact_person='".$args['contact_person']."', contact_phone='".$args['contact_phone']."', contact_email='".$args['contact_email']."', contact_fax='".$args['contact_fax']."', vat_reg_no='".$args['vat_reg_no']."', bankgiro_no='".$args['bankgiro_no']."' where id='".$args['customer_id']."'";
	mysql_query($sql);

	//  UPDATE 'cust'-table here....        <---------------- !!!
	$sql = "update cust set name='".$args['name']."', del_address='".$args['address']."', del_zip='".$args['zip']."', del_city='".$args['city']."', contact='".$args['contact_person']."', phone='".$args['contact_phone']."', contact_email='".$args['contact_email']."', fax='".$args['contact_fax']."', vat_reg_no='".$args['vat_reg_no']."', giro_no='".$args['bankgiro_no']."' where id='".$old_id."'";
	mysql_query($sql);

	if(mysql_errno()!=0)
	{
		logger($sql."  |  ".mysql_error());
		return "page=error";
	}
	
	return "page=manage_customers";
}

function public_deleteCustomer($args)
{	
	$sql = "delete from customers where id='".$args['customer_id']."'";
	mysql_query($sql);
	if(mysql_errno()!=0)
	{
		logger($sql."  |  ".mysql_error());
		return "page=error";
	}
	
	return "page=manage_customers";
}

function public_getCustomerContactPersonJSON($args)
{
	$sql = "select contact_person from customers where id='".$args['customer_id']."'";
	$result = mysql_query($sql);
	if(mysql_errno()!=0 || mysql_num_rows($result)==0)
	{
		print '{"contact_person": ""}';
		exit();
	}
	
	$row = mysql_fetch_assoc($result);
	
	print '{"contact_person": "'.$row['contact_person'].'"}';
	exit();
}

function public_autoSuggestCustomerJSON($args)
{
	header ("Content-Type: application/json; charset=utf-8");

	$sql = "select id, name from customers where name like '".$args['input']."%'";
	$result = mysql_query($sql);
	print '{"results": [';

	$first = true;
	while($row = mysql_fetch_assoc($result))
	{
		if(!$first)
			print ',';
		
		print '{"id": "'.$row['id'].'", "value": "'.$row['name'].'", "info": ""}';
		
		$first = false;
	}
	
	print ']}';
		
	exit();
}