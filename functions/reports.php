<?php
function public_saveCustomerLetterSentState($args)
{	
	if($args['state']=='sent')
		$state = 'Y';
	else
		$state = 'N';
	
	$sql = "update orders set customer_letter_sent = '".$state."' where id='".$args['order_id']."'";
	mysql_query($sql);
	if(mysql_errno()!=0)
	{
		logger($sql."  |  ".mysql_error());
		return "page=error";
	}
	
	return "page=blank";
}

function public_showCustomerDefaultReport($args)
{
	if(isset($args['customer_id']))
	{
		$result = mysql_query("select default_report_type from customers where id = '".$args['customer_id']."'");
		$row = mysql_fetch_assoc($result);

		if($row['default_report_type'] == "customer_letter")
		{
			return "page=reports_customer_letter&customer_id=".$args['customer_id']."&period_id=".$args['period_id'];
		}
		elseif($row['default_report_type'] == "customer_letter_simple")
		{
			return "page=reports_customer_letter_simple&customer_id=".$args['customer_id']."&period_id=".$args['period_id'];
		}
		elseif($row['default_report_type'] == "customer_letter_invoice_foundation")
		{
			return "page=reports_customer_letter_invoice_foundation&customer_id=".$args['customer_id']."&period_id=".$args['period_id'];
		}
	}
	elseif(isset($args['waybill_no']))
	{
		$result = mysql_query("select customers.default_report_type from customers, orders where orders.waybill_no = '".$args['waybill_no']."' and customers.id = orders.customer_id");
		$row = mysql_fetch_assoc($result);
		if($row['default_report_type'] == "customer_letter")
		{
			return "page=reports_customer_letter&waybill_no=".$args['waybill_no'];
		}
		elseif($row['default_report_type'] == "customer_letter_simple")
		{
			return "page=reports_customer_letter_simple&waybill_no=".$args['waybill_no'];
		}
		elseif($row['default_report_type'] == "customer_letter_invoice_foundation")
		{
			return "page=reports_customer_letter_invoice_foundation&waybill_no=".$args['waybill_no'];
		}
	}
	else {
		logger("neither customer_id or waybill_no specified in the showCustomerDefaultReport function call");
		return "page=error";
	}
}

function public_printCustomerDefaultReportJSON($args)
{
	$sql = "select default_report_type from customers where id='".$args['customer_id']."'";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	
	$data = '{"report_type": "'.$row['default_report_type'].'"}';
	
	print $data;
	
	exit();
}

function public_saveCustomerDefaultReport($args)
{
	$sql = "update customers set default_report_type = '".$args['report_type']."' where id = '".$args['customer_id']."'";
	mysql_query($sql);
	if(mysql_errno()!=0)
	{
		logger($sql."  |  ".mysql_error());
		return "page=error";
	}
	
	return "page=manage_reports";
}
?>