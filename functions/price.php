<?php
function public_printPriceJSON($args)
{
	$data = '{"prices": [';
	
	$added_products = array();
	$first = true;
	$first2 = true;
	
	$sql = "select product_id, price from prices where customer_id='".$args['customer_id']."'";
	$result = mysql_query($sql);
	if(mysql_num_rows($result)!=0)
	{
		while($row = mysql_fetch_assoc($result))
		{
			if(!$first)
				$data .= ",";
			
			$data .= '{"product_id": "'.$row['product_id'].'", "price": "'.$row['price'].'"}';
			
			array_push($added_products, $row['product_id']);
			
			$first = false;
		}
	}
	
	// set empty value on all products which wasn't specified above
	$sql = "select id from products";
	$result = mysql_query($sql);
	while($row = mysql_fetch_assoc($result))
	{
		if(!in_array($row['id'], $added_products))
		{
			if($first2)
			{
				if(!$first)
				{
					$data .= ",";
				}
			}
			else
			{
				$data .= ",";
			}
		
			$data .= '{"product_id": "'.$row['id'].'", "price": ""}';
			
			array_push($added_products, $row['product_id']);
			
			$first2 = false;
		}
	}
	
	$data .= ']}';
	
	print $data;
	
	exit();
}

function public_savePrice($args)
{
	foreach($args['price'] as $product_id=>$price)
	{
		$sql = "select price from prices where customer_id='".$args['customer_id']."' and product_id='".$product_id."'";
		$result = mysql_query($sql);
		// exists from before
		if(mysql_num_rows($result)!=0)
		{
			// delete
			if($price=='')
			{
				$sql = "delete from prices where customer_id='".$args['customer_id']."' and product_id='".$product_id."'";
				mysql_query($sql);
				if(mysql_errno()!=0)
				{
					logger($sql."  |  ".mysql_error());
					return "page=error";
				}
			}
			// update
			else
			{
				$sql = "update prices set price = '".$price."' where customer_id='".$args['customer_id']."' and product_id='".$product_id."'";
				mysql_query($sql);
				if(mysql_errno()!=0)
				{
					logger($sql."  |  ".mysql_error());
					return "page=error";
				}
			}
		}
		// new (and price is not empty)
		else if($price!="")
		{
			$sql = "insert into prices(customer_id, product_id, price) values('".$args['customer_id']."','".$product_id."','".$price."')";
			mysql_query($sql);
			if(mysql_errno()!=0)
			{
				logger($sql."  |  ".mysql_error());
				return "page=error";
			}		
		}
	
		// not when price is empty
		if($price!='')
		{
			// update price in all order data for orders that is not sent
			$sql = "update orders, order_data set order_data.price = '".$price."'";
			if($args['customer_id']!="0")
			{
				$sql .= ", order_data.price_origin = 'CUSTOMER' where orders.customer_id='".$args['customer_id']."' and order_data.price_origin in ('NOT_SET','DEFAULT','CUSTOMER') and";
			}
			else
			{
				$sql .= ", order_data.price_origin = 'DEFAULT' where order_data.price_origin in ('NOT_SET','DEFAULT') and";
			}
			$sql .= " order_data.product_id='".$product_id."' and orders.id = order_data.order_id and orders.customer_letter_sent = 'N'";
			mysql_query($sql);
			if(mysql_errno()!=0)
			{
				logger($sql."  |  ".mysql_error());
				return "page=error";
			}
		}
		// when price is empty, get default price instead (if it's not the DEFAULT customer which should be set)
		else
		{
			$sql = "select price from prices where customer_id='0' and product_id='".$product_id."'";
			$result = mysql_query($sql);
			if(mysql_num_rows($result)!=0 && $args['customer_id']!="0")
			{
				$row = mysql_fetch_assoc($result);
				$price = $row['price'];
				$origin = "DEFAULT";
			}
			else
			{
				$price = "0";
				$origin = "NOT_SET";
			}
			
			// update price in all order data for orders that is not sent
			$sql = "update orders, order_data set order_data.price = '".$price."', order_data.price_origin = '".$origin."' where order_data.product_id='".$product_id."' and orders.customer_id='".$args['customer_id']."' and orders.id = order_data.order_id and orders.customer_letter_sent = 'N'";
			mysql_query($sql);
			if(mysql_errno()!=0)
			{
				logger($sql."  |  ".mysql_error());
				return "page=error";
			}
		}
	}
	
	return "page=manage_prices";
}
?>