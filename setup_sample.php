<?
	// MySQL info
	$mysql = array();
	$mysql['host'] = "127.0.0.1";
	$mysql['user'] = "root";
	$mysql['password'] = "";
	$mysql['db'] = "terab";
	
	// the system's url
	$system_url = "http://192.168.1.36/terab/";
	
	// server paths
	$root_dir = "c:/terab_web/";
	$page_dir = $root_dir."pages/";
	$function_dir = $root_dir."functions/";
	$lib_dir = $root_dir."libs/";
	
	// upload dir
	$upload_dir = $root_dir."web/upload/";
	$upload_url = $wms_url."upload/";
	$upload_max_size = "2000000"; // can't be bigger than php.ini "upload_max_filesize" directive (often 2MB)
	
	// error and own logging
	$log_type = 3; // 1 = mail , 3 = file
	$log_destination = $root_dir."logs/messages.log";
	
	// turn off all error reporting
	//error_reporting(0);
	
	// make some variables globally reachable
	global $mysql, $system_url, $page_dir, $function_dir, $lib_dir, $upload_dir, $upload_max_size, $log_type, $log_destination;
?>