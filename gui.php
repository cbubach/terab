<?php 	include($file);

	// FIXME, not nice to clean the sort and edit forms at this place
	if (isset($_REQUEST['page']))
	{
	    if ($_REQUEST['page'] != "order_sort_form")
		unset($_SESSION['sort_orders']);
	    if ($_REQUEST['page'] != "order_edit_form")
		unset($_SESSION['edit_orders']);
	}
	
	header('Content-Type: text/html; charset=utf-8');
?>
<!--
-------------------------------------------------
Copyright Adev Solutions (info@adev.se) 2003-2006
-------------------------------------------------
-->
<html>
<head>
	<title>Terab</title>
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="style_gui.css">
	<script language="javascript">
	<!--
	-->
	</script>
<?php javascript(); ?>
</head>
<body bgcolor="#FFFFFF" marginwidth="0" marginheight="0" <?php body_onload(); ?>>
<table border="0" width="100%" cellspacing="0">
	<tr>
		<td valign="top" bgcolor="#FFFCD9" align="center">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<img src="images/blank.gif" width="0" height="80">
						<img src="images/logo.gif" valign="top">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" bgcolor="#FFFCD9" align="center">
			<table border="0" width="100%" cellspacing="0" cellpadding="5">
				<tr>
					<td align="center" valign="top">
						<span class="text8">
<?php if(isset($_SESSION['inlogged'])) { ?>
<a href="system.php?page=start">Startsidan</a> | <a href="system.php?function_file=login&function=logout">Logga ut</a>
<?php } else { ?>
&nbsp;
<?php } ?>
						</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<img src="images/blank.gif" width="1" height="10"><br>
<?php body(); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>