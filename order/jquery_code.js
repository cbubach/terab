//-------------------------------
//  TERAB booking, jQuery code
//   by Christoffer Bubach 2011
//-------------------------------
  $(document).ready(function() {

      // support for swedish week- and day-names
      jQuery(function($){
        $.datepicker.regional['sv'] = {
          closeText: 'Stäng',
          prevText: '&laquo;Förra',
          nextText: 'Nästa&raquo;',
          currentText: 'Idag',
          monthNames: ['Januari','Februari','Mars','April','Maj','Juni',
          'Juli','Augusti','September','Oktober','November','December'],
          monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
          'Jul','Aug','Sep','Okt','Nov','Dec'],
          dayNamesShort: ['Sön','Mån','Tis','Ons','Tor','Fre','Lör'],
          dayNames: ['Söndag','Måndag','Tisdag','Onsdag','Torsdag','Fredag','Lördag'],
          dayNamesMin: ['Sö','Må','Ti','On','To','Fr','Lö'],
          weekHeader: 'Ve',
          dateFormat: 'yy-mm-dd',
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['sv']);
      });
      
      // init auto-complete
      //----------------------
      $( "#cust_name0" ).autocomplete({
        source: "index.php?auto=1",
        minLength: 1,
        select: function( event, ui ) {
            document.getElementById('cust_id').value = ui.item.id;
        }
      });
      $( "#cust_name1" ).autocomplete({
        source: "index.php?auto=2",
        minLength: 1,
        select: function( event, ui ) {
            document.getElementById('cust_id').value = ui.item.id;
        }
      });
      $( ".artComplete0" ).autocomplete({
        source: "index.php?auto=3",
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,8);
          id = tmparr.join("");
          var name = 'artID' + id;
          document.getElementById(name).value = ui.item.id;
          if (ui.item.cat_id != "0")
              $("#PUG").val("1");
        }
      });
      $( ".artComplete1" ).autocomplete({
        source: "index.php?auto=4",
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,8);
          id = tmparr.join("");
          var name = 'artID' + id;
          document.getElementById(name).value = ui.item.id;
          if (ui.item.cat_id != "0")
              $("#PUG").val("1");
        }
      });

      $( ".art_Complete0" ).autocomplete({
        source: "index.php?auto=3",
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,7);
          id = tmparr.join("");
          var name = 'art_ID' + id;
          document.getElementById(name).value = ui.item.id;
          if (ui.item.cat_id != "0")
              $("#PUG").val("1");
        }
      });
      $( ".art_Complete1" ).autocomplete({
        source: "index.php?auto=4",
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,7);
          id = tmparr.join("");
          var name = 'art_ID' + id;
          document.getElementById(name).value = ui.item.id;
          if (ui.item.cat_id != "0")
              $("#PUG").val("1");
        }
      });


      // init drag'n'drop
      //-------------------
      $( ".droplist0" ).sortable({
        cancel: ".ui-state-disabled",
        connectWith: ".droplist0"
      });
      $( ".droplist1" ).sortable({
        cancel: ".ui-state-disabled",
        connectWith: ".droplist1"
      });
      
      // init tabs
      //-------------------
      $( "#custTabs" ).tabs();

      // receive sort event
      $( ".droplist0, .droplist1" ).bind( "sortreceive", function(event, ui) {
          var area_id = $(this).attr('id').substring(4);
          //alert("Element: "+ui.item.attr("id")+" släppt på " + area_id);

          $.ajax({
            type : 'GET',
            url : 'index.php',
            data : 'cmd=updatePlacement&id=' + ui.item.attr("id") + '&time=' + area_id,
            success : function(msg) {
              if (msg.length != 0) {
                $('<div>' + 'Fel - infon ej flyttad: error=' + msg + '</div>').dialog();
                return;
              }
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
              $('<div>' + 'Fel - infon ej flyttad: ' + textStatus + ' - ' + errorThrown + ' - ' + XMLHttpRequest + '</div>').dialog();
            }
          });
      });
      // disable text selection
      $( ".droplist0 .droplist1" ).disableSelection();

      $(".ui-state-default").live('dblclick', function() {
         var id = $(this).attr('id').substring(6);
         showEvent(id);
      });
      $(".ui-state-highlight").live('dblclick', function() {
         var id = $(this).attr('id').substring(6);
         showEvent(id);
      });


      // init datepicker
      //-----------------
      $( "#del_date" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      $( "#delDate" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      $( "#new_date" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      $( "#new_week" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      $( "#fDate" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      
      
      $( function() {
        var characterlimit = 30;
        $("#f_delinfo1,#f_delinfo2")
          .after( '<span style="padding-left: 10px;font-weight:bold;"></span>' ).next().hide().end()
          .keypress( function( e ){
            var current = $( this ).val().length;
            $( this ).next().show().text( characterlimit - current );
          }
        );
      });


      $('form#addEventForm').submit(function(e) {
          e.preventDefault();   // stop default event
          if ($("#bestName").val() == "")
          {
              $('<div title="Varning">OBS, du måste ange vem som lagt in beställningen!</div>').dialog({
                  modal: true,
                  buttons: {
                      Ok: function() {
                          $( this ).dialog( "close" );
                      }
                  }
              });
          }
          else if ($("#PUG").val() != "0")
          {
              $('<div title="Varning">OBS, glöm inte pall under gods!</div>').dialog({
                  modal: true,
                  buttons: {
                      "Ok, fortsätt": function() {
                          $( this ).dialog( "close" );
                          $("form#addEventForm")[0].submit();
                      },
                      "Ok, gå tillbaka": function() {
                          $( this ).dialog( "close" );
                      }
                  }
              });
          }
          else {
              $("form#addEventForm")[0].submit();
          }
      });

  });  // end of JS code that needs to run onload :)


  function addEvent(event_type, time)
  {
      // do some stuff :P
      document.getElementById('eventType').value = event_type;
      if (event_type == 0) {
          document.getElementById('newCust').innerHTML = '<a class="sexybutton" href="javascript:void(0);" onclick="addCust('+event_type+');"><span><span><img src="img/user_add.png" class="vmiddle" alt="Skapa kund" title="Skapa kund" /> Skapa ny kund</span></span></a>';
          document.getElementById('newArtField').innerHTML = '<br /><a class="sexybutton" href="javascript:void(0);" onclick="addArt('+event_type+');"><span><span><img src="img/application_add.png" class="vmiddle" alt="Skapa artikel" title="Skapa artikel" /> Skapa en artikel</span></span></a><br />' +
                                                             '<br /><a class="sexybutton" href="javascript:void(0);" onclick="addArticleToForm('+event_type+');"><span><span><img src="img/add.png" class="vmiddle" alt="Välj artikel" title="Välj artikel" /> Välj artikel</span></span></a><br />' +
                                                             '<br /><a class="sexybutton" href="javascript:void(0);" onclick="addPrevOrder('+event_type+');"><span><span><img src="img/book_previous.png" class="vmiddle" alt="Ladda senaste" title="Ladda senaste" /> Ladda senaste order</span></span></a><br />' +
                                                             '<br /><a class="sexybutton" href="javascript:void(0);" onclick="showPrevOrder('+event_type+');"><span><span><img src="img/book_previous.png" class="vmiddle" alt="Visa senaste" title="Visa senaste" /> Visa tidigare ordrar</span></span></a>';
      }
      else {
          document.getElementById('newCust').innerHTML = '<a class="sexybutton" href="javascript:void(0);" onclick="addCust('+event_type+');"><span><span><img src="img/user_add.png" class="vmiddle" alt="Skapa leverantör" title="Skapa leverantör" /> Skapa ny leverantör</span></span></a>';
          document.getElementById('newArtField').innerHTML = '<br /><a class="sexybutton" href="javascript:void(0);" onclick="addArt('+event_type+');"><span><span><img src="img/application_add.png" class="vmiddle" alt="Skapa artikel" title="Skapa artikel" /> Skapa en artikel</span></span></a><br />' +
                                                             '<br /><a class="sexybutton" href="javascript:void(0);" onclick="addPrevOrder('+event_type+');"><span><span><img src="img/book_previous.png" class="vmiddle" alt="Ladda senaste" title="Ladda senaste" /> Ladda senaste order</span></span></a><br />' +
                                                             '<br /><a class="sexybutton" href="javascript:void(0);" onclick="showPrevOrder('+event_type+');"><span><span><img src="img/book_previous.png" class="vmiddle" alt="Visa senaste" title="Visa senaste" /> Visa tidigare ordrar</span></span></a>';
      }

      document.getElementById('custField').innerHTML = '<input type="text" style="width:200px" id="cust_name'+event_type+'" name="cust_name'+event_type+'" value="" />';
      document.getElementById('artField').innerHTML = '<input type="text" class="artComplete'+event_type+'" style="width:150px" id="art_name1" name="art_name1" value="" />';
      document.getElementById('moreArtLink').innerHTML = '<a class="sexybutton" href="javascript:void(0);" onclick="addArtToForm('+event_type+');" style="margin-left: 20px;"><span><span><img src="img/add.png" class="vmiddle" alt="Fler artiklar" title="Fler artiklar" /> Fler artiklar</span></span></a>';

      // reset values
      document.getElementById('moreArticleFields2').innerHTML = "";
      $("#art_name1").val("");
      $("#bestName").val("");
      $("#art_amount1").val("");
      $("#artID1").val("");
      $("#artFieldNo").val("1");
      $("#cust_id").val("0");
      $("#PUG").val("0");
      $("#bestName").focus();

      // init jQuery autocomplete on customer name
      var auto_no = event_type + 1; // auto 1 or 2
      $( "#cust_name"+event_type ).autocomplete({
        source: "index.php?auto="+auto_no,
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('cust_id').value = ui.item.id;
        }
      });
      document.getElementById('date').value = time;
      $( "#addEvent" ).dialog({ height: 550, width: 550 });
      var auto_no = event_type + 3; // auto 3 or 4
      $( ".artComplete"+event_type ).autocomplete({
        source: "index.php?auto="+auto_no,
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,8);
          id = tmparr.join("");
          var name = 'artID' + id;
          document.getElementById(name).value = ui.item.id;
          if (ui.item.cat_id != "0")
              $("#PUG").val("1");
        }
      });
  }
  
  function addArticleToForm (event_type)
  {
      var url = 'index.php?cmd=getProdList&type=' + event_type;
      $.get(url, function (data) {
        document.getElementById('showAddArtCat').innerHTML = data;
        $("#accordion").accordion({autoHeight: false});
      });
      $("#showAddArtCat").dialog({ height: 500, width: 500 });
  }

  function addPrevOrder(event_type)
  {
      var FieldNo = document.getElementById('artFieldNo').value;
      var CustID  = document.getElementById('cust_id').value;

      var url = 'index.php?cmd=getEventIDfromCust&cust_id=' + CustID;
      $.get(url, function (data) {
        if (data != 0)
        {
          $.getJSON('index.php?cmd=getArtFromEventID&event_id=' + data, function(data) {
            var count = 0;
            $.each(data, function() {
              count++;
              if (count > FieldNo)
              {
                addArtToForm(event_type); // create fields if needed
              }
              document.getElementById('art_name' + count).value = data[count-1].name;
              document.getElementById('art_amount' + count).value = data[count-1].amount;
              document.getElementById('art_enhet' + count).value = data[count-1].enhet;
              document.getElementById('artID' + count).value = data[count-1].id;
            });
          });
          getLatestOrderNo();
        }
      });
  }
  
  function showPrevOrder(event_type)
  {
      var CustID  = document.getElementById('cust_id').value;

      var url = 'index.php?cmd=showPrevOrders&type=' + event_type + '&custID=' + CustID;
      $.get(url, function (data) {
        document.getElementById('showPrevOrders').innerHTML = data;
      });
      $("#showPrevOrders").dialog({ height: 500, width: 650 });
  }

  function addProductFromCat(prod_id, prod_name, event_type)
  {
      var FieldNo       = $('#artFieldNo').val();
      var latestArtID   = $('#artID'+FieldNo).val();
      var latestArtName = $('#art_name'+FieldNo).val();

      if ( latestArtID != "" || latestArtName != "" )
      {
          addArtToForm(event_type);
          FieldNo++;
      }

      $('#artID' + FieldNo).val(prod_id);
      $('#art_name' + FieldNo).val(prod_name);
      checkPUG(prod_id);
  }

  
  function getLatestOrderNo()
  {
      var CustID  = document.getElementById('cust_id').value;
      var url = 'index.php?cmd=getLatestOrderNo&cust_id=' + CustID;
      $.get(url, function (data) {
          if (data != 0)
          {
              document.getElementById('order_no').value = data;
          }
      });
  }

  function editNotes(t_time)
  {
      $('#noteDate').val(t_time);
      $('#note').val("");
      var url = 'index.php?cmd=getNoteFromDate&date=' + t_time;
      $.get(url, function (data) {
          if (data != 0)
          {
              document.getElementById('note').value = data;
          }
      });
      $("#editNote" ).dialog({ height: 470, width: 650 });
  }

    function interval(func, wait, times) {
        var interv = function(w, t){
            return function(){
                if(typeof t === "undefined" || t-- > 0){
                    setTimeout(interv, w);
                    try{
                        func.call(null);
                    }
                    catch(e){
                        t = 0;
                        throw e.toString();
                    }
                }
            };
        }(wait, times);

        setTimeout(interv, wait);
    };

    function refreshOrders(t_time, t_type)
    {
        var refreshId = interval(function() {
            $('#DL'+t_type+'_'+t_time).load('cmd.php?cmd=updateOrderList&time='+t_time+'&type='+t_type);
        }, 20000);
    }

  function removeEvent(id)
  {
      $('<div title="Radera bokning">Är du säker på att du vill radera denna bokning permanent?</div>').dialog({
        resizable: false,
        height:140,
        modal: true,
        buttons: {
          "Ja, radera": function() {
            var url = 'index.php?cmd=removeEvent&id=' + id;
            $.get(url, function (data) {
                window.location.reload();   // put this here to allow for GET request to finish
            });
            $( this ).dialog( "close" );
          },
          "Nej, avbryt": function() {
            $( this ).dialog( "close" );
          }
        }
      });
  }

  function addCust(event_type)
  {
      document.getElementById('custType').value = event_type;
      if (event_type==1)
      {
          $("#addCust").attr("title", "Skapa ny leverantör");
          $("#add_kundnamn").html("Leverantörsnamn:");
          $("#add_address").html("Hämt-adress:");
          $("#add_zip").html("Hämt-postnummer:");
          $("#add_city").html("Hämt-stad:");
          $("#alt_kundnamn").html("Alt. leverantörsnamn:");
          $("#add_country").html("Hämt-land:");
      }
      else
      {
          $("#addCust").attr("title", "Skapa ny kund");
          $("#add_kundnamn").html("Kundnamn:");
          $("#add_address").html("Leverans-adress:");
          $("#add_zip").html("Leverans-postnummer:");
          $("#add_city").html("Leverans-stad:");
          $("#alt_kundnamn").html("Alt. kundnamn:");
          $("#add_country").html("Leverans land:");
      }
      $( "#addCust" ).dialog({ height: 480, width: 800 });
  }

  function del_img_val(field_id)
  {
      $("#"+field_id).val("");
      $("#"+field_id+"_img").html('<img src="/order/img/error.png" alt="" />');
  }

  function addArt(event_type)
  {
      document.getElementById('artType').value = event_type;
      del_img_val("art_img");
      $( "#addArt" ).dialog({ height: 400, width: 350 });
  }
  
  function editEvent(id, event_type)
  {
      document.getElementById('art_type').value = event_type;
      document.getElementById('event_ID').value = id;
      document.getElementById('art_input').innerHTML = '<input type="text" class="art_Complete'+event_type+'" style="width:200px;" id="artName1" name="artName1" value="" />';
      document.getElementById('moreLink').innerHTML = '<a href="javascript:void(0);" onclick="addArtForm('+event_type+');" style="margin-left: 20px;"><img src="img/add.png" class="vmiddle" alt="Fler artiklar" title="Fler artiklar" /> Fler artiklar</a>';
      document.getElementById('custEditField').innerHTML = '<input type="text" style="width:200px" id="custName'+event_type+'" name="custName'+event_type+'" value="" />';

      // reset values - go away bugs!
      document.getElementById('moreArtFields2').innerHTML = "";
      document.getElementById('artName1').value = "";
      document.getElementById('artAmount1').value = "";
      document.getElementById('art_ID1').value = "";
      document.getElementById('fieldAmount').value ="1";


      $( "#editEvent" ).dialog({ height: 550, width: 450 });
      var auto_no = event_type + 1; // auto 1 or 2
      $( "#custName"+event_type ).autocomplete({
        source: "index.php?auto="+auto_no,
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('custID').value = ui.item.id;
        }
      });
      var auto_no = event_type + 3; // auto 3 or 4
      $( ".art_Complete"+event_type ).autocomplete({
        source: "index.php?auto="+auto_no,
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,7);       //keep an eye on this if you change field ID
          id = tmparr.join("");
          var name = 'art_ID' + id;
          document.getElementById(name).value = ui.item.id;
        }
      });
      // Load the order to fields
      loadOrderEdit(id, event_type);
  }

  function showEvent(id)
  {
      var url = 'index.php?cmd=showEvent&id=' + id;
      $.get(url, function (data) {
        document.getElementById('showEvent').innerHTML = data;
      });
      $( "#showEvent" ).dialog({ height: 500, width: 500 });
  }

  function toggleLockEvent(id)
  {
      var url = 'index.php?cmd=toggleLockEvent&id=' + id;
      $.get(url, function (data) {
          window.location.reload();
      });
  }
  
  function toggleLoading(id)
  {
     $('<div title="Lastning">Här kan du ändra lastningsalternativ för ordern</div>').dialog({
        resizable: false,
        height: 200,
        width: 350,
        modal: true,
        buttons: {
            "Inget": function() {
                var url = 'index.php?cmd=changeLoading&type=0&id=' + id;
                  $.get(url, function (data) {
                      window.location.reload();
                });
            },
            "Bilen": function() {
                var url = 'index.php?cmd=changeLoading&type=1&id=' + id;
                  $.get(url, function (data) {
                      window.location.reload();
                });
            },
            "Släpet": function() {
                var url = 'index.php?cmd=changeLoading&type=2&id=' + id;
                  $.get(url, function (data) {
                      window.location.reload();
                });
            },
            "Trailern": function() {
                var url = 'index.php?cmd=changeLoading&type=3&id=' + id;
                  $.get(url, function (data) {
                      window.location.reload();
                });
            },
            "Bil&Släp": function() {
                var url = 'index.php?cmd=changeLoading&type=5&id=' + id;
                  $.get(url, function (data) {
                      window.location.reload();
                });
            },
            "Lätt lastbil": function() {
                var url = 'index.php?cmd=changeLoading&type=6&id=' + id;
                  $.get(url, function (data) {
                      window.location.reload();
                });
            },
            "Hämtas": function() {
                var url = 'index.php?cmd=changeLoading&type=4&id=' + id;
                  $.get(url, function (data) {
                      window.location.reload();
                });
            }
        }
      });
  }

  function changeDate(id)
  {
      document.getElementById('eventID').value = id;
      $( "#changeDate" ).dialog({ height: 150, width: 200 });
  }
  
  function openKCFinder(field_id) {
      window.KCFinder = {};
      window.KCFinder.callBack = function(url) {
          window.KCFinder = null;
          $("#"+field_id).val(url);
          $("#"+field_id+"_img").html('<img src="/order/img/check.png" alt="" />');
      };
      window.open('/order/kcfinder/browse.php?type=images&lang=sv', 'kcfinder_single', "menubar=0,resizable=1,width=650,height=500");
  }

  function changeWeek()
  {
      //document.getElementById('eventID').value = id;
      $( "#changeWeek" ).dialog({ height: 150, width: 200 });
  }

  function round_float(x,n)
  {
    if(!parseInt(n))
        var n=0;
    if(!parseFloat(x))
        return false;
    return Math.round(x*Math.pow(10,n))/Math.pow(10,n);
  }

  function calcVolume()
  {
      var no_fields = $('#fNoArtFields').val();
      var c         = 0;
      CVtmp_v       = 0;
      CVtmp_val     = 0;

      for ( c = 1; c <= no_fields; c++ )    // c=1 instead of 0 to fix nasty bug.... took a while.
      {
          CVtmp_val = $('#fArtVolume'+c).val();
          if (CVtmp_val != "")
          {
              CVtmp_val = String(CVtmp_val).replace(/\D+$/, '');  // remove non numericals after last numerical
              CVtmp_val = String(CVtmp_val).replace(",", ".");
              CVtmp_v += Number(CVtmp_val);
          }
      }
      CVtmp_v = round_float(CVtmp_v, 1);      // round to 1 decimal place
      CVtmp_v = String(CVtmp_v).replace(".", ",");
      if (CVtmp_v != "" && CVtmp_v != "false")
      {
          $('#fM3').val(CVtmp_v);
      }
      else
      {
          $('#fM3').val("");
      }      
  }
  
  function prePrintCargoList(id, method) {
      if (method == "") {
          $("#shippingSetting").dialog({ height: 450, width: 500 });
		  $("#shipping_eventID").val(id);
	  } else {
	      printCargoList(id);
	  }
  }
  
  function checkShippingForm() {
       if ($('#otherShipping').is(':checked') && $("#otherShippingText").val() == "") {
	       alert("Du måste fylla i vilket när du väljer annat bolag!");
		   return;
	   } else if (!$("input[name='shippingMethodName']:checked").val()) {
	       alert("Du måste välja hur ordern fraktas för att kunna skriva ut fraktsedeln!");
		   return;
	   } else {
	       var shipping = $("input[name='shippingMethodName']").filter(":checked").val();
           if ($('#otherShipping').is(':checked')) {
		       shipping = shipping + ' - ' + $("#otherShippingText").val();
		   }
		   var id = $("#shipping_eventID").val();
           var url = 'index.php?cmd=setShippingMethod&order=' + id + '&shipping=' + shipping;
           $.get(url, function (data) {
		       $('#shippingSetting').dialog('close');
		       printCargoList(id);
		   });
	   }
  }

  function printCargoList(id)
  {
      $( "#printCargoList" ).dialog({ height: 620, width: 800 });
      $( "#fCargoListDiv" ).html(" ");
      $( "#feventID" ).val(id);
      //var dynamicArtList = "";

      $(':input','#cargoListForm').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');

      $( "#fDate" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd',
        currentText: 'Now'
      });

      var url = 'index.php?cmd=getfSenderData&id=1';
      $.get(url, function (data) {
        $('#aGAN').val(data[0].gan);
        $('#aName').val(data[0].name);
        $('#aAddress').val(data[0].address);
        $('#aZip').val(data[0].zip);
        $('#aCity').val(data[0].city);
        $('#aPhone').val(data[0].phone);
        $('#aRef').val(data[0].ref);
      });

      $("#fDate").datepicker('setDate', new Date());

      var url = 'index.php?cmd=getFno';
      $.get(url, function (data) {
        if (data == "Nr.serie slut!!")
        {
            $('<div title="Varning">OBS, nummerserien för fraktsedlar är slut! Du kan inte skriva ut denna fraktsedel innan detta är åtgärdat!</div>').dialog({
                modal: true,
                buttons: {
                    Ok: function() {
                        $( this ).dialog( "close" );
                        $('#printCargoList').dialog('close');
                        return false;
                    }
                }
            });
        }
        $('#fNo').val(data);
      });

      var url = 'index.php?cmd=getEventData&event_id=' + id;
      $.get(url, function (data) {
        document.getElementById('mName').value = data[0].name;
        document.getElementById('mRef').value = data[0].orderNo;

        var url = 'index.php?cmd=getCustData&cust_id=' + data[0].custID;
        $.get(url, function (data2) {

          if (data2[0].schenker_no != "")
              $('#mGAN').val(data2[0].schenker_no);
          if (data2[0].dhl_no != "")
              $('#mGAN').val(data2[0].dhl_no);
          if (data2[0].other_no != "")
              $('#mGAN').val(data2[0].other_no);
          document.getElementById('mAddress').value = data2[0].del_address;
          document.getElementById('mZip').value = data2[0].del_zip;
          document.getElementById('mCity').value = data2[0].del_city;
          document.getElementById('fDelInfo1').value = data2[0].f_delinfo1;
          document.getElementById('fDelInfo2').value = data2[0].f_delinfo2;

          if (data2[0].type == 1)
          {
              fSwitchSender();
          }

          $.getJSON('index.php?cmd=getArtFromEventID&event_id=' + id, function(data3) {
            var count  = 0;
            var weight = 0;
            var kolli  = 0;
            var tmp_w  = 0;

            var dynamicArtList = "";

            $.each(data3, function() {
              count++;

              if (data3[count-1].enhet != "st")
              {
                  weight = weight + (data3[count-1].amount * data3[count-1].weight );
                  tmp_w  = data3[count-1].amount * data3[count-1].weight ;
                  kolli  = kolli + (data3[count-1].amount * 1);

                  dynamicArtList = dynamicArtList + ' <tr>'+
                  '   <td> <input type="text" name="fArtMark'+count+'" id="fArtMark'+count+'" value="'+data3[count-1].amount+'" style="width: 50px" onchange="calcWeight(0);" /> </td>'+
                  '   <td> <input type="text" name="fArtAmount'+count+'" id="fArtAmount'+count+'" style="width: 50px" onchange="calcWeight(0);" /> </td>'+
                  '   <td> <input type="text" name="fArtType'+count+'" id="fArtType'+count+'" value="'+data3[count-1].enhet+'" style="width: 50px" /> </td>'+
                  '   <td> <input type="text" name="fArtName'+count+'" id="fArtName'+count+'" value="'+data3[count-1].name+'" style="width: 180px" /> </td>'+
                  '   <td> <input type="text" name="fArtNo'+count+'" id="fArtNo'+count+'" style="width: 50px" /> </td>'+
                  '   <td> <input type="text" name="fArtWeight'+count+'" id="fArtWeight'+count+'" value="'+tmp_w+'" style="width: 50px" onchange="calcWeight(1);" /> </td>'+
                  '   <td> <input type="text" name="fArtVolume'+count+'" id="fArtVolume'+count+'" style="width: 50px" onchange="calcVolume();" />'+
                  '     <input type="hidden" name="fOrgArtWeight'+count+'" id="fOrgArtWeight'+count+'" value="'+data3[count-1].weight+'" />'+
                  '     <input type="hidden" name="fArtID'+count+'" id="fArtID'+count+'" value="'+data3[count-1].id+'" />'+
                  '   </td>'+
                  ' </tr>';
              }
              else
              {
                  weight = weight + (data3[count-1].amount * data3[count-1].weight );
                  tmp_w  = data3[count-1].amount * data3[count-1].weight ;
                  kolli  = kolli + (data3[count-1].amount * 1);

                  dynamicArtList = dynamicArtList + ' <tr>'+
                  '   <td> <input type="text" name="fArtMark'+count+'" id="fArtMark'+count+'" style="width: 50px" onchange="calcWeight(0);" /> </td>'+
                  '   <td> <input type="text" name="fArtAmount'+count+'" id="fArtAmount'+count+'" value="'+data3[count-1].amount+'" style="width: 50px" onchange="calcWeight(0);" /> </td>'+
                  '   <td> <input type="text" name="fArtType'+count+'" id="fArtType'+count+'" value="st" style="width: 50px" /> </td>'+
                  '   <td> <input type="text" name="fArtName'+count+'" id="fArtName'+count+'" value="'+data3[count-1].name+'" style="width: 180px" /> </td>'+
                  '   <td> <input type="text" name="fArtNo'+count+'" id="fArtNo'+count+'" style="width: 50px" /> </td>'+
                  '   <td> <input type="text" name="fArtWeight'+count+'" id="fArtWeight'+count+'" value="'+tmp_w+'" style="width: 50px" onchange="calcWeight(1);" /> </td>'+
                  '   <td> <input type="text" name="fArtVolume'+count+'" id="fArtVolume'+count+'" style="width: 50px" onchange="calcVolume();" />'+
                  '     <input type="hidden" name="fOrgArtWeight'+count+'" id="fOrgArtWeight'+count+'" value="'+data3[count-1].weight+'" />'+
                  '     <input type="hidden" name="fArtID'+count+'" id="fArtID'+count+'" value="'+data3[count-1].id+'" />'+
                  '   </td>'+
                  ' </tr>';
              }
            });

            $('#fCargoListDiv').html(
            '<table>'+
            ' <tr>'+
            '   <td>Godsmärk.</td>'+
            '   <td>Kolliantal</td>'+
            '   <td>Kollislag</td>'+
            '   <td style="width: 200px;">Varuslag</td>'+
            '   <td>Varunr</td>'+
            '   <td>Vikt</td>'+
            '   <td>Volym</td>'+
            ' </tr>'+
            dynamicArtList +
            ' <tr>' +
            ' </tr>' +
            '</table>' +
            '<input type="hidden" name="fWeight" id="fWeight" value="'+ weight +'" />  ' +
            '<input type="hidden" name="fKAmount" id="fKAmount" value="'+ kolli +'" /> ' +
            '<input type="hidden" name="fM3" id="fM3" /> ' +
            '<input type="hidden" name="fNoArtFields" id="fNoArtFields" value="'+count+'" /> ');

            $.getJSON('index.php?cmd=getWayBillData&id=' + id, function(data) {
                if (data != null)
                {
                    //$('#printCargoList').populate(data);
                    $.each(data[0], function(key, value) {
                      //  alert(key+' = '+value);
                        if (value=="true")
                            $('#'+key).attr('checked',true);
                        else if (key != "fDate")                     // get old data but always use newer print date, discard saved
                            $('#'+key).val(value);
                    });
                }
                $('#fSenderPays').val("true");      // somewhere, somehow...
                $('#fRePays').val("true");          // they get value=""
                $('#fOtherPays').val("true");       // without this.
                $('#fAviPhone').val("true");        // and I don't have time
                $('#fAviFax').val("true");          // to fix the _real_ issue.

                calcVolume(); // make sure fM3 gets right value from values since last save.
                              // doesn't seem to work here?  put it at saveWayBill() too.
                if ($('#fNo').val() != "Nr.serie slut!!")
                {
                    saveWayBill(2); // making sure the user isn't slow in saving, thereby letting
                                    // another waybill-window get the same fno.
                                    // arg 2 is same as 1 except doesn't close window after saving
                }
            });

          });

        });
      });
  }

  function fSwitchSender ()
  {
      var tmp1_gan  = $('#aGAN').val();
      var tmp1_name = $('#aName').val();
      var tmp1_addr = $('#aAddress').val();
      var tmp1_zip  = $('#aZip').val();
      var tmp1_city = $('#aCity').val();
      var tmp1_ref  = $('#aRef').val();

      $('#aPhone').val("");

      $('#aGAN').val($('#mGAN').val());
      $('#aName').val($('#mName').val());
      $('#aAddress').val($('#mAddress').val());
      $('#aZip').val($('#mZip').val());
      $('#aCity').val($('#mCity').val());
      $('#aRef').val($('#mRef').val());

      $('#mGAN').val(tmp1_gan);
      $('#mName').val(tmp1_name);
      $('#mAddress').val(tmp1_addr);
      $('#mZip').val(tmp1_zip);
      $('#mCity').val(tmp1_city);
      $('#mRef').val(tmp1_ref);
  }

  var CWtmp_w = 0;

  function calcWeight (change_type)
  {
      var no_fields = $('#fNoArtFields').val();
      var c         = 0;
      CWtmp_w       = 0;
      CAtmp_a       = 0;
      
      //str.replace(",", ".");  // innan matte
      //str.replace(".", ",");  // efter matte

      for ( c = 1; c <= no_fields; c++ )    // c=1 instead of 0 to fix nasty bug.... took a while.
      {
          if ( change_type == 1 )
          {
              CWtmp_w += Number($('#fArtWeight'+c).val());
          }
          else
          {
              if ( ( $('#fArtMark'+c).length > 0 ) && ($('#fArtMark'+c).val() != "")  )
              {
                  var fArtMark = $('#fArtMark'+c).val();
                  fArtMark = Number(fArtMark.replace(/\D+$/, ''));
                  CWtmp_w = CWtmp_w + ( fArtMark * Number($('#fOrgArtWeight'+c).val()) );
                  CAtmp_a = CAtmp_a + Number($('#fArtAmount'+c).val());
                  var weight = fArtMark * Number($('#fOrgArtWeight'+c).val());
                  weight = Number((weight).toFixed(2));
                  weight = String(weight).replace(".", ",");
                  $('#fArtWeight'+c).val( weight );
              }
              else
              {
                  CWtmp_w = CWtmp_w + ( Number($('#fArtAmount'+c).val()) * Number($('#fOrgArtWeight'+c).val()) );
                  CAtmp_a = CAtmp_a + Number($('#fArtAmount'+c).val());
                  var weight = Number($('#fArtAmount'+c).val()) * Number($('#fOrgArtWeight'+c).val());
                  weight = Number((weight).toFixed(2));
                  weight = String(weight).replace(".", ",");
                  $('#fArtWeight'+c).val( weight );
              }
          }
          $('#fKAmount').val(CAtmp_a)
          $('#fWeight').val(CWtmp_w);
      }
  }

  function fHandleSender ()
  {
      var url = 'index.php?cmd=getfSenders';
      $.get(url, function (data) {
        $('#fSenderList').html('          <select name="fhSender" id="fhSender" style="width: 250px;" onchange="fhSenderChange(this.value)">'+
                              '            <option value="0">-Ny avsändare-</option>'+
                              data+
                              '          </select>');
      });

      $('#fhSenderID').val("0");
      $('#fhGAN').val("");
      $('#fhName').val("");
      $('#fhAddress').val("");
      $('#fhZip').val("");
      $('#fhCity').val("");
      $('#fhPhone').val("");
      $('#fhRef').val("");

    $( "#fHandleSender" ).dialog({ height: 430, width: 300 });
  }
  
  function fhSenderChange(senderID)
  {
      //var senderID = $('select.fhSender').val();
      if (senderID != 0)
      {
        var url = 'index.php?cmd=getfSenderData&id='+senderID;
        $.get(url, function (data) {
          $('#fhSenderID').val(data[0].id);
          $('#fhGAN').val(data[0].gan);
          $('#fhName').val(data[0].name);
          $('#fhAddress').val(data[0].address);
          $('#fhZip').val(data[0].zip);
          $('#fhCity').val(data[0].city);
          $('#fhPhone').val(data[0].phone);
          $('#fhRef').val(data[0].ref);
        });
      }
      else
      {
        $('#fhSenderID').val("0");
        $('#fhGAN').val("");
        $('#fhName').val("");
        $('#fhAddress').val("");
        $('#fhZip').val("");
        $('#fhCity').val("");
        $('#fhPhone').val("");
        $('#fhRef').val("");
      }
  }
  
  function fhSelectSender()
  {
    var senderID = $('#fhSenderID').val();
    var aGAN     = $('#fhGAN').val();
    var aName    = $('#fhName').val();
    var aAddress = $('#fhAddress').val();
    var aZip     = $('#fhZip').val();
    var aCity    = $('#fhCity').val();
    var aPhone   = $('#fhPhone').val();
    var aRef     = $('#fhRef').val();

    var url = 'index.php?cmd=setfSender&id='+senderID+'&aGAN='+aGAN+'&aName='+aName+'&aAddress='+aAddress+'&aZip='+aZip+'&aCity='+aCity+'&aPhone='+aPhone+'&aRef='+aRef;
    $.get(url, function (data) {
      $('#aGAN').val(aGAN);
      $('#aName').val(aName);
      $('#aAddress').val(aAddress);
      $('#aZip').val(aZip);
      $('#aCity').val(aCity);
      $('#aPhone').val(aPhone);
      $('#aRef').val(aRef);

      $('#fHandleSender').dialog('close');
    });
  }
  
  function saveWayBill(noPrint)
  {
      calcVolume();
      var arr = $('#printCargoList :input').serializeArray();
      if (noPrint == 1)
      {
          $.get('wb-save.php?noPrint=1', arr, function (data) {
              $('#printCargoList').dialog('close');
          });
      }
      else if (noPrint == 2)
      {
          //same as above except do not close window after saving!
          $.get('wb-save.php?noPrint=1', arr, function (data) {});
      }
      else
      {
          $.get('wb-save.php', arr, function (data) {
              //alert("Data Loaded: " + data);
              $('#printCargoList').dialog('close');
          });
      }
  }

  function fhDelSender()
  {
      var senderID = $('#fhSenderID').val();    
      $('<div title="Radera avsändare">Är du säker på att du vill radera denna avsändare permanent?</div>').dialog({
        resizable: false,
        height:140,
        modal: true,
        buttons: {
          "Ja, radera": function() {
            var url = 'index.php?cmd=removeWB&id=' + senderID;
            $.get(url, function (data) {
              $('#fhSender option[value='+senderID+']').attr('disabled','disabled');
              $('#fhSender option[value='+senderID+']').hide();
              $('#fhSender option[value=0]').attr('selected', 'selected');
              $('#fhSenderID').val("0");
              $('#fhGAN').val("");
              $('#fhName').val("");
              $('#fhAddress').val("");
              $('#fhZip').val("");
              $('#fhCity').val("");
              $('#fhPhone').val("");
              $('#fhRef').val("");
            });
            $( this ).dialog( "close" );
          },
          "Nej, avbryt": function() {
            $( this ).dialog( "close" );
          }
        }
      });
  }

  function editDate()
  {
    $( "#del_date" ).datepicker( "show" );
  }

  // adds new amount & article fields to the form
  function addArtToForm(event_type)
  {
    var FieldNo  = document.getElementById('artFieldNo').value;
    FieldNo++;
    var name1 = 'moreArticleFields'+FieldNo;
    var name2 = 'art_amount'+FieldNo;
    var name3 = 'art_name'+FieldNo;
    var name6 = 'art_enhet'+FieldNo;
    document.getElementById('artFieldNo').value = FieldNo;
    var name5 = 'artID'+FieldNo;
    FieldNo++;
    var name4 = 'moreArticleFields'+FieldNo;
    var html    = document.getElementById(name1).innerHTML;
    var amount  = '<input type="text" style="width:30px;" id="'+name2+'" name="'+name2+'" value="" /> ';
    var enhet   = '<input type="text" style="width:30px;" id="'+name6+'" name="'+name6+'" value="st" /> ';
    var article = '<input type="text" class="artComplete'+event_type+'" style="width:150px;" id="'+name3+'" name="'+name3+'" value="" /><br />';
    var idField = '<input type="hidden" id="'+name5+'" name="'+name5+'" value="" />';
    var newdiv  = '<div id="'+name4+'"> </div>';
    document.getElementById(name1).innerHTML = html + amount + enhet + article + idField + newdiv;

    // Make sure that Autocomplete inits for the JS generated inputs
    var auto_no = 3 + event_type;    // auto 3 or 4
    $( ".artComplete"+event_type ).autocomplete({
        source: "index.php?auto="+auto_no,
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,8);
          id = tmparr.join("");
          var name = 'artID' + id;
          document.getElementById(name).value = ui.item.id;
          if (ui.item.cat_id != "0")
              $("#PUG").val("1");
        }
    });
  }

  // adds new amount & article fields to the EDITform
  function addArtForm(event_type)
  {
    var FieldNo  = document.getElementById('fieldAmount').value;
    FieldNo++;
    var name1 = 'moreArtFields'+FieldNo;
    var name2 = 'artAmount'+FieldNo;
    var name6 = 'artEnhet'+FieldNo;
    var name3 = 'artName'+FieldNo;
    document.getElementById('fieldAmount').value = FieldNo;
    var name5 = 'art_ID'+FieldNo;
    FieldNo++;
    var name4 = 'moreArtFields'+FieldNo;
    var html    = document.getElementById(name1).innerHTML;
    var amount  = '<input type="text" style="width:40px; margin-right:1px;" id="'+name2+'" name="'+name2+'" value="" /> ';
    var enhet   = '<input type="text" style="width:40px; margin-right:1px;" id="'+name6+'" name="'+name6+'" value="st" /> ';
    var article = '<input type="text" class="art_Complete'+event_type+'" style="width:200px;margin-left: 7px;" id="'+name3+'" name="'+name3+'" value="" /><br />';
    var idField = '<input type="hidden" id="'+name5+'" name="'+name5+'" value="" />';
    var newdiv  = '<div id="'+name4+'"> </div>';
    document.getElementById(name1).innerHTML = html + amount + enhet + article + idField + newdiv;

    // Make sure that Autocomplete inits for the JS generated inputs
    var auto_no = 3 + event_type;    // auto 3 or 4
    $( ".art_Complete"+event_type ).autocomplete({
        source: "index.php?auto="+auto_no,
        minLength: 1,
        select: function( event, ui ) {
          var id = $(this).attr('id');
          var tmparr = id.split("");
          tmpaarr = tmparr.splice(0,7);     //changed to fit ID/name
          id = tmparr.join("");
          var name = 'art_ID' + id;
          document.getElementById(name).value = ui.item.id;
        }
    });
  }

  function loadOrderEdit(id, event_type)
  {
      var FieldNo = document.getElementById('fieldAmount').value;

      var url = 'index.php?cmd=getEventData&event_id=' + id;
      $.get(url, function (data) {
        document.getElementById('custName' + event_type).value = data[0].name;
        document.getElementById('custID').value = data[0].custID;
        document.getElementById('orderNo').value = data[0].orderNo;
        document.getElementById('custInfo').value = data[0].custInfo;
        document.getElementById('bestEditName').value = data[0].orderCreator;
        document.getElementById('delDate').value = data[0].prefDate;

        $.getJSON('index.php?cmd=getArtFromEventID&event_id=' + id, function(data) {
          var count = 0;
          $.each(data, function() {
            count++;
            if (count > FieldNo)
            {
              addArtForm(event_type); // create fields if needed
            }
            document.getElementById('artName' + count).value = data[count-1].name;
            document.getElementById('artAmount' + count).value = data[count-1].amount;
            document.getElementById('artEnhet' + count).value = data[count-1].enhet;
            document.getElementById('art_ID' + count).value = data[count-1].id;
          });
        });

      });

  }

  function postFormAJAX (form)
  {
    var query = $(form).serialize();
    var url = form.action + '?' + query;
    $.get(url);
    $('#addCust').dialog('close');   // do for "all" form dialogs (only one will actually close, since only one is open)
    $('#addArt').dialog('close');
  }

  function printLabel(form)
  {
    var eventType = $('#eventType').val();
    var custID    = $('#cust_id').val();
    var city      = "";

    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();

    var tmpDate = $('#date').val();
    tmpDate = tmpDate * 1000;
    var orderTime = new Date(tmpDate);
    var orderMonth = orderTime.getMonth() + 1;
    var orderDay = orderTime.getDate();
    var orderYear = orderTime.getFullYear();

    if (eventType==0)
    {
       allText = "Leverans "+orderDay+"/"+orderMonth+" "+orderYear+" (utskr. "+day+"/"+month+" "+year+')\n';
    }
    if (eventType==1)
    {
       allText = "Hämtning "+orderDay+"/"+orderMonth+" "+orderYear+" (utskr. "+day+"/"+month+" "+year+')\n';
    }

    var url = 'index.php?cmd=getCustData&cust_id=' + custID;
    $.get(url, function (data) {
      city = data[0].del_city;
      allText = allText + $('#cust_name'+eventType).val();// + ' ('+city+')\n';

      var elementTrue = 1;
      var count = 1;
      while (elementTrue != 0)
      {
        if ($('#art_amount'+count).length == 0)
        {
          elementTrue = 0;
          break;
        }
        else
        {
          allText = allText + $('#art_amount'+count).val() + ' ' + $('#art_name'+count).val() + '\n';
          count++;
        }
      }

      //printCustomLabel(allText);

      // post form
      var query = $(form).serialize();
      var url = form.action + '?' + query;
      $.get(url, function(){
        $('#addEvent').dialog('close');
        window.location.reload();
      });

    });
  }
  
  function toPascalCase(str)
  {
    var arr = str.split(/\s|_/);
    for(var i=0,l=arr.length; i<l; i++) {
        arr[i] = arr[i].substr(0,1).toUpperCase() +  arr[i].substr(1);
                 //(arr[i].length > 1 ? arr[i].substr(1).toLowerCase() : "");
    }
    return arr.join(" ");
  }
  /*
  function printCustomLabel(allText)
  {
      // open label
      var labelXml = '<?xml version="1.0" encoding="utf-8"?>\
      <DieCutLabel Version="8.0" Units="twips">\
        <PaperOrientation>Landscape</PaperOrientation>\
        <Id>Address</Id>\
        <PaperName>30252 Address</PaperName>\
        <DrawCommands/>\
        <ObjectInfo>\
            <TextObject>\
                <Name>Text</Name>\
                <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                <LinkedObjectName></LinkedObjectName>\
                <Rotation>Rotation0</Rotation>\
                <IsMirrored>False</IsMirrored>\
                <IsVariable>True</IsVariable>\
                <HorizontalAlignment>Left</HorizontalAlignment>\
                <VerticalAlignment>Top</VerticalAlignment>\
                <TextFitMode>ShrinkToFit</TextFitMode>\
                <UseFullFontHeight>True</UseFullFontHeight>\
                <Verticalized>False</Verticalized>\
                <StyledText/>\
            </TextObject>\
		<Bounds X="332" Y="150" Width="4455" Height="1260" />\
        </ObjectInfo>\
      </DieCutLabel>';

      var label = dymo.label.framework.openLabelXml(labelXml);

      // set label text
      label.setObjectText("Text", allText.replace("&", "&amp;"));

      // select printer to print on
      // for simplicity sake just use the first LabelWriter printer
      var printers = dymo.label.framework.getPrinters();
      if (printers.length == 0)
        throw "No DYMO printers are installed. Install DYMO printers.";

      var printerName = "";
      for (var i = 0; i < printers.length; ++i)
      {
        var printer = printers[i];
        if (printer.printerType == "LabelWriterPrinter")
        {
          printerName = printer.name;
          break;
        }
      }

      if (printerName == "")
        throw "No LabelWriter printers found. Install LabelWriter printer";

      // finally print the label
      label.print(printerName);
  }
  */