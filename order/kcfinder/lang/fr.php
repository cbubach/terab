<?php

/** French localization file for KCFinder
  * author: Damien BarrÃ¨re
  */

$lang = array(

    '_locale' => "fr_FR.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e %B, %Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

    "You don't have permissions to upload files." =>
    "Vous n'avez pas les droits nÃ©cessaires pour envoyer des fichiers.",

    "You don't have permissions to browse server." =>
    "Vous n'avez pas les droits nÃ©cessaires pour parcourir le serveur.",

    "Cannot move uploaded file to target folder." =>
    "Impossible de dÃ©placer le fichier tÃ©lÃ©chargÃ© vers le rÃ©pertoire de destination.",

    "Unknown error." =>
    "Erreur inconnue.",

    "The uploaded file exceeds {size} bytes." =>
    "Le fichier envoyÃ© dÃ©passe la taille maximale de {size} octects.",

    "The uploaded file was only partially uploaded." =>
    "Le fichier envoyÃ© ne l'a Ã©tÃ© que partiellement.",

    "No file was uploaded." =>
    "Aucun fichier n'a Ã©tÃ© envoyÃ©",

    "Missing a temporary folder." =>
    "Il manque un rÃ©pertoire temporaire.",

    "Failed to write file." =>
    "Impossible de crÃ©er le fichier.",

    "Denied file extension." =>
    "Type d'extension de fichier interdit.",

    "Unknown image format/encoding." =>
    "Format/encodage d'image inconnu.",

    "The image is too big and/or cannot be resized." =>
    "Image trop grande et/ou impossible de la redimensionner.",

    "Cannot create {dir} folder." =>
    "Impossible de crÃ©er le rÃ©pertoire {dir}.",

    "Cannot write to upload folder." =>
    "Impossible d'Ã©crire dans le rÃ©pertoire de destination.",

    "Cannot read .htaccess" =>
    "Impossible de lire le fichier .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "Fichier .htaccess incorrect. RÃ©Ã©criture du fichier impossible!",

    "Cannot read upload folder." =>
    "Impossible de lire le rÃ©pertoire d'envoi.",

    "Cannot access or create thumbnails folder." =>
    "Impossible d'accÃ©der ou de crÃ©er le rÃ©pertoire des miniatures.",

    "Cannot access or write to upload folder." =>
    "Impossible d'accÃ¨der ou d'Ã©crire dans le rÃ©pertoire d'envoi.",

    "Please enter new folder name." =>
    "Merci d'entrer le nouveau nom de dossier.",

    "Unallowable characters in folder name." =>
    "CaractÃ¨res non autorisÃ©s dans le nom du dossier.",

    "Folder name shouldn't begins with '.'" =>
    "Le nom du dossier ne peut pas commencer par '.'",

    "Please enter new file name." =>
    "Merci d'entrer le nouveau nom de fichier",

    "Unallowable characters in file name." =>
    "CaractÃ¨res non autorisÃ©s dans le nom du fichier.",

    "File name shouldn't begins with '.'" =>
    "Le nom du fichier ne peut pas commencer par '.'",

    "Are you sure you want to delete this file?" =>
    "Ãtes vous sÃ»r du vouloir supprimer ce fichier?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Ãtes vous sÃ»r du vouloir supprimer ce rÃ©pertoire et tous les fichiers qu'il contient?",

    "Non-existing directory type." =>
    "Type de rÃ©pertoire inexistant.",

    "Undefined MIME types." =>
    "MIME types non dÃ©clarÃ©s.",

    "Fileinfo PECL extension is missing." =>
    "L'extension' Fileinfo PECL est manquante.",

    "Opening fileinfo database failed." =>
    "Ouverture de la base de donnÃ©es fileinfo echouÃ©e.",

    "You can't upload such files." =>
    "Vous ne pouvez pas envoyer de tels fichiers.",

    "The file '{file}' does not exist." =>
    "Le fichier '{file}' n'existe pas.",

    "Cannot read '{file}'." =>
    "Impossible de lire le fichier '{file}'.",

    "Cannot copy '{file}'." =>
    "Impossible de copier le fichier '{file}'.",

    "Cannot move '{file}'." =>
    "Impossible de dÃ©placer le fichier '{file}'.",

    "Cannot delete '{file}'." =>
    "Impossible de supprimer le fichier '{file}'.",

    "Click to remove from the Clipboard" =>
    "Cliquez pour enlever du presse-papier",

    "This file is already added to the Clipboard." =>
    "Ce fihier a dÃ©ja Ã©tÃ© ajoutÃ© au presse-papier.",

    "Copy files here" =>
    "Copier les fichier ici",

    "Move files here" =>
    "DÃ©placer le fichiers ici",

    "Delete files" =>
    "Supprimer les fichiers",

    "Clear the Clipboard" =>
    "Vider le presse-papier",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Ãtes vous sÃ»r de vouloir supprimer tous les fichiers du presse-papier?",

    "Copy {count} files" =>
    "Copie de {count} fichiers",

    "Move {count} files" =>
    "DÃ©placement de {count} fichiers",

    "Add to Clipboard" =>
    "Ajouter au presse-papier",

    "New folder name:" => "Nom du nouveau dossier:",
    "New file name:" => "Nom du nouveau fichier:",

    "Upload" => "Envoyer",
    "Refresh" => "RafraÃ®chir",
    "Settings" => "ParamÃ¨tres",
    "Maximize" => "Agrandir",
    "About" => "A propos",
    "files" => "fichiers",
    "View:" => "Voir:",
    "Show:" => "Montrer:",
    "Order by:" => "Trier par:",
    "Thumbnails" => "Miniatures",
    "List" => "Liste",
    "Name" => "Nom",
    "Size" => "Taille",
    "Date" => "Date",
    "Descending" => "DÃ©croissant",
    "Uploading file..." => "Envoi en cours...",
    "Loading image..." => "Chargement de l'image'...",
    "Loading folders..." => "Chargement des dossiers...",
    "Loading files..." => "Chargement des fichiers...",
    "New Subfolder..." => "Nouveau sous-dossier...",
    "Rename..." => "Renommer...",
    "Delete" => "Supprimer",
    "OK" => "OK",
    "Cancel" => "Annuler",
    "Select" => "SÃ©lectionner",
    "Select Thumbnail" => "SÃ©lectionner la miniature",
    "View" => "Voir",
    "Download" => "TÃ©lÃ©charger",
    'Clipboard' => "Presse-papier",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "Impossible de renommer le dossier.",

    "Cannot delete the folder." =>
    "Impossible de supprimer le dossier.",

    "The files in the Clipboard are not readable." =>
    "Les fichiers du presse-papier ne sont pas lisibles.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "{count} fichiers dans le presse-papier ne sont pas lisibles. Voulez vous copier le reste?",

    "The files in the Clipboard are not movable." =>
    "Les fichiers du presse-papier ne peuvent pas Ãªtre dÃ©placÃ©s.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "{count} fichiers du presse-papier ne peuvent pas Ãªtre dÃ©placÃ©es. Voulez vous dÃ©placer le reste?",

    "The files in the Clipboard are not removable." =>
    "Les fichiers du presse-papier ne peuvent pas Ãªtre enlevÃ©s.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} fichiers du presse-papier ne peuvent pas Ãªtre enlevÃ©s. Vouslez vous supprimer le reste?",

    "The selected files are not removable." =>
    "Les fichiers sÃ©lectionnÃ©s ne peuvent pas Ãªtre enlevÃ©s.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "{count} fichier sÃ©lectionnÃ©s ne peuvent pas Ãªtre enlevÃ©s. Voulez vous supprimer le reste?",

    "Are you sure you want to delete all selected files?" =>
    "Ãtes vous sÃ»r de vouloir supprimer tous les fichiers sÃ©lectionnÃ©s?",

    "Failed to delete {count} files/folders." =>
    "Supression de {count} fichiers/dossiers impossible.",

    "A file or folder with that name already exists." =>
    "Un fichier ou dossier ayant ce nom existe dÃ©ja.",

    "Inexistant or inaccessible folder." =>
    "Dossier inexistant ou innacessible.",

    "selected files" => "fichiers sÃ©lectionnÃ©s",
    "Type" => "Type",
    "Select Thumbnails" => "SÃ©lectionner les miniatures",
    "Download files" => "TÃ©lÃ©charger les fichiers",

    // SINCE 2.4

    "Checking for new version..." => "VÃ©rifier l'existance d'une nouvelle version...",
    "Unable to connect!" => "Connexion impossible!",
    "Download version {version} now!" => "TÃ©lÃ©charger la version {version} maintenant!",
    "KCFinder is up to date!" => "KCFinder est Ã  jour!",
    "Licenses:" => "Licences:",
    "Attention" => "Alerte",
    "Question" => "Question",
    "Yes" => "Oui",
    "No" => "Non",
);

?>