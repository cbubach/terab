<?php

/** Bulgarian localization file for KCFinder
  * author: Pavel Tzonkov <pavelc@users.sourceforge.net>
  */

$lang = array(

    '_locale' => "bg_BG.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e %B, %Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

    "You don't have permissions to upload files." =>
    "ÐÑÐ¼Ð°ÑÐµ Ð¿ÑÐ°Ð²Ð° Ð·Ð° ÐºÐ°ÑÐ²Ð°Ð½Ðµ.",

    "You don't have permissions to browse server." =>
    "ÐÑÐ¼Ð°ÑÐµ Ð¿ÑÐ°Ð²Ð° Ð·Ð° ÑÐ°Ð·Ð³Ð»ÐµÐ¶Ð´Ð°Ð½Ðµ Ð½Ð° ÑÑÑÐ²ÑÑÐ°.",

    "Cannot move uploaded file to target folder." =>
    "Ð¤Ð°Ð¹Ð»ÑÑ Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° ÑÐµ Ð¿ÑÐµÐ¼ÐµÑÑÐ¸ Ð² ÑÐµÐ»ÐµÐ²Ð°ÑÐ° Ð¿Ð°Ð¿ÐºÐ°.",

    "Unknown error." =>
    "ÐÐµÐ¿Ð¾Ð·Ð½Ð°ÑÐ° Ð³ÑÐµÑÐºÐ°.",

    "The uploaded file exceeds {size} bytes." =>
    "ÐÐ°ÑÐµÐ½Ð¸ÑÑ ÑÐ°Ð¹Ð» Ð½Ð°Ð´ÑÐ²ÑÑÐ»Ñ {size} Ð±Ð°Ð¹ÑÐ°.",

    "The uploaded file was only partially uploaded." =>
    "ÐÐ°ÑÐµÐ½Ð¸ÑÑ ÑÐ°Ð¹Ð» Ð±ÐµÑÐµ ÐºÐ°ÑÐµÐ½ ÑÐ°Ð¼Ð¾ ÑÐ°ÑÑÐ¸ÑÐ½Ð¾.",

    "No file was uploaded." =>
    "Ð¤Ð°Ð¹Ð»ÑÑ Ð½Ðµ Ð±ÐµÑÐµ ÐºÐ°ÑÐµÐ½",

    "Missing a temporary folder." =>
    "ÐÐ¸Ð¿ÑÐ²Ð° Ð²ÑÐµÐ¼ÐµÐ½Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°.",

    "Failed to write file." =>
    "ÐÑÐµÑÐºÐ° Ð¿ÑÐ¸ Ð·Ð°Ð¿Ð¸ÑÐ²Ð°Ð½Ðµ Ð½Ð° ÑÐ°Ð¹Ð»Ð°.",

    "Denied file extension." =>
    "ÐÐ°Ð±ÑÐ°Ð½ÐµÐ½Ð¾ ÑÐ°Ð¹Ð»Ð¾Ð²Ð¾ ÑÐ°Ð·ÑÐ¸ÑÐµÐ½Ð¸Ðµ.",

    "Unknown image format/encoding." =>
    "Ð¤Ð°Ð¹Ð»ÑÑ Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð±ÑÐ´Ðµ ÑÐ°Ð·Ð¿Ð¾Ð·Ð½Ð°Ñ ÐºÐ°ÑÐ¾ Ð¸Ð·Ð¾Ð±ÑÐ°Ð¶ÐµÐ½Ð¸Ðµ.",

    "The image is too big and/or cannot be resized." =>
    "ÐÐ·Ð¾Ð±ÑÐ°Ð¶ÐµÐ½Ð¸ÐµÑÐ¾ Ðµ Ð¼Ð½Ð¾Ð³Ð¾ Ð³Ð¾Ð»ÑÐ¼Ð¾ Ð¸/Ð¸Ð»Ð¸ Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð±ÑÐ´Ðµ Ð¿ÑÐµÐ¾ÑÐ°Ð·Ð¼ÐµÑÐµÐ½Ð¾.",

    "Cannot create {dir} folder." =>
    "ÐÐµÐ²ÑÐ·Ð¼Ð¾Ð¶Ð½Ð¾ÑÑ Ð´Ð° ÑÐµ ÑÑÐ·Ð´Ð°Ð´Ðµ Ð¿Ð°Ð¿ÐºÐ° {dir}.",

    "Cannot rename the folder." =>
    "ÐÐ°Ð¿ÐºÐ°ÑÐ° Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° ÑÐµ Ð¿ÑÐµÐ¸Ð¼ÐµÑÐ²Ð°.",

    "Cannot write to upload folder." =>
    "ÐÐµ Ðµ Ð²ÑÐ·Ð¼Ð¾Ð¶Ð½Ð¾ Ð·Ð°Ð¿Ð¸ÑÐ²Ð°Ð½ÐµÑÐ¾ Ð½Ð° ÑÐ°Ð¹Ð»Ð¾Ð²Ðµ Ð² Ð¿Ð°Ð¿ÐºÐ°ÑÐ° Ð·Ð° ÐºÐ°ÑÐ²Ð°Ð½Ðµ.",

    "Cannot read .htaccess" =>
    "ÐÐµ Ðµ Ð²ÑÐ·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÑÐ¾ÑÐ¸ÑÐ°Ð½ÐµÑÐ¾ Ð½Ð° .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "ÐÐµÐ²Ð°Ð»Ð¸Ð´ÐµÐ½ .htaccess ÑÐ°Ð¹Ð». ÐÐµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° ÑÐµ Ð¿ÑÐµÐ·Ð°Ð¿Ð¸ÑÐµ Ð°Ð²ÑÐ¾Ð¼Ð°ÑÐ¸ÑÐ½Ð¾!",

    "Cannot read upload folder." =>
    "ÐÐµ Ðµ Ð²ÑÐ·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÑÐ¾ÑÐ¸ÑÐ°Ð½ÐµÑÐ¾ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°ÑÐ° Ð·Ð° ÐºÐ°ÑÐ²Ð°Ð½Ðµ.",

    "Cannot access or create thumbnails folder." =>
    "ÐÐµÐ²ÑÐ·Ð¼Ð¾Ð¶ÐµÐ½ Ð´Ð¾ÑÑÑÐ¿ Ð¸Ð»Ð¸ Ð½ÐµÐ²ÑÐ·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÑÐ·Ð´Ð°Ð²Ð°Ð½Ðµ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°ÑÐ° Ð·Ð° thumbnails.",

    "Cannot access or write to upload folder." =>
    "ÐÐ°Ð¿ÐºÐ°ÑÐ° Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° ÑÐµ Ð´Ð¾ÑÑÑÐ¿Ð¸ Ð¸Ð»Ð¸ Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° ÑÐµ Ð·Ð°Ð¿Ð¸ÑÐ²Ð° Ð² Ð½ÐµÑ.",

    "Please enter new folder name." =>
    "ÐÐ¾Ð»Ñ Ð²ÑÐ²ÐµÐ´ÐµÑÐµ Ð¸Ð¼Ðµ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°ÑÐ°.",

    "Unallowable characters in folder name." =>
    "ÐÐµÐ¿Ð¾Ð·Ð²Ð¾Ð»ÐµÐ½Ð¸ Ð·Ð½Ð°ÑÐ¸ Ð² Ð¸Ð¼ÐµÑÐ¾ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°ÑÐ°.",

    "Folder name shouldn't begins with '.'" =>
    "ÐÐ¼ÐµÑÐ¾ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°ÑÐ° Ð½Ðµ ÑÑÑÐ±Ð²Ð° Ð´Ð° Ð·Ð°Ð¿Ð¾ÑÐ²Ð° Ñ '.'",

    "Please enter new file name." =>
    "ÐÐ¾Ð»Ñ Ð²ÑÐ²ÐµÐ´ÐµÑÐµ Ð½Ð¾Ð²Ð¾ Ð¸Ð¼Ðµ Ð½Ð° ÑÐ°Ð¹Ð»Ð°",

    "Unallowable characters in file name." =>
    "ÐÐµÐ¿Ð¾Ð·Ð²Ð¾Ð»ÐµÐ½Ð¸ Ð·Ð½Ð°ÑÐ¸ Ð² Ð¸Ð¼ÐµÑÐ¾ Ð½Ð° ÑÐ°Ð¹Ð»Ð°.",

    "File name shouldn't begins with '.'" =>
    "ÐÐ¼ÐµÑÐ¾ Ð½Ð° ÑÐ°Ð¹Ð»Ð° Ð½Ðµ ÑÑÑÐ±Ð²Ð° Ð´Ð° Ð·Ð°Ð¿Ð¾ÑÐ²Ð° Ñ '.'",

    "Are you sure you want to delete this file?" =>
    "ÐÐ°Ð¸ÑÑÐ¸Ð½Ð° Ð»Ð¸ Ð¸ÑÐºÐ°ÑÐµ Ð´Ð° Ð¸Ð·ÑÑÐ¸ÐµÑÐµ ÑÐ¾Ð·Ð¸ ÑÐ°Ð¹Ð»?",

    "Are you sure you want to delete this folder and all its content?" =>
    "ÐÐ°Ð¸ÑÑÐ¸Ð½Ð° Ð»Ð¸ Ð¸ÑÐºÐ°ÑÐµ Ð´Ð° Ð¸Ð·ÑÑÐ¸ÐµÑÐµ ÑÐ°Ð·Ð¸ Ð¿Ð°Ð¿ÐºÐ° Ð¸ ÑÑÐ»Ð¾ÑÐ¾ Ð¹ ÑÑÐ´ÑÑÐ¶Ð°Ð½Ð¸Ðµ?",

    "Non-existing directory type." =>
    "ÐÐµÑÑÑÐµÑÑÐ²ÑÐ²Ð°Ñ ÑÐ¿ÐµÑÐ¸Ð°Ð»ÐµÐ½ ÑÐ¸Ð¿ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°.",

    "Undefined MIME types." =>
    "ÐÐµ ÑÐ° Ð´ÐµÑÐ¸Ð½Ð¸ÑÐ°Ð½Ð¸ MIME ÑÐ¸Ð¿Ð¾Ð²Ðµ.",

    "Fileinfo PECL extension is missing." =>
    "ÐÐ¸Ð¿ÑÐ²Ð° Fileinfo PECL ÑÐ°Ð·ÑÐ¸ÑÐµÐ½Ð¸Ðµ.",

    "Opening fileinfo database failed." =>
    "ÐÑÐµÑÐºÐ° Ð¿ÑÐ¸ Ð¾ÑÐ²Ð°ÑÑÐ½Ðµ Ð½Ð° fileinfo Ð´ÐµÑÐ¸Ð½Ð¸ÑÐ¸Ð¸.",

    "You can't upload such files." =>
    "ÐÐµ Ð¼Ð¾Ð¶ÐµÑÐµ Ð´Ð° ÐºÐ°ÑÐ²Ð°ÑÐµ ÑÐ°ÐºÐ¸Ð²Ð° ÑÐ°Ð¹Ð»Ð¾Ð²Ðµ.",

    "The file '{file}' does not exist." =>
    "Ð¤Ð°ÑÐ»ÑÑ '{file}' Ð½Ðµ ÑÑÑÐµÑÑÐ²ÑÐ²Ð°.",

    "Cannot read '{file}'." =>
    "Ð¤Ð°Ð¹Ð»ÑÑ '{file}' Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð±ÑÐ´Ðµ Ð¿ÑÐ¾ÑÐµÑÐµÐ½.",

    "Cannot copy '{file}'." =>
    "Ð¤Ð°Ð¹Ð»ÑÑ '{file}' Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð±ÑÐ´Ðµ ÐºÐ¾Ð¿Ð¸ÑÐ°Ð½.",

    "Cannot move '{file}'." =>
    "Ð¤Ð°Ð¹Ð»ÑÑ '{file}' Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð±ÑÐ´Ðµ Ð¿ÑÐµÐ¼ÐµÑÑÐµÐ½.",

    "Cannot delete '{file}'." =>
    "Ð¤Ð°Ð¹Ð»ÑÑ '{file}' Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð±ÑÐ´Ðµ Ð¸Ð·ÑÑÐ¸Ñ.",

    "Cannot delete the folder." =>
    "ÐÐ°Ð¿ÐºÐ°ÑÐ° Ð½Ðµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° Ð±ÑÐ´Ðµ Ð¸Ð·ÑÑÐ¸ÑÐ°.",

    "Click to remove from the Clipboard" =>
    "Ð¦ÑÐºÐ½ÐµÑÐµ Ð·Ð° Ð´Ð° Ð¿ÑÐµÐ¼Ð°ÑÐ½ÐµÑÐµ ÑÐ°Ð¹Ð»Ð° Ð¾Ñ ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð°",

    "This file is already added to the Clipboard." =>
    "Ð¢Ð¾Ð·Ð¸ ÑÐ°Ð¹Ð» Ð²ÐµÑÐµ Ðµ Ð´Ð¾Ð±Ð°Ð²ÐµÐ½ ÐºÑÐ¼ ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð°.",

    "The files in the Clipboard are not readable." =>
    "Ð¤Ð°Ð¹Ð»Ð¾Ð²ÐµÑÐµ Ð² ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð° Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° ÑÐµ Ð¿ÑÐ¾ÑÐµÑÐ°Ñ.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "{count} ÑÐ°Ð¹Ð»Ð° Ð² ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð° Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° ÑÐµ Ð¿ÑÐ¾ÑÐµÑÐ°Ñ. ÐÑÐºÐ°ÑÐµ Ð»Ð¸ Ð´Ð° ÐºÐ¾Ð¿Ð¸ÑÐ°ÑÐµ Ð¾ÑÑÐ°Ð½Ð°Ð»Ð¸ÑÐµ?",

    "The files in the Clipboard are not movable." =>
    "Ð¤Ð°Ð¹Ð»Ð¾Ð²ÐµÑÐµ Ð² ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð° Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° Ð±ÑÐ´Ð°Ñ Ð¿ÑÐµÐ¼ÐµÑÑÐµÐ½Ð¸.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "{count} ÑÐ°Ð¹Ð»Ð° Ð² ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð° Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° Ð±ÑÐ´Ð°Ñ Ð¿ÑÐµÐ¼ÐµÑÑÐµÐ½Ð¸. ÐÑÐºÐ°ÑÐµ Ð»Ð¸ Ð´Ð° Ð¿ÑÐµÐ¼ÐµÑÑÐ¸ÑÐµ Ð¾ÑÑÐ°Ð½Ð°Ð»Ð¸ÑÐµ?",

    "The files in the Clipboard are not removable." =>
    "Ð¤Ð°Ð¹Ð»Ð¾Ð²ÐµÑÐµ Ð² ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð° Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° Ð±ÑÐ´Ð°Ñ Ð¸Ð·ÑÑÐ¸ÑÐ¸.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} ÑÐ°Ð¹Ð»Ð° Ð² ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð° Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° Ð±ÑÐ´Ð°Ñ Ð¸Ð·ÑÑÐ¸ÑÐ¸. ÐÑÐºÐ°ÑÐµ Ð»Ð¸ Ð´Ð° Ð¸Ð·ÑÑÐ¸ÐµÑÐµ Ð¾ÑÑÐ°Ð½Ð°Ð»Ð¸ÑÐµ?",

    "The selected files are not removable." =>
    "ÐÐ·Ð±ÑÐ°Ð½Ð¸ÑÐµ ÑÐ°Ð¹Ð»Ð¾Ð²Ðµ Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° Ð±ÑÐ´Ð°Ñ Ð¸Ð·ÑÑÐ¸ÑÐ¸.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "{count} Ð¾Ñ Ð¸Ð·Ð±ÑÐ°Ð½Ð¸ÑÐµ ÑÐ°Ð¹Ð»Ð¾Ð²Ðµ Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° Ð±ÑÐ´Ð°Ñ Ð¸Ð·ÑÑÐ¸ÑÐ¸. ÐÑÐºÐ°ÑÐµ Ð»Ð¸ Ð´Ð° Ð¸Ð·ÑÑÐ¸ÐµÑÐµ Ð¾ÑÑÐ°Ð½Ð°Ð»Ð¸ÑÐµ?",

    "Are you sure you want to delete all selected files?" =>
    "ÐÐ°Ð¸ÑÑÐ¸Ð½Ð° Ð»Ð¸ Ð¸ÑÐºÐ°ÑÐµ Ð´Ð° Ð¸Ð·ÑÑÐ¸ÐµÑÐµ Ð²ÑÐ¸ÑÐºÐ¸ Ð¸Ð·Ð±ÑÐ°Ð½Ð¸ ÑÐ°Ð¹Ð»Ð¾Ð²Ðµ?",

    "Failed to delete {count} files/folders." =>
    "{count} ÑÐ°Ð¹Ð»Ð°/Ð¿Ð°Ð¿ÐºÐ¸ Ð½Ðµ Ð¼Ð¾Ð³Ð°Ñ Ð´Ð° Ð±ÑÐ´Ð°Ñ Ð¸Ð·ÑÑÐ¸ÑÐ¸.",

    "A file or folder with that name already exists." =>
    "ÐÐµÑÐµ Ð¸Ð¼Ð° ÑÐ°Ð¹Ð» Ð¸Ð»Ð¸ Ð¿Ð°Ð¿ÐºÐ° Ñ ÑÐ°ÐºÐ¾Ð²Ð° Ð¸Ð¼Ðµ.",

    "Copy files here" =>
    "ÐÐ¾Ð¿Ð¸ÑÐ°Ð¹ ÑÐ°Ð¹Ð»Ð¾Ð²ÐµÑÐµ ÑÑÐº",

    "Move files here" =>
    "ÐÑÐµÐ¼ÐµÑÑÐ¸ ÑÐ°Ð¹Ð»Ð¾Ð²ÐµÑÐµ ÑÑÐº",

    "Delete files" =>
    "ÐÐ·ÑÑÐ¸Ð¹ ÑÐ°Ð¹Ð»Ð¾Ð²ÐµÑÐµ",

    "Clear the Clipboard" =>
    "ÐÐ·ÑÐ¸ÑÑÐ¸ ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð°",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "ÐÐ°Ð¸ÑÑÐ¸Ð½Ð° Ð»Ð¸ Ð¸ÑÐºÐ°ÑÐµ Ð´Ð° Ð¸Ð·ÑÑÐ¸ÐµÑÐµ Ð²ÑÐ¸ÑÐºÐ¸ ÑÐ°Ð¹Ð»Ð¾Ð²Ðµ Ð¾Ñ ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð°?",

    "Copy {count} files" =>
    "ÐÐ¾Ð¿Ð¸ÑÐ°Ð¹ {count} ÑÐ°Ð¹Ð»Ð°",

    "Move {count} files" =>
    "ÐÑÐµÐ¼ÐµÑÑÐ¸ {count} ÑÐ°Ð¹Ð»Ð°",

    "Add to Clipboard" =>
    "ÐÐ¾Ð±Ð°Ð²Ð¸ ÐºÑÐ¼ ÐºÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´Ð°",

    "Inexistant or inaccessible folder." =>
    "ÐÐµÑÑÑÐµÑÑÐ²ÑÐ²Ð°ÑÐ° Ð¸Ð»Ð¸ Ð½ÐµÐ´Ð¾ÑÑÑÐ¿Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°.",

    "New folder name:" => "ÐÐ¼Ðµ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°ÑÐ°:",
    "New file name:" => "ÐÐ¾Ð²Ð¾ Ð¸Ð¼Ðµ Ð½Ð° ÑÐ°Ð¹Ð»Ð°:",

    "Upload" => "ÐÐ°ÑÐ¸",
    "Refresh" => "ÐÐºÑÑÐ°Ð»Ð¸Ð·Ð¸ÑÐ°Ð¹",
    "Settings" => "ÐÐ°ÑÑÑÐ¾Ð¹ÐºÐ¸",
    "Maximize" => "Ð Ð°Ð·Ð¿ÑÐ½Ð¸",
    "About" => "ÐÐ½ÑÐ¾ÑÐ¼Ð°ÑÐ¸Ñ",
    "files" => "ÑÐ°Ð¹Ð»Ð°",
    "selected files" => "Ð¸Ð·Ð±ÑÐ°Ð½Ð¸ ÑÐ°Ð¹Ð»Ð°",
    "View:" => "ÐÐ·Ð³Ð»ÐµÐ´:",
    "Show:" => "ÐÐ¾ÐºÐ°Ð¶Ð¸:",
    "Order by:" => "ÐÐ¾Ð´ÑÐµÐ´Ð¸ Ð¿Ð¾:",
    "Thumbnails" => "ÐÐ°ÑÑÐ¸Ð½ÐºÐ¸",
    "List" => "Ð¡Ð¿Ð¸ÑÑÐº",
    "Name" => "ÐÐ¼Ðµ",
    "Type" => "Ð¢Ð¸Ð¿",
    "Size" => "Ð Ð°Ð·Ð¼ÐµÑ",
    "Date" => "ÐÐ°ÑÐ°",
    "Descending" => "ÐÐ±ÑÐ°ÑÐµÐ½ ÑÐµÐ´",
    "Uploading file..." => "Ð¤Ð°Ð¹Ð»ÑÑ ÑÐµ ÐºÐ°ÑÐ²Ð°...",
    "Loading image..." => "ÐÐ·Ð¾Ð±ÑÐ°Ð¶ÐµÐ½Ð¸ÐµÑÐ¾ ÑÐµ Ð·Ð°ÑÐµÐ¶Ð´Ð°...",
    "Loading folders..." => "ÐÐ°ÑÐµÐ¶Ð´Ð°Ð½Ðµ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ¸ÑÐµ...",
    "Loading files..." => "ÐÐ°ÑÐµÐ¶Ð´Ð°Ð½Ðµ Ð½Ð° Ð¿Ð°Ð¿ÐºÐ°ÑÐ°...",
    "New Subfolder..." => "ÐÐ¾Ð²Ð° Ð¿Ð¾Ð´Ð¿Ð°Ð¿ÐºÐ°...",
    "Rename..." => "ÐÑÐµÐ¸Ð¼ÐµÐ½ÑÐ²Ð°Ð½Ðµ...",
    "Delete" => "ÐÐ·ÑÑÐ¸Ð¹",
    "OK" => "OK",
    "Cancel" => "ÐÑÐºÐ°Ð·",
    "Select" => "ÐÐ·Ð±ÐµÑÐ¸",
    "Select Thumbnail" => "ÐÐ·Ð±ÐµÑÐ¸ Ð¼Ð°Ð»ÑÐº Ð²Ð°ÑÐ¸Ð°Ð½Ñ",
    "Select Thumbnails" => "ÐÐ·Ð±ÐµÑÐ¸ Ð¼Ð°Ð»ÐºÐ¸ Ð²Ð°ÑÐ¸Ð°Ð½ÑÐ¸",
    "View" => "ÐÑÐµÐ³Ð»ÐµÐ´",
    "Download" => "Ð¡Ð²Ð°Ð»Ð¸",
    "Download files" => "Ð¡Ð²Ð°Ð»Ð¸ ÑÐ°Ð¹Ð»Ð¾Ð²ÐµÑÐµ",
    "Clipboard" => "ÐÐ»Ð¸Ð¿Ð±Ð¾ÑÐ´",

    // SINCE 2.4

    "Checking for new version..." => "ÐÑÐ¾Ð²ÐµÑÐºÐ° Ð·Ð° Ð½Ð¾Ð²Ð° Ð²ÐµÑÑÐ¸Ñ...",
    "Unable to connect!" => "ÐÐµ Ð¼Ð¾Ð¶Ðµ Ð´Ð° ÑÐµ ÑÐ²ÑÑÐ¶Ðµ!",
    "Download version {version} now!" => "Ð¡Ð²Ð°Ð»ÐµÑÐµ Ð²ÐµÑÑÐ¸Ñ {version} ÑÐµÐ³Ð°!",
    "KCFinder is up to date!" => "KCFinder Ðµ Ð°ÐºÑÑÐ°Ð»ÐµÐ½!",
    "Licenses:" => "ÐÐ¸ÑÐµÐ½Ð·Ð¸:",
    "Attention" => "ÐÐ½Ð¸Ð¼Ð°Ð½Ð¸Ðµ",
    "Question" => "ÐÑÐ¿ÑÐ¾Ñ",
    "Yes" => "ÐÐ°",
    "No" => "ÐÐµ",

    // SINCE 2.41

    "You cannot rename the extension of files!" =>
    "ÐÐµ Ð¼Ð¾Ð¶ÐµÑÐµ Ð´Ð° Ð¿ÑÐµÐ¸Ð¼ÐµÐ½ÑÐ²Ð°ÑÐµ ÑÐ°Ð·ÑÐ¸ÑÐµÐ½Ð¸ÑÑÐ° Ð½Ð° ÑÐ°Ð¹Ð»Ð¾Ð²ÐµÑÐµ!",

    // SINCE 2.5

    "Uploading file {number} of {count}... {progress}" =>
    "ÐÐ°ÑÐ²Ð°Ð½Ðµ Ð½Ð° ÑÐ°Ð¹Ð» {number} Ð¾Ñ {count}... {progress}",

    "Failed to upload {filename}!" => "ÐÐµÑÐ¿Ð¾Ð»ÑÑÐ»Ð¸Ð²Ð¾ ÐºÐ°ÑÐ²Ð°Ð½Ðµ Ð½Ð° {filename}!",
);

?>