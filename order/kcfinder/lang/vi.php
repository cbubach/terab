<?php

/** vi localization file for KCFinder
  * Tran Van Quyet - HQV ltd
  */

$lang = array(

    '_locale' => "vi_VN.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e %B, %Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

    "You don't have permissions to upload files." => "Báº¡n khÃ´ng cÃ³ quyá»n táº£i lÃªn",
    "You don't have permissions to browse server." => "Báº¡n khÃ´ng cÃ³ quyá»n truy cáº­p",
    "Cannot move uploaded file to target folder." => "KhÃ´ng thá» táº£i lÃªn thÆ° má»¥c ÄÃ­ch",
    "Unknown error." => "Lá»i khÃ´ng xÃ¡c Äá»nh",
    "The uploaded file exceeds {size} bytes." => "Táº­p tin táº£i lÃªn lá»n hÆ¡n {size}",
    "The uploaded file was only partially uploaded." => "CÃ¡c táº­p tin chá» ÄÆ°á»£c táº£i lÃªn má»t pháº§n",
    "No file was uploaded." => "KhÃ´ng cÃ³ táº­p tin ÄÆ°á»£c táº£i lÃªn",
    "Missing a temporary folder." => "KhÃ´ng tháº¥y thÆ° má»¥c táº¡m",
    "Failed to write file." => "KhÃ´ng thá» ghi",
    "Denied file extension." => "Pháº§n má» rá»ng khÃ´ng ÄÆ°á»£c phÃ©p",
    "Unknown image format/encoding." => "KhÃ´ng biáº¿t Äá»nh dáº¡ng áº£nh/mÃ£ hÃ³a nÃ y",
    "The image is too big and/or cannot be resized." => "HÃ¬nh áº£nh quÃ¡ lÆ¡n/hoáº·c khÃ´ng thá» thay Äá»i kÃ­ch thÆ°á»c",
    "Cannot create {dir} folder." => "KhÃ´ng thá» táº¡o thÆ° má»¥c {dir}",
    "Cannot rename the folder." => "KhÃ´ng thá» Äá»i tÃªn thÆ° má»¥c",
    "Cannot write to upload folder." => "KhÃ´ng thá» ghi vÃ o thÆ° má»¥c",
    "Cannot read .htaccess" => "KhÃ´ng thá» Äá»c táº­p tin .htaccess",
    "Incorrect .htaccess file. Cannot rewrite it!" => "khÃ´ng thá» ghi táº­p tin .htaccess",
    "Cannot read upload folder." => "KhÃ´ng thá» Äá»c thÆ° má»¥c Äá» táº£i lÃªn",
    "Cannot access or create thumbnails folder." => "KhÃ´ng cÃ³ quyá»n truy cáº­p hoáº·c khÃ´ng thá» táº¡o thÆ° má»¥c",
    "Cannot access or write to upload folder." => "KhÃ´ng cÃ³ quyá»n truy cáº­p hoáº·c khÃ´ng thá» ghi",
    "Please enter new folder name." => "Vui lÃ²ng nháº­p tÃªn thÆ° má»¥c",
    "Unallowable characters in folder name." => "TÃªn thÆ° má»¥c cÃ³ chá»©a nhá»¯ng kÃ½ tá»± khÃ´ng ÄÆ°á»£c phÃ©p",
    "Folder name shouldn't begins with '.'" => "ThÆ° má»¥c khÃ´ng thá» báº¯t Äáº§u báº±ng '.'",
    "Please enter new file name." => "Vui lÃ²ng nháº­p tÃªn táº­p tin",
    "Unallowable characters in file name." => "TÃªn táº­p tin chá»©a nhá»¯ng kÃ½ tá»± khÃ´ng ÄÆ°á»£c phÃ©p",
    "File name shouldn't begins with '.'" => "Táº­p tin khÃ´ng thá» báº¯t Äáº§u báº±ng '.'",
    "Are you sure you want to delete this file?" => "Báº¡n cÃ³ cháº¯c báº¡n muá»n xÃ³a táº­p tin nÃ y?",
    "Are you sure you want to delete this folder and all its content?" => "Báº¡n cÃ³ cháº¯c báº¡n muá»n xÃ³a thÆ° má»¥c vÃ  táº¥t cáº£ ná»i dung bÃªn trong?",
    "Non-existing directory type." => "KhÃ´ng tá»n táº¡i thÆ° má»¥c",
    "Undefined MIME types." => "KhÃ´ng biáº¿t kiá»u MIME nÃ y",
    "Fileinfo PECL extension is missing." => "Fileinfo PECL extension is missing",
    "Opening fileinfo database failed." => "Opening fileinfo database failed",
    "You can't upload such files." => "Báº¡n khÃ´ng thá» táº£i cÃ¡c táº­p tin nhÆ° váº­y.",
    "The file '{file}' does not exist." => "Táº­p tin '{file}' ÄÃ£ cÃ³",
    "Cannot read '{file}'." => "KhÃ´ng thá» Äá»c '{file}'.",
    "Cannot copy '{file}'." => "KhÃ´ng thá» sao chÃ©p '{file}'.",
    "Cannot move '{file}'." => "KhÃ´ng thá» di chuyá»n '{file}'.",
    "Cannot delete '{file}'." => "KhÃ´ng thá» xÃ³a táº­p tin '{file}'.",
    "Cannot delete the folder." => "KhÃ´ng thá» xÃ³a thÆ° má»¥c",
    "Click to remove from the Clipboard" => "KhÃ´ng thá» xÃ³a tá»« Bá» nhá»",
    "This file is already added to the Clipboard." => "Táº­p tin ÄÃ£ cÃ³ trong bá» nhá»",
    "The files in the Clipboard are not readable." => "KhÃ´ng thá» Äá»c cÃ¡c táº­p tin trong Bá» nhá»",
    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" => "{count} táº­p tin trong Bá» nhá» khÃ´ng thá» Äá»c. Báº¡n cháº¯c cháº¯n muá»n sao chÃ©p pháº§n cÃ²n láº¡i?",
    "The files in the Clipboard are not movable." => "KhÃ´ng thá» di chuyá»n cÃ¡c táº­p tin trong Bá» nhá»",
    "{count} files in the Clipboard are not movable. Do you want to move the rest?" => "{count} táº­p tin trong Bá» nhá» khÃ´ng thá» di chuyá»n. Báº¡n cháº¯c cháº¯n muá»n di chuyá»n pháº§n cÃ²n láº¡i?",
    "The files in the Clipboard are not removable." => "KhÃ´ng thá» xÃ³a cÃ¡c táº­p tin trong Bá» nhá»",
    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" => "{count} táº­p tin trong Bá» nhá» khÃ´ng thá» xÃ³a. Báº¡n cháº¯c cháº¯n muá»n xÃ³a pháº§n cÃ²n láº¡i?",
    "The selected files are not removable." => "Lá»±a chá»n táº­p tin Äá» xÃ³a",
    "{count} selected files are not removable. Do you want to delete the rest?" => "{count} táº­p tin khÃ´ng thá» xÃ³a. Báº¡n cháº¯c cháº¯n muá»n xÃ³a pháº§n cÃ²n láº¡i?",
    "Are you sure you want to delete all selected files?" => "Báº¡n cÃ³ cháº¯c báº¡n muá»n xÃ³a táº¥t cáº£ táº­p tin Äá»±oc chá»n?",
    "Failed to delete {count} files/folders." => "KhÃ´ng thá» xÃ³a {count} táº­p tin/thÆ° má»¥c",
    "A file or folder with that name already exists." => "ÄÃ£ tá»n táº¡i táº­p tin hoáº·c thÆ° má»¥c vá»i tÃªn nÃ y",
    "Copy files here" => "Sao chÃ©p á» ÄÃ¢y",
    "Move files here" => "Di chuyá»n á» ÄÃ¢y",
    "Delete files" => "XÃ³a táº­p tin",
    "Clear the Clipboard" => "XÃ³a bá» nhá»",
    "Are you sure you want to delete all files in the Clipboard?" => "Báº¡n cÃ³ cháº¯c báº¡n muá»n xÃ³a táº¥t cáº£ táº­p tin trong Bá» nhá»?",
    "Copy {count} files" => "Sao chep {count} táº­p tin",
    "Move {count} files" => "Di chuyá»n {count} táº­p tin",
    "Add to Clipboard" => "ThÃªm vÃ o Bá» nhá»",
    "Inexistant or inaccessible folder." => "ThÆ° má»¥c khÃ´ng tá»n táº¡i hoáº·c khÃ´ng thá» truy cáº­p",
    "New folder name:" => "TÃªn má»i cá»§a thÆ° má»¥c",
    "New file name:" => "TÃªn má»i cá»§a táº­p tin",
    "Upload" => "Táº£i lÃªn",
    "Refresh" => "LÃ m má»i",
    "Settings" => "Cáº¥u hÃ¬nh",
    "Maximize" => "Tá»i Äa",
    "About" => "Giá»i thiá»u",
    "files" => "táº­p tin",
    "selected files" => "chá»n táº­p tin",
    "View:" => "Xem",
    "Show:" => "Hiá»n",
    "Order by:" => "Thá»© tá»± bá»i",
    "Thumbnails" => "áº¢nh thu nhá»",
    "List" => "Danh sÃ¡ch",
    "Name" => "TÃªn",
    "Type" => "Kiá»u",
    "Size" => "KÃ­ch thÆ°á»c",
    "Date" => "NgÃ y thÃ¡ng",
    "Descending" => "Giáº£m dáº§n",
    "Uploading file..." => "Äang táº£i lÃªn",
    "Loading image..." => "Äang Äá»c áº£nh",
    "Loading folders..." => "Äang Äá»c thÆ° má»¥c...",
    "Loading files..." => "Äang Äá»c táº­p tin...",
    "New Subfolder..." => "ThÆ° má»¥c con má»i...",
    "Rename..." => "Äá»i tÃªn...",
    "Delete" => "XÃ³a",
    "OK" => "Äá»ng Ã",
    "Cancel" => "Há»§y",
    "Select" => "Chá»n",
    "Select Thumbnail" => "Chá»n áº£nh thu nhá»",
    "Select Thumbnails" => "Chá»n nhiá»u áº£nh thu nhá»",
    "View" => "Xem",
    "Download" => "Táº£i xuá»ng",
    "Download files" => "Táº£i xuá»ng táº­p tin",
    "Clipboard" => "Bá» nhá»",
    "Checking for new version..." => "Kiá»m tra phiÃªn báº£n má»i",
    "Unable to connect!" => "KhÃ´ng thá» káº¿t ná»i",
    "Download version {version} now!" => "CÃ³ phiÃªn báº£n má»i {version}, táº£i vá» ngay!",
    "KCFinder is up to date!" => "KhÃ´ng cÃ³ cáº­p nháº­t",
    "Licenses:" => "Báº£n quyá»n",
    "Attention" => "Cáº£nh bÃ¡o",
    "Question" => "CÃ¢u há»i",
    "Yes" => "CÃ³",
    "No" => "KhÃ´ng",
    "You cannot rename the extension of files!" => "Báº¡n khÃ´ng thá» Äá»i tÃªn pháº§n má» rá»ng cá»§a cÃ¡c táº­p tin!",
    "Uploading file {number} of {count}... {progress}" => "Äang táº£i táº­p tin thá»© {number} cá»§a {count}... {progress}",
    "Failed to upload {filename}!" => "Táº£i lÃªn tháº¥t báº¡i {filename}!",
);

?>