<?php

/** This file is part of KCFinder project
  *
  * @desc Japanese localization
  * @package KCFinder
  * @version 2.2
  * @author yama yamamoto@kyms.ne.jp
  * @license http://www.opensource.org/licenses/gpl-2.0.php GPLv2
  * @license http://www.opensource.org/licenses/lgpl-2.1.php LGPLv2
  */

$lang = array(
    '_locale' => "ja_JP.UTF-8",
    '_charset' => "utf-8",

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%Y/%m/%d %H:%M",
    '_dateTimeMid' => "%Y/%m/%d %H:%M",
    '_dateTimeSmall' => "%Y/%m/%d %H:%M",

    "You don't have permissions to upload files." =>
    "ã¢ããã­ã¼ãæ¨©éãããã¾ããã",

    "You don't have permissions to browse server." =>
    "ãµã¼ãã¼ãé²è¦§ããæ¨©éãããã¾ãã",

    "Cannot move uploaded file to target folder." =>
    "ãã¡ã¤ã«ãç§»åã§ãã¾ããã",

    "Unknown error." =>
    "åå ä¸æã®ã¨ã©ã¼ã§ãã",

    "The uploaded file exceeds {size} bytes." =>
    "ã¢ããã­ã¼ããããã¡ã¤ã«ã¯ {size} ãã¤ããè¶ãã¾ããã",

    "The uploaded file was only partially uploaded." =>
    "ã¢ããã­ã¼ããããã¡ã¤ã«ã¯ãä¸é¨ã®ã¿å¦çããã¾ããã",

    "No file was uploaded." =>
    "ãã¡ã¤ã«ã¯ããã¾ããã",

    "Missing a temporary folder." =>
    "ä¸æãã©ã«ããè¦ä»ããã¾ããã",

    "Failed to write file." =>
    "ãã¡ã¤ã«ã®æ¸ãè¾¼ã¿ã«å¤±æãã¾ããã",

    "Denied file extension." =>
    "ãã®ãã¡ã¤ã«ã¯æ±ãã¾ããã",

    "Unknown image format/encoding." =>
    "ãã®ç»åãã¡ã¤ã«ã®ç¨®å¥ãå¤å®ã§ãã¾ããã",

    "The image is too big and/or cannot be resized." =>
    "ç»åãã¡ã¤ã«ã®ãµã¤ãºãå¤§ãéãã¾ãã",

    "Cannot create {dir} folder." =>
    "ã{dir}ããã©ã«ããä½æã§ãã¾ããã",

    "Cannot write to upload folder." =>
    "ã¢ããã­ã¼ããã©ã«ãã«æ¸ãè¾¼ã¿ã§ãã¾ããã",

    "Cannot read .htaccess" =>
    ".htaccessãèª­ã¿è¾¼ãã¾ããã",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "ä¸æ­£ãª .htaccess ãã¡ã¤ã«ã§ããåç·¨éã§ãã¾ãã!",

    "Cannot fetch content of {dir} folder." =>
    "{dir} ãã©ã«ãã®ã³ã³ãã³ããèª­ã¿è¾¼ãã¾ããã",

    "Cannot read upload folder." =>
    "ã¢ããã­ã¼ããã©ã«ããèª­ã¿åãã¾ããã",

    "Cannot access or create thumbnails folder." =>
    "ãµã ãã¤ã«ãã©ã«ãã«ã¢ã¯ã»ã¹ãåã¯ãµã ãã¤ã«ãã©ã«ããä½æã§ãã¾ããã",

    "Cannot access or write to upload folder." =>
    "ã¢ããã­ã¼ããã©ã«ãã«ã¢ã¯ã»ã¹ãåã¯æ¸ãè¾¼ã¿ã§ãã¾ããã",

    "Please enter new folder name." =>
    "æ°ãããã©ã«ãåãå¥åãã¦ä¸ããã",

    "Unallowed characters in folder name." =>
    "ãã©ã«ãåã«ã¯å©ç¨ã§ããªãæå­ã§ãã",

    "Folder name shouldn't begins with '.'" =>
    "ãã©ã«ãåã¯ã'.'ã§éå§ããªãã§ä¸ããã",

    "Please enter new file name." =>
    "æ°ãããã¡ã¤ã«åãå¥åãã¦ä¸ããã",

    "Unallowed characters in file name." =>
    "ãã¡ã¤ã«åã«ã¯å©ç¨ã§ããªãæå­ã§ãã",

    "File name shouldn't begins with '.'" =>
    "ãã¡ã¤ã«åã¯ã'.'ã§éå§ããªãã§ä¸ããã",

    "Are you sure you want to delete this file?" =>
    "ãã®ãã¡ã¤ã«ãæ¬å½ã«åé¤ãã¦ãããããã§ãã?",

    "Are you sure you want to delete this folder and all its content?" =>
    "ãã®ãã©ã«ãã¨ãã©ã«ãåã®å¨ã¦ã®ã³ã³ãã³ããæ¬å½ã«åé¤ãã¦ãããããã§ãã?",

    "Unexisting directory type." =>
    "å­å¨ããªããã£ã¬ã¯ããªã®ç¨®é¡ã§ãã",

    "Undefined MIME types." =>
    "å®ç¾©ããã¦ããªãMIMEã¿ã¤ãã§ãã",

    "Fileinfo PECL extension is missing." =>
    "Fileinfo PECL æ¡å¼µã¢ã¸ã¥ã¼ã«ãè¦ä»ããã¾ããã",

    "Opening fileinfo database failed." =>
    "fileinfo ãã¼ã¿ãã¼ã¹ãéãã®ã«å¤±æãã¾ããã",

    "You can't upload such files." =>
    "ãã®ãããªãã¡ã¤ã«ãã¢ããã­ã¼ãã§ãã¾ããã",

    "The file '{file}' does not exist." =>
    "ãã¡ã¤ã«ã'{file}'ãã¯å­å¨ãã¾ããã",

    "Cannot read '{file}'." =>
    "ã'{file}'ããèª­ã¿åãã¾ããã",

    "Cannot copy '{file}'." =>
    "ã{file}ããã³ãã¼ã§ãã¾ããã",

    "Cannot move '{file}'." =>
    "ã{file}ããç§»åã§ãã¾ããã",

    "Cannot delete '{file}'." =>
    "ã'{file}'ããåé¤ã§ãã¾ããã",

    "Click to remove from the Clipboard" =>
    "ã¯ãªãããã¼ãããåé¤ãã",

    "This file is already added to the Clipboard." =>
    "ãã®ãã¡ã¤ã«ã¯æ¢ã«ã¯ãªãããã¼ãã«è¿½å ããã¦ãã¾ãã",

    "Copy files here" =>
    "ããã«ã³ãã¼",

    "Move files here" =>
    "ããã«ç§»å",

    "Delete files" =>
    "ããããå¨ã¦åé¤",

    "Clear the Clipboard" => //
    "ã¯ãªãããã¼ããåæå",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "ã¯ãªãããã¼ãã«è¨æ¶ããå¨ã¦ã®ãã¡ã¤ã«ãå®éã«åé¤ãã¾ãã",

    "Copy {count} files" =>
    "ãã¡ã¤ã«({count}å)ãããã«è¤å",

    "Move {count} files" =>
    "ãã¡ã¤ã«({count}å)ãããã«ç§»å",

    "Add to Clipboard" =>
    "ã¯ãªãããã¼ãã«è¨æ¶",
    "New folder name:" => "ãã©ã«ãå(åè§è±æ°):",
    "New file name:" => "ãã¡ã¤ã«å(åè§è±æ°):",
    "Folders" => "ãã©ã«ã",

    "Upload" => "ã¢ããã­ã¼ã",
    "Refresh" => "åè¡¨ç¤º",
    "Settings" => "è¡¨ç¤ºè¨­å®",
    "Maximize" => "æå¤§å",
    "About" => "About",
    "files" => "ãã¡ã¤ã«",
    "View:" => "è¡¨ç¤ºã¹ã¿ã¤ã«:",
    "Show:" => "è¡¨ç¤ºé ç®:",
    "Order by:" => "è¡¨ç¤ºé :",
    "Thumbnails" => "ãµã ãã¤ã«",
    "List" => "ãªã¹ã",
    "Name" => "ãã¡ã¤ã«å",
    "Size" => "ãµã¤ãº",
    "Date" => "æ¥ä»",
    "Descending" => "é åºãåè»¢",
    "Uploading file..." => "ãã¡ã¤ã«ãã¢ããã­ã¼ãä¸­...",
    "Loading image..." => "ç»åãèª­ã¿è¾¼ã¿ä¸­...",
    "Loading folders..." => "ãã©ã«ããèª­ã¿è¾¼ã¿ä¸­...",
    "Loading files..." => "èª­ã¿è¾¼ã¿ä¸­...",
    "New Subfolder..." => "ãã©ã«ããä½ã",
    "Rename..." => "ååã®å¤æ´",
    "Delete" => "åé¤",
    "OK" => "OK",
    "Cancel" => "ã­ã£ã³ã»ã«",
    "Select" => "ãã®ãã¡ã¤ã«ãé¸æ",
    "Select Thumbnail" => "ãµã ãã¤ã«ãé¸æ",
    "View" => "ãã¬ãã¥ã¼",
    "Download" => "ãã¦ã³ã­ã¼ã",
    'Clipboard' => "ã¯ãªãããã¼ã",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "ãã£ã¬ã¯ããªãååãå¤æ´ã§ãã¾ãã",

    "Non-existing directory type." =>
    "å­å¨ããªããã£ã¬ã¯ããªã®ç¨®é¡ã§ãã",

    "Cannot delete the folder." =>
    "ãã£ã¬ã¯ããªãåé¤ã§ãã¾ãã",

    "The files in the Clipboard are not readable." =>
    "ã¯ãªãããã¼ããããã¡ã¤ã«ãèª­ã¿åãã¾ãã",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "ã¯ãªãããã¼ãåã® {count} åã®ãã¡ã¤ã«ãèª­ã¿åãã¾ãããæ®ããã³ãã¼ãã¦ãããããã§ãã?",

    "The files in the Clipboard are not movable." =>
    "ã¯ãªãããã¼ããããã¡ã¤ã«ãç§»åã§ãã¾ãã",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "ã¯ãªãããã¼ãåã® {count} åã®ãã¡ã¤ã«ãç§»åã§ãã¾ãããæ®ããç§»åãã¦ãããããã§ãã?",

    "The files in the Clipboard are not removable." =>
    "ã¯ãªãããã¼ããåæåã§ãã¾ãã",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "ã¯ãªãããã¼ãåã® {count} åã®ãã¡ã¤ã«ãåé¤åºæ¥ã¾ãããæ®ããåé¤ãã¦ãããããã§ãã?",

    "The selected files are not removable." =>
    "é¸æãããã¡ã¤ã«ã¯åé¤ã§ãã¾ããã",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "é¸æããã {count} åã®ãã¡ã¤ã«ã¯åé¤ã§ãã¾ãããæ®ããåé¤ãã¦ãããããã§ãã?",

    "Are you sure you want to delete all selected files?" =>
    "é¸æãããå¨ã¦ã®ãã¡ã¤ã«ãæ¬å½ã«åé¤ãã¦ãããããã§ãã?",

    "Failed to delete {count} files/folders." =>
    "{count} åã®ãã¡ã¤ã« / ãã©ã«ãã®åé¤ã«å¤±æãã¾ããã",

    "A file or folder with that name already exists." =>
    "ãã®ååã®ãã¡ã¤ã«ãåã¯ãã©ã«ãã¯æ¢ã«å­å¨ãã¾ãã",

    "Inexistant or inaccessible folder." =>
    "å­å¨ããªããåã¯ã¢ã¯ã»ã¹ã§ããªããã©ã«ãã§ãã",

    "selected files" => "é¸æãããã¡ã¤ã«",
    "Type" => "ã¿ã¤ã",
    "Select Thumbnails" => "ãµã ãã¤ã«ãé¸æ",
    "Download files" => "ãã¡ã¤ã«ããã¦ã³ã­ã¼ããã",
);

?>