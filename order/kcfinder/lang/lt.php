<?php

/** Lithuanian localization file for KCFinder
  * author: Paulius LeÅ¡Äinskas <paulius.lescinskas@gmail.com>
  */

$lang = array(

    '_locale' => "lt_LT.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%F %T",
    '_dateTimeMid' => "%F %T",
    '_dateTimeSmall' => "%F %T",

	"You don't have permissions to upload files." =>
    "JÅ«s neturite teisiÅ³ Ä¯kelti failus",

    "You don't have permissions to browse server." =>
    "JÅ«s neturite teisiÅ³ narÅ¡yti po failus",

    "Cannot move uploaded file to target folder." =>
    "Nepavyko Ä¯kelti failo Ä¯ reikiamÄ katalogÄ.",

    "Unknown error." =>
    "NeÅ¾inoma klaida.",

    "The uploaded file exceeds {size} bytes." =>
    "Ä®keliamas failas virÅ¡ija {size} baitÅ³(-us).",

    "The uploaded file was only partially uploaded." =>
    "Failas buvo tik dalinai Ä¯keltas.",

    "No file was uploaded." =>
    "Failas nebuvo Ä¯keltas.",

    "Missing a temporary folder." =>
    "NÄra laikino katalogo.",

    "Failed to write file." =>
    "Nepavyko Ä¯raÅ¡yti failo.",

    "Denied file extension." =>
    "DraudÅ¾iama Ä¯kelti Å¡io tipo failus.",

    "Unknown image format/encoding." =>
    "NeÅ¾inomas paveikslÄlio formatas/kodavimas.",

    "The image is too big and/or cannot be resized." =>
    "PaveikslÄlis yra per didelis ir/arba negali bÅ«ti sumaÅ¾intas.",

    "Cannot create {dir} folder." =>
    "Nepavyko sukurti {dir} katalogo.",

    "Cannot write to upload folder." =>
    "Nepavyko Ä¯raÅ¡yti Ä¯ Ä¯keliamÅ³ failÅ³ katalogÄ.",

    "Cannot read .htaccess" =>
    "Nepavyko nuskaityti .htaccess failo",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "Blogas .htaccess failas. Nepavyko jo perraÅ¡yti",

    "Cannot read upload folder." =>
    "Nepavyko atidaryti Ä¯keliamÅ³ failÅ³ katalogo.",

    "Cannot access or create thumbnails folder." =>
    "Nepavyko atidaryti ar sukurti sumaÅ¾intÅ³ paveikslÄliÅ³ katalogo.",

    "Cannot access or write to upload folder." =>
    "Nepavyko atidaryti ar Ä¯raÅ¡yti Ä¯ Ä¯keliamÅ³ failÅ³ katalogÄ.",

    "Please enter new folder name." =>
    "Ä®veskite katalogo pavadinimÄ.",

    "Unallowable characters in folder name." =>
    "Katalogo pavadinime yra neleistinÅ³ simboliÅ³.",

    "Folder name shouldn't begins with '.'" =>
    "Katalogo pavadinimas negali prasidÄti '.'",

    "Please enter new file name." =>
    "Ä®veskite failo pavadinimÄ.",

    "Unallowable characters in file name." =>
    "Failo pavadinime yra neleistinÅ³ simboliÅ³",

    "File name shouldn't begins with '.'" =>
    "Failo pavadinimas negali prasidÄti '.'",

    "Are you sure you want to delete this file?" =>
    "Ar tikrai iÅ¡trinti Å¡Ä¯ failÄ?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Ar tikrai iÅ¡trinti Å¡Ä¯ katalogÄ su visu jo turiniu?",

    "Inexistant or inaccessible folder." =>
    "Katalogas neegzistuoja arba yra neprieinamas.",

    "Undefined MIME types." =>
    "Nenurodytas MIME tipas.",

    "Fileinfo PECL extension is missing." =>
    "TrÅ«ksa PECL plÄtinio Fileinfo",

    "Opening fileinfo database failed." =>
    "Nepavyko atidaryti Fileinfo duomenÅ³ bazÄs.",

    "You can't upload such files." =>
    "Negalima Ä¯kelti tokiÅ³ failÅ³.",

    "The file '{file}' does not exist." =>
    "Failas '{file}' neegzistuoja.",

    "Cannot read '{file}'." =>
    "Nepavyko atidaryti '{file}' failo.",

    "Cannot copy '{file}'." =>
    "Nepavyko nukopijuoti '{file}' failo.",

    "Cannot move '{file}'." =>
    "Nepavyko perkelti '{file}' failo.",

    "Cannot delete '{file}'." =>
    "Nepavyko iÅ¡trinti '{file}' failo.",

    "Click to remove from the Clipboard" =>
    "Zum entfernen aus der Zwischenablage, hier klicken.",

    "This file is already added to the Clipboard." =>
    "Å is failas jau Ä¯keltas Ä¯ laikinÄjÄ atmintÄ¯.",

    "Copy files here" =>
    "Kopijuoti failus Äia.",

    "Move files here" =>
    "Perkelti failus Äia.",

    "Delete files" =>
    "IÅ¡trinti failus.",

    "Clear the Clipboard" =>
    "IÅ¡valyti laikinÄjÄ atmintÄ¯",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Ar tikrai iÅ¡trinti visus failus, esanÄius laikinojoje atmintyje?",

    "Copy {count} files" =>
    "Kopijuoti {count} failÅ³(-us)",

    "Move {count} files" =>
    "Perkelti {count} failÅ³(-us)",

    "Add to Clipboard" =>
    "Ä®kelti Ä¯ laikinÄjÄ atmintÄ¯",

    "New folder name:" => "Naujo katalogo pavadinimas:",
    "New file name:" => "Naujo failo pavadinimas:",

    "Upload" => "Ä®kelti",
    "Refresh" => "Atnaujinti",
    "Settings" => "Nustatymai",
    "Maximize" => "Padidinti",
    "About" => "Apie",
    "files" => "Failai",
    "View:" => "PerÅ¾iÅ«ra:",
    "Show:" => "Rodyti:",
    "Order by:" => "Rikiuoti:",
    "Thumbnails" => "SumaÅ¾intos iliustracijos",
    "List" => "SÄraÅ¡as",
    "Name" => "Pavadinimas",
    "Size" => "Dydis",
    "Date" => "Data",
    "Descending" => "MaÅ¾ejanÄia tvarka",
    "Uploading file..." => "Ä®keliamas failas...",
    "Loading image..." => "Kraunami paveikslÄliai...",
    "Loading folders..." => "Kraunami katalogai...",
    "Loading files..." => "Kraunami failai...",
    "New Subfolder..." => "Naujas katalogas...",
    "Rename..." => "Pervadinti...",
    "Delete" => "IÅ¡trinti",
    "OK" => "OK",
    "Cancel" => "AtÅ¡aukti",
    "Select" => "PaÅ¾ymÄti",
    "Select Thumbnail" => "Pasirinkti sumaÅ¾intÄ paveikslÄlÄ¯",
    "View" => "PerÅ¾iÅ«ra",
    "Download" => "AtsisiÅ³sti",
    'Clipboard' => "Laikinoji atmintis",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "Nepavyko pervadinti katalogo.",

    "Non-existing directory type." =>
    "Neegzistuojantis katalogo tipas.",

    "Cannot delete the folder." =>
    "Nepavyko iÅ¡trinti katalogo.",

    "The files in the Clipboard are not readable." =>
    "Nepavyko nuskaityti failÅ³ iÅ¡ laikinosios atminties.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "Nepavyko atidaryti {count} failÅ³(-ai) iÅ¡ laikinosios atminties. Ar kopijuoti likusius?",

    "The files in the Clipboard are not movable." =>
    "Nepavyko perkelti failÅ³ iÅ¡ laikinosios atminties.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "Nepavyko perkelti {count} failÅ³(-ai) iÅ¡ laikinosios atminties. Ar perkelti likusius?",

    "The files in the Clipboard are not removable." =>
    "Nepavyko perkelti failÅ³ iÅ¡ laikinosios atminties.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "Nepavyko iÅ¡trinti {count} failÅ³(-ai) iÅ¡ laikinosios atminties. Ar iÅ¡trinti likusius?",

    "The selected files are not removable." =>
    "Nepavyko perkelti paÅ¾ymÄtÅ³ failÅ³.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "Nepavyko iÅ¡trinti {count} failÅ³(-ai) iÅ¡ laikinosios atminties. Ar iÅ¡trinti likusius?",

    "Are you sure you want to delete all selected files?" =>
    "Ar tikrai iÅ¡trinti visus paÅ¾ymÄtus failus?",

    "Failed to delete {count} files/folders." =>
    "Nepavyko iÅ¡trinti {count} failÅ³/katalogÅ³.",

    "A file or folder with that name already exists." =>
    "Failas arba katalogas tokiu pavadinimu jau egzistuoja.",

    "selected files" => "Pasirinkti failus",
    "Type" => "Tipas",
    "Select Thumbnails" => "Pasirinkti sumaÅ¾intus paveikslÄlius",
    "Download files" => "AtsisiÅ³sti failus",
);

?>
