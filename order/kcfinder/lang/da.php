<?php

/** Danish translation by Thomas Schou <thomas@schouweb.dk> **/

$lang = array(

    '_locale' => "da_DK.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e.%B.%Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d/%m/%Y %H:%M",

	"You don't have permissions to upload files." =>
    "Du har ikke tilladelser til at uploade filer.",

    "You don't have permissions to browse server." =>
    "Du har ikke tilladelser til at se filer.",

    "Cannot move uploaded file to target folder." =>
    "Kan ikke flytte fil til destinations mappe.",

    "Unknown error." =>
    "Ukendt fejl.",

    "The uploaded file exceeds {size} bytes." =>
    "Den uploadede fil overskrider {size} bytes.",

    "The uploaded file was only partially uploaded." =>
    "Den uploadede fil blev kun delvist uploadet.",

    "No file was uploaded." =>
    "Ingen fil blev uploadet.",

    "Missing a temporary folder." =>
    "Mangler en midlertidig mappe.",

    "Failed to write file." =>
    "Kunne ikke skrive fil.",

    "Denied file extension." =>
    "Nï¿½gtet filtypenavn.",

    "Unknown image format/encoding." =>
    "Ukendt billedformat / kodning.",

    "The image is too big and/or cannot be resized." =>
    "Billedet er for stort og / eller kan ikke ï¿½ndres.",

    "Cannot create {dir} folder." =>
    "Kan ikke lave mappen {dir}.",

    "Cannot write to upload folder." =>
    "Kan ikke skrive til upload mappen.",

    "Cannot read .htaccess" =>
    "Ikke kan lï¿½se .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "Forkert .htaccess fil. Kan ikke omskrive den!",

    "Cannot read upload folder." =>
    "Kan ikke lï¿½se upload mappen.",

    "Cannot access or create thumbnails folder." =>
    "Kan ikke fï¿½ adgang til eller oprette miniature mappe.",

    "Cannot access or write to upload folder." =>
    "Kan ikke fï¿½ adgang til eller skrive til upload mappe.",

    "Please enter new folder name." =>
    "Indtast venligst nyt mappe navn.",

    "Unallowable characters in folder name." =>
    "Ugyldige tegn i mappens navn.",

    "Folder name shouldn't begins with '.'" =>
    "Mappenavn bï¿½r ikke begynder med '.'",

    "Please enter new file name." =>
    "Indtast venligst nyt fil navn.",

    "Unallowable characters in file name." =>
    "Ugyldige tegn i filens navn",

    "File name shouldn't begins with '.'" =>
    "Fil navnet bï¿½r ikke begynder med '.'",

    "Are you sure you want to delete this file?" =>
    "Er du sikker pï¿½ du vil slette denne fil?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Er du sikker pï¿½ du vil slette denne mappe og al dens indhold?",

    "Inexistant or inaccessible folder." =>
    "utilgï¿½ngelige mappe.",

    "Undefined MIME types." =>
    "Udefineret MIME Type.",

    "Fileinfo PECL extension is missing." =>
    "Fil info PECL udvidelse mangler",

    "Opening fileinfo database failed." =>
    "ï¿½bning af fil info-database mislykkedes.",

    "You can't upload such files." =>
    "Du kan ikke uploade sï¿½danne filer.",

    "The file '{file}' does not exist." =>
    "Filen '{file}' eksisterer ikke.",

    "Cannot read '{file}'." =>
    "Kan ikke lï¿½se '{file}'.",

    "Cannot copy '{file}'." =>
    "Kan ikke kopi'ere '{file}'.",

    "Cannot move '{file}'." =>
    "Kan ikke flytte '{file}'.",

    "Cannot delete '{file}'." =>
    "Kan ikke slette '{file}'.",

    "Click to remove from the Clipboard" =>
    "Klik for at fjerne fra Udklipsholder.",

    "This file is already added to the Clipboard." =>
    "Denne fil er allerede fï¿½jet til Udklipsholder.",

    "Copy files here" =>
    "Kopiere filer her.",

    "Move files here" =>
    "Flyt filer her.",

    "Delete files" =>
    "Slet filer.",

    "Clear the Clipboard" =>
    "Zwischenablage leeren",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Tï¿½m udklipsholder?",

    "Copy {count} files" =>
    "Kopier {count} filer",

    "Move {count} files" =>
    "Flyt {count} filer",

    "Add to Clipboard" =>
    "Tilfï¿½j til udklipsholder",

    "New folder name:" =>
    "Nyt mappe navn:",

    "New file name:" =>
    "Nyt fil navn:",

    "Upload" => "Upload",
    "Refresh" => "Genopfriske",
    "Settings" => "Indstillinger",
    "Maximize" => "Maksimere",
    "About" => "Om",
    "files" => "filer",
    "View:" => "Vis:",
    "Show:" => "Vis:",
    "Order by:" => "Sorter efter:",
    "Thumbnails" => "Miniaturer",
    "List" => "Liste",
    "Name" => "Navn",
    "Size" => "Stï¿½rrelse",
    "Date" => "Dato",
    "Descending" => "Faldende",
    "Uploading file..." => "Uploader fil...",
    "Loading image..." => "Indlï¿½ser billede...",
    "Loading folders..." => "Indlï¿½ser mappe...",
    "Loading files..." => "Indlï¿½ser filer...",
    "New Subfolder..." => "Ny undermappe...",
    "Rename..." => "Omdï¿½b...",
    "Delete" => "Slet",
    "OK" => "Ok",
    "Cancel" => "Fortryd",
    "Select" => "Vï¿½lg",
    "Select Thumbnail" => "Vï¿½lg miniature",
    "View" => "Vis",
    "Download" => "Download",
    'Clipboard' => "Udklipsholder",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "Kan ikke omdï¿½be mappen.",

    "Non-existing directory type." =>
    "Ikke-eksisterende bibliotek type.",

    "Cannot delete the folder." =>
    "Kan ikke slette mappen.",

    "The files in the Clipboard are not readable." =>
    "Filerne i udklipsholder ikke kan lï¿½ses.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "{count} filer i udklipsholder ikke kan lï¿½ses. ï¿½nsker du at kopiere de ï¿½vrige?",

    "The files in the Clipboard are not movable." =>
    "Filerne i udklipsholder kan ikke flyttes.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "{count} filer i udklipsholder er ikke bevï¿½gelige. ï¿½nsker du at flytte resten?",

    "The files in the Clipboard are not removable." =>
    "Filerne i udklipsholder er ikke flytbare.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} filer i udklipsholder er ikke flytbare. ï¿½nsker du at slette resten?",

    "The selected files are not removable." =>
    "De valgte filer er ikke flytbare.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "{count} valgte filer som ikke kan fjernes. ï¿½nsker du at slette resten?",

    "Are you sure you want to delete all selected files?" =>
    "Er du sikker pï¿½ du vil slette alle markerede filer?",

    "Failed to delete {count} files/folders." =>
    "Kunne ikke slette {count} filer / mapper.",

    "A file or folder with that name already exists." =>
    "En fil eller mappe med det navn findes allerede.",

    "selected files" => "Valgte filer",
    "Type" => "Type",
    "Select Thumbnails" => "Vï¿½lg Miniaturer",
    "Download files" => "Download filer",
);

?>
