<?php

/** Turkish localization file for KCFinder
* translation by: Kursad Olmez <kursad.olmez@gmail.com>
**/

$lang = array(

    '_locale' => "en_US.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e.%B.%Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d/%m/%Y %H:%M",

    "You don't have permissions to upload files." =>
    "Dosya yÃ¼klemek iÃ§in yetkiniz yok.",

    "You don't have permissions to browse server." =>
    "Sunucuyu gezmek iÃ§in yetkiniz yok.",

    "Cannot move uploaded file to target folder." =>
    "YÃ¼klenilen dosyalar hedef klasÃ¶re taÅÄ±namÄ±yor.",

    "Unknown error." =>
    "Bilinmeyen hata.",

    "The uploaded file exceeds {size} bytes." =>
    "GÃ¶nderilen dosya boyutu, maksimum dosya boyutu limitini ({size} byte) aÅÄ±yor.",

    "The uploaded file was only partially uploaded." =>
    "DosyanÄ±n sadece bir kÄ±smÄ± yÃ¼klendi. YÃ¼klemeyi tekrar deneyin.",

    "No file was uploaded." =>
    "Dosya yÃ¼klenmedi.",

    "Missing a temporary folder." =>
    "GeÃ§ici dosya klasÃ¶rÃ¼ bulunamÄ±yor. KlasÃ¶rÃ¼ kontrol edin.",

    "Failed to write file." =>
    "Dosya yazÄ±lamÄ±yor. KlasÃ¶r yetkilerini kontrol edin.",

    "Denied file extension." =>
    "YasaklanmÄ±Å dosya tÃ¼rÃ¼.",

    "Unknown image format/encoding." =>
    "Bilinmeyen resim formatÄ±.",

    "The image is too big and/or cannot be resized." =>
    "Resim Ã§ok bÃ¼yÃ¼k ve/veya yeniden boyutlandÄ±rÄ±lamÄ±yor.",

    "Cannot create {dir} folder." =>
    "{dir} klasÃ¶rÃ¼ oluÅturulamÄ±yor.",

    "Cannot write to upload folder." =>
    "Dosya yÃ¼kleme klasÃ¶rÃ¼ne yazÄ±lamÄ±yor. KlasÃ¶r yetkisini kontrol edin.",

    "Cannot read .htaccess" =>
    ".htaccess dosyasÄ± okunamÄ±yor",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "HatalÄ± .htaccess dosyasÄ±. Dosyaya yeniden yazÄ±lamÄ±yor.",

    "Cannot read upload folder." =>
    "Dosya yÃ¼kleme klasÃ¶rÃ¼ okunamÄ±yor. KlasÃ¶r yetkilerini kontrol edin.",

    "Cannot access or create thumbnails folder." =>
    "Ãnizleme dosyalarÄ± klasÃ¶rÃ¼ne eriÅilemiyor yada oluÅturulamÄ±yor.",

    "Cannot access or write to upload folder." =>
    "Dosya yÃ¼kleme klasÃ¶rÃ¼ne ulaÅÄ±lamÄ±yor yada oluÅturulamÄ±yor.",

    "Please enter new folder name." =>
    "LÃ¼tfen yeni klasÃ¶r adÄ±nÄ± girin.",

    "Unallowable characters in folder name." =>
    "KlasÃ¶r adÄ±nda izin verilmeyen karakter kullandÄ±nÄ±z.",

    "Folder name shouldn't begins with '.'" =>
    "KlasÃ¶r adÄ± '.' ile baÅlayamaz.",

    "Please enter new file name." =>
    "LÃ¼tfen yeni dosya adÄ±nÄ± girin.",

    "Unallowable characters in file name." =>
    "Dosya adÄ±nda izin verilmeyen karakter kullandÄ±nÄ±z.",

    "File name shouldn't begins with '.'" =>
    "Dosya adÄ± '.' ile baÅlayamaz.",

    "Are you sure you want to delete this file?" =>
    "DosyayÄ± silmek istediÄinizden emin misiniz?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Bu klasÃ¶rÃ¼ ve tÃ¼m iÃ§eriÄini silmek istediÄinizden emin misiniz?",

    "Inexistant or inaccessible folder." =>
    "KlasÃ¶r yok yada ulaÅÄ±lamÄ±yor.",

    "Undefined MIME types." =>
    "TanÄ±msÄ±z MIME tÃ¼rÃ¼.",

    "Fileinfo PECL extension is missing." =>
    "Dosya Bilgisi PECL uzantÄ±sÄ± eksik.",

    "Opening fileinfo database failed." =>
    "Dosya Bilgisi veritabanÄ± aÃ§Ä±lÄ±rken hata oluÅtu.",

    "You can't upload such files." =>
    "Bu tÃ¼r dosyalarÄ± yÃ¼kleyemezsiniz.",

    "The file '{file}' does not exist." =>
    "'{file}' dosyasÄ± yok.",

    "Cannot read '{file}'." =>
    "'{file}' dosyasÄ± okunamÄ±yor.",

    "Cannot copy '{file}'." =>
    "'{file}' dosyasÄ± kopyalanamÄ±yor.",

    "Cannot move '{file}'." =>
    "'{file}' dosyasÄ± taÅÄ±namÄ±yor.",

    "Cannot delete '{file}'." =>
    "'{file}' dosyasÄ± silinemiyor.",

    "Click to remove from the Clipboard" =>
    "Panodan Ã§Ä±karmak iÃ§in tÄ±klayÄ±n",

    "This file is already added to the Clipboard." =>
    "Bu dosya zaten panoya eklenmiÅ.",

    "Copy files here" =>
    "DosyalarÄ± Buraya Kopyala",

    "Move files here" =>
    "DosyalarÄ± Buraya TaÅÄ±",

    "Delete files" =>
    "DosyalarÄ± Sil",

    "Clear the Clipboard" =>
    "Panoyu Temizle",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Panodaki tÃ¼m dosyalarÄ± silmek istediÄinizden emin misiniz?",

    "Copy {count} files" =>
    "{count} adet dosyayÄ± kopyala",

    "Move {count} files" =>
    "{count} adet dosyayÄ± taÅÄ±",

    "Add to Clipboard" =>
    "Panoya Ekle",

    "New folder name:" => "Yeni KlasÃ¶r AdÄ±:",
    "New file name:" => "Yeni Dosya AdÄ±:",

    "Upload" => "YÃ¼kle",
    "Refresh" => "Yenile",
    "Settings" => "Ayarlar",
    "Maximize" => "Pencereyi BÃ¼yÃ¼t",
    "About" => "HakkÄ±nda",
    "files" => "dosya",
    "View:" => "GÃ¶rÃ¼ntÃ¼leme:",
    "Show:" => "GÃ¶ster:",
    "Order by:" => "SÄ±ralama:",
    "Thumbnails" => "Ãnizleme",
    "List" => "Liste",
    "Name" => "Ad",
    "Size" => "Boyut",
    "Date" => "Tarih",
    "Descending" => "Azalarak",
    "Uploading file..." => "Dosya GÃ¶nderiliyor...",
    "Loading image..." => "Resim YÃ¼kleniyor...",
    "Loading folders..." => "KlasÃ¶rler YÃ¼kleniyor...",
    "Loading files..." => "Dosyalar YÃ¼kleniyor...",
    "New Subfolder..." => "Yeni Alt KlasÃ¶r...",
    "Rename..." => "Ä°sim DeÄiÅtir...",
    "Delete" => "Sil",
    "OK" => "Tamam",
    "Cancel" => "Ä°ptal",
    "Select" => "SeÃ§",
    "Select Thumbnail" => "Ãnizleme Resmini SeÃ§",
    "View" => "GÃ¶ster",
    "Download" => "Ä°ndir",
    "Clipboard" => "Pano",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "KlasÃ¶r adÄ± deÄiÅtirilemiyor.",

    "Non-existing directory type." =>
    "GeÃ§ersiz klasÃ¶r tÃ¼rÃ¼.",

    "Cannot delete the folder." =>
    "KlasÃ¶r silinemiyor.",

    "The files in the Clipboard are not readable." =>
    "Panodaki dosyalar okunamÄ±yor.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "Panodaki {count} adet dosya okunamÄ±yor. Geri kalanlarÄ±nÄ± kopyalamak istiyor musunuz?",

    "The files in the Clipboard are not movable." =>
    "Panodaki dosyalar taÅÄ±namÄ±yor.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "Panodaki {count} adet dosya taÅÄ±namÄ±yor. Geri kalanlarÄ±nÄ± taÅÄ±mak istiyor musunuz?",

    "The files in the Clipboard are not removable." =>
    "Dosyalar panodan Ã§Ä±kartÄ±lamÄ±yor.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} adet dosya panodan Ã§Ä±kartÄ±lamÄ±yor. Geri kalanlarÄ± silmek istiyor musunuz?",

    "The selected files are not removable." =>
    "SeÃ§ilen dosyalar panodan Ã§Ä±kartÄ±lamÄ±yor.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "SeÃ§ilen dosyalarÄ±n {count} adedi panodan Ã§Ä±kartÄ±lamÄ±yor. Geri kalanlarÄ± silmek istiyor musunuz?",

    "Are you sure you want to delete all selected files?" =>
    "SeÃ§ilen tÃ¼m dosyalarÄ± silmek istediÄinizden emin misiniz?",

    "Failed to delete {count} files/folders." =>
    "{count} adet dosya/klasÃ¶r silinemedi.",

    "A file or folder with that name already exists." =>
    "Bu isimde bir klasÃ¶r yada dosya zaten var.",

    "selected files" => "dosya seÃ§ildi",
    "Type" => "TÃ¼r",
    "Select Thumbnails" => "Ãnizleme Resimlerini SeÃ§",
    "Download files" => "DosyalarÄ± Ä°ndir",

    // SINCE 2.4

    "Checking for new version..." => "Yeni versiyon kontrol ediliyor...",
    "Unable to connect!" => "BaÄlantÄ± yapÄ±lamÄ±yor!",
    "Download version {version} now!" => " {version} versiyonunu hemen indir!",
    "KCFinder is up to date!" => "KCFinder gÃ¼ncel durumda!",
    "Licenses:" => "Lisanslar:",
    "Attention" => "Dikkat",
    "Question" => "Soru",
    "Yes" => "Evet",
    "No" => "HayÄ±r",

    // SINCE 2.41

    "You cannot rename the extension of files!" => "Dosya uzantÄ±larÄ±nÄ± deÄiÅtiremezsiniz!",
);

?>
