<?php

/** Russian localization file for KCFinder
	* author: Dark Preacher
	* E-mail: dark@darklab.ru
  */

$lang = array(

    '_locale' => "ru_RU.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e %B, %Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

    "You don't have permissions to upload files." =>
    "Ð£ Ð²Ð°Ñ Ð½ÐµÑ Ð¿ÑÐ°Ð² Ð´Ð»Ñ Ð·Ð°Ð³ÑÑÐ·ÐºÐ¸ ÑÐ°Ð¹Ð»Ð¾Ð².",

    "You don't have permissions to browse server." =>
    "Ð£ Ð²Ð°Ñ Ð½ÐµÑ Ð¿ÑÐ°Ð² Ð´Ð»Ñ Ð¿ÑÐ¾ÑÐ¼Ð¾ÑÑÐ° ÑÐ¾Ð´ÐµÑÐ¶Ð¸Ð¼Ð¾Ð³Ð¾ Ð½Ð° ÑÐµÑÐ²ÐµÑÐµ.",

    "Cannot move uploaded file to target folder." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÐµÑÐµÐ¼ÐµÑÑÐ¸ÑÑ Ð·Ð°Ð³ÑÑÐ¶ÐµÐ½Ð½ÑÐ¹ ÑÐ°Ð¹Ð» Ð² Ð¿Ð°Ð¿ÐºÑ Ð½Ð°Ð·Ð½Ð°ÑÐµÐ½Ð¸Ñ.",

    "Unknown error." =>
    "ÐÐµÐ¸Ð·Ð²ÐµÑÑÐ½Ð°Ñ Ð¾ÑÐ¸Ð±ÐºÐ°.",

    "The uploaded file exceeds {size} bytes." =>
    "ÐÐ°Ð³ÑÑÐ¶ÐµÐ½Ð½ÑÐ¹ ÑÐ°Ð¹Ð» Ð¿ÑÐµÐ²ÑÑÐ°ÐµÑ ÑÐ°Ð·Ð¼ÐµÑ {size} Ð±Ð°Ð¹Ñ.",

    "The uploaded file was only partially uploaded." =>
    "ÐÐ°Ð³ÑÑÐ¶ÐµÐ½Ð½ÑÐ¹ ÑÐ°Ð¹Ð» Ð±ÑÐ» Ð·Ð°Ð³ÑÑÐ¶ÐµÐ½ ÑÐ¾Ð»ÑÐºÐ¾ ÑÐ°ÑÑÐ¸ÑÐ½Ð¾.",

    "No file was uploaded." =>
    "Ð¤Ð°Ð¹Ð» Ð½Ðµ Ð±ÑÐ» Ð·Ð°Ð³ÑÑÐ¶ÐµÐ½",

    "Missing a temporary folder." =>
    "ÐÑÐµÐ¼ÐµÐ½Ð½Ð°Ñ Ð¿Ð°Ð¿ÐºÐ° Ð½Ðµ ÑÑÑÐµÑÑÐ²ÑÐµÑ.",

    "Failed to write file." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð·Ð°Ð¿Ð¸ÑÐ°ÑÑ ÑÐ°Ð¹Ð».",

    "Denied file extension." =>
    "Ð¤Ð°Ð¹Ð»Ñ ÑÑÐ¾Ð³Ð¾ ÑÐ¸Ð¿Ð° Ð·Ð°Ð¿ÑÐµÑÐµÐ½Ñ Ð´Ð»Ñ Ð·Ð°Ð³ÑÑÐ·ÐºÐ¸.",

    "Unknown image format/encoding." =>
    "ÐÐµÐ¸Ð·Ð²ÐµÑÑÐ½ÑÐ¹ ÑÐ¾ÑÐ¼Ð°Ñ Ð¸Ð·Ð¾Ð±ÑÐ°Ð¶ÐµÐ½Ð¸Ñ.",

    "The image is too big and/or cannot be resized." =>
    "ÐÐ·Ð¾Ð±ÑÐ°Ð¶ÐµÐ½Ð¸Ðµ ÑÐ»Ð¸ÑÐºÐ¾Ð¼ Ð±Ð¾Ð»ÑÑÐ¾Ðµ Ð¸/Ð¸Ð»Ð¸ Ð½Ðµ Ð¼Ð¾Ð¶ÐµÑ Ð±ÑÑÑ ÑÐ¼ÐµÐ½ÑÑÐµÐ½Ð¾.",

    "Cannot create {dir} folder." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ¾Ð·Ð´Ð°ÑÑ Ð¿Ð°Ð¿ÐºÑ {dir}.",

    "Cannot write to upload folder." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð·Ð°Ð¿Ð¸ÑÐ°ÑÑ Ð² Ð¿Ð°Ð¿ÐºÑ Ð·Ð°Ð³ÑÑÐ·ÐºÐ¸.",

    "Cannot read .htaccess" =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÑÐ¾ÑÐ¸ÑÐ°ÑÑ ÑÐ°Ð¹Ð» .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "ÐÐµÐ¿ÑÐ°Ð²Ð¸Ð»ÑÐ½ÑÐ¹ ÑÐ°Ð¹Ð» .htaccess. ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÐµÑÐµÐ·Ð°Ð¿Ð¸ÑÐ°ÑÑ!",

    "Cannot read upload folder." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÑÐ¾ÑÐ¸ÑÐ°ÑÑ Ð¿Ð°Ð¿ÐºÑ Ð·Ð°Ð³ÑÑÐ·ÐºÐ¸.",

    "Cannot access or create thumbnails folder." =>
    "ÐÐµÑ Ð´Ð¾ÑÑÑÐ¿Ð° Ð¸Ð»Ð¸ Ð½ÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ¾Ð·Ð´Ð°ÑÑ Ð¿Ð°Ð¿ÐºÑ Ð¼Ð¸Ð½Ð¸Ð°ÑÑÑ.",

    "Cannot access or write to upload folder." =>
    "ÐÐµÑ Ð´Ð¾ÑÑÑÐ¿Ð° Ð¸Ð»Ð¸ Ð½ÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð·Ð°Ð¿Ð¸ÑÐ°ÑÑ Ð² Ð¿Ð°Ð¿ÐºÑ Ð·Ð°Ð³ÑÑÐ·ÐºÐ¸.",

    "Please enter new folder name." =>
    "Ð£ÐºÐ°Ð¶Ð¸ÑÐµ Ð¸Ð¼Ñ Ð½Ð¾Ð²Ð¾Ð¹ Ð¿Ð°Ð¿ÐºÐ¸.",

    "Unallowable characters in folder name." =>
    "ÐÐµÐ´Ð¾Ð¿ÑÑÑÐ¸Ð¼ÑÐµ ÑÐ¸Ð¼Ð²Ð¾Ð»Ñ Ð² Ð¸Ð¼ÐµÐ½Ð¸ Ð¿Ð°Ð¿ÐºÐ¸.",

    "Folder name shouldn't begins with '.'" =>
    "ÐÐ¼Ñ Ð¿Ð°Ð¿ÐºÐ¸ Ð½Ðµ Ð¼Ð¾Ð¶ÐµÑ Ð½Ð°ÑÐ¸Ð½Ð°ÑÑÑÑ Ñ '.'",

    "Please enter new file name." =>
    "Ð£ÐºÐ°Ð¶Ð¸ÑÐµ Ð½Ð¾Ð²Ð¾Ðµ Ð¸Ð¼Ñ ÑÐ°Ð¹Ð»Ð°",

    "Unallowable characters in file name." =>
    "ÐÐµÐ´Ð¾Ð¿ÑÑÑÐ¸Ð¼ÑÐµ ÑÐ¸Ð¼Ð²Ð¾Ð»Ð½Ñ Ð² Ð¸Ð¼ÐµÐ½Ð¸ ÑÐ°Ð¹Ð»Ð°.",

    "File name shouldn't begins with '.'" =>
    "ÐÐ¼Ñ ÑÐ°Ð¹Ð»Ð° Ð½Ðµ Ð¼Ð¾Ð¶ÐµÑ Ð½Ð°ÑÐ¸Ð½Ð°ÑÑÑÑ Ñ '.'",

    "Are you sure you want to delete this file?" =>
    "ÐÑ ÑÐ²ÐµÑÐµÐ½Ñ, ÑÑÐ¾ ÑÐ¾ÑÐ¸ÑÐµ ÑÐ´Ð°Ð»Ð¸ÑÑ ÑÑÐ¾Ñ ÑÐ°Ð¹Ð»?",

    "Are you sure you want to delete this folder and all its content?" =>
    "ÐÑ ÑÐ²ÐµÑÐµÐ½Ñ, ÑÑÐ¾ ÑÐ¾ÑÐ¸ÑÐµ ÑÐ´Ð°Ð»Ð¸ÑÑ ÑÑÑ Ð¿Ð°Ð¿ÐºÑ Ð¸ Ð²ÑÑ ÐµÑ ÑÐ¾Ð´ÐµÑÐ¶Ð¸Ð¼Ð¾Ðµ?",

    "Non-existing directory type." =>
    "ÐÐµÑÑÑÐµÑÑÐ²ÑÑÑÐ¸Ð¹ ÑÐ¸Ð¿ Ð¿Ð°Ð¿ÐºÐ¸.",

    "Undefined MIME types." =>
    "ÐÐµÐ¾Ð¿ÑÐµÐ´ÐµÐ»ÑÐ½Ð½ÑÐµ ÑÐ¸Ð¿Ñ MIME.",

    "Fileinfo PECL extension is missing." =>
    "Ð Ð°ÑÑÐ¸ÑÐµÐ½Ð¸Ðµ Fileinfo PECL Ð¾ÑÑÑÑÑÑÐ²ÑÐµÑ.",

    "Opening fileinfo database failed." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¾ÑÐºÑÑÑÑ Ð±Ð°Ð·Ñ Ð´Ð°Ð½Ð½ÑÑ fileinfo.",

    "You can't upload such files." =>
    "ÐÑ Ð½Ðµ Ð¼Ð¾Ð¶ÐµÑÐµ Ð·Ð°Ð³ÑÑÐ¶Ð°ÑÑ ÑÐ°Ð¹Ð»Ñ ÑÑÐ¾Ð³Ð¾ ÑÐ¸Ð¿Ð°.",

    "The file '{file}' does not exist." =>
    "Ð¤Ð°Ð¹Ð» '{file}' Ð½Ðµ ÑÑÑÐµÑÑÐ²ÑÐµÑ.",

    "Cannot read '{file}'." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÑÐ¾ÑÐ¸ÑÐ°ÑÑ ÑÐ°Ð¹Ð» '{file}'.",

    "Cannot copy '{file}'." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐºÐ¾Ð¿Ð¸ÑÐ¾Ð²Ð°ÑÑ ÑÐ°Ð¹Ð» '{file}'.",

    "Cannot move '{file}'." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÐµÑÐµÐ¼ÐµÑÑÐ¸ÑÑ ÑÐ°Ð¹Ð» '{file}'.",

    "Cannot delete '{file}'." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ´Ð°Ð»Ð¸ÑÑ ÑÐ°Ð¹Ð» '{file}'.",

    "Click to remove from the Clipboard" =>
    "ÐÐ°Ð¶Ð¼Ð¸ÑÐµ Ð´Ð»Ñ ÑÐ´Ð°Ð»ÐµÐ½Ð¸Ñ Ð¸Ð· Ð±ÑÑÐµÑÐ° Ð¾Ð±Ð¼ÐµÐ½Ð°",

    "This file is already added to the Clipboard." =>
    "Ð­ÑÐ¾Ñ ÑÐ°Ð¹Ð» ÑÐ¶Ðµ Ð´Ð¾Ð±Ð°Ð²Ð»ÐµÐ½ Ð² Ð±ÑÑÐµÑ Ð¾Ð±Ð¼ÐµÐ½Ð°.",

    "Copy files here" =>
    "Ð¡ÐºÐ¾Ð¿Ð¸ÑÐ¾Ð²Ð°ÑÑ ÑÐ°Ð¹Ð»Ñ ÑÑÐ´Ð°",

    "Move files here" =>
    "ÐÐµÑÐµÐ¼ÐµÑÑÐ¸ÑÑ ÑÐ°Ð¹Ð»Ñ ÑÑÐ´Ð°",

    "Delete files" =>
    "Ð£Ð´Ð°Ð»Ð¸ÑÑ ÑÐ°Ð¹Ð»Ñ",

    "Clear the Clipboard" =>
    "ÐÑÐ¸ÑÑÐ¸ÑÑ Ð±ÑÑÐµÑ Ð¾Ð±Ð¼ÐµÐ½Ð°",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "ÐÑ ÑÐ²ÐµÑÐµÐ½Ñ, ÑÑÐ¾ ÑÐ¾ÑÐ¸ÑÐµ ÑÐ´Ð°Ð»Ð¸ÑÑ Ð²ÑÐµ ÑÐ°Ð¹Ð»Ñ Ð² Ð±ÑÑÐµÑÐµ Ð¾Ð±Ð¼ÐµÐ½Ð°?",

    "Copy {count} files" =>
    "Ð¡ÐºÐ¾Ð¿Ð¸ÑÐ¾Ð²Ð°ÑÑ {count} ÑÐ°Ð¹Ð»(Ð¾Ð²)",

    "Move {count} files" =>
    "ÐÐµÑÐµÐ¼ÐµÑÑÐ¸ÑÑ {count} ÑÐ°Ð¹Ð»(Ð¾Ð²)",

    "Add to Clipboard" =>
    "ÐÐ¾Ð±Ð°Ð²Ð¸ÑÑ Ð² Ð±ÑÑÐµÑ Ð¾Ð±Ð¼ÐµÐ½Ð°",

    "New folder name:" => "ÐÐ¾Ð²Ð¾Ðµ Ð¸Ð¼Ñ Ð¿Ð°Ð¿ÐºÐ¸:",
    "New file name:" => "ÐÐ¾Ð²Ð¾Ðµ Ð¸Ð¼Ñ ÑÐ°Ð¹Ð»Ð°:",

    "Upload" => "ÐÐ°Ð³ÑÑÐ·Ð¸ÑÑ",
    "Refresh" => "ÐÐ±Ð½Ð¾Ð²Ð¸ÑÑ",
    "Settings" => "Ð£ÑÑÐ°Ð½Ð¾Ð²ÐºÐ¸",
    "Maximize" => "Ð Ð°Ð·Ð²ÐµÑÐ½ÑÑÑ",
    "About" => "Ð ÑÐºÑÐ¸Ð¿ÑÐµ",
    "files" => "ÑÐ°Ð¹Ð»Ñ",
    "View:" => "ÐÑÐ¾ÑÐ¼Ð¾ÑÑ:",
    "Show:" => "ÐÐ¾ÐºÐ°Ð·ÑÐ²Ð°ÑÑ:",
    "Order by:" => "Ð£Ð¿Ð¾ÑÑÐ´Ð¾ÑÐ¸ÑÑ Ð¿Ð¾:",
    "Thumbnails" => "ÐÐ¸Ð½Ð¸Ð°ÑÑÑÑ",
    "List" => "Ð¡Ð¿Ð¸ÑÐ¾Ðº",
    "Name" => "ÐÐ¼Ñ",
    "Size" => "Ð Ð°Ð·Ð¼ÐµÑ",
    "Date" => "ÐÐ°ÑÐ°",
    "Descending" => "ÐÐ¾ ÑÐ±ÑÐ²Ð°Ð½Ð¸Ñ",
    "Uploading file..." => "ÐÐ°Ð³ÑÑÐ·ÐºÐ° ÑÐ°Ð¹Ð»Ð°...",
    "Loading image..." => "ÐÐ°Ð³ÑÑÐ·ÐºÐ° Ð¸Ð·Ð¾Ð±ÑÐ°Ð¶ÐµÐ½Ð¸Ñ...",
    "Loading folders..." => "ÐÐ°Ð³ÑÑÐ·ÐºÐ° Ð¿Ð°Ð¿Ð¾Ðº...",
    "Loading files..." => "ÐÐ°Ð³ÑÑÐ·ÐºÐ° ÑÐ°Ð¹Ð»Ð¾Ð²...",
    "New Subfolder..." => "Ð¡Ð¾Ð·Ð´Ð°ÑÑ Ð¿Ð°Ð¿ÐºÑ...",
    "Rename..." => "ÐÐµÑÐµÐ¸Ð¼ÐµÐ½Ð¾Ð²Ð°ÑÑ...",
    "Delete" => "Ð£Ð´Ð°Ð»Ð¸ÑÑ",
    "OK" => "OK",
    "Cancel" => "ÐÑÐ¼ÐµÐ½Ð°",
    "Select" => "ÐÑÐ±ÑÐ°ÑÑ",
    "Select Thumbnail" => "ÐÑÐ±ÑÐ°ÑÑ Ð¼Ð¸Ð½Ð¸Ð°ÑÑÑÑ",
    "View" => "ÐÑÐ¾ÑÐ¼Ð¾ÑÑ",
    "Download" => "Ð¡ÐºÐ°ÑÐ°ÑÑ",
    'Clipboard' => "ÐÑÑÐµÑ Ð¾Ð±Ð¼ÐµÐ½Ð°",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÐµÑÐµÐ¸Ð¼ÐµÐ½Ð¾Ð²Ð°ÑÑ Ð¿Ð°Ð¿ÐºÑ.",

    "Cannot delete the folder." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ´Ð°Ð»Ð¸ÑÑ Ð¿Ð°Ð¿ÐºÑ.",

    "The files in the Clipboard are not readable." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÑÐ¾ÑÐ¸ÑÐ°ÑÑ ÑÐ°Ð¹Ð»Ñ Ð² Ð±ÑÑÐµÑÐµ Ð¾Ð±Ð¼ÐµÐ½Ð°.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÑÐ¾ÑÐ¸ÑÐ°ÑÑ {count} ÑÐ°Ð¹Ð»(Ð¾Ð²) Ð² Ð±ÑÑÐµÑÐµ Ð¾Ð±Ð¼ÐµÐ½Ð°. ÐÑ ÑÐ¾ÑÐ¸ÑÐµ ÑÐºÐ¾Ð¿Ð¸ÑÐ¾Ð²Ð°ÑÑ Ð¾ÑÑÐ°Ð²ÑÐ¸ÐµÑÑ?",

    "The files in the Clipboard are not movable." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÐµÑÐµÐ¼ÐµÑÑÐ¸ÑÑ ÑÐ°Ð¹Ð»Ñ Ð² Ð±ÑÑÐµÑÐµ Ð¾Ð±Ð¼ÐµÐ½Ð°.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿ÐµÑÐµÐ¼ÐµÑÑÐ¸ÑÑ {count} ÑÐ°Ð¹Ð»(Ð¾Ð²) Ð² Ð±ÑÑÐµÑÐµ Ð¾Ð±Ð¼ÐµÐ½Ð°. ÐÑ ÑÐ¾ÑÐ¸ÑÐµ Ð¿ÐµÑÐµÐ¼ÐµÑÑÐ¸ÑÑ Ð¾ÑÑÐ°Ð²ÑÐ¸ÐµÑÑ?",

    "The files in the Clipboard are not removable." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ´Ð°Ð»Ð¸ÑÑ ÑÐ°Ð¹Ð»Ñ Ð² Ð±ÑÑÐµÑÐµ Ð¾Ð±Ð¼ÐµÐ½Ð°.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ´Ð°Ð»Ð¸ÑÑ {count} ÑÐ°Ð¹Ð»(Ð¾Ð²) Ð² Ð±ÑÑÐµÑÐµ Ð¾Ð±Ð¼ÐµÐ½Ð°. ÐÑ ÑÐ¾ÑÐ¸ÑÐµ ÑÐ´Ð°Ð»Ð¸ÑÑ Ð¾ÑÑÐ°Ð²ÑÐ¸ÐµÑÑ?",

    "The selected files are not removable." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ´Ð°Ð»Ð¸ÑÑ Ð²ÑÐ±ÑÐ°Ð½Ð½ÑÐµ ÑÐ°Ð¹Ð»Ñ.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ´Ð°Ð»Ð¸ÑÑ Ð²ÑÐ±ÑÐ°Ð½Ð½ÑÐ¹(Ðµ) {count} ÑÐ°Ð¹Ð»(Ñ). ÐÑ ÑÐ¾ÑÐ¸ÑÐµ ÑÐ´Ð°Ð»Ð¸ÑÑ Ð¾ÑÑÐ°Ð²ÑÐ¸ÐµÑÑ?",

    "Are you sure you want to delete all selected files?" =>
    "ÐÑ ÑÐ²ÐµÑÐµÐ½Ñ, ÑÑÐ¾ ÑÐ¾ÑÐ¸ÑÐµ ÑÐ´Ð°Ð»Ð¸ÑÑ Ð²ÑÐµ Ð²ÑÐ±ÑÐ°Ð½Ð½ÑÐµ ÑÐ°Ð¹Ð»Ñ?",

    "Failed to delete {count} files/folders." =>
    "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ ÑÐ´Ð°Ð»Ð¸ÑÑ {count} ÑÐ°Ð¹Ð»Ð¾Ð²/Ð¿Ð°Ð¿Ð¾Ðº.",

    "A file or folder with that name already exists." =>
    "Ð¤Ð°Ð¹Ð» Ð¸Ð»Ð¸ Ð¿Ð°Ð¿ÐºÐ° Ñ ÑÐ°ÐºÐ¸Ð¼ Ð¸Ð¼ÐµÐ½ÐµÐ¼ ÑÐ¶Ðµ ÑÑÑÐµÑÑÐ²ÑÑÑ.",

    "Inexistant or inaccessible folder." =>
    "ÐÐµÑÑÑÐµÑÑÐ²ÑÑÑÐ°Ñ Ð¸Ð»Ð¸ Ð½ÐµÐ´Ð¾ÑÑÑÐ¿Ð½Ð°Ñ Ð¿Ð°Ð¿ÐºÐ°.",

    "selected files" => "Ð²ÑÐ±ÑÐ°Ð½Ð½ÑÐµ ÑÐ°Ð¹Ð»Ñ",
    "Type" => "Ð¢Ð¸Ð¿",
    "Select Thumbnails" => "ÐÑÐ±ÑÐ°ÑÑ Ð¼Ð¸Ð½Ð¸Ð°ÑÑÑÑ",
    "Download files" => "Ð¡ÐºÐ°ÑÐ°ÑÑ ÑÐ°Ð¹Ð»Ñ",

    // SINCE 2.4

    "Checking for new version..." => "ÐÑÐ¾Ð²ÐµÑÑÐµÐ¼ Ð½Ð°Ð»Ð¸ÑÐ¸Ðµ Ð¾Ð±Ð½Ð¾Ð²Ð»ÐµÐ½Ð¸Ð¹...",
    "Unable to connect!" => "ÐÐµÐ²Ð¾Ð·Ð¼Ð¾Ð¶Ð½Ð¾ Ð¿Ð¾Ð´ÐºÐ»ÑÑÐ¸ÑÑÑÑ!",
    "Download version {version} now!" => "Ð¡ÐºÐ°ÑÐ°ÑÑ Ð²ÐµÑÑÐ¸Ñ {version} ÑÐµÐ¹ÑÐ°Ñ!",
    "KCFinder is up to date!" => "ÐÑ Ð¸ÑÐ¿Ð¾Ð»ÑÐ·ÑÐµÑÐµ Ð¿Ð¾ÑÐ»ÐµÐ´Ð½ÑÑ Ð²ÐµÑÑÐ¸Ñ KCFinder'Ð°!",
    "Licenses:" => "ÐÐ¸ÑÐµÐ½Ð·Ð¸Ð¸:",
    "Attention" => "ÐÐ½Ð¸Ð¼Ð°Ð½Ð¸Ðµ",
    "Question" => "ÐÐ¾Ð¿ÑÐ¾Ñ",
    "Yes" => "ÐÐ°",
    "No" => "ÐÐµÑ",

    // SINCE 2.41

    "You cannot rename the extension of files!" => "ÐÑ Ð½Ðµ Ð¼Ð¾Ð¶ÐµÑÐµ Ð¸Ð·Ð¼ÐµÐ½ÑÑÑ ÑÐ°ÑÑÐ¸ÑÐµÐ½Ð¸Ñ ÑÐ°Ð¹Ð»Ð¾Ð²!",
);

?>