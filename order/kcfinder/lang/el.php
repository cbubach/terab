<?php

/** Greek  localization file for KCFinder
  * author: Spiros Kabasakalis
  */

$lang = array(

    '_locale' => "el_GR.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e %B, %Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

    "You don't have permissions to upload files." =>
    "ÎÎµÎ½ Î­ÏÎµÏÎµ Î´Î¹ÎºÎ±Î¯ÏÎ¼Î± Î½Î± Î±Î½ÎµÎ²Î¬ÏÎµÏÎµ Î±ÏÏÎµÎ¯Î±.",

    "You don't have permissions to browse server." =>
    "ÎÎµÎ½ Î­ÏÎµÏÎµ Î´Î¹ÎºÎ±Î¯ÏÎ¼Î± Î½Î± Î´ÎµÎ¯ÏÎµ ÏÎ± Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Î´Î¹Î±ÎºÎ¿Î¼Î¹ÏÏÎ®.",

    "Cannot move uploaded file to target folder." =>
    "Î¤Î¿ Î±ÏÏÎµÎ¯Î¿ Î´Îµ Î¼ÏÎ¿ÏÎµÎ¯ Î½Î± Î¼ÎµÏÎ±ÏÎµÏÎ¸ÎµÎ¯ ÏÏÎ¿ ÏÎ¬ÎºÎµÎ»Î¿ ÏÏÎ¿Î¿ÏÎ¹ÏÎ¼Î¿Ï.",

    "Unknown error." =>
    "'ÎÎ³Î½ÏÏÏÎ¿ ÏÏÎ¬Î»Î¼Î±.",

    "The uploaded file exceeds {size} bytes." =>
    "Î¤Î¿ Î±ÏÏÎµÎ¯Î¿ ÏÏÎµÏÎ²Î±Î¯Î½ÎµÎ¹ ÏÎ¿ Î¼Î­Î³ÎµÎ¸Î¿Ï ÏÏÎ½  {size} bytes.",

    "The uploaded file was only partially uploaded." =>
    "ÎÎ½Î± Î¼ÏÎ½Î¿ Î¼Î­ÏÎ¿Ï ÏÎ¿Ï Î±ÏÏÎµÎ¯Î¿Ï Î±Î½Î­Î²Î·ÎºÎµ.",

    "No file was uploaded." =>
    "ÎÎ±Î½Î­Î½Î± Î±ÏÏÎµÎ¯Î¿ Î´ÎµÎ½ Î±Î½Î­Î²Î·ÎºÎµ.",

    "Missing a temporary folder." =>
    "ÎÎµÎ¯ÏÎµÎ¹ Î¿ ÏÎ¬ÎºÎµÎ»Î¿Ï ÏÏÎ½ ÏÏÎ¿ÏÏÏÎ¹Î½ÏÎ½ Î±ÏÏÎµÎ¯ÏÎ½.",

    "Failed to write file." =>
    "Î£ÏÎ¬Î»Î¼Î± ÏÏÎ· ÏÏÎ¿ÏÎ¿ÏÎ¿Î¯Î·ÏÎ· ÏÎ¿Ï Î±ÏÏÎµÎ¯Î¿Ï.",

    "Denied file extension." =>
    "ÎÎµÎ½ ÎµÏÎ¹ÏÏÎ­ÏÎ¿Î½ÏÎ±Î¹ Î±ÏÏÎ¿Ï ÏÎ¿Ï ÎµÎ¯Î´Î¿ÏÏ Î±ÏÏÎµÎ¯Î±.",

    "Unknown image format/encoding." =>
    "ÎÎ³Î½ÏÏÏÎ· ÎºÏÎ´Î¹ÎºÎ¿ÏÎ¿Î¯Î·ÏÎ· ÎµÎ¹ÎºÏÎ½Î±Ï.",

    "The image is too big and/or cannot be resized." =>
    "Î ÎµÎ¹ÎºÏÎ½Î± ÎµÎ¯Î½Î±Î¹ ÏÎ¬ÏÎ± ÏÎ¿Î»Ï Î¼ÎµÎ³Î¬Î»Î· ÎºÎ±Î¹/Î· Î´ÎµÎ½ Î¼ÏÎ¿ÏÎµÎ¯ Î½Î± Î±Î»Î»Î¬Î¾ÎµÎ¹ Î¼Î­Î³ÎµÎ¸Î¿Ï.",

    "Cannot create {dir} folder." =>
    "ÎÎ´ÏÎ½Î±ÏÎ¿Î½ Î½Î± Î´Î·Î¼Î¹Î¿ÏÏÎ³Î·Î¸ÎµÎ¯ Î¿ ÏÎ¬ÎºÎµÎ»Î¿Ï {dir}.",

    "Cannot write to upload folder." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· ÎµÎ³Î³ÏÎ±ÏÎ® ÏÏÎ¿ ÏÎ¬ÎºÎµÎ»Î¿ ÏÏÎ¿Î¿ÏÎ¹ÏÎ¼Î¿Ï.",

    "Cannot read .htaccess" =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· Î±Î½Î¬Î³Î½ÏÏÎ· ÏÎ¿Ï .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "ÎÏÏÎ±Î»Î¼Î­Î½Î¿ Î±ÏÏÎµÎ¯Î¿ .htaccess.ÎÎ´ÏÎ½Î±ÏÎ· Î·  ÏÏÎ¿ÏÎ¿ÏÎ¿Î¯Î·ÏÎ® ÏÎ¿Ï!",

    "Cannot read upload folder." =>
    "ÎÎ· Î±Î½Î±Î³Î½ÏÏÎ¹Î¼Î¿Ï ÏÎ¬ÎºÎµÎ»Î¿Ï ÏÏÎ¿Î¿ÏÎ¹ÏÎ¼Î¿Ï.",

    "Cannot access or create thumbnails folder." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· ÏÏÏÏÎ²Î±ÏÎ· ÎºÎ±Î¹ Î±Î½Î¬Î³Î½ÏÏÎ· ÏÎ¿Ï ÏÎ±ÎºÎ­Î»Î¿Ï Î¼Îµ ÏÎ¹Ï Î¼Î¹ÎºÏÎ¿Î³ÏÎ±ÏÎ¯ÎµÏ ÎµÎ¹ÎºÏÎ½ÏÎ½.",

    "Cannot access or write to upload folder." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· ÏÏÏÏÎ²Î±ÏÎ· ÎºÎ±Î¹ ÏÏÎ¿ÏÎ¿ÏÎ¿Î¯Î·ÏÎ· ÏÎ¿Ï ÏÎ±ÎºÎ­Î»Î¿Ï ÏÏÎ¿Î¿ÏÎ¹ÏÎ¼Î¿Ï.",

    "Please enter new folder name." =>
    "Î Î±ÏÎ±ÎºÎ±Î»Î¿ÏÎ¼Îµ ÎµÎ¹ÏÎ¬Î³ÎµÏÎµ Î­Î½Î± Î½Î­Î¿ ÏÎ½Î¿Î¼Î± ÏÎ±ÎºÎ­Î»Î¿Ï. ",

    "Unallowable characters in folder name." =>
    "ÎÎ· ÎµÏÎ¹ÏÏÎµÏÏÎ¿Î¯ ÏÎ±ÏÎ±ÎºÏÎ®ÏÎµÏ ÏÏÎ¿ ÏÎ½Î¿Î¼Î± ÏÎ±ÎºÎ­Î»Î¿Ï.",

    "Folder name shouldn't begins with '.'" =>
    "Î¤Î¿ ÏÎ½Î¿Î¼Î± ÏÎ¿Ï ÏÎ±ÎºÎ­Î»Î¿Ï Î´Îµ ÏÏÎ­ÏÎµÎ¹ Î½Î± Î±ÏÏÎ¯Î¶ÎµÎ¹ Î¼Îµ '.'",

    "Please enter new file name." =>
    "Î Î±ÏÎ±ÎºÎ±Î»Î¿ÏÎ¼Îµ ÎµÎ¹ÏÎ¬Î³ÎµÏÎµ Î­Î½Î± Î½Î­Î¿ ÏÎ½Î¿Î¼Î± Î±ÏÏÎµÎ¯Î¿Ï.",

    "Unallowable characters in file name." =>
    "ÎÎ· ÎµÏÎ¹ÏÏÎµÏÏÎ¿Î¯ ÏÎ±ÏÎ±ÎºÏÎ®ÏÎµÏ ÏÏÎ¿ ÏÎ½Î¿Î¼Î± Î±ÏÏÎµÎ¯Î¿Ï.",

    "File name shouldn't begins with '.'" =>
    "Î¤Î¿ ÏÎ½Î¿Î¼Î± ÏÎ¿Ï Î±ÏÏÎµÎ¯Î¿Ï Î´Îµ ÏÏÎ­ÏÎµÎ¹ Î½Î± Î±ÏÏÎ¯Î¶ÎµÎ¹ Î¼Îµ '.'",

    "Are you sure you want to delete this file?" =>
    "Î£Î¯Î³Î¿ÏÏÎ± Î¸Î­Î»ÎµÏÎµ Î½Î± Î´Î¹Î±Î³ÏÎ¬ÏÎµÏÎµ Î±ÏÏÏ ÏÎ¿ Î±ÏÏÎµÎ¯Î¿?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Î£Î¯Î³Î¿ÏÏÎ± Î¸Î­Î»ÎµÏÎµ Î½Î± Î´Î¹Î±Î³ÏÎ¬ÏÎµÏÎµ Î±ÏÏÏ ÏÎ¿ ÏÎ¬ÎºÎµÎ»Î¿ Î¼Î±Î¶Î¯ Î¼Îµ ÏÎ»Î± ÏÎ± ÏÎµÏÎ¹ÎµÏÏÎ¼ÎµÎ½Î±?",

    "Non-existing directory type." =>
    "ÎÎ½ÏÏÎ±ÏÎºÏÎ¿Ï ÏÏÏÎ¿Ï ÏÎ±ÎºÎ­Î»Î¿Ï.",

    "Undefined MIME types." =>
    "ÎÏÏÎ¿ÏÎ´Î¹ÏÏÎ¹ÏÏÎ¿Î¹ ÏÏÏÎ¿Î¹ MIME.",

    "Fileinfo PECL extension is missing." =>
    "Î ÏÎ»Î·ÏÎ¿ÏÎ¿ÏÎ¯Î± Î±ÏÏÎµÎ¯Î¿Ï ÎµÏÎ­ÎºÏÎ±ÏÎ· PECL Î´ÎµÎ½ ÏÏÎ¬ÏÏÎµÎ¹.",

    "Opening fileinfo database failed." =>
    "Î ÏÏÏÏÎ²Î±ÏÎ· ÏÏÎ¹Ï ÏÎ»Î·ÏÎ¿ÏÎ¿ÏÎ¯ÎµÏ ÏÎ¿Ï Î±ÏÏÎµÎ¯Î¿Ï Î±ÏÎ­ÏÏÏÎµ.",

    "You can't upload such files." =>
    "ÎÎµ Î¼ÏÎ¿ÏÎµÎ¯ÏÎµ Î½Î± Î±Î½ÎµÎ²Î¬ÏÎµÏÎµ ÏÎ­ÏÎ¿Î¹Î± Î±ÏÏÎµÎ¯Î±.",

    "The file '{file}' does not exist." =>
    "Î¤Î¿ Î±ÏÏÎµÎ¯Î¿ '{file}' Î´ÎµÎ½ ÏÏÎ¬ÏÏÎµÎ¹.",

    "Cannot read '{file}'." =>
    "ÎÏÏÎµÎ¯Î¿ '{file}' Î¼Î· Î±Î½Î±Î³Î½ÏÏÎ¹Î¼Î¿.",

    "Cannot copy '{file}'." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· Î±Î½ÏÎ¹Î³ÏÎ±ÏÎ® ÏÎ¿Ï '{file}'.",

    "Cannot move '{file}'." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· Î¼ÎµÏÎ±ÎºÎ¯Î½Î·ÏÎ· ÏÎ¿Ï '{file}'.",

    "Cannot delete '{file}'." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· Î´Î¹Î±Î³ÏÎ±ÏÎ® ÏÎ¿Ï '{file}'.",

    "Click to remove from the Clipboard" =>
    "Î Î±ÏÎ®ÏÏÎµ Î³Î¹Î±  Î´Î¹Î±Î³ÏÎ±ÏÎ® Î±ÏÏ ÏÎ¿ Clipboard.",

    "This file is already added to the Clipboard." =>
    "ÎÏÏÏ ÏÎ¿ Î±ÏÏÎµÎ¯Î¿ Î²ÏÎ¯ÏÎºÎµÏÎ±Î¹ Î®Î´Î· ÏÏÎ¿ Clipboard.",

    "Copy files here" =>
    "ÎÎ½ÏÎ¹Î³ÏÎ¬ÏÏÎµ Î±ÏÏÎµÎ¯Î± ÎµÎ´Ï.",

    "Move files here" =>
    "ÎÎµÏÎ±ÎºÎ¹Î½Î®ÏÏÎµ Î±ÏÏÎµÎ¯Î± ÎµÎ´Ï.",

    "Delete files" =>
    "ÎÎ¹Î±Î³ÏÎ±ÏÎ® Î±ÏÏÎµÎ¯ÏÎ½",

    "Clear the Clipboard" =>
    "ÎÎ±Î¸Î±ÏÎ¹ÏÎ¼ÏÏ Clipboard",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Î£Î¯Î³Î¿ÏÏÎ± Î¸Î­Î»ÎµÏÎµ Î½Î± Î´Î¹Î±Î³ÏÎ¬ÏÎµÏÎµ ÏÎ»Î± ÏÎ± Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Clipboard?",

    "Copy {count} files" =>
    "ÎÎ½ÏÎ¹Î³ÏÎ±ÏÎ®  {count} Î±ÏÏÎµÎ¯ÏÎ½.",

    "Move {count} files" =>
    "ÎÎµÏÎ±ÎºÎ¯Î½Î·ÏÎ· {count} Î±ÏÏÎµÎ¯ÏÎ½.",

    "Add to Clipboard" =>
    "Î ÏÎ¿ÏÎ¸Î®ÎºÎ· ÏÏÎ¿ Clipboard",

    "New folder name:" => "ÎÎ­Î¿ ÏÎ½Î¿Î¼Î± ÏÎ±ÎºÎ­Î»Î¿Ï:",
    "New file name:" => "ÎÎ­Î¿ ÏÎ½Î¿Î¼Î± Î±ÏÏÎµÎ¯Î¿Ï:",

    "Upload" => "ÎÎ½Î­Î²Î±ÏÎ¼Î±",
    "Refresh" => "ÎÎ½Î±Î½Î­ÏÏÎ·",
    "Settings" => "Î¡ÏÎ¸Î¼Î¯ÏÎµÎ¹Ï",
    "Maximize" => "ÎÎµÎ³Î¹ÏÏÎ¿ÏÎ¿Î¯Î·ÏÎ·",
    "About" => "Î£ÏÎµÏÎ¹ÎºÎ¬",
    "files" => "Î±ÏÏÎµÎ¯Î±",
    "View:" => "Î ÏÎ¿Î²Î¿Î»Î®:",
    "Show:" => "ÎÎ¼ÏÎ¬Î½Î¹ÏÎ·:",
    "Order by:" => "Î¤Î±Î¾Î¹Î½ÏÎ¼Î·ÏÎ· ÎºÎ±ÏÎ¬:",
    "Thumbnails" => "ÎÎ¹ÎºÏÎ¿Î³ÏÎ±ÏÎ¯ÎµÏ",
    "List" => "ÎÎ¯ÏÏÎ±",
    "Name" => "ÎÎ½Î¿Î¼Î±",
    "Size" => "ÎÎ­Î³ÎµÎ¸Î¿Ï",
    "Date" => "ÎÎ¼ÎµÏÎ¿Î¼Î·Î½Î¯Î±",
    "Descending" => "Î¦Î¸Î¯Î½Î¿ÏÏÎ±",
    "Uploading file..." => "Î¤Î¿ Î±ÏÏÎµÎ¯Î¿ Î±Î½ÎµÎ²Î±Î¯Î½ÎµÎ¹...",
    "Loading image..." => "Î ÎµÎ¹ÎºÏÎ½Î± ÏÎ¿ÏÏÏÎ½ÎµÎ¹...",
    "Loading folders..." => "ÎÎ¹ ÏÎ¬ÎºÎµÎ»Î¿Î¹ ÏÎ¿ÏÏÏÎ½Î¿ÏÎ½...",
    "Loading files..." => "Î¤Î± Î±ÏÏÎµÎ¯Î± ÏÎ¿ÏÏÏÎ½Î¿ÏÎ½...",
    "New Subfolder..." => "ÎÎ­Î¿Ï ÏÏÎ¿ÏÎ¬ÎºÎµÎ»Î¿Ï...",
    "Rename..." => "ÎÎµÏÎ¿Î½Î¿Î¼Î±ÏÎ¯Î±...",
    "Delete" => "ÎÎ¹Î±Î³ÏÎ±ÏÎ®",
    "OK" => "OK",
    "Cancel" => "ÎÎºÏÏÏÏÎ·",
    "Select" => "ÎÏÎ¹Î»Î¿Î³Î®",
    "Select Thumbnail" => "ÎÏÎ¹Î»Î¿Î³Î® ÎÎ¹ÎºÏÎ¿Î³ÏÎ±ÏÎ¯Î±Ï",
    "View" => "Î ÏÎ¿Î²Î¿Î»Î®",
    "Download" => "ÎÎ±ÏÎ­Î²Î±ÏÎ¼Î±",
    'Clipboard' => "Clipboard",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· Î¼ÎµÏÎ¿Î½Î¿Î¼Î±ÏÎ¯Î± ÏÎ¿Ï ÏÎ±ÎºÎ­Î»Î¿Ï.",

    "Cannot delete the folder." =>
    "ÎÎ´ÏÎ½Î±ÏÎ· Î· Î´Î¹Î±Î³ÏÎ±ÏÎ® ÏÎ¿Ï ÏÎ±ÎºÎ­Î»Î¿Ï.",

    "The files in the Clipboard are not readable." =>
    "Î¤Î± Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Clipboard ÎµÎ¯Î½Î±Î¹ Î¼Î· Î±Î½Î±Î³Î½ÏÏÎ¹Î¼Î±.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "{count} Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Clipboard ÎµÎ¯Î½Î±Î¹ Î¼Î· Î±Î½Î±Î³ÏÏÎ¹Î¼Î±.ÎÎ­Î»ÎµÏÎµ Î½Î± Î±Î½ÏÎ¹Î³ÏÎ¬ÏÎµÏÎµ ÏÎ± ÏÏÏÎ»Î¿Î¹ÏÎ±?",

    "The files in the Clipboard are not movable." =>
    "Î¤Î± Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Clipboard ÎµÎ¯Î½Î±Î¹ Î±Î´ÏÎ½Î±ÏÎ¿ Î½Î± Î¼ÎµÏÎ±ÎºÎ¹Î½Î·Î¸Î¿ÏÎ½.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "{count} Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Clipboard Î´ÎµÎ½ ÎµÎ¯Î½Î±Î¹ Î´ÏÎ½Î±ÏÏ Î½Î± Î¼ÎµÏÎ±ÎºÎ¹Î½Î·Î¸Î¿ÏÎ½.ÎÎ­Î»ÎµÏÎµ Î½Î± Î¼ÎµÏÎ±ÎºÎ¹Î½Î®ÏÎµÏÎµ ÏÎ± ÏÏÏÎ»Î¿Î¹ÏÎ±?",

    "The files in the Clipboard are not removable." =>
    "Î¤Î± Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Clipboard ÎµÎ¯Î½Î±Î¹ Î±Î´ÏÎ½Î±ÏÎ¿ Î½Î± Î±ÏÎ±Î¹ÏÎµÎ¸Î¿ÏÎ½.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} Î±ÏÏÎµÎ¯Î± ÏÏÎ¿ Clipboard Î´ÎµÎ½ ÎµÎ¯Î½Î±Î¹ Î´ÏÎ½Î±ÏÏ Î½Î± Î±ÏÎ±Î¹ÏÎµÎ¸Î¿ÏÎ½.ÎÎ­Î»ÎµÏÎµ Î½Î± Î±ÏÎ±Î¹ÏÎ­ÏÎµÏÎµ ÏÎ± ÏÏÏÎ»Î¿Î¹ÏÎ±?",

    "The selected files are not removable." =>
    "Î¤Î± ÎµÏÎ¹Î»ÎµÎ³Î¼Î­Î½Î± Î±ÏÏÎµÎ¯Î± Î´Îµ Î¼ÏÎ¿ÏÎ¿ÏÎ½ Î½Î± Î±ÏÎ±Î¹ÏÎµÎ¸Î¿ÏÎ½.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "{count} ÎµÏÎ¹Î»ÎµÎ³Î¼Î­Î½Î± Î±ÏÏÎµÎ¯Î± Î´ÎµÎ½ ÎµÎ¯Î½Î±Î¹ Î´ÏÎ½Î±ÏÏ Î½Î± Î±ÏÎ±Î¹ÏÎµÎ¸Î¿ÏÎ½.ÎÎ­Î»ÎµÏÎµ Î½Î± Î´Î¹Î±Î³ÏÎ¬ÏÎµÏÎµ ÏÎ± ÏÏÏÎ»Î¿Î¹ÏÎ±?",

    "Are you sure you want to delete all selected files?" =>
    "Î£Î¯Î³Î¿ÏÏÎ± Î¸Î­Î»ÎµÏÎµ Î½Î± Î´Î¹Î±Î³ÏÎ¬ÏÎµÏÎµ ÏÎ»Î± ÏÎ± ÎµÏÎ¹Î»ÎµÎ³Î¼Î­Î½Î± Î±ÏÏÎµÎ¯Î±?",

    "Failed to delete {count} files/folders." =>
    "Î Î´Î¹Î±Î³ÏÎ±ÏÎ®  {count} Î±ÏÏÎµÎ¯ÏÎ½/ÏÎ±ÎºÎ­Î»ÏÎ½ Î±ÏÎ­ÏÏÏÎµ.",

    "A file or folder with that name already exists." =>
    "ÎÎ½Î± Î±ÏÏÎµÎ¯Î¿ Î® ÏÎ¬ÎºÎµÎ»Î¿Ï Î¼Îµ Î±ÏÏÏ ÏÎ¿ ÏÎ½Î¿Î¼Î± ÏÏÎ¬ÏÏÎµÎ¹ Î®Î´Î·.",

    "Inexistant or inaccessible folder." =>
    "ÎÎ½ÏÏÎ±ÏÎºÏÎ¿Ï Î· Î¼Î· ÏÏÎ¿ÏÎ²Î¬ÏÎ¹Î¼Î¿Ï ÏÎ¬ÎºÎµÎ»Î¿Ï.",

    "selected files" => "ÎµÏÎ¹Î»ÎµÎ³Î¼Î­Î½Î± Î±ÏÏÎµÎ¯Î±",
    "Type" => "Î¤ÏÏÎ¿Ï",
    "Select Thumbnails" => "ÎÏÎ¹Î»Î­Î¾ÏÎµ ÎÎ¹ÎºÏÎ¿Î³ÏÎ±ÏÎ¯ÎµÏ",
    "Download files" => "ÎÎ±ÏÎ­Î²Î±ÏÎ¼Î± ÎÏÏÎµÎ¯ÏÎ½",

     // SINCE 2.4

    "Checking for new version..." => "ÎÎ»ÎµÎ³ÏÎ¿Ï Î³Î¹Î± Î½Î­Î± Î­ÎºÎ´Î¿ÏÎ·...",
    "Unable to connect!" => "ÎÎ´ÏÎ½Î±ÏÎ· Î· ÏÏÎ½Î´ÎµÏÎ·!",
    "Download version {version} now!" => "ÎÎ±ÏÎµÎ²Î¬ÏÏÎµ ÏÎ·Î½ Î­ÎºÎ´Î¿ÏÎ· {version} ÏÏÏÎ±!",
    "KCFinder is up to date!" => "Î¤Î¿ KCFinder ÎµÎ¯Î½Î±Î¹ ÎµÎ½Î·Î¼ÎµÏÏÎ¼Î­Î½Î¿ Î¼Îµ ÏÎ· ÏÎ¹Î¿ ÏÏÏÏÏÎ±ÏÎ· Î­ÎºÎ´Î¿ÏÎ·!",
    "Licenses:" => "ÎÎ´ÎµÎ¹ÎµÏ:",
    "Attention" => "Î ÏÎ¿ÏÎ¿ÏÎ®",
    "Question" => "ÎÏÏÏÎ·ÏÎ·",
    "Yes" => "ÎÎ±Î¹",
    "No" => "ÎÏÎ¹",
);

?>