<?php

/** French localization file for KCFinder
  * author: Krzysztof Lorenc
  */

$lang = array(

    '_locale' => "pl_PL.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e %B, %Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

    "You don't have permissions to upload files." =>
    "Nie masz zezwolenia na wysyÅanie plikÃ³w.",

    "You don't have permissions to browse server." =>
    "Nie masz zezwolenia na przeglÄdanie serwera.",

    "Cannot move uploaded file to target folder." =>
    "Nie moÅ¼na przenieÅÄ wysÅanego pliku do folderu plikÃ³w wysÅanych.",

    "Unknown error." =>
    "NieokreÅlony bÅÄd.",

    "The uploaded file exceeds {size} bytes." =>
    "WysyÅany plik przekroczyÅ rozmiar {size} bajtÃ³w",

    "The uploaded file was only partially uploaded." =>
    "WysyÅany plik nie zostaÅ przesÅany w caÅoÅci.",

    "No file was uploaded." =>
    "Å»aden plik nie zostaÅ przesÅany",

    "Missing a temporary folder." =>
    "Brak katalogu domyÅlnego.",

    "Failed to write file." =>
    "BÅÄd zapisu pliku.",

    "Denied file extension." =>
    "Niedozwolone rozszerzenie pliku.",

    "Unknown image format/encoding." =>
    "Nie znany format/kodowanie pliku.",

    "The image is too big and/or cannot be resized." =>
    "Obraz jest zbyt duÅ¼y i/lub nie moÅ¼e zostaÄ zmieniony jego rozmiar.",

    "Cannot create {dir} folder." =>
    "Nie moÅ¼na utworzyÄ katalogu {dir}.",

    "Cannot write to upload folder." =>
    "Nie moÅ¼na zapisywaÄ do katalogu plikÃ³w wysÅanych.",

    "Cannot read .htaccess" =>
    "Nie moÅ¼na odczytaÄ pliku .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "Nie prawidÅowy plik .htaccess. Nie moÅ¼na go zapisaÄ!",

    "Cannot read upload folder." =>
    "Nie moÅ¼na odczytaÄ katalogu plikÃ³w wysÅanych.",

    "Cannot access or create thumbnails folder." =>
    "Nie ma dostÄpu lub nie moÅ¼na utworzyÄ katalogu miniatur.",

    "Cannot access or write to upload folder." =>
    "Nie ma dostÄpu lub nie moÅ¼na zapisywaÄ do katalogu plikÃ³w wysÅanych.",

    "Please enter new folder name." =>
    "ProszÄ podaÄ nowÄ nazwÄ katalogu.",

    "Unallowable characters in folder name." =>
    "Niedozwolony znak w nazwie folderu.",

    "Folder name shouldn't begins with '.'" =>
    "Nazwa katalogu nie moÅ¼e zaczynaÄ siÄ od '.'",

    "Please enter new file name." =>
    "ProszÄ podaÄ nowÄ nazwÄ pliku",

    "Unallowable characters in file name." =>
    "Nie dozwolony znak w nazwie pliku.",

    "File name shouldn't begins with '.'" =>
    "Nazwa pliku nie powinna zaczynaÄ siÄ od '.'",

    "Are you sure you want to delete this file?" =>
    "Czy jesteÅ pewien, Å¼e chcesz skasowaÄ ten plik?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Czy jesteÅ pewien, Å¼e chcesz skasowaÄ ten katalog i jego zawartoÅÄ?",

    "Non-existing directory type." =>
    "Nie istniejÄcy katalog.",

    "Undefined MIME types." =>
    "Niezidentyfikowany typ MIME.",

    "Fileinfo PECL extension is missing." =>
    "Brak rozszerzenia Fileinfo PECL.",

    "Opening fileinfo database failed." =>
    "Otwieranie bazy danych fileinfo nie udane.",

    "You can't upload such files." =>
    "Nie moÅ¼esz wysyÅaÄ plikÃ³w tego typu.",

    "The file '{file}' does not exist." =>
    "Plik {file} nie istnieje.",

    "Cannot read '{file}'." =>
    "Nie moÅ¼na odczytaÄ pliku '{file}'.",

    "Cannot copy '{file}'." =>
    "Nie moÅ¼na skopiowaÄ pliku '{file}'.",

    "Cannot move '{file}'." =>
    "Nie moÅ¼na przenieÅÄ pliku '{file}'.",

    "Cannot delete '{file}'." =>
    "Nie moÅ¼na usunÄÄ pliku '{file}'.",

    "Click to remove from the Clipboard" =>
    "Kliknij aby usunÄÄ ze Schowka",

    "This file is already added to the Clipboard." =>
    "Ten plik juÅ¼ zostaÅ dodany do Schowka.",

    "Copy files here" =>
    "Kopiuj pliki tutaj",

    "Move files here" =>
    "PrzenieÅ pliki tutaj",

    "Delete files" =>
    "UsuÅ pliki",

    "Clear the Clipboard" =>
    "WyczyÅÄ Schowek",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Czy jesteÅ pewien, Å¼e chcesz usunÄÄ wszystkie pliki ze schowka?",

    "Copy {count} files" =>
    "Kopiowanie {count} plikÃ³w",

    "Move {count} files" =>
    "Przenoszenie {count} plikÃ³w",

    "Add to Clipboard" =>
    "Dodaj do Schowka",

    "New folder name:" => "Nazwa nowego katalogu:",
    "New file name:" => "Nowa nazwa pliku:",

    "Upload" => "WyÅlij",
    "Refresh" => "OdÅwieÅ¼",
    "Settings" => "Ustawienia",
    "Maximize" => "Maksymalizuj",
    "About" => "O...",
    "files" => "pliki",
    "View:" => "Widok:",
    "Show:" => "PokaÅ¼:",
    "Order by:" => "Sortuj wedÅug:",
    "Thumbnails" => "Miniatury",
    "List" => "Lista",
    "Name" => "Nazwa",
    "Size" => "Rozmiar",
    "Date" => "Data",
    "Descending" => "MalejÄco",
    "Uploading file..." => "WysyÅanie pliku...",
    "Loading image..." => "Åadowanie obrazu...",
    "Loading folders..." => "Åadowanie katalogÃ³w...",
    "Loading files..." => "Åadowanie plikÃ³w...",
    "New Subfolder..." => "Nowy pod-katalog...",
    "Rename..." => "ZmieÅ nazwÄ...",
    "Delete" => "UsuÅ",
    "OK" => "OK",
    "Cancel" => "Anuluj",
    "Select" => "Wybierz",
    "Select Thumbnail" => "Wybierz miniaturÄ",
    "View" => "PodglÄd",
    "Download" => "Pobierz",
    'Clipboard' => "Schowek",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "Nie moÅ¼na zmieniÄ nazwy katalogu.",

    "Cannot delete the folder." =>
    "Nie moÅ¼na usunÄÄ katalogu.",

    "The files in the Clipboard are not readable." =>
    "Pliki w Schowku nie mogÄ zostaÄ odczytane.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "{count} plik(i/Ã³w) ze Schowka  nie moÅ¼e zostaÄ odczytanych. Czy chcesz skopiowaÄ pozostaÅe?",

    "The files in the Clipboard are not movable." =>
    "Pliki w Schowku nie mogÄ zostaÄ przeniesione.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "{count} plik(i/Ã³w) ze Schowka nie moÅ¼e zostaÄ przeniesionych. Czy chcesz przenieÅÄ pozostaÅe?",

    "The files in the Clipboard are not removable." =>
    "Nie moÅ¼na usunÄÄ plikÃ³w ze Schowka.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} plik(i/Ã³w) ze Schowka nie moÅ¼e zostaÄ usuniÄty(ch). Czy usunÄÄ pozostaÅe?",

    "The selected files are not removable." =>
    "Wybrane pliki nie mogÄ zostaÄ usuniÄte.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "{count} wybrany(ch) plikÃ³w nie moÅ¼e zostaÄ usuniÄte. Czy usunÄÄ pozostaÅe?",

    "Are you sure you want to delete all selected files?" =>
    "Czy jesteÅ pewien Å¼e, chcesz usunÄÄ wszystkie wybrane pliki?",

    "Failed to delete {count} files/folders." =>
    "Nie udaÅo siÄ usunÄÄ {count} plik(i/Ã³w) / folder(u/Ã³w).",

    "A file or folder with that name already exists." =>
    "Plik lub katalog o tej nazwie juÅ¼ istnieje.",

    "Inexistant or inaccessible folder." =>
    "NieistniejÄcy lub niedostÄpny folder.",

    "selected files" => "wybrane pliki",
    "Type" => "Typ",
    "Select Thumbnails" => "Wybierz miniatury",
    "Download files" => "Pobierz pliki",
);

?>