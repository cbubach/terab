<?php

/** Chinese Simplified localization file for KCFinder
  * author: yutuo
  * country: China
  * E-mail: yutuo5@gmail.com
  * URL: yutuo.net
  */

$lang = array(

    '_locale' => "zh_CN.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e %B, %Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%Y-%m-%d %H:%M",

    "You don't have permissions to upload files." =>
    "æ¨æ²¡ææéä¸ä¼ æä»¶ã",

    "You don't have permissions to browse server." =>
    "æ¨æ²¡ææéæ¥çæå¡å¨æä»¶ã",

    "Cannot move uploaded file to target folder." =>
    "æ æ³ç§»å¨ä¸ä¼ æä»¶å°æå®æä»¶å¤¹ã",

    "Unknown error." =>
    "åçä¸å¯é¢ç¥å¼å¸¸ã",

    "The uploaded file exceeds {size} bytes." =>
    "æä»¶å¤§å°è¶è¿{size}å­èã",

    "The uploaded file was only partially uploaded." =>
    "æä»¶æªå®å¨ä¸ä¼ ã",

    "No file was uploaded." =>
    "æä»¶æªä¸ä¼ ã",

    "Missing a temporary folder." =>
    "ä¸´æ¶æä»¶å¤¹ä¸å­å¨ã",

    "Failed to write file." =>
    "åå¥æä»¶å¤±è´¥ã",

    "Denied file extension." =>
    "ç¦æ­¢çæä»¶æ©å±åã",

    "Unknown image format/encoding." =>
    "æ æ³ç¡®è®¤å¾çæ ¼å¼ã",

    "The image is too big and/or cannot be resized." =>
    "å¾çå¤§å¤ªï¼ä¸ï¼æï¼æ æ³æ´æ¹å¤§å°ã",

    "Cannot create {dir} folder." =>
    "æ æ³åå»º{dir}æä»¶å¤¹ã",

    "Cannot write to upload folder." =>
    "æ æ³åå¥ä¸ä¼ æä»¶å¤¹ã",

    "Cannot read .htaccess" =>
    "æä»¶.htaccessæ æ³è¯»åã",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "æä»¶.htaccesséè¯¯ï¼æ æ³éåã",

    "Cannot read upload folder." =>
    "æ æ³è¯»åä¸ä¼ ç®å½ã",

    "Cannot access or create thumbnails folder." =>
    "æ æ³è®¿é®æåå»ºç¼©ç¥å¾æä»¶å¤¹ã",

    "Cannot access or write to upload folder." =>
    "æ æ³è®¿é®æåå¥ä¸ä¼ æä»¶å¤¹ã",

    "Please enter new folder name." =>
    "è¯·è¾å¥æä»¶å¤¹åã",

    "Unallowable characters in folder name." =>
    "æä»¶å¤¹åå«æç¦æ­¢å­ç¬¦ã",

    "Folder name shouldn't begins with '.'" =>
    "æä»¶å¤¹åä¸è½ä»¥ç¹ï¼.ï¼ä¸ºé¦å­ç¬¦ã",

    "Please enter new file name." =>
    "è¯·è¾å¥æ°æä»¶åã",

    "Unallowable characters in file name." =>
    "æä»¶åå«æç¦æ­¢å­ç¬¦ã",

    "File name shouldn't begins with '.'" =>
    "æä»¶åä¸è½ä»¥ç¹ï¼.ï¼ä¸ºé¦å­ç¬¦ã",

    "Are you sure you want to delete this file?" =>
    "æ¯å¦ç¡®è®¤å é¤è¯¥æä»¶ï¼",

    "Are you sure you want to delete this folder and all its content?" =>
    "æ¯å¦ç¡®è®¤å é¤è¯¥æä»¶å¤¹ä»¥åå¶å­æä»¶åå­ç®å½ï¼",

    "Inexistant or inaccessible folder." =>
    "ä¸å­å¨æä¸å¯è®¿é®çæä»¶å¤¹ã",

    "Undefined MIME types." =>
    "æªå®ä¹çMIMEç±»åã",

    "Fileinfo PECL extension is missing." =>
    "æä»¶PECLå±æ§ä¸å­å¨ã",

    "Opening fileinfo database failed." =>
    "æå¼æä»¶å±æ§æ°æ®åºåºéã",

    "You can't upload such files." =>
    "ä½ æ æ³ä¸ä¼ è¯¥æä»¶ã",

    "The file '{file}' does not exist." =>
    "æä»¶{file}ä¸å­å¨ã",

    "Cannot read '{file}'." =>
    "æ æ³è¯»åæä»¶{file}ã",

    "Cannot copy '{file}'." =>
    "æ æ³å¤å¶æä»¶{file}ã",

    "Cannot move '{file}'." =>
    "æ æ³ç§»å¨æä»¶{file}ã",

    "Cannot delete '{file}'." =>
    "æ æ³å é¤æä»¶{file}ã",

    "Click to remove from the Clipboard" =>
    "ç¹å»ä»åªè´´æ¿å é¤",

    "This file is already added to the Clipboard." =>
    "æä»¶å·²å¤å¶å°åªè´´æ¿ã",

    "Copy files here" =>
    "å¤å¶å°è¿é",

    "Move files here" =>
    "ç§»å¨å°è¿é",

    "Delete files" =>
    "å é¤è¿äºæä»¶",

    "Clear the Clipboard" =>
    "æ¸é¤åªè´´æ¿",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "æ¯å¦ç¡®è®¤å é¤ææå¨åªè´´æ¿çæä»¶ï¼",

    "Copy {count} files" =>
    "å¤å¶ {count} ä¸ªæä»¶",

    "Move {count} files" =>
    "ç§»å¨ {count} ä¸ªæä»¶ ",

    "Add to Clipboard" =>
    "æ·»å å°åªè´´æ¿",

    "New folder name:" => "æ°æä»¶å¤¹åï¼",
    "New file name:" => "æ°æä»¶å¤¹ï¼",

    "Upload" => "ä¸ä¼ ",
    "Refresh" => "å·æ°",
    "Settings" => "è®¾ç½®",
    "Maximize" => "æå¤§å",
    "About" => "å³äº",
    "files" => "æä»¶",
    "View:" => "è§å¾ï¼",
    "Show:" => "æ¾ç¤ºï¼",
    "Order by:" => "æåºï¼",
    "Thumbnails" => "å¾æ ",
    "List" => "åè¡¨",
    "Name" => "æä»¶å",
    "Size" => "å¤§å°",
    "Date" => "æ¥æ",
    "Descending" => "éåº",
    "Uploading file..." => "æ­£å¨ä¸ä¼ æä»¶...",
    "Loading image..." => "æ­£å¨å è½½å¾ç...",
    "Loading folders..." => "æ­£å¨å è½½æä»¶å¤¹...",
    "Loading files..." => "æ­£å¨å è½½æä»¶...",
    "New Subfolder..." => "æ°å»ºæä»¶å¤¹...",
    "Rename..." => "éå½å...",
    "Delete" => "å é¤",
    "OK" => "OK",
    "Cancel" => "åæ¶",
    "Select" => "éæ©",
    "Select Thumbnail" => "éæ©ç¼©ç¥å¾",
    "View" => "æ¥ç",
    "Download" => "ä¸è½½",
    "Clipboard" => "åªè´´æ¿",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "æ æ³éå½åè¯¥æä»¶å¤¹ã",

    "Non-existing directory type." =>
    "ä¸å­å¨çç®å½ç±»åã",

    "Cannot delete the folder." =>
    "æ æ³å é¤è¯¥æä»¶å¤¹ã",

    "The files in the Clipboard are not readable." =>
    "åªè´´æ¿ä¸è¯¥æä»¶æ æ³è¯»åã",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "åªè´´æ¿{count}ä¸ªæä»¶æ æ³è¯»åã æ¯å¦å¤å¶éææä»¶ï¼",

    "The files in the Clipboard are not movable." =>
    "åªè´´æ¿ä¸è¯¥æä»¶æ æ³ç§»å¨ã",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "åªè´´æ¿{count}ä¸ªæä»¶æ æ³ç§»å¨ã æ¯å¦ç§»å¨éææä»¶ï¼",

    "The files in the Clipboard are not removable." =>
    "åªè´´æ¿ä¸è¯¥æä»¶æ æ³å é¤ã",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "åªè´´æ¿{count}ä¸ªæä»¶æ æ³å é¤ã æ¯å¦å é¤éææä»¶ï¼",

    "The selected files are not removable." =>
    "éä¸­æä»¶æªå é¤ã",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "éä¸­ç{count}ä¸ªæä»¶æªå é¤ãæ¯å¦å é¤éææä»¶ï¼",

    "Are you sure you want to delete all selected files?" =>
    "æ¯å¦ç¡®è®¤å é¤éä¸­æä»¶ï¼",

    "Failed to delete {count} files/folders." =>
    "{count}ä¸ªæä»¶ææä»¶å¤¹æ æ³å é¤ã",

    "A file or folder with that name already exists." =>
    "æä»¶ææä»¶å¤¹å·²å­å¨ã",

    "selected files" => "éä¸­çæä»¶",
    "Type" => "ç§ç±»",
    "Select Thumbnails" => "éæ©ç¼©ç¥å¾",
    "Download files" => "ä¸è½½æä»¶",
);

?>
