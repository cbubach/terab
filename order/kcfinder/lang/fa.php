<?php

/** This file is part of KCFinder project
  *
  *      @desc Persian(Farsi) localization file
  *   @package KCFinder
  *   @version 2.2
  *    @author Hamid Kamalpour <djhamidfatal@gmail.com><http://www.ssfmusic.com>
  * @copyright 2010 KCFinder Project
  *   @license http://www.opensource.org/licenses/gpl-2.0.php GPLv2
  *   @license http://www.opensource.org/licenses/lgpl-2.1.php LGPLv2
  *      @link http://kcfinder.sunhater.com
  */

$lang = array(
    '_locale' => "fa_IR.UTF-8",
    '_charset' => "utf-8",

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %B %e, %Y %H:%M",
    '_dateTimeMid' => "%a %b %e %Y %H:%M",
    '_dateTimeSmall' => "%Y/%m/%d %H:%M",

    "You don't have permissions to upload files." =>
    ".Ø´ÙØ§ ÙØ§ÙØ¯ ÙØ¬ÙØ² Ø¨Ø±Ø§Û  Ø§Ø±Ø³Ø§Ù ÙØ§ÛÙ ÙØ§ ÙØ³ØªÛØ¯",

    "You don't have permissions to browse server." =>
    ".Ø´ÙØ§ ÙØ§ÙØ¯ ÙØ¬ÙØ² Ø¨Ø±Ø§Û Ø¬Ø³ØªØ¬Ù Ø¯Ø± Ø³Ø±ÙØ± ÙØ³ØªÛØ¯",

    "Cannot move uploaded file to target folder." =>
    ".Ø¨Ø±ÙØ§ÙÙ ÙÙÛ ØªÙØ§ÙØ¯ ÙØ§ÛÙ Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û Ø´Ø¯Ù Ø±Ø§ Ø§ÙØªÙØ§Ù Ø¯ÙØ¯ Ø¨Ù Ù¾ÙØ´Ù ÙÙØ±Ø¯ ÙØ¸Ø±",

    "Unknown error." =>
    ".Ø®Ø·Ø§Û ÙØ§ÙØ´Ø®Øµ",

    "The uploaded file exceeds {size} bytes." =>
    ".Ø¨Ø§ÛØª Ø§Ø³Øª {size} Ø­Ø¬Ù ÙØ§ÛÙ Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û Ø´Ø¯Ù Ø¨ÛØ´ØªØ± Ø§Ø²",

    "The uploaded file was only partially uploaded." =>
    ".ÙØ§ÛÙ ÙØ§ÙØµ Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û Ø´Ø¯",

    "No file was uploaded." =>
    ".ÙØ§ÛÙ Ø§Ø±Ø³Ø§Ù ÙØ´Ø¯",

    "Missing a temporary folder." =>
    ".Ù¾ÙØ´Ù ØªÙÙ¾ Ù¾ÛØ¯Ø§ ÙØ´Ø¯",

    "Failed to write file." =>
    ".Ø®Ø·Ø§ Ø¯Ø± ÙÙØ´ØªÙ ÙØ§ÛÙ",

    "Denied file extension." =>
    ".Ù¾Ø³ÙÙØ¯ ÙØ§ÛÙ ØºÛØ± ÙØ¬Ø§Ø² Ø§Ø³Øª",

    "Unknown image format/encoding." =>
    ".Ø¹Ú©Ø³ ÙØ¹ØªØ¨Ø± ÙÛØ³Øª format/encoding",

    "The image is too big and/or cannot be resized." =>
    ".Ø¹Ú©Ø³ Ø§ÙØªØ®Ø§Ø¨Û ÛØ§ Ø¨Ø²Ø±Ú¯ Ø§Ø³Øª ÛØ§ ØªØºÛÛØ± Ø§ÙØ¯Ø§Ø²Ù Ø¯Ø§Ø¯Ù ÙÙÛ Ø´ÙØ¯",

    "Cannot create {dir} folder." =>
    ".{dir}ÙØ´Ú©Ù Ø¯Ø± Ø³Ø§Ø®Øª Ù¾ÙØ´Ù",

    "Cannot write to upload folder." =>
    ".ÙØ´Ú©Ù Ø¯Ø± ÙÙØ´ØªÙ Ø§Ø·ÙØ§Ø¹Ø§Øª Ø¯Ø± Ù¾ÙØ´Ù Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û",

    "Cannot read .htaccess" =>
    ".htaccess Ø®Ø·Ø§ Ø¯Ø± Ø®ÙØ§ÙØ¯Ù ÙØ§ÛÙ",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    ".ØºÛØ±ÙØ§Ø¨Ù Ø¨Ø§Ø²ÙÙÛØ³Û Ø§Ø³Øª .htaccess ÙØ§ÛÙ",

    "Cannot read upload folder." =>
    ".ÙØ´Ú©Ù Ø¯Ø± Ø®ÙØ§ÙØ¯Ù Ù¾ÙØ´Ù Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û",

    "Cannot access or create thumbnails folder." =>
    ".ÙØ´Ú©Ù Ø¯Ø± Ø¯Ø³ØªØ±Ø³Û ÛØ§ Ø³Ø§Ø®Øª Ù¾ÙØ´Ù ØªØ§Ù",

    "Cannot access or write to upload folder." =>
    ".ÙØ´Ú©Ù Ø¯Ø± Ø¯Ø³ØªØ±Ø³Û Ø¨Ø±Ø§Û ÙÙØ´ØªÙ Ø§Ø·ÙØ§Ø¹Ø§Øª Ø¯Ø± Ù¾ÙØ´Ù Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û",

    "Please enter new folder name." =>
    ".ÙØ·ÙØ§ ÙØ§Ù Ù¾ÙØ´Ù Ø¬Ø¯ÛØ¯ Ø±Ø§ ÙØ§Ø±Ø¯ Ú©ÙÛØ¯",

    "Unallowable characters in folder name." =>
    ".ÙØ§Ù Ù¾ÙØ´Ù Ø¯Ø§Ø±Ø§Û Ø­Ø±ÙÙ ØºÛØ± ÙØ¬Ø§Ø² Ø§Ø³Øª",

    "Folder name shouldn't begins with '.'" =>
    ".ÙØ§Ù Ù¾ÙØ´Ù ÙØ¨Ø§ÛØ¯ Ø¨Ø§ '.' Ø´Ø±ÙØ¹ Ø´ÙØ¯",

    "Please enter new file name." =>
    ".ÙØ·ÙØ§ ÙØ§Ù ÙØ§ÛÙ Ø¬Ø¯ÛØ¯ Ø±Ø§ ÙØ§Ø±Ø¯ Ú©ÙÛØ¯",

    "Unallowable characters in file name." =>
    ".ÙØ§Ù ÙØ§ÛÙ Ø¯Ø§Ø±Ø§Û Ø­Ø±ÙÙ ØºÛØ± ÙØ¬Ø§Ø² Ø§Ø³Øª",

    "File name shouldn't begins with '.'" =>
    ".ÙØ§Ù ÙØ§ÛÙ ÙØ¨Ø§ÛØ¯ Ø¨Ø§ '.' Ø´Ø±ÙØ¹ Ø´ÙØ¯",

    "Are you sure you want to delete this file?" =>
    "Ø¢ÛØ§ Ø§Ø² Ø­Ø°Ù Ø§ÛÙ ÙØ§ÛÙ Ø§Ø·ÙÛÙØ§Ù Ø¯Ø§Ø±ÛØ¯Ø",

    "Are you sure you want to delete this folder and all its content?" =>
    "Ø¢ÛØ§ Ø§Ø² Ø­Ø°Ù Ø§ÛÙ Ù¾ÙØ´Ù Ù ØªÙØ§Ù ÙØ­ØªÙÛØ§Øª Ø¯Ø§Ø®Ù Ø¢Ù Ø§Ø·ÙÛÙØ§Ù Ø¯Ø§Ø±ÛØ¯Ø",

    "Inexistant or inaccessible folder." =>
    "Tipo di cartella non esistente.",

    "Undefined MIME types." =>
    ".ØªØ¹Ø±ÛÙ ÙØ´Ø¯Ù Ø§ÙØ¯ MIME Ù¾Ø³ÙÙØ¯ ÙØ§Û ",

    "Fileinfo PECL extension is missing." =>
    "Manca estensione PECL del file.",

    "Opening fileinfo database failed." =>
    ".Ø®Ø·Ø§ Ø¯Ø± Ø¨Ø§Ø²Ú©Ø±Ø¯Ù Ø¨Ø§ÙÚ© Ø§Ø·ÙØ§Ø¹Ø§ØªÛ ÙØ´Ø®ØµØ§Øª ÙØ§ÛÙ",

    "You can't upload such files." =>
    ".Ø´ÙØ§ Ø§ÙÚ©Ø§Ù Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û Ø§ÛÙ ÙØ§ÛÙ ÙØ§ Ø±Ø§ ÙØ¯Ø§Ø±ÛØ¯",

    "The file '{file}' does not exist." =>
    ".ÙÙØ¬ÙØ¯ ÙÛØ³Øª '{file}' ÙØ§ÛÙ",

    "Cannot read '{file}'." =>
    ".'{file}' ÙØ´Ú©Ù Ø¯Ø± Ø®ÙØ§ÙØ¯Ù",

    "Cannot copy '{file}'." =>
    ".'{file}' ÙÙÛ ØªÙØ§ÙÛØ¯ Ú©Ù¾Û Ú©ÙÛØ¯",

    "Cannot move '{file}'." =>
    ".'{file}' ÙÙÛ ØªÙØ§ÙÛØ¯ Ø§ÙØªÙØ§Ù Ø¯ÙÛØ¯",

    "Cannot delete '{file}'." =>
    ".'{file}'ÙÙÛ ØªÙØ§ÙÛØ¯ Ø­Ø°Ù Ú©ÙÛØ¯",

    "Click to remove from the Clipboard" =>
    ".Ø¨Ø±Ø§Û Ø­Ø°Ù Ø§Ø² Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ Ú©ÙÛÚ© Ú©ÙÛØ¯",

    "This file is already added to the Clipboard." =>
    ".Ø§ÛÙ ÙØ§ÛÙ ÙØ¨ÙØ§ Ø¯Ø± Ø­Ø§ÙØ¸Ù Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ Ø§ÙØ²ÙØ¯Ù Ø´Ø¯Ù Ø§Ø³Øª",

    "Copy files here" =>
    "Ú©Ù¾Û ÙØ§ÛÙ ÙØ§ Ø¨Ù Ø§ÛÙØ¬Ø§",

    "Move files here" =>
    "Ø§ÙØªÙØ§Ù ÙØ§ÛÙ ÙØ§ Ø¨Ù Ø§ÛÙØ¬Ø§",

    "Delete files" =>
    "Ø­Ø°Ù ÙØ§ÛÙ ÙØ§",

    "Clear the Clipboard" =>
    "Ù¾Ø§Ú© Ú©Ø±Ø¯Ù Ø­Ø§ÙØ¸Ù Ú©ÙÛÙ¾ Ø¨Ø±Ø¯",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Ø¢ÛØ§ Ø§Ø² Ø­Ø°Ù ÙØ§ÛÙ ÙØ§Û ÙÙØ¬ÙØ¯ Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ Ø§Ø·ÙÛÙØ§Ù Ø¯Ø§Ø±ÛØ¯Ø",

    "Copy {count} files" =>
    "...ØªØ¹Ø¯Ø§Ø¯ {count} ÙØ§ÛÙ Ø¢ÙØ§Ø¯Ù Ú©Ù¾Û Ø¨Ù",

    "Move {count} files" =>
    "...ØªØ¹Ø¯Ø§Ø¯ {count} ÙØ§ÛÙ Ø¢ÙØ§Ø¯Ù Ø§ÙØªÙØ§Ù Ø¨Ù",

    "Add to Clipboard" =>
    "Ø§ÙØ²ÙØ¯Ù Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯",

    "New folder name:" => "ÙØ§Ù Ù¾ÙØ´Ù Ø¬Ø¯ÛØ¯:",
    "New file name:" => "ÙØ§Ù ÙØ§ÛÙ Ø¬Ø¯ÛØ¯:",

    "Upload" => "Ø§Ø±Ø³Ø§Ù ÙØ§ÙÙ",
    "Refresh" => "Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û ÙØ¬Ø¯Ø¯",
    "Settings" => "ØªÙØ¸ÙÙØ§Øª",
    "Maximize" => "ØªÙØ§Ù ØµÙØ­Ù",
    "About" => "Ø¯Ø±Ø¨Ø§Ø±Ù",
    "files" => "ÙØ§ÙÙ ÙØ§",
    "View:" => ": ÙÙØ§ÛØ´",
    "Show:" => ": ÙÙØ§ÙØ´",
    "Order by:" => ": ÙØ±ØªØ¨ Ú©Ø±Ø¯Ù Ø¨Ø± ÙØ¨ÙØ§Ù",
    "Thumbnails" => "ÙÙØ§ÙØ´ Ú©ÙÚÚ© Ø¹Ú©Ø³ÙØ§",
    "List" => "ÙÙØ³Øª",
    "Name" => "ÙØ§Ù",
    "Size" => "Ø­Ø¬Ù",
    "Date" => "ØªØ§Ø±ÙØ®",
    "Type" => "Ù¾Ø³ÙÙØ¯",
    "Descending" => "ÙØ²ÙÙÙ",
    "Uploading file..." => "... Ø¯Ø±Ø­Ø§Ù Ø§Ø±Ø³Ø§Ù ÙØ§ÛÙ",
    "Loading image..." => "... Ø¯Ø±Ø­Ø§Ù Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û Ø¹Ú©Ø³",
    "Loading folders..." => "... Ø¯Ø±Ø­Ø§Ù Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û Ù¾ÙØ´Ù ÙØ§",
    "Loading files..." => "... Ø¯Ø±Ø­Ø§Ù Ø¨Ø§Ø±Ú¯Ø°Ø§Ø±Û ÙØ§ÛÙ ÙØ§",
    "New Subfolder..." => "...Ø³Ø§Ø®Øª Ø²ÛØ±Ù¾ÙØ´Ù Ø¬Ø¯ÛØ¯",
    "Rename..." => "... ØªØºÛÛØ± ÙØ§Ù",
    "Delete" => "Ø­Ø°Ù",
    "OK" => "Ø§Ø¯Ø§ÙÙ",
    "Cancel" => "Ø§ÙØµØ±Ø§Ù",
    "Select" => "Ø§ÙØªØ®Ø§Ø¨",
    "Select Thumbnail" => "Ø§ÙØªØ®Ø§Ø¨ Ø¹Ú©Ø³ Ø¨Ø§ Ø§ÙØ¯Ø§Ø²Ù Ú©ÙÚÚ©",
    "View" => "ÙÙØ§ÛØ´",
    "Download" => "Ø¯Ø±ÛØ§ÙØª ÙØ§ÛÙ",
    "Clipboard" => "Ø­Ø§ÙØ¶Ù Ú©ÙÛÙ¾ Ø¨Ø±Ø¯",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    ".ÙØ§Ù ÙØ§Ø±Ø¯ Ø´Ø¯Ù ØªÚ©Ø±Ø§Ø±Û Ø§Ø³Øª. Ù¾ÙØ´Ù Ø§Û Ø¨Ø§ Ø§ÛÙ ÙØ§Ù ÙØ¬ÙØ¯ Ø¯Ø§Ø±Ø¯. ÙØ·ÙØ§ ÙØ§Ù Ø¬Ø¯ÛØ¯Û Ø§ÙØªØ®Ø§Ø¨ Ú©ÙÛØ¯",

    "Non-existing directory type." =>
    ".ÙÙØ¹ ÙÙØ±Ø³Øª ÙØ¬ÙØ¯ ÙØ¯Ø§Ø±Ø¯",

    "Cannot delete the folder." =>
    ".ÙÙÛ ØªÙØ§ÙÛØ¯ Ø§ÛÙ Ù¾ÙØ´Ù Ø±Ø§ Ø­Ø°Ù Ú©ÙÛØ¯",

    "The files in the Clipboard are not readable." =>
    ".ÙØ§ÛÙ ÙØ§Û ÙÙØ¬ÙØ¯ Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ ÙØ§Ø¨Ù Ø®ÙØ§ÙØ¯Ù ÙÛØ³ØªÙØ¯",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "ØªØ¹Ø¯Ø§Ø¯ {count} ÙØ§ÛÙ ÙÙØ¬ÙØ¯ Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ ÙØ§Ø¨Ù Ø®ÙØ§ÙØ¯Ù ÙÛØ³ØªÙØ¯. Ø¢ÛØ§ ÙØ§ÛÙÛØ¯  Ø¨ÙÛÙ ÙØ§ÛÙ ÙØ§ Ú©Ù¾Û Ø´ÙÙØ¯Ø",

    "The files in the Clipboard are not movable." =>
    ".ÙØ§ÛÙ ÙØ§Û ÙÙØ¬ÙØ¯ Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ ØºÛØ± ÙØ§Ø¨Ù Ø§ÙØªÙØ§Ù ÙØ³ØªÙØ¯. ÙØ·ÙØ§ Ø¯Ø³ØªØ±Ø³Û ÙØ§ÛÙ ÙØ§ Ø±Ø§ ÚÚ© Ú©ÙÛØ¯",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "ØªØ¹Ø¯Ø§Ø¯ {count} ÙØ§ÛÙ Ø§Ø² ÙØ§ÛÙ ÙØ§Û ÙÙØ¬ÙØ¯ Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ ØºÛØ± ÙØ§Ø¨Ù Ø§ÙØªÙØ§Ù ÙØ³ØªÙØ¯. Ø¢ÛØ§ ÙØ§ÛÙÛØ¯ Ø¨ÙÛÙ ÙØ§ÛÙ ÙØ§ ÙÙØªÙÙ Ø´ÙÙØ¯Ø",

    "The files in the Clipboard are not removable." =>
    ".ÙØ§ÛÙ ÙØ§Û ÙÙØ¬ÙØ¯ Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ ÙØ§Ø¨Ù Ù¾Ø§Ú© Ø´Ø¯Ù ÙÛØ³ØªÙØ¯. Ø¯Ø³ØªØ±Ø³Û ÙØ§ÛÙ ÙØ§ Ø±Ø§ ÚÚ© Ú©ÙÛØ¯",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "ØªØ¹Ø¯Ø§Ø¯ {count} ÙØ§ÛÙ Ø§Ø² ÙØ§ÛÙ ÙØ§Û ÙÙØ¬ÙØ¯ Ø¯Ø± Ú©ÙÛÙ¾ Ø¨Ø±Ø¯ ØºÛØ± ÙØ§Ø¨Ù Ø­Ø°Ù ÙØ³ØªÙØ¯. Ø¢ÛØ§ ÙØ§ÛÙÛØ¯ Ø¨ÙÛÙ ÙØ§ÛÙ ÙØ§ Ø­Ø°Ù Ø´ÙÙØ¯Ø",

    "The selected files are not removable." =>
    ".ÙØ§ÛÙ ÙØ§Û Ø§ÙØªØ®Ø§Ø¨ Ø´Ø¯Ù ÙØ§Ø¨Ù Ø¨Ø±Ø¯Ø§Ø´ØªÙ ÙÛØ³Øª",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "ØªØ¹Ø¯Ø§Ø¯ {count} ÙØ§ÛÙ Ø§Ø² ÙØ§ÛÙ ÙØ§Û Ø§ÙØªØ®Ø§Ø¨Û ØºÛØ± ÙØ§Ø¨Ù Ø­Ø°Ù ÙØ³ØªÙØ¯.Ø¢ÛØ§ ÙØ§ÛÙÛØ¯ Ø¨ÙÛÙ ÙØ§ÛÙ ÙØ§ Ø­Ø°Ù Ø´ÙÙØ¯Ø",

    "Are you sure you want to delete all selected files?" =>
    "Ø¢ÛØ§ Ø§Ø² Ø­Ø°Ù ØªÙØ§Ù ÙØ§ÛÙ ÙØ§Û Ø§ÙØªØ®Ø§Ø¨Û Ø§Ø·ÙÛÙØ§Ù Ø¯Ø§Ø±ÛØ¯Ø",

    "Failed to delete {count} files/folders." =>
    ".ÙØ§ÛÙ/Ù¾ÙØ´Ù {count} Ø®Ø·Ø§ Ø¯Ø± Ù¾Ø§Ú© Ú©Ø±Ø¯Ù",

    "A file or folder with that name already exists." =>
    ".ÛÚ© Ù¾ÙØ´Ù ÛØ§ ÙØ§ÛÙ Ø¨Ø§ Ø§ÛÙ ÙØ§Ù ÙØ¬ÙØ¯ Ø¯Ø§Ø±Ø¯.ÙØ·ÙØ§ ÙØ§Ù Ø¯ÛÚ¯Ø±Û Ø§ÙØªØ®Ø§Ø¨ Ú©ÙÛØ¯",

    "selected files" => "ÙØ§ÛÙ ÙØ§Û Ø§ÙØªØ®Ø§Ø¨ Ø´Ø¯Ù",
    "Select Thumbnails" => "Ø§ÙØªØ®Ø§Ø¨ Ø¹Ú©Ø³ ÙØ§Û Ú©ÙÚÚ©",
    "Download files" => "Ø¯Ø±ÛØ§ÙØª ÙØ§ÛÙ ÙØ§",

    // SINCE 2.4

    "Checking for new version..." => "...ÙØ¬ÙØ¯ ÙØ³Ø®Ù Ø¬Ø¯ÛØ¯ Ø±Ø§ Ø¨Ø±Ø±Ø³Û Ú©Ù",
    "Unable to connect!" => "!ÙØ´Ú©Ù Ø¯Ø± Ø¨Ø±ÙØ±Ø§Ø±Û Ø§Ø±ØªØ¨Ø§Ø·",
    "Download version {version} now!" => "!Ø±Ø§ Ø¯Ø§ÙÙÙØ¯ Ú©Ù {version} ÙÙØ³ÛÙ Ø­Ø§ÙØ§ ÙØ³Ø®Ù ",
    "KCFinder is up to date!" => "!Ø¨Ø±ÙØ² Ø§Ø³Øª KCFinder",
    "Licenses:" => "ÙØ¬ÙØ²",
    "Attention" => "ØªÙØ¬Ù",
    "Question" => "Ù¾Ø±Ø³Ø´",
    "Yes" => "Ø¨ÙÙ",
    "No" => "Ø®ÛØ±",

    // SINCE 2.41

    "You cannot rename the extension of files!" => "!Ø´ÙØ§ ÙÙÛ ØªÙØ§ÙÛØ¯ Ù¾Ø³ÙÙØ¯ ÙØ§ÛÙÙØ§ Ø±Ø§ ØªØºÛÛØ± Ø¯ÙÛØ¯",
);

?>
