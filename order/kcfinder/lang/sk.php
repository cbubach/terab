<?php

/** Slovak localization file for KCFinder
  * author: drejk1 <drejk@inmail.sk>
  */

$lang = array(

    '_locale' => "sk_SK.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e.%B.%Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

	"You don't have permissions to upload files." =>
    "NemÃ¡te prÃ¡vo nahrÃ¡vaÅ¥ sÃºbory.",

    "You don't have permissions to browse server." =>
    "NemÃ¡te prÃ¡vo prehliadaÅ¥ sÃºbory na servery.",

    "Cannot move uploaded file to target folder." =>
    "Nie je moÅ¾nÃ© presunÃºÅ¥ sÃºbor do yvolenÃ©ho adresÃ¡ra.",

    "Unknown error." =>
    "NeznÃ¡ma chyba.",

    "The uploaded file exceeds {size} bytes." =>
    "NahratÃ½ sÃºbor presahuje {size} bytov.",

    "The uploaded file was only partially uploaded." =>
    "NahratÃ½ sÃºbor bol nahranÃ½ len ÄiastoÄne.",

    "No file was uploaded." =>
    "Å½iadnÃ½ sÃºbor alebol nahranÃ½ na server.",

    "Missing a temporary folder." =>
    "Chyba doÄasnÃ½ adresÃ¡r.",

    "Failed to write file." =>
    "SÃºbor se nepodarilo se uloÅ¾iÅ¥.",

    "Denied file extension." =>
    "NepodporovanÃ½ typ sÃºboru.",

    "Unknown image format/encoding." =>
    "NeznamÃ½ formÃ¡t obrÃ¡zku/encoding.",

    "The image is too big and/or cannot be resized." =>
    "ObrÃ¡zok je prÃ­liÅ¡ veÄ¾kÃ½/alebo nemohol byÅ¥ zmenÅ¡enÃ½.",

    "Cannot create {dir} folder." =>
    "AdresÃ¡r {dir} nie je moÅ¾nÃ© vytvoriÅ¥.",

    "Cannot write to upload folder." =>
    "Nie je moÅ¾nÃ© ukladaÅ¥ do adresÃ¡ru pre nahrÃ¡vÃ¡nie.",

    "Cannot read .htaccess" =>
    "Nie je moÅ¾nÃ© ÄÃ­taÅ¥ sÃºbor .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "ChybnÃ½ sÃºbor .htaccess. SÃºbor nemoÅ¾no prepÃ­saÅ¥!",

    "Cannot read upload folder." =>
    "Nie je moÅ¾nÃ© ÄÃ­taÅ¥ z adresÃ¡ru pre nahrÃ¡vÃ¡nie sÃºborov.",

    "Cannot access or create thumbnails folder." =>
    "AdresÃ¡r pre nÃ¡hÄ¾ady nie je moÅ¾nÃ© vytvoriÅ¥ alebo nie je prÃ­stupnÃ½.",

    "Cannot access or write to upload folder." =>
    "Nie je moÅ¾nÃ© pristupova alebo zapisovaÅ¥ do adresÃ¡ru pre nahrÃ¡vanie sÃºborov.",

    "Please enter new folder name." =>
    "Zadajte prosÃ­m novÃ© meno adresÃ¡ru.",

    "Unallowable characters in folder name." =>
    "NepovolenÃ© znaky v nÃ¡zve adresÃ¡ru.",

    "Folder name shouldn't begins with '.'" =>
    "Meno adresÃ¡ra nesmie zaÄÃ­naÅ¥ znakom '.'",

    "Please enter new file name." =>
    "VloÅ¾te prosÃ­m novÃ© meno sÃºboru.",

    "Unallowable characters in file name." =>
    "NepovolenÃ© znaky v nÃ¡zve sÃºboru.",

    "File name shouldn't begins with '.'" =>
    "NÃ¡zov sÃºboru nesmie zaÄÃ­naÅ¥ znakom '.'",

    "Are you sure you want to delete this file?" =>
    "Ste si istÃ½ Å¾e chcete vymazaÅ¥ tento sÃºbor?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Ste si istÃ½ Å¾e chcete vymazaÅ¥ tento adresÃ¡r a celÃ½ jeho obsah?",

    "Inexistant or inaccessible folder." =>
    "NeexistujÃºci alebo neprÃ­stupnÃ½ adresÃ¡r.",

    "Undefined MIME types." =>
    "NedefinovanÃ½ MIME typ sÃºboru.",

    "Fileinfo PECL extension is missing." =>
    "RozÅ¡Ã­renie PECL pre zistenie informÃ¡ciÃ­ o sÃºbore chÃ½ba.",

    "Opening fileinfo database failed." =>
    "NaÄÃ­tanie informÃ¡ciÃ­ o sÃºbore zlyhalo.",

    "You can't upload such files." =>
    "Tieto sÃºbory nemÃ´Å¾ete nahraÅ¥ na server.",

    "The file '{file}' does not exist." =>
    "Tento sÃºbor '{file}' neexistuje.",

    "Cannot read '{file}'." =>
    "Nie je moÅ¾nÃ© naÄÃ­taÅ¥ '{file}'.",

    "Cannot copy '{file}'." =>
    "Nie je moÅ¾nÃ© kopÃ­rovaÅ¥ '{file}'.",

    "Cannot move '{file}'." =>
    "Nie je moÅ¾nÃ© presunÃºÅ¥ '{file}'.",

    "Cannot delete '{file}'." =>
    "Nie je moÅ¾nÃ© vymazaÅ¥ '{file}'.",

    "Click to remove from the Clipboard" =>
    "Kliknite pre odstrÃ¡nenie zo schrÃ¡nky",

    "This file is already added to the Clipboard." =>
    "Tento sÃºbor je uÅ¾ v schrÃ¡nke uloÅ¾enÃ½.",

    "Copy files here" =>
    "KopÃ­rovaÅ¥ sÃºbory na toto miesto",

    "Move files here" =>
    "PresunÃºÅ¥ sÃºbory na toto miesto",

    "Delete files" =>
    "ZmazaÅ¥ sÃºbory",

    "Clear the Clipboard" =>
    "VyÄistiÅ¥ schrÃ¡nku",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Ste si istÃ½ Å¾e chcete vymazaÅ¥ vÅ¡etky sÃºbory zo schrÃ¡nky?",

    "Copy {count} files" =>
    "KopÃ­rovaÅ¥ {count} sÃºborov",

    "Move {count} files" =>
    "PresunÃºÅ¥ {count} sÃºborov",

    "Add to Clipboard" =>
    "VloÅ¾iÅ¥ do schrÃ¡nky",

    "New folder name:" => "NovÃ½ nÃ¡zov adresÃ¡ra:",
    "New file name:" => "NovÃ½ nÃ¡zov sÃºboru:",

    "Upload" => "NahraÅ¥",
    "Refresh" => "ObnoviÅ¥",
    "Settings" => "Nastavenia",
    "Maximize" => "MaxializovaÅ¥",
    "About" => "O aplikÃ¡cii",
    "files" => "sÃºbory",
    "View:" => "ZobraziÅ¥:",
    "Show:" => "UkÃ¡zaÅ¥:",
    "Order by:" => "ZoradiÅ¥ podÄ¾a:",
    "Thumbnails" => "NÃ¡hÄ¾ady",
    "List" => "Zoznam",
    "Name" => "Meno",
    "Size" => "VeÄ¾kosÅ¥",
    "Date" => "DÃ¡tum",
    "Descending" => "Zostupne",
    "Uploading file..." => "NahrÃ¡vanie sÃºborov...",
    "Loading image..." => "NaÄÃ­tanie obrÃ¡zkov...",
    "Loading folders..." => "NaÄÃ­tanie adresÃ¡rov...",
    "Loading files..." => "NaÄÃ­tanie sÃºborov...",
    "New Subfolder..." => "NovÃ½ adresÃ¡r...",
    "Rename..." => "PremenovaÅ¥...",
    "Delete" => "ZmazaÅ¥",
    "OK" => "OK",
    "Cancel" => "ZruÅ¡it",
    "Select" => "VybraÅ¥",
    "Select Thumbnail" => "VybraÅ¥ nÃ¡hÄ¾ad",
    "View" => "ZobraziÅ¥",
    "Download" => "StahnuÅ¥",
    'Clipboard' => "SchrÃ¡nka",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "AdresÃ¡r nie je moÅ¾nÃ© premenovaÅ¥.",

    "Non-existing directory type." =>
    "NeexistujÃºci typ adresÃ¡ra.",

    "Cannot delete the folder." =>
    "AdresÃ¡r nie je moÅ¾nÃ© vymazaÅ¥.",

    "The files in the Clipboard are not readable." =>
    "SÃºbory v schrÃ¡nke nie je moÅ¾nÃ© naÄÃ­taÅ¥.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "{count} sÃºborov v schrÃ¡nke nie je moÅ¾nÃ© naÄÃ­taÅ¥. Chcete skopÃ­rovaÅ¥ ostatnÃ© sÃºbory?",

    "The files in the Clipboard are not movable." =>
    "SÃºbory v schrÃ¡nke nie je moÅ¾nÃ© presunÃºÅ¥.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "{count} sÃºborov v schrÃ¡nke nie je moÅ¾nÃ© presunÃºÅ¥. Chcete presunÃºÅ¥ ostatnÃ© sÃºbory?",

    "The files in the Clipboard are not removable." =>
    "SÃºbory v schrÃ¡nke nie je moÅ¾nÃ© vymazaÅ¥.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} sÃºborov v schrÃ¡nke nie je moÅ¾nÃ© vymazaÅ¥. Chcete vymazaÅ¥ ostatnÃ© sÃºbory?",

    "The selected files are not removable." =>
    "OznaÄenÃ© sÃºbory nie je moÅ¾nÃ© vymazaÅ¥.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "{count} oznaÄenÃ½ch sÃºborov nie je moÅ¾nÃ© vymazaÅ¥. Chcete vymazaÅ¥ ostatnÃ© sÃºbory?",

    "Are you sure you want to delete all selected files?" =>
    "Ste si istÃ½ Å¾e chcete vymazaÅ¥ vybranÃ© sÃºbory?",

    "Failed to delete {count} files/folders." =>
    "Nebolo vymazanÃ½ch {count} sÃºborov/adresÃ¡rov.",

    "A file or folder with that name already exists." =>
    "Soubor alebo adresÃ¡r s takovÃ½m menom uÅ¾ existuje.",

    "selected files" => "vybranÃ© sÃºbory",
    "Type" => "Typ",
    "Select Thumbnails" => "VybraÅ¥ nÃ¡hÄ¾ad",
    "Download files" => "StiahnuÅ¥ sÃºbory",
);

?>