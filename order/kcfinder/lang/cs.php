<?php

/** Czech localization file for KCFinder
  * author: Rostislav Sivak <rsivak@rsivak.com>
  */

$lang = array(

    '_locale' => "cs_CZ.UTF-8",  // UNIX localization code
    '_charset' => "utf-8",       // Browser charset

    // Date time formats. See http://www.php.net/manual/en/function.strftime.php
    '_dateTimeFull' => "%A, %e.%B.%Y %H:%M",
    '_dateTimeMid' => "%a %e %b %Y %H:%M",
    '_dateTimeSmall' => "%d.%m.%Y %H:%M",

	"You don't have permissions to upload files." =>
    "NemÃ¡te prÃ¡va pro nahrÃ¡vÃ¡nÃ­ souborÅ¯.",

    "You don't have permissions to browse server." =>
    "NemÃ¡te prÃ¡va pro prohlÃ­Å¾enÃ­ serveru.",

    "Cannot move uploaded file to target folder." =>
    "Nelze pÅesunout soubor do urÄenÃ©ho adresÃ¡Åe.",

    "Unknown error." =>
    "NeznÃ¡mÃ¡ chyba.",

    "The uploaded file exceeds {size} bytes." =>
    "NahranÃ½ soubor pÅesahuje {size} bytÅ¯.",

    "The uploaded file was only partially uploaded." =>
    "NahranÃ½ soubor byl nahrÃ¡n pouze ÄÃ¡steÄnÄ.",

    "No file was uploaded." =>
    "Å½Ã¡dnÃ½ soubor nebyl nahrÃ¡n na server.",

    "Missing a temporary folder." =>
    "ChybÃ­ doÄasnÃ½ adresÃ¡Å.",

    "Failed to write file." =>
    "Soubor se nepodaÅilo se uloÅ¾it.",

    "Denied file extension." =>
    "NepodporovanÃ½ typ souboru.",

    "Unknown image format/encoding." =>
    "NeznÃ¡mÃ½ formÃ¡t obrÃ¡zku/encoding.",

    "The image is too big and/or cannot be resized." =>
    "ObrÃ¡zek je pÅÃ­liÅ¡ velkÃ½/nebo nemohl bÃ½t zmenÅ¡en.",

    "Cannot create {dir} folder." =>
    "AdresÃ¡Å {dir} nelze vytvoÅit.",

    "Cannot write to upload folder." =>
    "Nelze uklÃ¡dat do adresÃ¡Åe pro nahrÃ¡vÃ¡nÃ­.",

    "Cannot read .htaccess" =>
    "NenÃ­ moÅ¾no ÄÃ­st soubor .htaccess",

    "Incorrect .htaccess file. Cannot rewrite it!" =>
    "ChybnÃ½ soubor .htaccess. Soubor nelze pÅepsat!",

    "Cannot read upload folder." =>
    "Nelze ÄÃ­st z adresÃ¡Åe pro nahrÃ¡vÃ¡nÃ­ souborÅ¯.",

    "Cannot access or create thumbnails folder." =>
    "AdresÃ¡Å pro nÃ¡hledy nelze vytvoÅit nebo nenÃ­ pÅÃ­stupnÃ½.",

    "Cannot access or write to upload folder." =>
    "Nelze pÅistoupit, nebo zapisovat do adresÃ¡Åe pro nahrÃ¡vÃ¡nÃ­ souborÅ¯.",

    "Please enter new folder name." =>
    "VloÅ¾te prosÃ­m novÃ© jmÃ©no adresÃ¡Åe.",

    "Unallowable characters in folder name." =>
    "NepovolenÃ© znaky v nÃ¡zvu adresÃ¡Åe.",

    "Folder name shouldn't begins with '.'" =>
    "JmÃ©no adresÃ¡Åe nesmÃ­ zaÄÃ­nat znakem '.'",

    "Please enter new file name." =>
    "VloÅ¾te prosÃ­m novÃ© jmÃ©no souboru.",

    "Unallowable characters in file name." =>
    "NepovolenÃ© znaky v nÃ¡zvu souboru.",

    "File name shouldn't begins with '.'" =>
    "NÃ¡zev soubor nesmÃ­ zaÄÃ­nat znakem '.'",

    "Are you sure you want to delete this file?" =>
    "Jste si jistÃ½ Å¾e chcete smazat tento soubor?",

    "Are you sure you want to delete this folder and all its content?" =>
    "Jste si jistÃ½ Å¾e chcete smazat tento adresÃ¡Å a celÃ½ jeho obsah?",

    "Inexistant or inaccessible folder." =>
    "NeexistujÃ­cÃ­ nebo nepÅÃ­stupnÃ½ adresÃ¡Å.",

    "Undefined MIME types." =>
    "NedefinovanÃ½ MIME typ souboru.",

    "Fileinfo PECL extension is missing." =>
    "RozÅÃ­ÅenÃ­ PECL pro zjiÅ¡tÄnÃ­ informacÃ­ o souboru chybÃ­.",

    "Opening fileinfo database failed." =>
    "NaÄtenÃ­ informacÃ­ o souboru selhalo.",

    "You can't upload such files." =>
    "Tyto soubory nemÅ¯Å¾ete nahrÃ¡t na server.",

    "The file '{file}' does not exist." =>
    "Tento soubor '{file}' neexistuje.",

    "Cannot read '{file}'." =>
    "Nelze naÄÃ­st '{file}'.",

    "Cannot copy '{file}'." =>
    "Nelze kopÃ­rovat '{file}'.",

    "Cannot move '{file}'." =>
    "Nelze pÅesunout '{file}'.",

    "Cannot delete '{file}'." =>
    "Nelze smazat '{file}'.",

    "Click to remove from the Clipboard" =>
    "KliknÄte pro odstranÄnÃ­ ze schrÃ¡nky",

    "This file is already added to the Clipboard." =>
    "Tento soubor je jiÅ¾ ve schrÃ¡nce vloÅ¾en.",

    "Copy files here" =>
    "KopÃ­rovat soubory na toto mÃ­sto",

    "Move files here" =>
    "PÅesunout soubory na toto mÃ­sto",

    "Delete files" =>
    "Smazat soubory",

    "Clear the Clipboard" =>
    "VyÄistit schrÃ¡nku",

    "Are you sure you want to delete all files in the Clipboard?" =>
    "Jste si jistÃ½ Å¾e chcete vymazat vÅ¡echny soubory ze schrÃ¡nky?",

    "Copy {count} files" =>
    "KopÃ­rovat {count} souborÅ¯",

    "Move {count} files" =>
    "PÅesunout {count} souborÅ¯",

    "Add to Clipboard" =>
    "VloÅ¾it do schrÃ¡nky",

    "New folder name:" => "NovÃ½ nÃ¡zev adresÃ¡Åe:",
    "New file name:" => "NovÃ½ nÃ¡zev souboru:",

    "Upload" => "NahrÃ¡t",
    "Refresh" => "Obnovit",
    "Settings" => "NastavenÃ­",
    "Maximize" => "Maxializovat",
    "About" => "O aplikaci",
    "files" => "soubory",
    "View:" => "Zobrazit:",
    "Show:" => "UkÃ¡zat:",
    "Order by:" => "Åadit podle:",
    "Thumbnails" => "NÃ¡hledy",
    "List" => "Seznam",
    "Name" => "JmÃ©no",
    "Size" => "Velikost",
    "Date" => "Datum",
    "Descending" => "SestupnÄ",
    "Uploading file..." => "NahrÃ¡vÃ¡nÃ­ souboru...",
    "Loading image..." => "NaÄÃ­tÃ¡nÃ­ obrÃ¡zku...",
    "Loading folders..." => "NaÄÃ­tÃ¡nÃ­ adresÃ¡ÅÅ¯...",
    "Loading files..." => "NaÄÃ­tÃ¡nÃ­ souborÅ¯...",
    "New Subfolder..." => "NovÃ½ adresÃ¡Å...",
    "Rename..." => "PÅejmenovat...",
    "Delete" => "Smazat",
    "OK" => "OK",
    "Cancel" => "ZruÅ¡it",
    "Select" => "Vybrat",
    "Select Thumbnail" => "Vybrat nÃ¡hled",
    "View" => "Zobrazit",
    "Download" => "StaÅ¾enÃ­",
    'Clipboard' => "SchrÃ¡nka",

    // VERSION 2 NEW LABELS

    "Cannot rename the folder." =>
    "AdresÃ¡Å nelze pÅejmenovat.",

    "Non-existing directory type." =>
    "NeexistujÃ­cÃ­ typ adresÃ¡Åe.",

    "Cannot delete the folder." =>
    "AdresÃ¡Å nelze smazat.",

    "The files in the Clipboard are not readable." =>
    "Soubory ve schrÃ¡nce nelze naÄÃ­st.",

    "{count} files in the Clipboard are not readable. Do you want to copy the rest?" =>
    "{count} souborÅ¯ ve schrÃ¡nce nelze naÄÃ­st. Chcete zkopÃ­rovat zbylÃ© soubory?",

    "The files in the Clipboard are not movable." =>
    "Soubory ve schrÃ¡nce nelze pÅesunout.",

    "{count} files in the Clipboard are not movable. Do you want to move the rest?" =>
    "{count} souborÅ¯ ve schrÃ¡nce nelze pÅesunout. Chcete pÅesunout zbylÃ© soubory?",

    "The files in the Clipboard are not removable." =>
    "Soubory ve schrÃ¡nce nelze smazat.",

    "{count} files in the Clipboard are not removable. Do you want to delete the rest?" =>
    "{count} souborÅ¯ ve schrÃ¡nce nelze smazat. Chcete smazat zbylÃ© soubory?",

    "The selected files are not removable." =>
    "OznaÄenÃ© soubory nelze smazat.",

    "{count} selected files are not removable. Do you want to delete the rest?" =>
    "{count} oznaÄenÃ½ch souborÅ¯ nelze smazat. Chcete smazat zbylÃ© soubory?",

    "Are you sure you want to delete all selected files?" =>
    "Jste si jistÃ½ Å¾e chcete smazat vybranÃ© soubory?",

    "Failed to delete {count} files/folders." =>
    "Nebylo smazÃ¡no {count} souborÅ¯/adresÃ¡ÅÅ¯.",

    "A file or folder with that name already exists." =>
    "Soubor nebo adresÃ¡Å s takovÃ½m jmÃ©nem jiÅ¾ existuje.",

    "selected files" => "vybranÃ© soubory",
    "Type" => "Typ",
    "Select Thumbnails" => "Vybrat nÃ¡hled",
    "Download files" => "StÃ¡hnout soubory",

    // SINCE 2.4

    "Checking for new version..." => "Zkontrolovat novou verzi...",
    "Unable to connect!" => "Nelze pÅipojit!",
    "Download version {version} now!" => "StÃ¡hnout verzi {version} nynÃ­!",
    "KCFinder is up to date!" => "KCFinder je aktuÃ¡lnÃ­!",
    "Licenses:" => "Licence:",
    "Attention" => "UpozornÄnÃ­",
    "Question" => "OtÃ¡zka",
    "Yes" => "Ano",
    "No" => "Ne",
);

?>