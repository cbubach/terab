<?php
session_start();
@mysql_connect("127.0.0.1", "root", "mysqlpass") or die("Could not connect: " . mysql_error());
mysql_select_db("terab") or die("Could not connect: " . mysql_error());
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
date_default_timezone_set("Europe/Stockholm");
include("cmd.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB bokningar</title>
    <script type="text/javascript" src="JS/jquery.min.js"></script>
    <script type="text/javascript" src="JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery.populate.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="admin/billingJs.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!-- <script type="text/javascript" src="JS/DYMO.Label.Framework.latest.js" charset="UTF-8"></script> -->
    <script type="text/javascript" src="jquery.gomap.min.js"></script>
    <link rel="stylesheet" href="jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="SexyButtons/sexybuttons.css" type="text/css" />

</head>
<body style="overflow-x:hidden">

<?php

    include("dialogs.php");

    if (!isset($_GET['tmstp'])) {
        if (date('w') == 1) { // w = numerical day of the week
            $str = 'today';
        } else {
            $str = 'last monday';
        }
        $timestamp = strtotime($str);
    } else {
        if (date('N', $_GET['tmstp']) != 1) {    // check for  monday-timestamp first
            $timestamp = strtotime("last monday", $_GET['tmstp']);
        } else {
            $timestamp = $_GET['tmstp'];
        }
    }

    // move bookings ahead to todays date if not marked as done
    if (isset($_GET['moveForward'])) {
        $tmptime = strtotime('today');
        mysql_query("UPDATE event SET date='{$tmptime}' WHERE date<$tmptime AND date!=0 AND locked='0'") or die(mysql_error());
    }

    print '<div style="text-align: center; width:98%;  border:5px solid #eee; padding:5px;">';
    print '<a href="?tmstp='.strtotime('-7 days', $timestamp).'" style="font-size:large;"><img src="img/prev.png" style="vertical-align:top;" alt="Föregående" /> <span style="vertical-align:top;">Föregående veckor</span></a>';
    print '<img src="img/terab.png" class="margin50" alt="" />';
    print '<a href="?tmstp='.strtotime('+7 days', $timestamp).'" style="font-size:large;"><span style="vertical-align:top;">Nästkommande veckor</span> <img src="img/next.png" style="vertical-align:top;" alt="Nästa" /></a>';
    print '<br /><a href="javascript:void(0);" onclick="changeWeek();">Ändra vecka</a> | <a href="/TERAB/">Tillbaka till administrationen</a>';
    print "</div>\n\n";

    print '<div id="drag">'."\n\n";

    $activeDay = 0;
    for ($j = 0; $j < 14; $j++) {
        if ($j < 5 || ($j > 6 && $j < 12)) {
            $activeDay = strtotime('+'.$j.' days', $timestamp);
        }
        if ($activeDay == strtotime('today') || $activeDay > strtotime('today')) {
            break;
        }
    }

    $weekData       = "";
    $currState      = "DELIVER_NODATE";
    $eventType      = 0;
    
    for ($i = 0; $i < 14; $i++) {
        // first loop for each table, print header
        if ($currState == "DELIVER_NODATE" || $currState == "PICKUP_NODATE") {
            // alternate new booking button between 2 tables
            if ($currState == "DELIVER_NODATE") {
                print '<br /><div style="text-align: center; width:100%;"><a class="sexybutton sexysimple sexyxxl" style="background: #ffa; color: #000 !important;" href="javascript:void(0);" onclick="addEvent(0, '.$activeDay.');"><img src="img/add.png" alt="Ny" title="Ny order" /> Bokning leverera</a></div>'."\n";
            } else {
                print '<div style="text-align: center; width:100%;"><br /><a class="sexybutton sexysimple sexyxxl" style="background: #faa; color: #000 !important;"  href="javascript:void(0);" onclick="addEvent(1, '.$activeDay.');"><img src="img/add.png" alt="Ny" title="Ny order" /> Bokning hämta</a></div>'."\n";
            }
            print '<table class="calendarView">'."\n";
            print '    <tr>'."\n";
            print '        <td></td>'."\n";
            print '        <td class="mark" style="background-color: #eee; color: #000; height:25px"><h3>Vecka '.date('W', $timestamp)."</h3></td>\n";
            print '        <td></td><td></td><td></td><td></td>'."\n";
            print '        <td class="mark" style="background-color: #eee; color: #000; height:25px"><h3>Vecka '.date('W', strtotime('+7 days', $timestamp))."</h3></td>\n";
            print '        <td></td><td></td><td></td><td></td>'."\n";
            print "    </tr>\n    <tr>\n";
        }
        
        if ($i < 5 || ($i > 6 && $i < 12) ) {
            $time = strtotime('+'.$i.' days', $timestamp);
            if ($currState == "DELIVER_NODATE" || $currState == "PICKUP_NODATE") {
                $time = 0;  // Get bookings for no date specified.
            }

            if ($time == strtotime('today')) {
                print '        <td class="mark" style="height:25px; width:140px; background: #ffa;>'."\n";
            } else {
                if (in_array($currState, array("DELIVER_NODATE", "PICKUP_NODATE"))) {
                    print '        <td class="mark" style="background-color: #eee; color: #000; height:25px"><h3>Inget datum valt</h3>'."\n";
                } else {
                    print '        <td class="mark" style="height:25px; width:140px;">'."\n";
                }
            }
            
            if (!in_array($currState, array("DELIVER_NODATE", "PICKUP_NODATE"))) {
                print '            <div style="width: 100px;"><a href="javascript:void(0);" onclick="addEvent('.$eventType.', '.$time.');" style="display:block; height:100%; width: 100%; color: #000;">';
                print '               <img src="img/add.png" alt="Ny" title="Ny order" /> '.veckodag(date("N", $time)).' '.date("j/n", $time).'</a>';
                print '            </div>'."\n";
            }
            
            print "        </td>\n";
            $weekData .= "        <td class=\"empty vtop\">\n\n";

            // daily notes & move old bookings forward
            if (isset($_SESSION['admin']) && $currState == "DELIVER") {
                $weekData .= '<div style="width:100%; text-align:center;">';
                $weekData .= '    <a href="javascript:void(0);" onclick="editNotes('.$time.');"><img src="img/page_edit.png" alt="" /> Redigera  dags-noteringar</a>';
                $weekData .= '</div>'."\n";
                
                $weekData .= '<div style="width:100%; background-color: #fff; padding: 5px;">';
                $weekData .=  @mysql_result(mysql_query("SELECT note FROM notes WHERE date='{$time}'"), 0) or die("0");
                $weekData .= '</div>'."\n";
                
                if ($time == strtotime('today')) {
                    $weekData .= '<div style="width:100%; text-align:center;"><a href="?moveForward=">Flytta fram äldre bokningar</a></div>'."\n";
                }
            }

            $weekData .= '            <ul class="droplist'.$eventType.'" id="DL'.$eventType.'_'.$time.'">'."\n\n";

            $result = mysql_query("SELECT * FROM event WHERE date='$time' AND type='".$eventType."'") or die(mysql_error());
            if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $weekData .= '                <li id="drop'.$eventType.'_'.$row['id'].'" class="ui-state-highlight';
                    if ($row['locked'] == 1) {
                        $weekData .= " ui-state-disabled";
                    }
                  
                    if (in_array($currState, array("DELIVER_NODATE", "DELIVER"))) {
                        $weekData .= "\">\n";
                    } else {
                        $weekData .= '" style="border-color: #f00; background: #faa;">'."\n";
                    }

                    $weekData .= '                    <div style="float:right;">'."\n";
                    $weekData .= '                        <a href="javascript:void(0);" onclick="showEvent('.$row['id'].');">';

                    $r1 = mysql_query("SELECT id FROM waybill WHERE name='feventID' AND value='{$row['id']}'");
                    if (mysql_num_rows($r1) > 0) {
                        if (in_array($currState, array("DELIVER_NODATE", "DELIVER"))) {
                            $wbID = mysql_result($r1, 0);
                            $r2   = mysql_query("SELECT * FROM waybill WHERE id='{$wbID}' AND name='noPrint' AND value='1'");
                            if (mysql_num_rows($r2) > 0) {
                                $weekData .= '<img src="img/page_go.png" alt="Visa" title="Visa" /></a><br />'."\n";
                            } else {
                                $weekData .= '<img src="img/check.png" alt="Visa" title="Visa" /></a><br />'."\n";
                            }
                        } else {
                            $weekData .= '<img src="img/check.png" alt="Visa" title="Visa" /></a><br />'."\n";
                        }
                    } else {
                        $weekData .= '<img src="img/page.png" alt="Visa" title="Visa" /></a><br />'."\n";
                    }

                    if (!empty($row['cust_info'])) {
                        $weekData .= '                        <img src="img/error.png" alt="" /><br />'."\n";
                    }

                    $weekData .= '                        <a href="javascript:void(0);" onclick="toggleLoading('.$row['id'].');"><img src="img/car.png"></a>'."\n";
                    $weekData .= '                    </div>'."\n";
                    $weekData .= '                    <div>'.mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['cust_id']}'"), 0);
                    $weekData .= ' <small style="font-size: 0.6em">('.mysql_result(mysql_query("SELECT del_city FROM cust WHERE id='{$row['cust_id']}'"), 0).")</small></div>\n";
                  
                    if ($row['loading'] == 1) {
                        $weekData .= '<small>BILEN</small>'."\n";
                    } elseif ($row['loading'] == 2) {
                        $weekData .= '<small>SLÄPET</small>'."\n";
                    } elseif ($row['loading'] == 3) {
                        $weekData .= '<small>TRAILER</small>'."\n";
                    } elseif ($row['loading'] == 4) {
                        $weekData .= '<small>HÄMTAS</small>'."\n";
                    } elseif ($row['loading'] == 5) {
                        $weekData .= '<small>BIL&SLÄP</small>'."\n";
                    } elseif ($row['loading'] == 6) {
                        $weekData .= '<small>LÄTT LASTBIL</small>'."\n";
                    }

                    $weekData .= '                    <div style="clear:both;"></div>'."\n";
                    $weekData .= "                </li>\n\n";
                }
            }
            $weekData .= "            </ul>\n\n";
            $weekData .= "          <script type=\"text/javascript\">refreshOrders(".$time.", ".$eventType.");</script>\n";
            $weekData .= "        </td>\n";
        }
        
        // switch state, modify loop count and possibly print buffer for next iteration
        if ($currState == "DELIVER_NODATE") {
            $currState = "DELIVER";
            $i--;
        } elseif ($currState == "PICKUP_NODATE") {
            $currState = "PICKUP";
            $i--;
        } elseif ($i > 12) {
            print "    </tr>\n\n    <tr>\n        \n".$weekData."    </tr>\n";
            print "</table>\n\n\n";
            
            $weekData = "";
            
            if ($currState == "DELIVER") {
                $currState = "PICKUP_NODATE";
                $eventType = 1;
                $i = -1;
            }
        }
    }
  
    if (isset($_GET['showEvent']) && !empty($_GET['showEvent'])) {
        print "<script type=\"text/javascript\">showEvent(".$_GET['showEvent'].");</script>\n";
    }

?>

</div><!-- End drag -->

</body>
</html>