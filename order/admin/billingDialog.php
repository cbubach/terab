<!-- add new billing, popup dialog -->
<?php
require_once("db.php");

$cmd         = "addBill";
$billId      = 0;  // not used on adding only edit
$eventType   = isset($_GET['billEventType']) ? $_GET['billEventType'] : 0;
$waybill_no  = isset($_GET['waybill_id']) ? intval($_GET['waybill_id']) : 0;
$ref         = isset($_GET['ref']) ? $_GET['ref'] : "";
$date        = isset($_GET['billDate']) ? $_GET['billDate'] : date("Y-m-d");

$settingsRes = mysql_query("SELECT * FROM bill_settings WHERE id='0'") or die(mysql_error());
$billSetting = mysql_fetch_array($settingsRes);  
$currency    = "SEK";
$iban        = $billSetting['iban'];
$due         = $billSetting['due'];
$msg         = $billSetting['message'];

$bill_number = intval(mysql_result(mysql_query("SELECT `bill_number` + 1 FROM `bill_settings` WHERE `id`=0"), 0)); 

if (isset($_GET['create_bill']))
{
    if (isset($_GET['waybill_id']) && ($_GET['waybill_id'] != 0))
    {
        $eventId     = mysql_result(mysql_query("SELECT `value` FROM `waybill` WHERE `id`='".$waybill_no."' AND `name` = 'feventID'"), 0);
        $custId      = mysql_result(mysql_query("SELECT `cust_id` FROM `event` WHERE `id`=".$eventId), 0);
        $custName    = mysql_result(mysql_query("SELECT `name` FROM `cust` WHERE `id`=".$custId), 0);

        $n = 1;
        $x = 'go';
        $artPrice = $fArtName = $fArtID = $fArtType = $fArtAmount = array();
        while($x == 'go')
        {
            //artName
            $query = "SELECT `value` FROM `waybill` WHERE `id` = '".$waybill_no."' AND `name` = 'fArtName".$n."'";
            if (!$artNameNew = @mysql_result(mysql_query($query), 0)) {
                $x = 'stop';
            }

            else
                //if that fArtName exist then get all the other data related to that $n
            {
                $fArtID_query     = "SELECT `value` FROM `waybill` WHERE `id` = '".$waybill_no."' AND `name` = 'fArtID".$n."'";
                $fArtType_query   = "SELECT `value` FROM `waybill` WHERE `id` = '".$waybill_no."' AND `name` = 'fArtType".$n."'";

                $idQuery = mysql_query($fArtID_query);
                $id = empty($idQuery) ? 0 : @mysql_result($idQuery, 0);

                $fArtAmount_query = "SELECT `art_amount` FROM `event_art` WHERE `event_id` = '".$eventId."' AND `art_id` = '".$id."'";

                array_push($fArtName, $artNameNew);
                array_push($fArtID, $id);

                if (!empty($id)) {
                    $price = @mysql_result(mysql_query("SELECT `price` FROM `price` WHERE `cust_id`=".$custId." AND `art_id`=".$id),0);
                    if (empty($price)) {
                        $price = mysql_result(mysql_query("SELECT `price` FROM `price` WHERE `cust_id`=0 AND `art_id`=".$id),0);
                    }
                } else {
                    $price = '';
                }

                array_push($fArtType, mysql_result(mysql_query($fArtType_query), 0));
                array_push($fArtAmount, mysql_result(mysql_query($fArtAmount_query), 0));
                array_push($artPrice, $price);
                $n++;
            }
        }
    }
} else if (isset($_GET['edit_bill_id'])) {
    $cmd         = "editBill";
    $billId      = $_GET['edit_bill_id'];
    $result      = mysql_query("SELECT * FROM bill WHERE id='".$billId."'") or die(mysql_error());
    $bill        = mysql_fetch_array($result);
    $bill_number = $bill['order_id'];
    $ref         = $bill['ref_no']; 
    $date        = $bill['date'];
    $waybill_no  = $bill['waybill_id'];
    $custName    = @mysql_result(mysql_query('SELECT name FROM cust WHERE id="'.$bill['cust_id'].'"'), 0);
    $custId      = $bill['cust_id'];
    $settingsRes = mysql_query("SELECT * FROM bill_settings WHERE id='".$bill['settings_id']."'") or die(mysql_error());
    $billSetting = mysql_fetch_array($settingsRes);  
    $currency    = "";
    $iban        = $billSetting['iban'];
    $due         = $billSetting['due'];
    $msg         = $billSetting['message'];
    $artPrice    = $fArtName = $fArtID = $fArtType = $fArtAmount = $artCurrency = array();
    $eventType   = 0;

    $billItems = mysql_query("SELECT * FROM bill_item WHERE bill_id='".$billId."'") or die(mysql_error());

    if (mysql_num_rows($billItems) > 0) {
        while ($billItem = mysql_fetch_array($billItems)) {
            array_push($fArtName, $billItem['name']);
            array_push($fArtID, $billItem['item_id']);
            array_push($fArtType, $billItem['unit']);
            array_push($fArtAmount, $billItem['amount']);
            array_push($artPrice, $billItem['price']);
            array_push($artCurrency, $billItem['currency']);

            $currency  = $billItem['currency'];
            $eventType = intval(mysql_result(mysql_query('SELECT type FROM art WHERE id="'.$billItem['item_id'].'"'), 0));
        }
    }
}
?>

<!--
<script type="text/javascript" src="billingJs.js"></script>
<div id="addBill" title="Skapa ny faktura" style="display: none;">
-->
    <form method="get" action="/TERAB/order/admin/billingCmd.php" onsubmit="postFormAJAXbill(this); return false;">
        <div>
            <input type="hidden" name="cmd" value="<?php echo $cmd; ?>" />
            <input type="hidden" name="billId" value="<?php echo $billId; ?>" />
            <input type="hidden" name="newestBillId" value="<?php echo $bill_number; ?>" />
            <input type="hidden" id="billEventType" value="<?php echo $eventType; ?>" />

            <table style="text-align: center;">
                <tr>
                    <td>
                        <?php if (!isset($_GET['deliveryNote'])) { ?>
                        <label for="bill_number">
                            <span style="float:left; width:45%;">Fakturanummer</span><br>
                            <script type="text/javascript">
                            <?php
                                $result = mysql_query("SELECT order_id FROM `bill`");
                                if (mysql_num_rows($result) > 0) {
                                    echo "var allBillingNumbers = ".json_encode(array_values(mysql_fetch_row($result))).";";
                                }
                            ?>
                            </script>
                            <input style="width:45%;" name="bill_number" id="bill_number" value="<?php print $bill_number; ?>" type="text">
                        </label>
                        <?php } ?>
                    </td>
                    <td>
                        <label for="reference">
                            <span style="float:left; width:45%;">Referens/orderId</span><br>
                            <input style="width:45%;" name="reference" id="reference" value="<?php print $ref; ?>" type="text">
                        </label>
                    </td>
                    <td>
                        <label for="billDate">
                            <span style="float:left; width:45%;">Faktura/orderdatum:</span><br>
                            <input type="text" value="<?php print $date; ?>" name="billDate" id="billDate" style="width:150px">
                        </label>
                    </td>
                    <td align="right">
                        <input name="creditBill" value="0" type="hidden">
                        <?php if (isset($bill['status'])) { ?>
                            <?php $checked = ($bill['status']==2) ? "checked" : "" ?>
                            <label for="creditBillCheck" style="width:200px; float:right;">
                                <span style="width:75%;">Kryssa i för kreditfaktura: </span><br>
                                <input style="width:25%;" name="creditBill" id="creditBillCheck" value="1" type="checkbox" <?php echo $checked; ?>>
                            </label>
                        <?php } else { ?>
                            <?php if (!isset($_GET['deliveryNote'])) { ?>
                                <label for="" style="width:200px; float:right;">
                                    <span style="float:right; width:75%;">Kryssa i för kreditfaktura: </span><br>
                                    <input style="width:25%;" name="creditBill" value="1" type="checkbox">
                                </label>
                            <?php } ?>
                        <?php } ?>
                    </td>
                </tr>
            </table><br>

            <table id="billRowTable" style="width:100%; padding-top:0px;">
                <tr>
                    <td class="white" colspan="3">
                    Företag:
                    <?php
                        $custName = isset($custName) ? $custName : '';
                        $custId   = isset($custId) ? $custId : 0;
                        print "<input type=\"text\" id=\"billCustName\" value=\"".$custName."\" />";
                        print "<input type=\"hidden\" id=\"billCustId\" name=\"billCustId\" value=\"".$custId."\" />";
                    ?>
                    </td>
                    <td class="white" colspan="3"></td>
                </tr>
                <tr>
                    <td class="white">Antal: </td>
                    <td class="white">Enhet: </td>
                    <td class="white">Namn: </td>
                    <td class="white">Moms%: </td>
                    <td class="white">Pris: </td>
                    <td class="white"><input type="hidden" id="billEventId" name="billEventId" value="0" /></td>
                </tr>
                <?php
                if ((isset($_GET['create_bill']) && isset($_GET['waybill_id'])) || isset($_GET['edit_bill_id']))
                {
                    for ($i = 0; $i < count($fArtName); $i++) {
                        print "<tr class=\"orderRow\">";
                        print "<td class=\"white\"> <input type=\"text\" style=\"width:40px; margin-right:1px;\" name=\"artAmount[]\" value=\"".$fArtAmount[$i]."\" /></td>";
                        print "<td class=\"white\"> <input type=\"text\" style=\"width:40px; margin-right:1px;\" name=\"artUnit[]\" value=\"".$fArtType[$i]."\" /></td>";
                        print "<td class=\"white\"> <input type=\"text\" class=\"billArtComplete\" style=\"width:200px;\" id=\"billArtName".$i."\" name=\"artName[]\" value=\"".$fArtName[$i]."\" /></td>";
                        print "<td class=\"white\"> <input type=\"text\" style=\"width:40px; margin-right:1px;\" name=\"artVAT[]\" value=\"25\" /></td>";
                        print "<td class=\"white\"> <input type=\"text\" style=\"width:80px; margin-right:1px;\" id=\"billArtPrice".$i."\" name=\"artPrice[]\" value=\"".$artPrice[$i]."\" /></td>";
                        print "<td><input type=\"hidden\" id=\"billArtId".$i."\" name=\"art_ID[]\" value=\"".$fArtID[$i]."\" />";
                        print "<span onclick=\"moveBillingRowDown(this);\"><img src=\"/TERAB/order/img/down.png\"></span></td>";
                        print "</tr>";
                    }
                }
                else { ?>
                    <tr class="orderRow">
                        <td class="white"> <input type="text" style="width:40px; margin-right:1px;" name="artAmount[]" value="" /></td>
                        <td class="white"> <input type="text" style="width:40px; margin-right:1px;" name="artUnit[]" value="st" /></td>
                        <td class="white"> <input type="text" class="billArtComplete" id="billArtName0" style="width:200px;" name="artName[]" value="" /></td>
                        <td class="white"> <input type="text" style="width:40px; margin-right:1px;" name="artVAT[]" value="25" /></td>
                        <td class="white"> <input type="text" style="width:80px; margin-right:1px;" id="billArtPrice0" name="artPrice[]" value="" /></td>
                        <td class="white"><input type="hidden" id="billArtId0" name="art_ID[]" value="" /><span onclick="moveBillingRowDown(this);"><img src="/TERAB/order/img/down.png"></span></td>
                    </tr>
                <?php
                }
                $waybill_no = isset($waybill_no) ? $waybill_no : 0;
                print "<input type=\"hidden\" name=\"waybill_no\" value=\"".$waybill_no."\">";
                ?>
            </table>
            <a href="javascript:void(0);" onclick="addBillArtForm($('#billEventType').val());" style="margin-left: 20px;"><img src="/TERAB/order/img/add.png" class="vmiddle" alt="Fler artiklar" title="Fler artiklar"> Fler artiklar</a>
            <br />
            <br />
            <table width="100%">
                <tr>
                    <td valign="top">
					    Valuta<br />
						<input type="text" name="currency" style="width: 40px;" value="<?php echo $currency; ?>">
					</td>
					<td valign="top">
					    IBAN<br />
						<input type="text" name="iban" value="<?php echo $iban; ?>">
					</td>
                    <td valign="top">
					    Betalningsvillkor<br />
						<input type="text" style="width: 40px;" name="due" value="<?php echo (empty($due) ? "" : $due); ?>">
					</td>
                    <td valign="top">
					    Fritext meddelande <br />
						<textarea name="msg" rows="4" cols="30"><?php echo $msg; ?></textarea>
					</td>
                </tr>
            </table>
            <button name="save" type="submit" style="margin-left: 300px; height: 50px; width:150px"><img src="../img/page_save.png" alt=""> Spara fakturan</button>
        </div>
    </form>
<!--
</div>
-->