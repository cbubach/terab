//-------------------------------
//  TERAB booking, jQuery code
//   by Christoffer Bubach 2011
//-------------------------------
      function addArticleListner() {
              $( ".artComplete0" ).autocomplete({
              source: "../index.php?auto=3",
              minLength: 1,
              select: function( event, ui ) {
                var id = $(this).attr('id');
                var tmparr = id.split("");
                tmpaarr = tmparr.splice(0,7);     //changed to fit ID/name
                id = tmparr.join("");
                var name = 'art_ID' + id;
                document.getElementById(name).value = ui.item.id;
                this.value = ui.item.id;
                var custId = $("#cust_id").val();
                $.get("cmd.php?cmd=getPrice&artNo="+ui.item.id+'&custId='+custId, function(data){
                  $("#artPrice"+id).val(data);
                });
              }
          });
      }
  $(document).ready(function() {


      addArticleListner();

      // init auto-complete
      //----------------------
      $( "#custName0" ).autocomplete({
        source: "../index.php?auto=1",
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('custID').value = ui.item.id;
        }
      });
        
      $( "#custName1" ).autocomplete({
        source: "../index.php?auto=2",
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('cust_id').value = ui.item.id;
        }
      });

      $( "#artName0" ).autocomplete({
        source: "../index.php?auto=3",
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('artID0').value = ui.item.id;
        }
      });
      $( "#artName1" ).autocomplete({
        source: "../index.php?auto=4",
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('artID1').value = ui.item.id;
        }
      });
	 

      // Init listnav
      if ($("#myList").length > 0)
      {
        $('#myList').listnav({
          includeAll: false,
          includeOther: true,
          flagDisabled: false,
          noMatchText: 'Ingen hittades.',
          showCounts: false
        });
      }
      if ($("#myList2").length > 0)
      {
        $('#myList2').listnav({
          includeAll: false,
          includeOther: true,
          flagDisabled: false,
          noMatchText: 'Ingen hittades.',
          showCounts: false
        });
      }


      // init datepicker
      //------------------
      $( "#date1" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      $( "#date_1" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      $( "#date2" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      $( "#date_2" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });

      // init tabs
      //-----------------
      $( "#tabs" ).tabs();
      $( "#custTabs" ).tabs();
      $( "#custTabsE" ).tabs();

      // init paging
      //-----------------
      /* $("ul.paging").quickPager({pagerLocation:"both", pageSize:"16"}); */ // 16 so 15 + empty LI


      // handle product categories
      //---------------------------
      $(".prodcat_list").focusout(function(){
          if(this.value == 0){
              var tmparr = $(this).attr('id').split("");
              tmpaarr = tmparr.splice(0,8);
              id = tmparr.join("");
              $('#newProdCatName_'+id).show();
              $(this).hide();
          }
      });
      $('.prodcat_list').change(function() {
          var tmparr = $(this).attr('id').split("");
          tmpaarr = tmparr.splice(0,8);
          id = tmparr.join("");
          var cat = $(this).val();
          if (cat != "0")
          {
              $.get('products.php?AJAX-cmd=updateProdCat&prodID='+id+'&catName='+cat, function (data) { });
          }
      });
      $('.newProdCatName').change(function() {
          var tmparr = $(this).attr('id').split("");
          tmpaarr = tmparr.splice(0,15);
          id = tmparr.join("");
          var cat = $(this).val();
          $.get('products.php?AJAX-cmd=updateProdCat&prodID='+id+'&catName='+cat, function (data) {
              $('#newProdCatName_'+id).hide();
              $('#newProdCatName_'+id).val("Ange nytt namn här");
              $('#prodcat_'+id+' option:selected').removeAttr("selected");
              $('.prodcat_list').append('<option value="'+cat+'">'+cat+'</option>');
              $('#prodcat_'+id+' option[value='+cat+']').attr('selected', 'selected');
              $('#prodcat_'+id).show();
          });
      });
      $('.newProdCatName').focusout(function() {
          var tmparr = $(this).attr('id').split("");
          tmpaarr = tmparr.splice(0,15);
          id = tmparr.join("");
          if(this.value == "Ange nytt namn här"){
              $('#newProdCatName_'+id).hide();
              $('#newProdCatName_'+id).val("Ange nytt namn här");
              $('#prodcat_'+id).show();
          }
      });

      $( function() {
        var characterlimit = 30;
        $("#f_delinfo1,#f_delinfo2,#f_delinfo1E,#f_delinfo2E")
          .after( '<span style="padding-left: 10px;font-weight:bold;"></span>' ).next().hide().end()
          .keypress( function( e ){
            var current = $( this ).val().length;
            $( this ).next().show().text( characterlimit - current );
          }
        );
      });



  });  // end of JS code that needs to run onload :)

  function editCust(id)
  {
      $.get('cmd.php?cmd=getCustData&cust_id='+id, function (data) {
        document.getElementById('custIDE').value = data[0].id;
        document.getElementById('nameE').value = data[0].name;
        document.getElementById('contactE').value = data[0].contact;
        document.getElementById('del_addressE').value = data[0].del_address;
        document.getElementById('phoneE').value = data[0].phone;
        document.getElementById('del_zipE').value = data[0].del_zip;
        document.getElementById('faxE').value = data[0].fax;
        document.getElementById('del_cityE').value = data[0].del_city;
        document.getElementById('contact_emailE').value = data[0].contact_email;
        document.getElementById('bill_nameE').value = data[0].bill_name;
        document.getElementById('mobile_phoneE').value = data[0].mobile_phone;
        document.getElementById('bill_addressE').value = data[0].bill_address;
        document.getElementById('vat_reg_noE').value = data[0].vat_reg_no;
        document.getElementById('bill_zipE').value = data[0].bill_zip;
        document.getElementById('giro_noE').value = data[0].giro_no;
        document.getElementById('bill_boxE').value = data[0].bill_box;
        document.getElementById('bill_cityE').value = data[0].bill_city;
        document.getElementById('alt_nameE').value = data[0].alt_name;
        document.getElementById('del_countryE').value = data[0].del_country;
        document.getElementById('attE').value = data[0].att;
        document.getElementById('bill_countryE').value = data[0].bill_country;
        document.getElementById('contact_email1E').value = data[0].contact_email1;
        document.getElementById('contact2E').value = data[0].contact2;
        document.getElementById('mob2E').value = data[0].mob2;
        document.getElementById('contact_email2E').value = data[0].contact_email2;
        document.getElementById('schenker_noE').value = data[0].schenker_no;
        document.getElementById('dhl_noE').value = data[0].dhl_no;
        document.getElementById('other_noE').value = data[0].other_no;
        document.getElementById('f_delinfo1E').value = data[0].f_delinfo1;
        document.getElementById('f_delinfo2E').value = data[0].f_delinfo2;

        var custType = data[0].type;
        var custIDname = "custID";
        custType++;
        if (custType == 2)
        {
           custIDname = "cust_id";
           $("#edit_kundnamn").html("Leverantörsnamn:");
           $("#edit_address").html("Hämt-adress:");
           $("#edit_zip").html("Hämt-postnummer:");
           $("#edit_city").html("Hämt-stad:");
           $("#alt_kundnamnE").html("Alt. leverantörsnamn:");
        }
        else
        {
           $("#edit_kundnamn").html("Kundnamn:");
           $("#edit_address").html("Leverans-adress:");
           $("#edit_zip").html("Leverans-postnummer:");
           $("#edit_city").html("Leverans-stad:");
           $("#alt_kundnamnE").html("Alt. kundnamn:");
        }
        document.getElementById('custPriceEdit').innerHTML = '<a class="sexybutton" href="prices.php?custName'+data[0].type+'='+data[0].name+'&'+custIDname+'='+data[0].id+'&save=#tabs-'+custType+'"><span><span>Visa anpassade kundpriser</span></span></a>';
        if ( custType==2) {
            $("#editCust").dialog({ height: 480, width: 800, "title": "Ändra leverantörsuppgifter" });
        }
        else {
            $("#editCust").dialog({ height: 480, width: 800, "title": "Ändra kunduppgifter"  });
        }
      });
      $.get('cmd.php?cmd=getEvents&cust_id='+id, function (data) {
          $('#waybill_links').append(data);

         console.log(data);
      });
  }
  
  function openKCFinder(field_id, type, preview) {
      var type    = type    || "images";
      var preview = preview || false;
      window.KCFinder = {};
      window.KCFinder.callBack = function(url) {
          window.KCFinder = null;
          $("#"+field_id).val(url);
          if (preview) {
              $("#"+preview).attr("src", url);
          } else {
              $("#"+field_id+"_img").html('<img src="/order/img/check.png" alt="" />');
          }
      };
      window.open('./../kcfinder/browse.php?type='+type+'&lang=sv', 'kcfinder_single', "menubar=0,resizable=1,width=650,height=500");
  }
  
  function editArt(id)
  {
      $.get('cmd.php?cmd=getArtData&id='+id, function (data) {
        document.getElementById('artIDE').value = data[0].id;
        document.getElementById('art_noE').value = data[0].art_no;
        document.getElementById('art_nameE').value = data[0].art_name;
        document.getElementById('weightE').value = data[0].weight;
        document.getElementById('priceE').value = data[0].price;
        document.getElementById('art_imgE').value = data[0].img;
        if (data[0].img == "")
            $("#art_imgE_img").html('<img src="/order/img/error.png" alt="" />');
        else
            $("#art_imgE_img").html('<img src="/order/img/check.png" alt="" />');
      });
      $( "#editArt" ).dialog({ height: 450, width: 350 });
  }
  
  function del_img_val(field_id)
  {
      $("#"+field_id).val("");
      $("#"+field_id+"_img").html('<img src="/order/img/error.png" alt="" />');
  }

  function addArtPrice(event_type)
  {
    var custID;
    if (event_type==0)
        custID = '<input type="hidden" name="custID" value="'+document.getElementById('custID').value+'" />';
    else
        custID = '<input type="hidden" name="custID" value="'+document.getElementById('cust_id').value+'" />';
    $('#artNameField').html('<input type="text" name="artName" id="artName'+event_type+'" value="" /><input type="hidden" name="artID" id="artID'+event_type+'" />'+custID);

    $( "#artName0" ).autocomplete({
        source: "../index.php?auto=3",
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('artID0').value = ui.item.id;
        }
    });
    $( "#artName1" ).autocomplete({
        source: "../index.php?auto=4",
        minLength: 1,
        select: function( event, ui ) {
          document.getElementById('artID1').value = ui.item.id;
        }
    });
    $( "#addArtPrice" ).dialog({ height: 250, width: 300 });
  }



  function addCust(event_type)
  {
      //document.getElementById('custType').value = event_type;
      //$( "#addCust" ).dialog({ height: 470, width: 650 });
      document.getElementById('custType').value = event_type;
      if (event_type==1)
      {
          //$("div#addCust").attr("title", "Skapa ny leverantör");
          $("#add_kundnamn").html("Leverantörsnamn:");
          $("#add_address").html("Hämt-adress:");
          $("#add_zip").html("Hämt-postnummer:");
          $("#add_city").html("Hämt-stad:");
          $("#alt_kundnamn").html("Alt. leverantörsnamn:");
          $("#addCust" ).dialog({ height: 480, width: 800, "title": "Skapa ny leverantör" });
      }
      else
      {
          //$("div#addCust").attr("title", "Skapa ny kund");
          $("#add_kundnamn").html("Kundnamn:");
          $("#add_address").html("Leverans-adress:");
          $("#add_zip").html("Leverans-postnummer:");
          $("#add_city").html("Leverans-stad:");
          $("#alt_kundnamn").html("Alt. kundnamn:");
          $("#addCust" ).dialog({ height: 480, width: 800, "title": "Skapa ny kund" });
      }
  }

  function addArt(event_type)
  {
      document.getElementById('artType').value = event_type;
      del_img_val("art_img");
      $( "#addArt" ).dialog({ height: 400, width: 350 });
  }

  function postFormAJAXnew (form) {
    var query = $(form).serialize();
    var url = form.action + '?' + query;
    $.get(url, function (data) {
      $('#editCust').dialog('close');
      $('#editArt').dialog('close');
      $('#addCust').dialog('close');
      $('#addBill').dialog('close');
	  window.location = "/terab/order/admin/faktura.php?billId=" + data;
    });
  }
  
  function postFormAJAX (form)
  {
    var query = $(form).serialize();
    var url = form.action + '?' + query;
    $.get(url, function (data) {
      $('#editCust').dialog('close');
      $('#editArt').dialog('close');
      $('#addCust').dialog('close');
      $('#addBill').dialog('close');
	  window.location.reload(true);
    });
  }
  
  function toPascalCase(str)
  {
    var arr = str.split(/\s|_/);
    for(var i=0,l=arr.length; i<l; i++) {
        arr[i] = arr[i].substr(0,1).toUpperCase() +  arr[i].substr(1);
                 //(arr[i].length > 1 ? arr[i].substr(1).toLowerCase() : "");
    }
    return arr.join(" ");
  }