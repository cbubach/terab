    <!--
         Show dialog for adding a new customer
      -->
    <div id="addCust" title="Skapa ny kund" style="display: none;">
	<form method="get" action="../index.php" onsubmit="postFormAJAX(this); return false;">
          <p>
            <input type="hidden" name="cmd" value="addCust" />
            <input type="hidden" id="custType" name="custType" value="0" />

            <div id="custTabs">
                <ul>
                    <li><a href="#Ctabs-1">Grundläggande uppgifter</a></li>
                    <li><a href="#Ctabs-2">Fakturauppgifter</a></li>
                    <li><a href="#Ctabs-3">Transportnummer</a></li>
                    <li><a href="#Ctabs-4">Leveransanvisning</a></li>
                </ul>
                <div id="Ctabs-1">
                    <table style="width:100%; padding-top:0px; margin:0px;">

                      <tr>
                        <td class="white">
        	          <div id="add_kundnamn">Kundnamn:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="name" value="" tabindex="1" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Telefonnummer:
                        </td>
                        <td class="white">
                          <input type="text" name="phone" value="" tabindex="8" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
        	            <div id="alt_kundnamn">Alt. kundnamn:</div>
                        </td>
                        <td class="white">
                            <input type="text" name="alt_name" value="" tabindex="2" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Kontaktperson:
                        </td>
                        <td class="white">
                          <input type="text" name="contact" value="" tabindex="9" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_address">Leverans-adress:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_address" value="" tabindex="3" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Mobiltelefon-nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="mobile_phone" value="" tabindex="10" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_zip">Leverans-postnummer:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_zip" value="" tabindex="4" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                        </td>
                        <td class="white">
                          Kontakt e-post:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email1" value="" tabindex="11" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_city">Leverans-stad:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_city" value="" tabindex="5" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Alt kontaktperson:
                        </td>
                        <td class="white">
                          <input type="text" name="contact2" value="" tabindex="12" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Leverans land:
                        </td>
                        <td class="white">
                          <input type="text" name="del_country" value="" tabindex="6" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Alt. mobiltelefon-nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="mob2" value="" tabindex="13" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fax:
                        </td>
                        <td class="white">
                          <input type="text" name="fax" value="" tabindex="7" />
                        </td>
                        <td class="white">
                          Alt. kontakt e-post:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email2" value="" tabindex="14" />
                        </td>
                      </tr>

                    </table>

                </div>
                <div id="Ctabs-2">

                    <table style="width:100%; padding-top:0px; margin:0px;">

                      <tr>
                        <td class="white">
                          Fakturerings-kund:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_name" value="" tabindex="15" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Email:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email" value="" tabindex="20" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Att:
                        </td>
                        <td class="white">
                          <input type="text" name="att" value="" tabindex="16" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Org nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="vat_reg_no" value="" tabindex="21" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-adress:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_address" value="" tabindex="17" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Bank/Plusgiro:
                        </td>
                        <td class="white">
                          <input type="text" name="giro_no" value="" tabindex="22" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-postnummer:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_zip" value="" tabindex="18" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                        </td>
                        <td class="white">
                          Fakturerings-box:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_box" value="" tabindex="23" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-stad:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_city" value="" tabindex="19" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Fakturerings-land:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_country" value="" tabindex="24" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                      </tr>

                    </table>
                </div>
                <div id="Ctabs-3">

                    <table style="width:70%; padding-top:0px; margin:0px;">
                      <tr>
                        <td class="white">
                          Schenker:
                        </td>
                        <td class="white">
                          <input type="text" name="schenker_no" value="" tabindex="25" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          DHL:
                        </td>
                        <td class="white">
                          <input type="text" name="dhl_no" value="" tabindex="26" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Annan:
                        </td>
                        <td class="white">
                          <input type="text" name="other_no" value="" tabindex="27" />
                        </td>
                      </tr>

                    </table>

                </div>
                <div id="Ctabs-4">
                   <p>Leveransanvisningar att visas på fraktsedeln.</p>
                   <table style="width:70%; padding-top:0px; margin:0px;">
                      <tr>
                        <td class="white">
                          Rad 1:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo1" id="f_delinfo1" value="" tabindex="28" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Rad 2:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo2" id="f_delinfo2" value="" tabindex="29" />
                        </td>
                      </tr>

                    </table>

                </div>
            </div>

	    <br /><button type="submit" style="width:150px" id="save_booking" name="save_booking"><img alt="" src="../img/page_save.png"> Spara</button>
          </p>
	</form>
    </div>



    <!--
         Show dialog for editing a customer
      -->
    <div id="editCust" title="Ändra kunduppgifter" style="display: none;">
	<form method="get" action="cmd.php" onsubmit="postFormAJAX(this); return false;">
	  <p>
	    <input type="hidden" name="cmd" value="editCust" />
	    <input type="hidden" id="custTypeE" name="custTypeE" value="0" />
	    <input type="hidden" id="custIDE" name="custIDE" value="0" />


            <div id="custTabsE">
                <ul>
                    <li><a href="#Etabs-1">Grundläggande uppgifter</a></li>
                    <li><a href="#Etabs-2">Fakturauppgifter</a></li>
                    <li><a href="#Etabs-3">Transportnummer</a></li>
                    <li><a href="#Etabs-4">Leveransanvisning</a></li>
                    <li><a href="#Etabs-5">Lista leveranser</a></li>
                </ul>
                <div id="Etabs-1">

                  <table style="width:100%; padding-top:0px; margin:0px;">

                    <tr>
                      <td class="white">
                        <div id="edit_kundnamn">Kundnamn:</div>
                      </td>
                      <td class="white">
                        <input type="text" name="nameE" id="nameE" value="" tabindex="1" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                      <td class="white">
                        Telefonnummer:
                      </td>
                      <td class="white">
                        <input type="text" name="phoneE" id="phoneE" value="" tabindex="8" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
      	                <div id="alt_kundnamnE">Alt. kundnamn:</div>
                      </td>
                      <td class="white">
                          <input type="text" name="alt_nameE" id="alt_nameE" value="" tabindex="2" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                      <td class="white">
                        Kontaktperson:
                      </td>
                      <td class="white">
                        <input type="text" name="contactE" id="contactE" value="" tabindex="9" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        <div id="edit_address">Leverans-adress:</div>
                      </td>
                      <td class="white">
                        <input type="text" name="del_addressE" id="del_addressE" value="" tabindex="3" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                      <td class="white">
                        Mobiltelefon-nummer:
                      </td>
                      <td class="white">
                        <input type="text" name="mobile_phoneE" id="mobile_phoneE" value="" tabindex="10" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        <div id="edit_zip">Leverans-postnummer:</div>
                      </td>
                      <td class="white">
                        <input type="text" name="del_zipE" id="del_zipE" value="" tabindex="4" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                      </td>
                      <td class="white">
                        Kontakt e-post:
                      </td>
                      <td class="white">
                        <input type="text" name="contact_email1E" id="contact_email1E" value="" tabindex="11" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        <div id="edit_city">Leverans-stad:</div>
                      </td>
                      <td class="white">
                        <input type="text" name="del_cityE" id="del_cityE" value="" tabindex="5" onchange="this.value = this.value.toUpperCase();" />
                      </td>
                      <td class="white">
                        Alt kontaktperson:
                      </td>
                      <td class="white">
                        <input type="text" name="contact2E" id="contact2E" value="" tabindex="12" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        Leverans land:
                      </td>
                      <td class="white">
                        <input type="text" name="del_countryE" id="del_countryE" value="" tabindex="6" onchange="this.value = this.value.toUpperCase();" />
                      </td>
                      <td class="white">
                        Alt. mobiltelefon-nummer:
                      </td>
                      <td class="white">
                        <input type="text" name="mob2E" id="mob2E" value="" tabindex="13" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        Fax:
                      </td>
                      <td class="white">
                        <input type="text" name="faxE" id="faxE" value="" tabindex="7" />
                      </td>
                      <td class="white">
                        Alt. kontakt e-post:
                      </td>
                      <td class="white">
                        <input type="text" name="contact_email2E" id="contact_email2E" value="" tabindex="14" />
                      </td>
                    </tr>
                  </table>

                </div>
                <div id="Etabs-2">

                  <table style="width:100%; padding-top:0px; margin:0px;">

                    <tr>
                      <td class="white">
                        Fakturerings-kund:
                      </td>
                      <td class="white">
                        <input type="text" name="bill_nameE" id="bill_nameE" value="" tabindex="15" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                      <td class="white">
                        Email:
                      </td>
                      <td class="white">
                        <input type="text" name="contact_emailE" id="contact_emailE" value="" tabindex="20" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        Att:
                      </td>
                      <td class="white">
                        <input type="text" name="attE" id="attE" value="" tabindex="16" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                      <td class="white">
                        Org nummer:
                      </td>
                      <td class="white">
                        <input type="text" name="vat_reg_noE" id="vat_reg_noE" value="" tabindex="21" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        Fakturerings-adress:
                      </td>
                      <td class="white">
                        <input type="text" name="bill_addressE" id="bill_addressE" value="" tabindex="17" onchange="this.value = toPascalCase(this.value);" />
                      </td>
                      <td class="white">
                        Bank/Plusgiro:
                      </td>
                      <td class="white">
                        <input type="text" name="giro_noE" id="giro_noE" value="" tabindex="22" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        Fakturerings-postnummer:
                      </td>
                      <td class="white">
                        <input type="text" name="bill_zipE" id="bill_zipE" value="" tabindex="18" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                      </td>
                      <td class="white">
                        Fakturerings-box:
                      </td>
                      <td class="white">
                        <input type="text" id="bill_boxE" name="bill_boxE" value="" tabindex="23" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        Fakturerings-stad:
                      </td>
                      <td class="white">
                        <input type="text" name="bill_cityE" id="bill_cityE" value="" tabindex="19" onchange="this.value = this.value.toUpperCase();" />
                      </td>
                      <td class="white">
                        Fakturerings-land:
                      </td>
                      <td class="white">
                        <input type="text" name="bill_countryE" id="bill_countryE" value="" tabindex="24" onchange="this.value = this.value.toUpperCase();" />
                      </td>
                    </tr>

                  </table>

                </div>
                <div id="Etabs-3">

                  <table style="width:100%; padding-top:0px; margin:0px;">

                    <tr>
                      <td class="white">
                        Schenker:
                      </td>
                      <td class="white">
                        <input type="text" name="schenker_noE" id="schenker_noE" value="" tabindex="25" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        DHL:
                      </td>
                      <td class="white">
                        <input type="text" name="dhl_noE" id="dhl_noE" value="" tabindex="26" />
                      </td>
                    </tr>

                    <tr>
                      <td class="white">
                        Annan:
                      </td>
                      <td class="white">
                        <input type="text" name="other_noE" id="other_noE" value="" tabindex="27" />
                      </td>
                    </tr>

                  </table>

                  </table>

                </div>
                <div id="Etabs-4">
                   <p>Leveransanvisningar att visas på fraktsedeln.</p>
                   <table style="width:70%; padding-top:0px; margin:0px;">
                      <tr>
                        <td class="white">
                          Rad 1:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo1E" id="f_delinfo1E" value="" tabindex="28" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Rad 2:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo2E" id="f_delinfo2E" value="" tabindex="29" />
                        </td>
                      </tr>

                    </table>
                </div>
                <div id="Etabs-5">
                   <p>Listning av fraktsedlar.</p>
                   
                   <div id="waybill_links">

                   </div>
                </div>
            </div>
            <br />
            <div style="float:left;"><button name="save_custE" id="save_custE" type="submit"><img src="../img/page_save.png" alt="" /> Spara</button></div>
	    <div id="custPriceEdit" style="float: left; margin-left:20px;"></div>
          </p>
	</form>
    </div>


    <!--
        Show dialog for adding an article
     -->
    <div id="addArt" title="Skapa ny artikel" style="display: none;">
        <form method="get" action="../index.php" onsubmit="postFormAJAX(this); return false;">
          <p>
	    <input type="hidden" name="cmd" value="addArt" />
	    <input type="hidden" id="artType" name="artType" value="0" />

            Artikelns pris är standardvärde, kan justeras för enskilda kunder.
            <table style="width:100%; padding-top:0px;">
            <tr><td class="white" colspan="2">

            <a href="javascript:void(0);" onclick="openKCFinder('art_img');">Klicka här för att välja produktbild</a>
            <div id="art_img_img"><img src="/order/img/error.png" alt="" /></div>
	    <input type="hidden" name="art_img" id="art_img" value="" />

	    </td></tr><tr><td class="white">

	    Artikelnummer: </td><td class="white">
	    <input type="text" name="art_no" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 20px;">

	    Artikelnamn: </td><td class="white">
	    <input type="text" name="art_name" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 40px;">

	    Vikt (kg): </td><td class="white">
	    <input type="text" name="weight" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 30px;">

	    Artikelpris: </td><td class="white">
	    <input type="text" name="price" value="" />

            </td></tr></table><br />

	    <input type="submit" style="width:150px" id="save_art" value="Spara" />
          </p>
        </form>
    </div>

    <!--
        Show dialog for editing an article
     -->
    <div id="editArt" title="Ändra en artikel" style="display: none;">
        <form method="get" action="cmd.php" onsubmit="postFormAJAX(this); return false;">
          <p>
	    <input type="hidden" name="cmd" value="editArt" />
	    <input type="hidden" id="artTypeE" name="artType" value="0" />
	    <input type="hidden" id="artIDE" name="artIDE" value="0" />

            Artikelns pris är standardvärde, kan justeras för enskilda kunder. Utan ändring/åtgärd sparas artikel med nuvarande bild.<br /><br />
            <a href="javascript:void(0);" onclick="del_img_val('art_imgE');">Klicka här</a> för att ta bort bild helt från artikeln innan sparning.
            <table style="width:100%; padding-top:0px;">
            <tr><td class="white" colspan="2">

            <a href="javascript:void(0);" onclick="openKCFinder('art_imgE');">Klicka här för att välja produktbild</a>
            <input type="hidden" name="art_imgE" id="art_imgE" value="" />
            <div id="art_imgE_img"><img src="/order/img/error.png" alt="" /></div>

	    </td></tr><tr><td class="white">

	    Artikelnummer: </td><td class="white">
	    <input type="text" name="art_noE" id="art_noE" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 20px;">

	    Artikelnamn: </td><td class="white">
	    <input type="text" name="art_nameE" id="art_nameE" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 40px;">

	    Vikt (kg): </td><td class="white">
	    <input type="text" name="weightE" id="weightE" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 30px;">

	    Artikelpris: </td><td class="white">
	    <input type="text" name="priceE" id="priceE" value="" />

            </td></tr></table><br />

	    <input type="submit" style="width:150px" id="save_art" value="Spara" />
          </p>
        </form>
    </div>

    <!--
        Show dialog for adding an article price to customer
     -->
    <div id="addArtPrice" title="Lägg till anpassat pris" style="display: none;">
        <form method="get" action="cmd.php" onsubmit="postFormAJAX(this); return false;">
          <p>
	    <input type="hidden" name="cmd" value="addArtPrice" />
	    <input type="hidden" id="artPriceType" name="artPriceType" value="0" />

            Välj en artikel att sätta kundbaserat pris på.
            <table style="width:100%; padding-top:0px;"><tr><td class="white">

	    Artikelnamn: </td><td class="white">
	    <div id="artNameField"></div>

	    </td></tr></table><br />

	    <input type="submit" style="width:150px" value="Välj" />
          </p>
        </form>
    </div>