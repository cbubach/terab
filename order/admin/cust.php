<?php
    include("db.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB administration - Kunddatabas</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="jquery.listnav.pack-2.1.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">

<?php

  include("dialogs.php");

?>

<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<div id="tabs" style="margin: 20px; font-size: 12px;">
    <ul>
        <li><a href="#tabs-1">Kund</a></li>
        <li><a href="#tabs-2">Leverantör</a></li>
    </ul>
    <div id="tabs-1">
        <div style="width:80%; margin-left: 100px; text-align: center;">
        <a href="cmd.php?cmd=CustEmail&type=0">Exportera kunders e-post adresser</a> <br /><br />
	<form method="get" action="cust.php" onsubmit="editCust(this.custID.value); return false;">
            <table style="width:700px; padding-top:0px; margin:0px;">
              <tr>
                <td class="white">
                    <strong> Sök på kundnamn: </strong>
                </td>
                <td class="white">
                    <input type="text" name="custName0" id="custName0" value="" style="width:170px; height:20px;" />
                    <input type="hidden" name="custID" id="custID" value="0" />
                </td>
                <td class="white">
                    <button name="save" type="submit"><img src="../img/page_white_edit.png" alt="" /> Ändra kundinformation</button>
                </td>
                <td>
                    <a href="javascript:void(0);" onclick="addCust(0);"><img src="../img/user_add.png" class="vmiddle" alt="Skapa kund" title="Skapa kund" /> Skapa ny kund</a>
                </td>
              </tr>
            </table>
        </form>
	<form method="get" action="cust.php">
            <table>
              <tr>
                <td><input type="text" name="cityName" id="cityName" value="" style="width:250px; height:20px;" /></td>
                <td><button name="search" type="submit"><img src="../img/page_white_edit.png" alt="" /> Sök på ort</button></td>
              </tr>
            </table>
        </form>
        </div>

        <br />

        <div id="myList-nav"></div> <br />
        <ul id="myList" class="paging">
<?php
  if (isset($_GET['cityName']) && $_GET['cityName']!="")
  {
      $result = mysql_query("SELECT * FROM cust WHERE type=0 AND del_city='{$_GET['cityName']}' ORDER BY name ASC") or die(mysql_error());
  }
  else
  {
      $result = mysql_query("SELECT * FROM cust WHERE type=0 ORDER BY name ASC") or die(mysql_error());
  }
  if (mysql_num_rows($result) > 0)
  {
      while ($row = mysql_fetch_array($result))
      {
          print '            <li class="ui-widget-content"><a href="javascript:void(0);" onclick="editCust(\''.$row['id'].'\');" class="listHover">'.$row['name']." - (".$row['del_city'].") ";
          if ($row['contact_email'] != "")
              print $row['contact_email']."</a></li>\n";
          else
              print $row['contact_email1']."</a></li>\n";
      }
  }
  else
  {
      print "            <li>Inga kunder än!</li>\n";
  }
?>
        </ul>
        <div style="clear:both;"></div>
    </div>
    <div id="tabs-2">
        <div style="width:80%; margin-left: 100px; text-align: center;">
        <a href="cmd.php?cmd=CustEmail&type=1">Exportera leverantörers e-post adresser</a> <br /><br />
	<form method="get" action="cust.php" onsubmit="editCust(this.cust_id.value); return false;">
            <table style="width:700px; padding-top:0px; margin:0px;">
              <tr>
                <td class="white">
                    <strong> Sök på kundnamn: </strong>
                </td>
                <td class="white">
                    <input type="text" name="custName1" id="custName1" value="" style="width:170px; height:20px;" />
                    <input type="hidden" name="cust_id" id="cust_id" value="0" />
                </td>
                <td class="white">
                    <button name="save" type="submit"><img src="../img/page_white_edit.png" alt="" /> Ändra kundinformation</button>
                </td>
                <td>
                    <a href="javascript:void(0);" onclick="addCust(1);"><img src="../img/user_add.png" class="vmiddle" alt="Skapa kund" title="Skapa kund" /> Skapa ny leverantör</a>
                </td>
              </tr>
            </table>
        </form>
	<form method="get" action="cust.php#tabs-2">
            <table>
              <tr>
                <td><input type="text" name="cityName" id="cityName" value="" style="width:250px; height:20px;" /></td>
                <td><button name="search" type="submit"><img src="../img/page_white_edit.png" alt="" /> Sök på ort</button></td>
              </tr>
            </table>
        </form>
        </div>


        <br />

        <div id="myList2-nav"></div> <br />
        <ul id="myList2" class="paging">
<?php
  if (isset($_GET['cityName']) && $_GET['cityName']!="")
  {
      $result = mysql_query("SELECT * FROM cust WHERE type=1 AND del_city='{$_GET['cityName']}' ORDER BY name ASC") or die(mysql_error());
  }
  else
  {
      $result = mysql_query("SELECT * FROM cust WHERE type=1 ORDER BY name ASC") or die(mysql_error());
  }
  if (mysql_num_rows($result) > 0)
  {
      while ($row = mysql_fetch_array($result))
      {
          print '            <li class="ui-widget-content"><a href="javascript:void(0);" onclick="editCust(\''.$row['id'].'\');" class="listHover">'.$row['name']." - (".$row['del_city'].")</a></li>\n";
      }
  }
  else
  {
      print "            <li>Inga kunder än!</li>\n";
  }
?>
        </ul>
        <div style="clear:both;"></div>
    </div>
</div>
<?php
 if (isset($_GET['showCust'])) {
     echo '<script type="text/javascript">editCust("'.$_GET['showCust'].'")</script>';
 }
?>

</body>
</html>