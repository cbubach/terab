<?php
/*   faktura.php
 *
 *   Tar emot GET data för att generera en faktura
 *                    Christoffer Bubach, 2015
 */
include("db.php");
include("../mpdf51/mpdf.php");


$mPDF = new mPDF('','', 0, '', 7, 7, 5, 5, 0, 0, 'P');

if (empty($_GET['billId'])) {
    exit;
}
$billId       = intval($_GET['billId']);
$billing      = mysql_fetch_assoc(mysql_query("SELECT * FROM `bill` WHERE `id` =".$billId));
$billCustomer = mysql_fetch_assoc(mysql_query("SELECT * FROM `cust` WHERE `id` =".$billing['cust_id']));
$billSettings = mysql_fetch_assoc(mysql_query("SELECT * FROM `bill_settings` WHERE `id`=0"));

// load bill specific settings from other id than the standard 0.
if ($billing['settings_id'] !== 0) {
    $newSettings = mysql_fetch_assoc(mysql_query("SELECT * FROM `bill_settings` WHERE `id`=".$billing['settings_id']));

    if (!empty($newSettings['iban'])) {
        $billSettings['iban'] = $newSettings['iban'];
    }
    if (!empty($newSettings['message'])) {
        $billSettings['message'] = $newSettings['message'];
    }
    if (!empty($newSettings['due'])) {
        $billSettings['due'] = $newSettings['due'];
    }
}
$billItems = array();
$result    = mysql_query("SELECT * FROM `bill_item` WHERE `bill_id`=".$billId);

while ($row = mysql_fetch_assoc($result)) {
    $billItems[] = $row;
}


$mPDF->addPage();
$mPDF->WriteHTML('
<style type="text/css">
 body, h1 {
   font-family: verdana, helvetica, sans-serif;
 }
 body {
   padding: 10px;
   font-size: 12px;
 }
 td { padding: 5px;}
 span {
  font-size: 14px;
 }

</style>');
$mPDF->SetColumns(2, 'J', 3);

//$mPDF->WriteText(30, 10, "Faktura");

$dueDate = strtotime('+'.$billSettings['due'].' days', strtotime($billing['date']));

$addressOrBox = empty($billCustomer['bill_box']) ? $billCustomer['bill_address'] : $billCustomer['bill_box'];
$name         = empty($billCustomer['bill_name']) ? $billCustomer['name'] : $billCustomer['bill_name'];
$reference    = empty($billing['ref_no']) ? $billSettings['reference'] : $billing['ref_no'];

// TODO: put delivery address in rounded rectangle here. ($billCustomer['del_address'])
//RoundedRect(float x, float y, float w, float h, float radius[, string style])
//$mPDF->RoundedRect(10, 7, 95, 10, 2, '');
//var_dump($billCustomer); exit;

if ($billing["status"] == 0) {
    $billing_settings = '
      <img src="'.$billSettings['logo_file'].'" width="350" height="100"><br><br>
      <table style="padding-left:15px;">
        <tr>
          <td><b>Fakturadatum</b></td>
          <td><span>'.$billing['date'].'</span></td>
        </tr>
        <tr>
          <td><b>Betalningsvillkor</b></td>
          <td><span>'.$billSettings['due'].' dagar</span></td>
        </tr>
        <tr>
          <td><b>Förfallodatum</b></td>
          <td><span>'.date("Y-m-d", $dueDate).'</span></td>
        </tr>
        <tr>
          <td><b>Referens</b></td>
          <td><span>'.$reference.'</span></td>
        </tr>
      </table>
    ';
} else if ($billing["status"] == 2) {
    $billing_settings = '
      <img src="'.$billSettings['logo_file'].'" width="350" height="100"><br><br>
      <table style="padding-left:15px;">
        <tr><td> &#160; </td><td> &#160; </td></tr>
        <tr><td> &#160; </td><td> &#160; </td></tr>
        <tr>
          <td><b>Fakturadatum</b></td>
          <td><span>'.$billing['date'].'</span></td>
        </tr>
        <tr>
          <td><b>Referens</b></td>
          <td><span>'.$reference.'</span></td>
        </tr>
      </table>
    ';
} else {
    $billing_settings = '
      <img src="'.$billSettings['logo_file'].'" width="350" height="100"><br><br>
      <table style="padding-left:15px;">
        <tr>
          <td><b>Datum</b></td>
          <td><span>'.$billing['date'].'</span></td>
        </tr>
        <tr>
          <td><b>Referens</b></td>
          <td><span>'.$reference.'</span></td>
        </tr>
        <tr><td> &#160; </td><td> &#160; </td></tr>
        <tr><td> &#160; </td><td> &#160; </td></tr>
      </table>
    ';
}

$mPDF->WriteHTML($billing_settings);

  
$mPDF->addColumn();
$mPDF->RoundedRect(108, 7, 95, 10, 2, '');
if ($billing["status"] == 0) {
    $mPDF->WriteHTML('<table style="margin-top:8px; width:90%;"><tr><td><h2> &#160; Faktura '.$billing['order_id'].'</h2></td></tr></table><br><br>');
} else if ($billing["status"] == 1) {
    $mPDF->WriteHTML('<table style="margin-top:8px; width:90%;"><tr><td><h2> &#160; Följdesedel </h2></td><td align="right"> </td></tr></table><br><br>');
} else {
    $mPDF->WriteHTML('<table style="margin-top:8px; width:90%;"><tr><td><h2> &#160; Kreditfaktura '.$billing['order_id'].'</h2></td></tr></table><br><br>');
}
//RoundedRect(float x, float y, float w, float h, float radius[, string style])
$mPDF->RoundedRect(108, 25, 95, 40, 2, '');
/*
if ($billing["status"] == 0 || $billing["status"] == 2) {
    $addressLabel = "Fakturaadress";
} else {
    $addressLabel = "Adress";
}*/
$addressLabel = "";

$mPDF->WriteHTML('
  <p style="padding-left: 50px">'.$addressLabel.'</p>
  <table style="padding-left: 20px">
    <tr>
      <td><span>'.$name.'</span></td>
    </tr>
    <tr>
      <td><span>'.$addressOrBox.'</span></td>
    </tr>
    <tr>
      <td><span>'.$billCustomer['bill_zip'].' '.$billCustomer['bill_city'].'</span></td>
    </tr>
  </table>
  ');

$mPDF->SetColumns(1);
$mPDF->setY(69);
$mPDF->RoundedRect(7, 75, 196, 150, 2, '');
$mPDF->RoundedRect(7, 83, 196, 1, 0, '');

$totalSum    = 0;
$totalSumVat = 0;
$totalVat    = 0;
$currency    = "SEK";
$itemHtml    = "";
$defaultVat  = null;
foreach ($billItems as $row) {
    $vatCalc      = empty($row['VAT']) ? 1.0 : "1.".$row['VAT'];
    $vatCalc      = floatval($vatCalc);
    $totalSum    += $row['sum'];
    $totalSumVat += (float)$row['sum'] * $vatCalc;
    $totalVat    += (round($row['sum'] * $vatCalc, 2) - $row['sum']);
    $currency     = $row['currency'];

    if (empty($defaultVat)) {
        $defaultVat = $row['VAT'];
    }
    //<td>'.$row['art_no'].'</td>
    if ($billing["status"] == 0) {
        $itemHtml .= '<tr>
            <td>'.$row['name'].'</td>
            <td align="right">'.$row['amount'].'</td>
            <td>'.$row['unit'].'</td>
            <td align="right">'.number_format((float)$row['price'], 2, ",", " ").'</td>
            <td></td>
            <td align="right">'.number_format((float)$row['sum'], 2, ",", " ").'</td></tr>'; // '.$row['VAT'].'%
    } else if ($billing["status"] == 1) {
        $itemHtml .= '<tr>
            <td>'.$row['name'].'</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td align="right">'.$row['amount'].'</td>
            <td>'.$row['unit'].'</td></tr>';
    } else {
        $itemHtml .= '<tr>
            <td>'.$row['name'].'</td>
            <td align="right">'.$row['amount'].'</td>
            <td>'.$row['unit'].'</td>
            <td align="right">-'.number_format((float)$row['price'], 2, ",", " ").'</td>
            <td></td>
            <td align="right">-'.number_format((float)$row['sum'], 2, ",", " ").'</td></tr>'; // '.$row['VAT'].'%
    }
}

//      <td style="width:10%">Art nr</td>
if ($billing["status"] == 0 || $billing["status"] == 2) {
    $html = '
      <table style="width:100%; padding:15px; padding-top:25px;">
        <tr>
          <td style="width:44%">Benämning</td>
          <td style="width:10%" align="right">Antal</td>
          <td style="width:10%">Enhet</td>
          <td style="width:13%" align="right">Pris ('.$currency.')</td>
          <td style="width:10%"></td>
          <td style="width:13%">Summa ('.$currency.')</td>
        </tr>
        <tr><td>&#160;</td><td></td><td></td><td></td><td></td><td></td></tr>'; //Moms
} else {
    $html = '
      <table style="width:100%; padding:15px; padding-top:25px;">
        <tr>
          <td style="width:44%">Benämning</td>
          <td style="width:10%"></td>
          <td style="width:13%"></td>
          <td style="width:10%"></td>
          <td style="width:10%">Antal</td>
          <td style="width:13%">Enhet</td>
        </tr>
        <tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
}

$mPDF->WriteHTML($html.$itemHtml.'</table>');

$roundingTitle = "";
$roundingValue = "";
$roundingDiff  = round(round($totalSumVat, 0) - $totalSumVat, 2);
if ($roundingDiff != 0) {
    $roundingDiff  = (($roundingDiff >= 0) ? '+' : '').number_format((float)$roundingDiff, 2, ",", " ");
    $totalSumVat   = round($totalSumVat, 0);
    $roundingTitle = "<td>Öresavrundning</td>";
    $roundingValue = "<td>".$roundingDiff." ".$currency."</td>";
}

if (!empty($billSettings['message'])) {
    $oldX = $mPDF->getX();
    //RoundedRect(float x, float y, float w, float h, float radius[, string style])
    $mPDF->RoundedRect(7, 230, 196, 30, 2, '');
    $mPDF->setY(235);
    $mPDF->setX(7);
    $mPDF->WriteHTML('<p style="padding-left:17px">'.nl2br($billSettings['message'])."</p>");
    $mPDF->setX($oldX);
}

if ($billing["status"] == 0) {
    $sumHtml = '<table align="right">
      <tr>
        <td>Summa ex. moms</td>
        <td>Moms ('.$defaultVat.'%)</td>
        '.$roundingTitle.'
        <td><strong>Att betala</strong></td>
      </tr>
      <tr>
        <td>'.number_format((float)$totalSum, 2, ",", " ").' '.$currency.'</td>
        <td>'.number_format((float)$totalVat, 2, ",", " ").' '.$currency.'</td>
        '.$roundingValue.'
        <td>'.number_format((float)$totalSumVat, 2, ",", " ").' '.$currency.'</td>
      </tr>
    </table>';
} else if ($billing["status"] == 2) {
    $sumHtml = '<table align="right">
      <tr>
        <td>Summa ex. moms</td>
        <td>Moms ('.$defaultVat.'%)</td>
        '.$roundingTitle.'
        <td><strong>Summa totalt</strong></td>
      </tr>
      <tr>
        <td>-'.number_format((float)$totalSum, 2, ",", " ").' '.$currency.'</td>
        <td>-'.number_format((float)$totalVat, 2, ",", " ").' '.$currency.'</td>
        '.$roundingValue.'
        <td>-'.number_format((float)$totalSumVat, 2, ",", " ").' '.$currency.'</td>
      </tr>
    </table>';
} else {
    $sumHtml = "";
}
$mPDF->setY(205);
$mPDF->WriteHTML($sumHtml);


$cplImage = "";
if (is_file("../kcfinder/upload/billing/cpl.png")) {
    $cplImage = '<img src="/TERAB/order/kcfinder/upload/billing/cpl.png" width="80px" align="left">';
}

$billFooter = '
<table style="width:100%; padding: 0px; padding-left:10px; margin:0px; font-size:14px;">
  <tr>
    <td width="25%">
      <b>Adress</b><br>
      '.nl2br($billSettings['address']).'
   </td>
   <td width="30%">
      <table>
        <tr>
          <td>
            <b>Org. nr</b> <br>
            <b>SWIFT</b> <br>
            <b>IBAN</b> <br>
            <b>Bankgiro</b>
          </td>
          <td>
            &#160;'.$billSettings['vat_reg_no'].'<br>
            &#160;'.$billSettings['swift'].'<br>
            &#160;'.$billSettings['iban'].'<br>
            &#160;'.$billSettings['giro'].'
          </td>
        </tr>
     </table>
   </td>
   <td width="30%">
      <table>
        <tr>
          <td>
            <b>Telefon</b> <br>
            <b>Fax</b> <br>
            <b>Hemsida</b> <br>
            <b>E-post</b>
          </td>
          <td>
            &#160;'.$billSettings['tel'].'<br>
            &#160;'.$billSettings['fax'].'<br>
            &#160;'.$billSettings['homepage'].'<br>
            &#160;'.$billSettings['email'].'
          </td>
        </tr>
      </table>
   </td>
   <td width="14%" valign="top">
       '.$cplImage.'
   </td>
 </tr>
 </table>';

$mPDF->setY(265);
$mPDF->WriteHTML($billFooter);
$mPDF->Output();
exit;