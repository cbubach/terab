<?php
    include("db.php");
    
    if (isset($_GET['AJAX-cmd']))
    {
        if ($_GET['AJAX-cmd']=="updateProdCat")
        {
            //$_GET = array_map("utf8_decode", $_GET);
            //print_r($_GET); exit;
            $result = mysql_query("SELECT * FROM prod_cat WHERE prod_id='{$_GET['prodID']}'");
            if (mysql_affected_rows()==0) {
                $result = mysql_query("INSERT INTO prod_cat (prod_id, cat) values ('{$_GET['prodID']}','{$_GET['catName']}')");
            }
            else {
                $result = mysql_query("UPDATE prod_cat SET cat='{$_GET['catName']}' WHERE prod_id='{$_GET['prodID']}'");
            }
            exit;
        }
    }
    
    if (isset($_GET['move-up']))
    {
        $sort = mysql_result(mysql_query("SELECT sort FROM art WHERE id='{$_GET['move-up']}'"),0);
        if (mysql_num_rows(mysql_query("SELECT id FROM art WHERE sort < '$sort'")) > 0)
        {
            mysql_query("UPDATE art SET sort = sort + 1 WHERE sort = {$sort} - 1") or die();
            mysql_query("UPDATE art SET sort = {$sort} - 1 WHERE id='{$_GET['move-up']}'") or die();
        }
        header("Location: products.php#tabs-2");
        exit;
    }
    else if (isset($_GET['move-down']))
    {
        $sort = mysql_result(mysql_query("SELECT sort FROM art WHERE id='{$_GET['move-down']}'"),0);
        if (mysql_num_rows(mysql_query("SELECT id FROM art WHERE sort > '$sort'")) > 0)
        {
            mysql_query("UPDATE art SET sort = sort - 1 WHERE sort = {$sort} + 1") or die();
            mysql_query("UPDATE art SET sort = {$sort} + 1 WHERE id='{$_GET['move-down']}'") or die();
        }
        header("Location: products.php#tabs-2");
        exit;
   }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB administration - Artikeldatabas</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="jquery.listnav.pack-2.1.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">

<?php

  include("dialogs.php");

?>

<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<div id="tabs" style="margin: 20px; font-size: 12px;">
    <ul>
        <li><a href="#tabs-1">Kundprodukter</a></li>
        <li><a href="#tabs-2">Leverantörsprodukter</a></li>
    </ul>
    <div id="tabs-1">
        <div style="width:80%; margin-left: 100px; text-align: center;">
	<form method="get" action="products.php" onsubmit="editArt(this.artID0.value); return false;">
            <table style="width:700px; padding-top:0px; margin:0px;">
              <tr>
                <td class="white">
                    <strong> Sök på produktnamn: </strong>
                </td>
                <td class="white">
                    <input type="text" name="artName0" id="artName0" value="" style="width:170px; height:20px;" />
                    <input type="hidden" name="artID0" id="artID0" value="0" />
                </td>
                <td class="white">
                    <button name="save" type="submit"><img src="../img/page_white_edit.png" alt="" /> Ändra produktinformation</button>
                </td>
                <td>
                    <a href="javascript:void(0);" onclick="addArt(0);"><img src="../img/application_add.png" class="vmiddle" alt="Skapa artikel" title="Skapa artikel" /> Skapa en artikel</a>
                </td>
              </tr>
            </table>
        </form>
        </div>

        <br />

        <div id="myList-nav"></div> <br />
        <ul id="myList" class="paging">
<?php
  $result = mysql_query("SELECT * FROM art WHERE type=0 ORDER BY art_name ASC") or die(mysql_error());
  if (mysql_num_rows($result) > 0)
  {
      while ($row = mysql_fetch_array($result))
      {
          $price = str_replace(".", ",", mysql_result(mysql_query("SELECT price FROM price WHERE art_id='{$row['id']}' AND cust_id='0'"), 0));
          print '            <li class="ui-widget-content"><span style="display:none;">'.$row['art_name'].'</span><span style="float:right;" id="prodCatBox'.$row['id'].'">';
          print '<select name="product_category" id="prodcat_'.$row['id'].'" class="prodcat_list">'."\n";
          print '  <option value="0">--Ny produktkategori</option>'."\n";
          $res2 = mysql_query("SELECT DISTINCT(cat) FROM prod_cat");
          if (mysql_num_rows($res2) > 0)
          {
              $count = 2;
              while ($row2 = mysql_fetch_array($res2))
              {
                  $count++;
                  if (mysql_num_rows(mysql_query("SELECT * FROM prod_cat WHERE prod_id='{$row['id']}' AND cat='{$row2['cat']}'"))>0)
                      print '  <option value="'.$row2['cat'].'" selected="selected">'.$row2['cat'].'</option>'."\n";
                  else
                      print '  <option value="'.$row2['cat'].'">'.$row2['cat'].'</option>'."\n";
              }
          }
          print '</select><input type="text" name="newProdCatName" id="newProdCatName_'.$row['id'].'" class="newProdCatName" value="Ange nytt namn här" style="display:none;" />'."\n";
          print '</span><a href="javascript:void(0);" onclick="editArt(\''.$row['id'].'\');" class="listHover">'.$row['art_name']." (<b>".$price."</b>)</a></li>\n";
      }
  }
  else
  {
      print "            <li>Inga produkter än!</li>\n";
  }
?>
        </ul>
        <div style="clear:both;"></div>
    </div>
    <div id="tabs-2">
        <div style="width:80%; margin-left: 100px; text-align: center;">
	<form method="get" action="products.php" onsubmit="editArt(this.artID1.value); return false;">
            <table style="width:700px; padding-top:0px; margin:0px;">
              <tr>
                <td class="white">
                    <strong> Sök på produktnamn: </strong>
                </td>
                <td class="white">
                    <input type="text" name="artName1" id="artName1" value="" style="width:170px; height:20px;" />
                    <input type="hidden" name="artID1" id="artID1" value="0" />
                </td>
                <td class="white">
                    <button name="save" type="submit"><img src="../img/page_white_edit.png" alt="" /> Ändra produktinformation</button>
                </td>
                <td>
                    <a href="javascript:void(0);" onclick="addArt(1);"><img src="../img/application_add.png" class="vmiddle" alt="Skapa artikel" title="Skapa artikel" /> Skapa en artikel</a>
                </td>
              </tr>
            </table>
        </form>
        </div>


        <ul class="paging">
<?php
  $result = mysql_query("SELECT * FROM art WHERE type=1 ORDER BY sort ASC") or die(mysql_error());
  if (mysql_num_rows($result) > 0)
  {
      while ($row = mysql_fetch_array($result))
      {
          $price = str_replace(".", ",", mysql_result(mysql_query("SELECT price FROM price WHERE art_id='{$row['id']}' AND cust_id='0'"), 0));
          print '            <li class="ui-widget-content" style="height:30px;">'."\n";
          print '                <a href="javascript:void(0);" onclick="editArt(\''.$row['id'].'\');" class="listHover" style="width:80%;float:left;">'.$row['art_name']." (<b>".$price."</b>)</a>\n";
          print '                <a href="?move-up='.$row['id'].'#tabs-2" style="display: inline; float:right; padding: 0px;"><img src="../img/up.png" width="20px"></a>'."\n";
          print '                <a href="?move-down='.$row['id'].'#tabs-2" style="display: inline; float:right; padding: 0px;"><img src="../img/down.png" width="20px"></a><span style="clear:both;">&#160;</span>'."\n";
          print "            </li>\n";
      }
  }
  else
  {
      print "            <li>Inga produkter än!</li>\n";
  }
?>
        </ul>
        <div style="clear:both;"></div>
    </div>
</div>



</body>
</html>
