<?php
    include("db.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB prisregister</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="quickpager.jquery.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">

<?php

  include("dialogs.php");

?>

<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

       <ul class="paging"></ul>

<div id="tabs" style="margin: 20px; font-size: 12px;">
    <ul>
        <li><a href="#tabs-1">Kundpriser</a></li>
        <li><a href="#tabs-2">Leverantörspriser</a></li>
    </ul>
    <div id="tabs-1">
        <div style="width:80%; margin-left: 100px; text-align: center;">
	<form method="get" action="prices.php#tabs-1">
            <table style="width:800px; padding-top:0px; margin:0px;">
              <tr>
                <td class="white">
                    <strong> Sök på kundnamn: </strong>
                </td>
                <td class="white">
                    <input type="text" name="custName0" id="custName0" value="<?php print isset($_GET['custName0']) ? $_GET['custName0'] : ''; ?>" style="width:170px; height:20px;" />
                    <input type="hidden" name="custID" id="custID" value="<?php print isset($_GET['custID']) ? $_GET['custID'] : ''; ?>" />
                </td>
                <td class="white">
                    <button name="save" type="submit"><img src="../img/page_white_edit.png" alt="" /> Ändra kundpriser</button>
                </td>
                <td style="width: 300px;">
                    <a href="javascript:void(0);" onclick="addArtPrice(0);"> <img src="../img/add.png" alt="" /> Lägg till artikel för att kundanpassa pris</a>
                </td>
              </tr>
            </table>
        </form>
        </div>

<?php

    if (isset($_GET['custID']) && $_GET['custID'] != 0)
    {
        $query = "SELECT price.art_id, price.price
                  FROM price
                  WHERE cust_id = '{$_GET['custID']}'
                  UNION ALL
                  SELECT e.art_id, p.price
                  FROM (

                      SELECT DISTINCT art_id
                      FROM event e
                      JOIN event_art ea ON ea.event_id = e.id
                      WHERE e.cust_id = '{$_GET['custID']}'
                      AND ea.art_id NOT
                      IN (

                          SELECT price.art_id
                          FROM price
                          WHERE cust_id = '{$_GET['custID']}'
                      )
                  ) e
                  JOIN price p ON p.cust_id = 0
                  AND p.art_id = e.art_id";

        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
		    print '<br /><a href="cust.php?showCust='.$_GET['custID'].'" style="text-decoration:underline;">Ändra på kunduppgifterna</a><br />';
            print "<br />Ändra till pris \"0\" för att ta bort anpassat pris på vald artikel.<br />\n";
            print "<form method=\"get\" action=\"cmd.php\" onsubmit=\"postFormAJAX(this); return false;\">\n";
            print '<input type="hidden" name="custID" value="'.$_GET['custID'].'" /><table>'."\n";
            $i = 0;
            $print_array = array();  // store all printed data to this array for abc char sorting
            while ($row = mysql_fetch_array($result))
            {
                $i++;
                $price = mysql_result(mysql_query("SELECT price FROM price WHERE cust_id=0 AND art_id='{$row['art_id']}'"),0);
                $print_array[$i - 1] = "<tr><td>".mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$row['art_id']}'"),0).' ('.$price.')</td>';
                if ( $row['price'] != 0 )
                    $price = str_replace(".", ",", $row['price']);
                $print_array[$i - 1] .= '<td><input type="text" name="artP'.$i.'" value="'.$price.'" /><input type="hidden" name="art'.$i.'" value="'.$row['art_id'].'" /></td></tr>';
            }
            sort($print_array);
            foreach ($print_array as $key => $val)
            {
                print $val."\n";
            }
            print '<input type="hidden" name="art_no" value="'.$i.'" /><input type="hidden" name="cmd" value="savePrices" />'."\n";
            print "</table><br /><input type=\"submit\" value=\"Spara priser\" /></form>\n";
        }
    }
?>

        <div style="clear:both;"></div>
    </div>
    <div id="tabs-2">
        <div style="width:80%; margin-left: 100px; text-align: center;">
	<form method="get" action="prices.php#tabs-2">
            <table style="width:800px; padding-top:0px; margin:0px;">
              <tr>
                <td class="white">
                    <strong> Sök på kundnamn: </strong>
                </td>
                <td class="white">
                    <input type="text" name="custName1" id="custName1" value="<?php print isset($_GET['custName1']) ? $_GET['custName1'] : ''; ?>" style="width:170px; height:20px;" />
                    <input type="hidden" name="cust_id" id="cust_id" value="<?php print isset($_GET['cust_id']) ? $_GET['cust_id'] : ''; ?>" />
                </td>
                <td class="white">
                    <button name="save" type="submit"><img src="../img/page_white_edit.png" alt="" /> Ändra kundpriser</button>
                </td>
                <td style="width: 300px;">
                    <a href="javascript:void(0);" onclick="addArtPrice(1);"> <img src="../img/add.png" alt="" /> Lägg till artikel för att kundanpassa pris</a>
                </td>
              </tr>
            </table>
        </form>
        </div>

<?php

    if (isset($_GET['cust_id']) && $_GET['cust_id'] != 0)
    {
        $query = "SELECT price.art_id, price.price
                  FROM price
                  WHERE cust_id = '{$_GET['cust_id']}'
                  UNION ALL
                  SELECT e.art_id, p.price
                  FROM (

                      SELECT DISTINCT art_id
                      FROM event e
                      JOIN event_art ea ON ea.event_id = e.id
                      WHERE e.cust_id = '{$_GET['cust_id']}'
                      AND ea.art_id NOT
                      IN (

                          SELECT price.art_id
                          FROM price
                          WHERE cust_id = '{$_GET['cust_id']}'
                      )
                  ) e
                  JOIN price p ON p.cust_id = 0
                  AND p.art_id = e.art_id";

        $result = mysql_query($query) or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
            print "<br />Ändra till pris \"0\" för att ta bort anpassat pris på vald artikel.<br />\n";
            print "<form method=\"get\" action=\"cmd.php\" onsubmit=\"postFormAJAX(this); return false;\">\n";
            print '<input type="hidden" name="custID" value="'.$_GET['cust_id'].'" /><table>'."\n";
            $i = 0;
            while ($row = mysql_fetch_array($result))
            {
                $i++;
                $price = mysql_result(mysql_query("SELECT price FROM price WHERE cust_id=0 AND art_id='{$row['art_id']}'"),0);
                print "<tr><td>";
                print mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$row['art_id']}'"),0).' ('.$price.')</td>';
                if ( $row['price'] != 0 )
                    $price = str_replace(".", ",", $row['price']);
                print '<td><input type="text" name="artP'.$i.'" value="'.$price.'" /><input type="hidden" name="art'.$i.'" value="'.$row['art_id'].'" /></td></tr>';
            }
            print '<input type="hidden" name="art_no" value="'.$i.'" /><input type="hidden" name="cmd" value="savePrices" />'."\n";
            print "</table><br /><input type=\"submit\" value=\"Spara priser\" /></form>\n";
        }
    }
?>

        <div style="clear:both;"></div>
    </div>
</div>



</body>
</html>
