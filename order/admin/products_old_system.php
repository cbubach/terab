<?php
    include("db.php");
    
    if (isset($_GET['move-up']))
    {
        $sort_index = mysql_result(mysql_query("SELECT sort_index FROM products WHERE id='{$_GET['move-up']}'"),0);
        if (mysql_num_rows(mysql_query("SELECT id FROM products WHERE sort_index < '$sort_index'")) > 0)
        {
            mysql_query("UPDATE products SET sort_index = sort_index + 1 WHERE sort_index = {$sort_index} - 1") or die();
            mysql_query("UPDATE products SET sort_index = {$sort_index} - 1 WHERE id='{$_GET['move-up']}'") or die();
        }
    }
    else if (isset($_GET['move-down']))
    {
        $sort_index = mysql_result(mysql_query("SELECT sort_index FROM products WHERE id='{$_GET['move-down']}'"),0);
        if (mysql_num_rows(mysql_query("SELECT id FROM products WHERE sort_index > '$sort_index'")) > 0)
        {
            mysql_query("UPDATE products SET sort_index = sort_index - 1 WHERE sort_index = {$sort_index} + 1") or die();
            mysql_query("UPDATE products SET sort_index = {$sort_index} + 1 WHERE id='{$_GET['move-down']}'") or die();
        }
   }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB administration - Artikeldatabas</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="jquery.listnav.pack-2.1.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


        <ul id="myList" class="paging" style="width:500px;">
<?php
  $result = mysql_query("SELECT * FROM products ORDER BY sort_index ASC") or die(mysql_error());
  if (mysql_num_rows($result) > 0)
  {
      while ($row = mysql_fetch_array($result))
      {
          print '            <li class="ui-widget-content" style="padding:5px;">';
          print '<a href="?move-up='.$row['id'].'" style="display: inline; padding: 0px;"><img src="../img/up.png"></a>';
          print '<a href="?move-down='.$row['id'].'" style="display: inline; padding: 0px; padding-right: 15px;"><img src="../img/down.png"></a>';
          print '<span style="font-size: 14px;">'.$row['name']."</span></li>\n";
      }
  }
  else
  {
      print "            <li>Inga produkter än!</li>\n";
  }
?>
        </ul>


</body>
</html>
