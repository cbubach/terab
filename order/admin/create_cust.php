<?php
    include("db.php");
    
    if ($_GET['cmd'] == "addCust")
    {  
        //$GET = array_map('utf8_decode', $_GET);  // Fix jQuery UTF-8

        $type = $_GET['custType'];
        $query  = "INSERT INTO cust ";
        $query .= "(name, del_address, del_zip, del_city, bill_name, bill_address, bill_zip, bill_box, bill_city, ";
        $query .= "phone, mobile_phone, contact, contact_email, fax, vat_reg_no, giro_no, type, ";
        $query .= "alt_name, del_country, att, bill_country, contact_email1, contact2, mob2, contact_email2, ";
        $query .= "schenker_no, dhl_no, other_no, f_delinfo1, f_delinfo2) VALUES (";
        $query .= "'{$_GET['name']}', '{$_GET['del_address']}', '{$_GET['del_zip']}', '{$_GET['del_city']}', '{$_GET['bill_name']}', ";
        $query .= "'{$_GET['bill_address']}', '{$_GET['bill_zip']}', '{$_GET['bill_box']}', '{$_GET['bill_city']}', ";
        $query .= "'{$_GET['phone']}', '{$_GET['mobile_phone']}', '{$_GET['contact']}', '{$_GET['contact_email']}', '{$_GET['fax']}', ";
        $query .= "'{$_GET['vat_reg_no']}', '{$_GET['giro_no']}', '$type', '{$_GET['alt_name']}', '{$_GET['del_country']}', '{$_GET['att']}', ";
        $query .= "'{$_GET['bill_country']}', '{$_GET['contact_email1']}', '{$_GET['contact2']}', '{$_GET['mob2']}', '{$_GET['contact_email2']}', ";
        $query .= "'{$_GET['schenker_no']}', '{$_GET['dhl_no']}', '{$_GET['other_no']}', '{$_GET['f_delinfo1']}', '{$_GET['f_delinfo2']}')";

        mysql_query($query) or die(mysql_error());
        
	$sql = "insert into customers(name, address, zip, city, contact_person, contact_phone, contact_email, contact_fax, vat_reg_no, bankgiro_no, default_report_type)
         values('".$_GET['name']."','".$_GET['del_address']."','".$_GET['del_zip']."','".$_GET['del_city']."','".$_GET['contact']."','".$_GET['phone']."','".$_GET['contact_email']."',
         '".$GET['fax']."','".$GET['vat_reg_no']."','".$GET['giro_no']."', 'customer_letter')";
	mysql_query($sql);

        header ("Location: /terab/system.php?page=manage_customers");
        exit;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB administration - Skapa ny kund</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="quickpager.jquery.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

        <br />
        <h2 style="margin-left: 200px;">Skapa ny kund</h2>
        <br />



     <div id="addCust" title="Skapa ny kund" style="width: 95%; margin-left:10px;">
	<form method="get" action="create_cust.php">
          <p>
            <input type="hidden" name="cmd" value="addCust" />
            <input type="hidden" id="custType" name="custType" value="1" />

            <div id="custTabs">
                <ul>
                    <li><a href="#Ctabs-1">Grundläggande uppgifter</a></li>
                    <li><a href="#Ctabs-2">Fakturauppgifter</a></li>
                    <li><a href="#Ctabs-3">Transportnummer</a></li>
                    <li><a href="#Ctabs-4">Leveransanvisning</a></li>
                </ul>
                <div id="Ctabs-1">
                    <table style="width:100%; padding-top:0px; margin:0px;">

                      <tr>
                        <td class="white">
        	          <div id="add_kundnamn">Kundnamn:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="name" value="" tabindex="1" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Telefonnummer:
                        </td>
                        <td class="white">
                          <input type="text" name="phone" value="" tabindex="8" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
        	            <div id="alt_kundnamn">Alt. kundnamn:</div>
                        </td>
                        <td class="white">
                            <input type="text" name="alt_name" value="" tabindex="2" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Kontaktperson:
                        </td>
                        <td class="white">
                          <input type="text" name="contact" value="" tabindex="9" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_address">Leverans-adress:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_address" value="" tabindex="3" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Mobiltelefon-nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="mobile_phone" value="" tabindex="10" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_zip">Leverans-postnummer:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_zip" value="" tabindex="4" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                        </td>
                        <td class="white">
                          Kontakt e-post:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email1" value="" tabindex="11" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_city">Leverans-stad:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_city" value="" tabindex="5" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Alt kontaktperson:
                        </td>
                        <td class="white">
                          <input type="text" name="contact2" value="" tabindex="12" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Leverans land:
                        </td>
                        <td class="white">
                          <input type="text" name="del_country" value="" tabindex="6" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Alt. mobiltelefon-nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="mob2" value="" tabindex="13" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fax:
                        </td>
                        <td class="white">
                          <input type="text" name="fax" value="" tabindex="7" />
                        </td>
                        <td class="white">
                          Alt. kontakt e-post:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email2" value="" tabindex="14" />
                        </td>
                      </tr>

                    </table>

                </div>
                <div id="Ctabs-2">

                    <table style="width:100%; padding-top:0px; margin:0px;">

                      <tr>
                        <td class="white">
                          Fakturerings-kund:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_name" value="" tabindex="15" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Email:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email" value="" tabindex="20" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Att:
                        </td>
                        <td class="white">
                          <input type="text" name="att" value="" tabindex="16" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Org nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="vat_reg_no" value="" tabindex="21" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-adress:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_address" value="" tabindex="17" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Bank/Plusgiro:
                        </td>
                        <td class="white">
                          <input type="text" name="giro_no" value="" tabindex="22" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-postnummer:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_zip" value="" tabindex="18" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                        </td>
                        <td class="white">
                          Fakturerings-box:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_box" value="" tabindex="23" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-stad:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_city" value="" tabindex="19" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Fakturerings-land:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_country" value="" tabindex="24" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                      </tr>

                    </table>
                </div>
                <div id="Ctabs-3">

                    <table style="width:70%; padding-top:0px; margin:0px;">
                      <tr>
                        <td class="white">
                          Schenker:
                        </td>
                        <td class="white">
                          <input type="text" name="schenker_no" value="" tabindex="25" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          DHL:
                        </td>
                        <td class="white">
                          <input type="text" name="dhl_no" value="" tabindex="26" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Annan:
                        </td>
                        <td class="white">
                          <input type="text" name="other_no" value="" tabindex="27" />
                        </td>
                      </tr>

                    </table>

                </div>
                <div id="Ctabs-4">
                   <p>Leveransanvisningar att visas på fraktsedeln.</p>
                   <table style="width:70%; padding-top:0px; margin:0px;">
                      <tr>
                        <td class="white">
                          Rad 1:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo1" id="f_delinfo1" value="" tabindex="28" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Rad 2:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo2" id="f_delinfo2" value="" tabindex="29" />
                        </td>
                      </tr>

                    </table>

                </div>
            </div>

	    <br /><button type="submit" style="width:150px" id="save_booking" name="save_booking"><img alt="" src="../img/page_save.png"> Spara</button>
          </p>
	</form>
    </div>

</body>
</html>
