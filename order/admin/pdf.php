<?php
    include("db.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB administration - PDF:er</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="quickpager.jquery.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

        <br />
        <h2 style="margin-left: 200px;">Tilläggs PDF:er</h2>
        <br />

        <table style="margin-left: 150px;"><tr><td>
<?php

  if(isset($_POST['delete']))
  {
      @mysql_query("DELETE FROM pdfer WHERE id='{$_POST['pdf']}'");
      unlink("pdfer/dokument".$_POST['pdf'].".pdf");
  }
  if(isset($_POST['upload']))
  {
      mysql_query("INSERT INTO pdfer (name) values ('{$_POST['name']}')");
      $id = mysql_insert_id();

      $uploaddir = '../pdfer/';
      $uploadfile = $uploaddir . 'dokument'.$id.'.pdf';

      if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
          print "Filen uppladdad!<br />\n";
      } else {
          print "Misslyckades med uppladdningen!<br />\n";
          @mysql_query("DELETE FROM pdfer WHERE id='$id'");
      }

  }


  $result = mysql_query("SELECT * FROM pdfer") or die(mysql_error());
  if (mysql_num_rows($result) > 0)
  {
      print '<form method="post"><select name="pdf" style="width:300px">';
      while ($row = mysql_fetch_array($result))
      {
          print '<option value="'.$row['id'].'">'.$row['name'].'</option>';
      }
      print "</select>\n";
      print '<input type="submit" value="&#160; Ta bort &#160;" name="delete" /></form>'."\n";
  }
  else
  {
      print "            Inga PDF:er än!\n";
  }
?>

<br /><br /><br /><h2>Ladda upp ny PDF</h2>

<form enctype="multipart/form-data" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="52428800" />
    Namn: <input type="text" name="name" /><br />
    Fil: &#160; &#160; <input name="userfile" type="file" /><br />
    <input type="submit" name="upload" value=" Ladda upp " />
</form>
        </td></tr></table>


</body>
</html>
