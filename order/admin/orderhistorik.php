<?php
    include("db.php");    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB orderhistorik</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="quickpager.jquery.js"></script>
    <script type="text/javascript" src="jquery.tablesorter.min.js"></script>

  <script type="text/javascript" src="http://www.google.com/jsapi"></script>
  <script type="text/javascript">
    $(document).ready(function()
        {
            $("#myTable").tablesorter();
        }
    );
    google.load('visualization', '1', {packages: ['annotatedtimeline']});
    google.load("visualization", "1", {packages:["corechart"]});
    
<?php
    if(isset($_GET['show']))
    {
?>
    function drawVisualization() {
      var formatter = new google.visualization.DateFormat({pattern: 'dd.MM.yyyy'});
      var data = new google.visualization.DataTable();
      //data.addColumn('date', 'Datum');
      data.addColumn('string', 'Datum');
      data.addColumn('number', 'Antal artiklar');
      data.addRows(
<?php
        if(isset($_GET['artID0']))
        {
            if ($_GET['artName0']=="")
                unset($_GET['artID0']);
        }

        $query = "SELECT * FROM event WHERE type='0'";
        if ( ( $_GET['custID'] != 0 || !empty($_GET['custID']) ) && !empty($_GET['custName0']))
            $query = $query." AND cust_id='{$_GET['custID']}'";
        if ( (!empty($_GET['date1'])) && (!empty($_GET['date2'])) )
        {
            $date1 = strtotime($_GET['date1']);
            $date2 = strtotime($_GET['date2']);
            $query = $query." AND date>'$date1' AND date<'$date2'";
        }
        $query = $query." ORDER BY date DESC";
        print "//".$query."\n";
        $result = mysql_query($query) or die(mysql_error());
        $prev_date = 0;
        $art_count = 0;
        $c         = 0;
        if (mysql_num_rows($result) > 0)
        {
            print "[\n";
            while ($row = mysql_fetch_array($result))
            {
                $date = (int)$row['date'];

                $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}'") or die(mysql_error());
                if (mysql_num_rows($r2) > 0) {
                    while ($row2 = mysql_fetch_array($r2)) {
                        if ( (isset($_GET['artID0']) && $_GET['artID0'] != "") && ($_GET['artID0'] == $row2['art_id']) )
                            $art_count += $row2['art_amount'];
                        else if ( !isset($_GET['artID0']) || $_GET['artID0'] == "")
                            $art_count += $row2['art_amount'];
                    }
                }

                if (($art_count != 0) && ($date != $prev_date))
                {
                    $prev_date = $date;
                    if ($c != 0)
                        print ",\n";
                    else
                        $c = 1;
                    print "[formatter.formatValue(new Date('".date('Y-m-d',$prev_date)."')),".$art_count."]";
                    $art_count = 0;
                }
                
                $prev_date = $date;
            }
            print "\n]";
        }
?>
      );
      
        var chart = new google.visualization.ColumnChart(document.getElementById('visualization'));
        chart.draw(data, {width: window.width, height: 500, hAxis: {direction: -1}});
    }
<?php
    } elseif(isset($_GET['show_lev'])) {
?>
    function drawVisualization() {
      var formatter = new google.visualization.DateFormat({pattern: 'dd.MM.yyyy'});
      var data = new google.visualization.DataTable();
      //data.addColumn('date', 'Datum');
      data.addColumn('string', 'Datum');
      data.addColumn('number', 'Antal artiklar');
      data.addRows(
<?php
        if(isset($_GET['artID1']))
        {
            if ($_GET['artName1']=="")
                unset($_GET['artID1']);
        }

        $query = "SELECT * FROM event WHERE type='1'";
        if ( ( $_GET['cust_id1'] != 0 || !empty($_GET['cust_id1']) ) && !empty($_GET['custName1']))
            $query = $query." AND cust_id='{$_GET['cust_id1']}'";
        if ( (!empty($_GET['date_1'])) && (!empty($_GET['date_2'])) )
        {
            $date1 = strtotime($_GET['date_1']);
            $date2 = strtotime($_GET['date_2']);
            $query = $query." AND date>'$date1' AND date<'$date2'";
        }
        $query = $query." ORDER BY date DESC";
        print "//".$query."\n";
        $result = mysql_query($query) or die(mysql_error());
        $prev_date = 0;
        $art_count = 0;
        $c         = 0;
        if (mysql_num_rows($result) > 0)
        {
            print "[\n";
            while ($row = mysql_fetch_array($result))
            {
                $date = (int)$row['date'];

                $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}'") or die(mysql_error());
                if (mysql_num_rows($r2) > 0) {
                    while ($row2 = mysql_fetch_array($r2)) {
                        if ( (isset($_GET['artID1']) && $_GET['artID1'] != "") && ($_GET['artID1'] == $row2['art_id']) )
                            $art_count += $row2['art_amount'];
                        else if ( !isset($_GET['artID1']) || $_GET['artID1'] == "")
                            $art_count += $row2['art_amount'];
                    }
                }

                if (($art_count != 0) && ($date != $prev_date))
                {
                    $prev_date = $date;
                    if ($c != 0)
                        print ",\n";
                    else
                        $c = 1;
                    print "[formatter.formatValue(new Date('".date('Y-m-d',$prev_date)."')),".$art_count."]";
                    $art_count = 0;
                }
                
                $prev_date = $date;
            }
            print "\n]";
        }
?>
      );
      
        var chart = new google.visualization.ColumnChart(document.getElementById('visualization2'));
        chart.draw(data, {width: window.width, height: 500, hAxis: {direction: -1}});
    }
<?php
    }
?>
    
    google.setOnLoadCallback(drawVisualization);
  </script>

    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">

<?php

  include("dialogs.php");

?>

<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

       <ul class="paging"></ul>

<div id="tabs" style="margin: 20px; font-size: 12px;">

    <ul>
        <li><a href="#order">Orderhistorik</a></li>
        <li><a href="#lev">Orderhistorik leverantör</a></li>
    </ul>
    <div id="order">
        <div style="width:80%; margin-left: 100px; text-align: center;">
            <form method="get" action="orderhistorik.php#order">
                <table style="width:850px; padding-top:0px; margin:0px;">
                  <tr>
                    <td class="white">
                        <strong> Sök på kundnamn: </strong>
                    </td>
                    <td class="white">
                        <input type="text" name="custName0" id="custName0" value="<?php print isset($_GET['custName0']) ? $_GET['custName0'] : ''; ?>" style="width:170px; height:20px;" />
                        <input type="hidden" name="custID" id="custID" value="<?php print isset($_GET['custID']) ? $_GET['custID'] : ''; ?>" />
                    </td>
                    <td rowspan="2">
                        <div style="margin-bottom: 10px;">Begränsa visning mellan dessa datum:</div><br /> <input type="text" name="date1" id="date1" value="<?php print isset($_GET['date1']) ? $_GET['date1'] : ''; ?>" size="10" /> till <input type="text" name="date2" id="date2" value="<?php print isset($_GET['date2']) ? $_GET['date2'] : ''; ?>" size="10" />
                    </td>
                    <td class="white" rowspan="2">
                        <button name="show" type="submit"><img src="../img/page_white_edit.png" alt="" /> Visa vald orderhistorik</button><br /><br />
                    </td>
                  </tr>
                  <tr>
                    <td class="white">
                        <strong> Eller sök på produkt: </strong>
                    </td>
                    <td class="white">
                        <input type="text" name="artName0" id="artName0" value="<?php print isset($_GET['artName0']) ? $_GET['artName0'] : ''; ?>" style="width:170px; height:20px;" />
                        <input type="hidden" name="artID0" id="artID0" value="<?php print isset($_GET['artID0']) ? $_GET['artID0'] : ''; ?>" />
                    </td>
                  </tr>
                </table>
            </form>
        </div>


        <div id="visualization" style="width: 100%; height: 500px;"></div>

<?php
    if(isset($_GET['show']))
    {
        $query = "SELECT * FROM event WHERE type='0'";
        if ( ( $_GET['custID'] != 0 || !empty($_GET['custID']) ) && !empty($_GET['custName0']))
            $query = $query." AND cust_id='{$_GET['custID']}'";
        if ( (!empty($_GET['date1'])) && (!empty($_GET['date2'])) )
        {
            $date1 = strtotime($_GET['date1']);
            $date2 = strtotime($_GET['date2']);
            $query = $query." AND date>'$date1' AND date<'$date2'";
        }
        $query = $query." ORDER BY date ASC";
        $result = mysql_query($query) or die(mysql_error());
        
        $outp_buffer  = "";
        $total_amount = 0;
        $total_weight = 0;
        $tmp_weight   = 0;

        if (mysql_num_rows($result) > 0)
        {
            while ($row = mysql_fetch_array($result))
            {
                $date = (int)$row['date'];

                if (isset($_GET['artID0']))
                    $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}' AND art_id='{$_GET['artID0']}'") or die(mysql_error());
                else
                    $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}'") or die(mysql_error());
                if (mysql_num_rows($r2) > 0) {
                    while ($row2 = mysql_fetch_array($r2))
                    {
                        $outp_buffer .= "<tr>\n";
                        $outp_buffer .= "    <td>".date('Y-m-d',$date)."</td>\n";
                        if (isset($_GET['artID0']))
                        {
                            $art_name = @mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$_GET['artID0']}'"), 0);
                            $outp_buffer .= '    <td><a href="../index.php?tmstp='.$date.'&showEvent='.$row['id'].'">'.$art_name.'</a></td>'."\n";
                        }
                        else
                        {
                            $art_name = @mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$row2['art_id']}'"), 0);
                            $outp_buffer .= '    <td><a href="../index.php?tmstp='.$date.'&showEvent='.$row['id'].'">'.$art_name.'</a></td>'."\n";
                        }
                        $outp_buffer .= "    <td>".$row2['art_amount']."</td>\n";
                        $total_amount += $row2['art_amount'];
                        $tmp_weight   =  @mysql_result(mysql_query("SELECT weight FROM art WHERE id='{$row2['art_id']}'"), 0);
                        $tmp_weight   =  $tmp_weight * $row2['art_amount'];
                        $outp_buffer .= "    <td>".$tmp_weight."</td>\n";
                        $total_weight += $tmp_weight;
                        $outp_buffer  .= "    <td>".@mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['cust_id']}'"), 0)."</td>\n";
                        $outp_buffer  .= "</tr>\n";
                    }
                }

            }
            print "<table id=\"myTable\" class=\"tablesorter\">\n";
            print "<thead>\n";
            print "<tr>\n";
            print "    <th>Datum</th>\n";
            print "    <th>Artikel</th>\n";
            print "    <th>Antal (totalt <big>$total_amount</big>)</th>\n";
            print "    <th>Vikt (totalt <big>$total_weight</big>)</th>\n";            
            print "    <th>Kundnamn</th>\n";
            print "</tr>\n";
            print "</thead>\n";
            print "<tbody>\n";
            print $outp_buffer;
            print "</tbody>\n";
            print "</table>\n";
        }
  }
?>

        <div style="clear:both;"></div>
    </div>
    
    <div id="lev">
        <div style="width:80%; margin-left: 100px; text-align: center;">
            <form method="get" action="orderhistorik.php#lev">
                <table style="width:850px; padding-top:0px; margin:0px;">
                  <tr>
                    <td class="white">
                        <strong> Sök på kundnamn: </strong>
                    </td>
                    <td class="white">
                        <input type="text" name="custName1" id="custName1" value="<?php print isset($_GET['custName1']) ? $_GET['custName1'] : ''; ?>" style="width:170px; height:20px;" />
                        <input type="hidden" name="cust_id1" id="cust_id" value="<?php print isset($_GET['cust_id1']) ? $_GET['cust_id1'] : ''; ?>" />
                    </td>
                    <td rowspan="2">
                        <div style="margin-bottom: 10px;">Begränsa visning mellan dessa datum:</div><br /> <input type="text" name="date_1" id="date_1" value="<?php print isset($_GET['date_1']) ? $_GET['date_1'] : ''; ?>" size="10" /> till <input type="text" name="date_2" id="date_2" value="<?php print isset($_GET['date_2']) ? $_GET['date_2'] : ''; ?>" size="10" />
                    </td>
                    <td class="white" rowspan="2">
                        <button name="show_lev" type="submit"><img src="../img/page_white_edit.png" alt="" /> Visa vald orderhistorik</button><br /><br />
                    </td>
                  </tr>
                  <tr>
                    <td class="white">
                        <strong> Eller sök på produkt: </strong>
                    </td>
                    <td class="white">
                        <input type="text" name="artName1" id="artName1" value="<?php print isset($_GET['artName1']) ? $_GET['artName1'] : ''; ?>" style="width:170px; height:20px;" />
                        <input type="hidden" name="artID1" id="artID1" value="<?php print isset($_GET['artID1']) ? $_GET['artID1'] : ''; ?>" />
                    </td>
                  </tr>
                </table>
            </form>
        </div>


        <div id="visualization2" style="width: 100%; height: 500px;"></div>

<?php
  if(isset($_GET['show_lev']))
  {
        $query = "SELECT * FROM event WHERE type='1'";
        if ( ( $_GET['cust_id1'] != 0 || !empty($_GET['cust_id1']) ) && !empty($_GET['custName1']))
            $query = $query." AND cust_id='{$_GET['cust_id1']}'";
        if ( (!empty($_GET['date_1'])) && (!empty($_GET['date_2'])) )
        {
            $date1 = strtotime($_GET['date_1']);
            $date2 = strtotime($_GET['date_2']);
            $query = $query." AND date>'$date1' AND date<'$date2'";
        }
        $query = $query." ORDER BY date ASC";
        $result = mysql_query($query) or die(mysql_error());
        
        $outp_buffer  = "";
        $total_amount = 0;
        $total_weight = 0;
        $tmp_weight   = 0;

        if (mysql_num_rows($result) > 0)
        {
            while ($row = mysql_fetch_array($result))
            {
                $date = (int)$row['date'];

                if (isset($_GET['artID1']))
                    $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}' AND art_id='{$_GET['artID1']}'") or die(mysql_error());
                else
                    $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}'") or die(mysql_error());
                if (mysql_num_rows($r2) > 0) {
                    while ($row2 = mysql_fetch_array($r2))
                    {
                        $outp_buffer .= "<tr>\n";
                        $outp_buffer .= "    <td>".date('Y-m-d',$date)."</td>\n";
                        if (isset($_GET['artID1']))
                        {
                            $art_name = @mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$_GET['artID1']}'"), 0);
                            $outp_buffer .= '    <td><a href="../index.php?tmstp='.$date.'&showEvent='.$row['id'].'">'.$art_name.'</a></td>'."\n";
                        }
                        else
                        {
                            $art_name = @mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$row2['art_id']}'"), 0);
                            $outp_buffer .= '    <td><a href="../index.php?tmstp='.$date.'&showEvent='.$row['id'].'">'.$art_name.'</a></td>'."\n";
                        }
                        $outp_buffer .= "    <td>".$row2['art_amount']."</td>\n";
                        $total_amount += $row2['art_amount'];
                        $tmp_weight   =  @mysql_result(mysql_query("SELECT weight FROM art WHERE id='{$row2['art_id']}'"), 0);
                        $tmp_weight   =  $tmp_weight * $row2['art_amount'];
                        $outp_buffer .= "    <td>".$tmp_weight."</td>\n";
                        $total_weight += $tmp_weight;
                        $outp_buffer  .= "    <td>".@mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['cust_id']}'"), 0)."</td>\n";
                        $outp_buffer  .= "</tr>\n";
                    }
                }

            }
            print "<table id=\"myTable\" class=\"tablesorter\">\n";
            print "<thead>\n";
            print "<tr>\n";
            print "    <th>Datum</th>\n";
            print "    <th>Artikel</th>\n";
            print "    <th>Antal (totalt <big>$total_amount</big>)</th>\n";
            print "    <th>Vikt (totalt <big>$total_weight</big>)</th>\n";            
            print "    <th>Kundnamn</th>\n";
            print "</tr>\n";
            print "</thead>\n";
            print "<tbody>\n";
            print $outp_buffer;
            print "</tbody>\n";
            print "</table>\n";
        }
  }
?>

        <div style="clear:both;"></div>
    </div>
</div>



</body>
</html>
