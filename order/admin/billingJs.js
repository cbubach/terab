/**
 * Faktura-JS
 */

function postFormAJAXbill (form)
{
    var query = $(form).serialize();
    var url = form.action + '?' + query;
    $.get(url, function (data) {
        console.log(data);
        $('#addBill').dialog('close');
        window.location = "/terab/order/admin/faktura.php?billId=" + data;
    });
}

function billAction(id)
{
    $("#hiddenBillId").val(id);
    $( "#billAction" ).dialog({ height: 250, width: 220 });
}

function billRedirect(billId)
{
    var billId = billId || $("#hiddenBillId").val();
    window.open(
        "/terab/order/admin/faktura.php?billId="+billId,
        '_blank'
    );
}

function moveBillingRowDown(elem) {
    var row = $(elem).closest('tr').get();
    
    if ($(row).nextAll('tr.orderRow').length > 0) {
        $(row).insertAfter($(row).nextAll('tr.orderRow'));
    }
}

function billStatusChange()
{
    var id = $("#hiddenBillId").val();
    $.get('cmd.php?cmd=billStatusChange&id='+id, function(data) {
        $('#billAction').dialog('close');
        window.location = window.location.pathname;
    });
}

function billDelete()
{
    $('  <div id="billingDelete-confirm" title="Ta bort fakturan?">' +
    '        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' +
    '        Fakturan kommer tas bort och går inte att återställa. Är du säker?</p> ' +
    '    </div>').dialog({
        resizable: false,
        height: 160,
        modal: true,
        buttons: {
            "Radera": function() {
                var id = $("#hiddenBillId").val();
                $.get('cmd.php?cmd=billDelete&id='+id, function(data) {
                    $('#billAction').dialog('close');
                    $( this ).dialog( "close" );
                    window.location = window.location.pathname;
                });
            },
            "Avbryt": function() {
                $('#billAction').dialog('close');
                $( this ).dialog( "close" );
            }
        }
    });
}

function billingArtComplete(event_type, indexLocation)
{
    indexLocation = indexLocation || "../index.php?auto=";
    var auto_no = event_type + 3; // auto 3 or 4
    $( ".billArtComplete" ).autocomplete({
        source: indexLocation + auto_no,
        minLength: 1,
        select: function( event, ui ) {
            var id = $(this).attr('id');
            var tmparr = id.split("");
            tmpaarr = tmparr.splice(0,11);     //changed to fit ID/name
            id = tmparr.join("");
            var name = 'billArtId' + id;
            document.getElementById(name).value = ui.item.id;
            this.value = ui.item.id;
            var custId = $("#billCustId").val();
            $.get("cmd.php?cmd=getPrice&artNo="+ui.item.id+'&custId='+custId, function(data){
                $("#billArtPrice"+id).val(data);
            });
        }
    });
}

function addBillArtForm(event_type)
{
    var count = $('.billArtComplete').length;
    var html =  '<tr class="orderRow"><td><input type="text" style="width:40px; margin-right:1px;" name="artAmount[]" value="" /></td> ';
    html +=  '<td><input type="text" style="width:40px; margin-right:1px;" name="artUnit[]" value="st" /></td> ';
    html +=  '<td><input type="text" class="billArtComplete" style="width:200px" id="billArtName'+count+'" name="artName[]" value="" /></td> ';
    html +=  '<td><input type="text" style="width:40px; margin-right:1px;"  name="artVAT[]" value="25" /></td> ';
    html +=  '<td><input type="text" style="width:40px; margin-right:1px;" id="billArtPrice'+count+'" name="artPrice[]" value="" /></td> ';
    html +=  '<td><input type="hidden" id="billArtId'+count+'" name="art_ID[]" value="" />';
    html +=  '<span onclick="moveBillingRowDown(this);"><img src="/TERAB/order/img/down.png"></span></td>';
    html +=  '</tr>';
    $("#billRowTable").append(html);
    var indexLocation = "../index.php?auto=";
    if (jQuery('#printCargoList').length) {
        indexLocation = "index.php?auto=";
    }
    billingArtComplete(event_type, indexLocation);
}

function billEdit()
{
    var bill_id = $("#hiddenBillId").val();
    var url = '/TERAB/order/admin/billingDialog.php?edit_bill_id=' + bill_id;
    $.get(url, function (data) {
        document.getElementById('addBill').innerHTML = data;

        event_type = $('#billEventType').val();
        $("#billDate" ).datepicker({
            showWeek: true,
            dateFormat: 'yy-mm-dd'

        });
        billingArtComplete(event_type);
        $("#addBill").dialog({ height: 480, width: 800, "title": "Redigera faktura" });

        var auto_no = event_type + 1; // auto 1 or 2
        $("#billCustName").autocomplete({
            source: "../index.php?auto="+auto_no,
            minLength: 1,
            select: function( event, ui ) {
                document.getElementById('billCustId').value = ui.item.id;
            }
        });
    });
}

function addBill(event_type)
{
    $( "#billDate" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
    });
    $('#billEventType').val(event_type);
    $("div#addBill").attr("title", "Skapa ny faktura");
    $("#add_kundnamn").html("Kundnamn:");
    $("#add_address").html("Leverans-adress:");
    $("#add_zip").html("Leverans-postnummer:");
    $("#add_city").html("Leverans-stad:");
    $("#alt_kundnamn").html("Alt. kundnamn:");
    $("#addBill" ).dialog({ height: 480, width: 800, "title": "Skapa ny faktura" });

    // init jQuery autocomplete on customer name
    var auto_no = event_type + 1; // auto 1 or 2
    $( "#billCustName").autocomplete({
        source: "../index.php?auto="+auto_no,
        minLength: 1,
        select: function( event, ui ) {
            document.getElementById('billCustId').value = ui.item.id;
        }
    });
    billingArtComplete(event_type);
}

function showBillingPopup(event_type, waybillid, ref, date)
{
    var waybillid = waybillid || 0;
    var url = 'admin/billingDialog.php?billEventType=' + event_type + '&create_bill=1&waybill_id=' + waybillid;
    if (typeof(ref) !== "undefined" && ref !== null ) {
        url += "&ref="+ref;
    }
    if (typeof(date) !== "undefined" && date !== null ) {
        url += "&billDate="+date;
    }
    $.get(url, function (data) {
        document.getElementById('addBill').innerHTML = data;
        $( "#billDate" ).datepicker({
            showWeek: true,
            dateFormat: 'yy-mm-dd'
        });
        billingArtComplete(event_type, "index.php?auto=");
    });
    $("#addBill" ).dialog({ height: 480, width: 800, "title": "Skapa ny faktura" });
}