<?php
    include("db.php");

    if (isset($_GET['fno']))
    {
        mysql_query("UPDATE settings SET value='{$_GET['fno']}' WHERE name='fno'") or die(mysql_error());
        mysql_query("UPDATE settings SET value='{$_GET['fno_amount']}' WHERE name='fno_amount'") or die(mysql_error());
        mysql_query("UPDATE settings SET value='{$_GET['fno_current']}' WHERE name='fno_current'") or die(mysql_error());
        header("Location: /order/admin/fno.php");
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB administration - Fraktsedelnummer</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="quickpager.jquery.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

        <br />
        <h2 style="margin-left: 200px;">Hantera fraktsedel-nummerserie</h2>
        <br />

        <?php

          $fno         = mysql_result(mysql_query("SELECT value FROM settings WHERE name='fno'"), 0);
          $fno_amount  = mysql_result(mysql_query("SELECT value FROM settings WHERE name='fno_amount'"), 0);
          $fno_current = mysql_result(mysql_query("SELECT value FROM settings WHERE name='fno_current'"), 0);

        ?>

        <div style="width:500px; margin-left: 100px; text-align: center;">
	<form method="get" action="fno.php">

            <label for="fno" style="display:block; width: 300px;">
                <span style="float:left; width:45%;">Start på nummerserie<br />(9 siffor)</span>
                <input style="width:45%;" type="text" name="fno" id="fno" value="<?php print $fno; ?>" />
            </label>   <br />
            <label for="fno_amount" style="display:block; width: 300px;">
                <span style="float:left; width:45%;">Antal nummer i serie</span>
                <input style="width:45%;" type="text" name="fno_amount" id="fno_amount" value="<?php print $fno_amount; ?>" />
            </label>   <br />
            <label for="fno_current" style="display:block; width: 300px;">
                <span style="float:left; width:45%;">Nuvarande nummer<br />(9 siffor)</span>
                <input style="width:45%;" type="text" name="fno_current" id="fno_current" value="<?php print $fno_current; ?>" />
            </label>

            <br />
            <button name="save" type="submit"><img src="../img/page_save.png" alt="" /> Spara &#160;</button>

        </form>
        </div>

</body>
</html>
