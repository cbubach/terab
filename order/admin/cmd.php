<?php
//-------------------------------------------
//
//    TERAB booking
//
//      File that handles all AJAX and
//      form requests.
//
//   (c)2011, Christoffer Bubach - weboo.se
//
//-------------------------------------------

    include("db.php");
//--------------------------------------
//  Handle all FORM and AJAX requests :)
//--------------------------------------
if (isset($_GET['cmd']))
{
    if ($_GET['cmd'] == "getCustData")
    {
        $result = mysql_query("SELECT * FROM cust WHERE id='{$_GET['cust_id']}'") or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
            header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
            header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header ("Pragma: no-cache"); // HTTP/1.0
            header ("Content-Type: application/json; charset=utf-8");

            print "[";

            while ($row = mysql_fetch_array($result))
            {
                print '{"id": "'.$row['id'].'", "name": "'.$row['name'].'", "del_address": "'.$row['del_address'].'", "del_zip": "'.$row['del_zip'].'", "del_city": "'.$row['del_city'].'", ';
                print '"bill_name": "'.$row['bill_name'].'", "bill_address": "'.$row['bill_address'].'", "bill_zip": "'.$row['bill_zip'].'", "bill_box": "'.$row['bill_box'].'", ';
                print '"bill_city": "'.$row['bill_city'].'", "phone": "'.$row['phone'].'", "mobile_phone": "'.$row['mobile_phone'].'", "contact": "'.$row['contact'].'", "contact_email": "'.$row['contact_email'].'", ';
                print '"fax": "'.$row['fax'].'", "vat_reg_no": "'.$row['vat_reg_no'].'", "giro_no": "'.$row['giro_no'].'", "type": "'.$row['type'].'", "alt_name": "'.$row['alt_name'].'", "del_country": "'.$row['del_country'].'", ';
                print '"att": "'.$row['att'].'", "bill_country": "'.$row['bill_country'].'", "contact_email1": "'.$row['contact_email1'].'", "contact2": "'.$row['contact2'].'", "mob2": "'.$row['mob2'].'", ';
                print '"contact_email2": "'.$row['contact_email2'].'", "schenker_no": "'.$row['schenker_no'].'", "dhl_no": "'.$row['dhl_no'].'", "other_no": "'.$row['other_no'].'", ';
                print '"f_delinfo1": "'.$row['f_delinfo1'].'", "f_delinfo2": "'.$row['f_delinfo2'].'"}';
            }
            print "]";
        }
        exit;
    }
    elseif($_GET['cmd'] == 'billStatusChange')
    {
        $bill_id = $_GET['id'];
        if(mysql_result(mysql_query("SELECT `active` FROM `bill` WHERE `id` = '".$bill_id."'"),0) == 0)
            mysql_query("UPDATE `bill` SET `active` = '1' WHERE `id` = '".$bill_id."'");  
        else
            mysql_query("UPDATE `bill` SET `active` = '0' WHERE `id` = '".$bill_id."'");
        exit;
    }
    elseif($_GET['cmd'] == 'billDelete')
    {
        $bill_id = $_GET['id'];
        $selectedBillingNo  = intval(mysql_result(mysql_query("SELECT `order_id` FROM `bill` WHERE `id`='".$bill_id."'"), 0));
        $dbCurrentBillingNo = intval(mysql_result(mysql_query("SELECT `bill_number` FROM `bill_settings` WHERE `id`=0"), 0));
        if ($selectedBillingNo == $dbCurrentBillingNo) {
            mysql_query("UPDATE `bill_settings` SET `bill_number` = `bill_number` - 1 WHERE id=0");
        }
        mysql_query("DELETE FROM `bill` WHERE `id` = '".$bill_id."'");  
        mysql_query("DELETE FROM `bill_item` WHERE `bill_id` = '".$bill_id."'");
        exit;
    }
    elseif ($_GET['cmd'] == "getPrice")
    {
        if(!empty($_GET['custId']))
            $custId = intval($_GET['custId']);
        else
            $custId = 0;


        $price = @mysql_result(mysql_query("SELECT `price` FROM `price` WHERE `cust_id`=".$custId." AND `art_id`=".intval($_GET['artNo'])), 0);

        if(empty($price))
            $price = mysql_result(mysql_query("SELECT `price` FROM `price` WHERE `cust_id`=0 AND `art_id`=".intval($_GET['artNo'])), 0);

        print $price;
        exit;
    }
    elseif($_GET['cmd'] == "getEvents") {
        $result = mysql_query("SELECT id, date FROM event WHERE cust_id='{$_GET['cust_id']}' ORDER BY date DESC");
        //$json = array();
        if (mysql_num_rows($result) > 0) {
            while( $row = mysql_fetch_array($result)) {
                $wbres = mysql_query("SELECT id FROM waybill WHERE name='feventId' AND value='{$row['id']}'");
				if (mysql_num_rows($wbres) > 0) {
				    $waybill_id = @mysql_result($wbres,0);
                    print "<a href=\"billing.php?create_bill=yes&waybill_id=".$waybill_id."\">".date('Y-m-d H:m:s', $row['date'])."</a><br>";
				}
                #$json[] = $row;
            }
        }
        //header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        //header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
        //header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        //header ("Pragma: no-cache"); // HTTP/1.0
        //header ("Content-Type: application/json; charset=utf-8");
        //echo json_encode($json);
    }
    elseif ($_GET['cmd'] == "savePrices")
    {
        if ($_GET['custID'] != 0 && $_GET['art_no'] != 0)
        {
            for ($i=1; $i < $_GET['art_no']+1; $i++)
            {
                $artID = $_GET['art'.$i];
                $artP = str_replace(",", ".", $_GET['artP'.$i]);
                if ($artP == "0") {
                    mysql_query("DELETE FROM price WHERE cust_id='{$_GET['custID']}' AND art_id='{$artID}'") or die(mysql_error());
                }
                else
                {
                    $res = mysql_query("SELECT * FROM price WHERE cust_id='{$_GET['custID']}' AND art_id='{$artID}'");
                    if ( mysql_num_rows($res) > 0 )
                        mysql_query("UPDATE price SET price='{$artP}' WHERE cust_id='{$_GET['custID']}' AND art_id='{$artID}'") or die(mysql_error());
                    else
                        mysql_query("INSERT INTO price SET cust_id='{$_GET['custID']}', art_id='{$artID}', price='{$artP}'") or die(mysql_error());
                }
            }
        }
        exit;
    }
    elseif ($_GET['cmd'] == "addArtPrice")
    {
        if ($_GET['custID'] != 0 && $_GET['artID'] != 0)
        {
            $price = mysql_result(mysql_query("SELECT price FROM price WHERE cust_id=0 AND art_id='{$_GET['artID']}'"),0);
            mysql_query("INSERT INTO price SET cust_id='{$_GET['custID']}', art_id='{$_GET['artID']}', price='$price'") or die(mysql_error());
        }
        exit;
    }
    elseif ($_GET['cmd'] == "editCust")
    {
        //$GET = array_map('utf8_decode', $_GET);  // Fix jQuery UTF-8

        // get original cust data to compare against customer db for UPDATE below.
        // customer ID's isn't the same, so have to compare all fields and find one ID to fit.
        //
        //    SELECT t1.id FROM `cust` AS t1, customers AS t2 WHERE t1.name = t2.name..... and so on...
        
        $sql_query =  "SELECT t1.id FROM `customers` AS t1, `cust` AS t2";
        $sql_query .= " WHERE t1.name = t2.name";
        $sql_query .= " AND t1.address = t2.del_address";
        $sql_query .= " AND t1.zip = t2.del_zip";
        $sql_query .= " AND t1.city = t2.del_city";
        $sql_query .= " AND t1.contact_person = t2.contact";
        $sql_query .= " AND t1.contact_phone = t2.phone";
        $sql_query .= " AND t1.contact_email = t2.contact_email";
        $sql_query .= " AND t1.contact_fax = t2.fax";
        $sql_query .= " AND t1.vat_reg_no = t2.vat_reg_no";
        $sql_query .= " AND t1.bankgiro_no = t2.giro_no";
        $sql_query .= " AND t2.id = '".$_GET['custIDE']."'";
        
        $old_id = mysql_result(mysql_query($sql_query), 0);

        $query  = "UPDATE cust SET ";
        $query .= "name='{$_GET['nameE']}', del_address='{$_GET['del_addressE']}', del_zip='{$_GET['del_zipE']}', del_city='{$_GET['del_cityE']}', bill_name='{$_GET['bill_nameE']}', ";
        $query .= "bill_address='{$_GET['bill_addressE']}', bill_zip='{$_GET['bill_zipE']}', bill_box='{$_GET['bill_boxE']}', bill_city='{$_GET['bill_cityE']}', phone='{$_GET['phoneE']}', ";
        $query .= "mobile_phone='{$_GET['mobile_phoneE']}', contact='{$_GET['contactE']}', contact_email='{$_GET['contact_emailE']}', fax='{$_GET['faxE']}', vat_reg_no='{$_GET['vat_reg_noE']}', giro_no='{$_GET['giro_noE']}', ";
        $query .= "alt_name='{$_GET['alt_nameE']}', del_country='{$_GET['del_countryE']}', att='{$_GET['attE']}', bill_country='{$_GET['bill_countryE']}', contact_email1='{$_GET['contact_email1E']}', ";
        $query .= "contact2='{$_GET['contact2E']}', mob2='{$_GET['mob2E']}', contact_email2='{$_GET['contact_email2E']}', schenker_no='{$_GET['schenker_noE']}', dhl_no='{$_GET['dhl_noE']}', other_no='{$_GET['other_noE']}', ";
        $query .= "f_delinfo1='{$_GET['f_delinfo1E']}', f_delinfo2='{$_GET['f_delinfo2E']}'";
        $query .= "WHERE id='{$_GET['custIDE']}'";

        //  UPDATE 'customer'-table here with ID found above....    <----------------  !!!
	$sql = "UPDATE customers SET name='".$_GET['nameE']."', address='".$_GET['del_addressE']."', zip='".$_GET['del_zipE']."', city='".$_GET['del_cityE']."', contact_person='".$_GET['contactE']."', contact_phone='".$_GET['phoneE']."', contact_email='".$_GET['contact_emailE']."', contact_fax='".$_GET['faxE']."', vat_reg_no='".$_GET['vat_reg_noE']."', bankgiro_no='".$_GET['giro_noE']."' WHERE id='".$old_id."'";
	mysql_query($sql);

        mysql_query($query) or die(mysql_error());
        header ("Location: cust.php");
        exit;
    }
    elseif ($_GET['cmd'] == "getArtData")
    {
        $result = mysql_query("SELECT * FROM art WHERE id='{$_GET['id']}'") or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
            header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
            header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header ("Pragma: no-cache"); // HTTP/1.0
            header ("Content-Type: application/json; charset=utf-8");

            print "[";

            while ($row = mysql_fetch_array($result))
            {
                $result2 = mysql_query("SELECT price FROM price WHERE art_id='{$_GET['id']}' AND cust_id='0'") or die(mysql_error());
                if (mysql_num_rows($result2) > 0)
                {
                    $price = str_replace(".", ",", mysql_result($result2, 0));
                }
                else
                {
                    $price = 0;
                }
                print '{"id": "'.$row['id'].'", "art_name": "'.$row['art_name'].'", "art_no": "'.$row['art_no'].'", "weight": "'.$row['weight'].'", ';
                print '"price": "'.$price.'", "img": "'.$row['img'].'"}';
            }
            print "]";
        }
        exit;
    }
    elseif ($_GET['cmd'] == "editArt")
    {
        //$GET = array_map('utf8_decode', $_GET);  // Fix jQuery UTF-8

        $query  = "UPDATE art SET ";
        $query .= "art_no='{$_GET['art_noE']}', art_name='{$_GET['art_nameE']}', weight='{$_GET['weightE']}', img='{$_GET['art_imgE']}' ";
        $query .= "WHERE id='{$_GET['artIDE']}'";

        mysql_query($query) or die(mysql_error());

        $result = mysql_query("SELECT price FROM price WHERE art_id='{$_GET['artIDE']}' AND cust_id='0'") or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
            $price = str_replace(",", ".", $_GET['priceE']);
            mysql_query("UPDATE `price` SET price='{$price}' WHERE cust_id='0' AND art_id='{$_GET['artIDE']}'") or die(mysql_error());
        }
        else
        {
            $price = str_replace(",", ".", $_GET['priceE']);
            mysql_query("INSERT INTO `price` (`cust_id`, `art_id`, `price`) VALUES ('0', '{$_GET['artIDE']}', '{$price}')") or die(mysql_error());
        }
        header ("Location: cust.php");
        exit;
    }
    elseif ($_GET['cmd'] == "CustEmail")
    {
        if (isset($_GET['type']))
        {
            $result = mysql_query("SELECT contact_email, contact_email1, contact_email2 FROM cust WHERE type='{$_GET['type']}'") or die(mysql_error());
            if (mysql_num_rows($result) > 0)
            {
                $first = 1;
                header('Content-type: text/plain');
                if ($_GET['type'] == 0)
                    header('Content-disposition: attachment;filename=kunder.txt');
                else
                    header('Content-disposition: attachment;filename=levantorer.txt');
                while ($row = mysql_fetch_array($result))
                {
                    if ($row['contact_email'] != "") {
                        if ($first == 1) {
                            $first = 0;
                            print trim($row['contact_email']);
                        }
                        else
                            print ", ".trim($row['contact_email']);
                    }
                    if ($row['contact_email1'] != "") {
                        if ($first == 1) {
                            $first = 0;
                            print trim($row['contact_email1']);
                        }
                        else
                            print ", ".trim($row['contact_email1']);
                    }
                    if ($row['contact_email2'] != "") {
                        if ($first == 1) {
                            $first = 0;
                            print trim($row['contact_email2']);
                        }
                        else
                            print ", ".trim($row['contact_email2']);
                    }
                }
            }
        }
    }
}
