<?php

  require_once("db.php");

    if ( isset($_POST['save_settings']) ) {
        $billId = 0;
        $result = mysql_query("SELECT order_id FROM bill ORDER BY order_id DESC LIMIT 1") or die(mysql_error());
        if (mysql_num_rows($result) > 0) {
            $billId = mysql_result($result, 0);
        }
        mysql_query("UPDATE bill_settings SET bill_number='{$_POST['bill_number']}', company='{$_POST['company']}', due='{$_POST['due']}',
                      reference='{$_POST['reference']}', swift='{$_POST['swift']}', iban='{$_POST['iban']}',
                      vat_reg_no='{$_POST['vat']}', address='{$_POST['address']}', tel='{$_POST['tel']}', giro='{$_POST['giro']}',logo_file='{$_POST['logo_file']}',
                      fax='{$_POST['fax']}', homepage='{$_POST['homepage']}', email='{$_POST['email']}', company_hq='{$_POST['company_hq']}' WHERE id=0") or die(mysql_error());
        $url = "billing.php?saved=1";
        if ($billId != $_POST['bill_number']) {
            $url .= "&mismatch=1";
        }
        header("Location: ".$url);
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title>TERAB administration - Fakturor</title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="../jquery.populate.js"></script>
    <script type="text/javascript" src="jquery.listnav.pack-2.1.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />
 <?php 
    if (isset($_GET['create_bill'])) {
        print "<body onload=\"javascript:addBill(0)\">";
    }
?>
</head>
<body style="margin:0px; padding:0px;">

    <!--
        Show dialog for adding an waybill
     -->
    <script type="text/javascript" src="billingJs.js"></script>
    <div id="addBill" title="Skapa ny faktura" style="display: none;">
        <?php
        $_GET['billEventType'] = 0;
        include "billingDialog.php";
        ?>
    </div>

    <div id="billAction" title="Alternativ" style="display: none;">
        <input id="hiddenBillId" type="hidden" />
        <button name="show_bill" onclick="billRedirect()" style="padding:10px; width:100%;">&nbsp; Visa fakturan &nbsp;</button><br /><br />
        <button name="edit_bill" onclick="billEdit()" style="padding:10px; width:100%;">&nbsp; Redigera fakturan &nbsp;</button><br /><br />
        <button name="show_bill" onclick="billDelete()" style="padding:10px; width:100%;">&nbsp; Radera fakturan &nbsp;</button><br /><br />
        <button name="change_bill_status" onclick="billStatusChange()" style="padding:10px; width:100%;">&nbsp; Ändra till betald/obetald &nbsp;</button>
    </div>

<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<div id="tabs" style="margin: 20px; font-size: 12px;">
    <ul>
        <li><a href="#tabs-1">Obetalda fakturor</a></li>
        <li><a href="#tabs-2">Betalda fakturor</a></li>
        <li><a href="#tabs-3">Fakturainställningar</a></li>
    </ul>
    <div style="margin-top:-32px; margin-right: 10px; margin-left:10px;"><button onclick="addBill(0);"><img class="vmiddle" style="padding-right: 10px;" src="../img/add.png">Ny faktura</button></div>
    <div id="tabs-1">

        <ul id="myList1" class="paging">

<?php

  $result = mysql_query("SELECT * FROM bill WHERE active='1' AND status!='1' ORDER BY id DESC") or die(mysql_error());

  if (mysql_num_rows($result) > 0)
  {
      while ($row = mysql_fetch_array($result))
      {   
          $customerName = @mysql_result(mysql_query("SELECT `name` FROM `cust` WHERE `id`=".$row['cust_id']), 0);
          print '            <li class="ui-widget-content"><a href="javascript:void(0);" onclick="billAction(\''.$row['id'].'\');" class="listHover">'.$customerName." - (".$row['order_id'].")</a></li>\n";
      }
  }
  else
  {
      print "            <li>Inga obetalda fakturor än!</li>\n";
  }

?>
        </ul>
        <div style="clear:both;"></div>
    </div>
    <div id="tabs-2">
        <ul id="myList2" class="paging">
<?php

  $result = mysql_query("SELECT * FROM bill WHERE active='0' AND status!='1' ORDER BY date DESC") or die(mysql_error());

  if (mysql_num_rows($result) > 0)
  {
      while ($row = mysql_fetch_array($result))
      {
            $customerName = mysql_result(mysql_query("SELECT `name` FROM `cust` WHERE `id`=".$row['cust_id']), 0);
          print '            <li class="ui-widget-content"><a href="javascript:void(0);" onclick="billAction(\''.$row['id'].'\');" class="listHover">'.$customerName." - (".$row['order_id'].")</a></li>\n";
      }
  }
  else
  {
      print "            <li>Inga fakturor arkiverade än!</li>\n";
  }

?>
        </ul>
        <div style="clear:both;"></div>
    </div>

    <div id="tabs-3">

       <br>
        <h2 style="margin-left: 200px;">Allmänna uppgifter och löpnummer för faktura</h2>
        <br>



                <?php

                  //hämta grundinställningar för fakturaavsändare
                  
                  $settings = mysql_fetch_array(mysql_query("SELECT * FROM bill_settings WHERE id=0"));
                ?>

        <form method="post" action="billing.php">
            <div style="width:500px; margin-left: 300px; text-align: center;">
                <label for="bill_number" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Aktuellt fakturanummer</span>
                    <input style="width:45%;" name="bill_number" id="bill_number" value="<?php echo $settings['bill_number']; ?>" type="text">
                </label>   <br>
            </div>
            <div style="width:400px; margin-left: 100px; float: left; text-align: center;">
                <label for="company" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Företagsnamn</span>
                    <input style="width:45%;" name="company" id="company" value="<?php echo $settings['company']; ?>" type="text">
                </label>   <br>
                <label for="due" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Betalningsvillkor</span>
                    <input style="width:32%;" name="due" id="due" value="<?php echo $settings['due']; ?>" type="text"> dagar.
                </label>   <br>
                <label for="reference" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Referens</span>
                    <input style="width:45%;" name="reference" id="reference" value="<?php echo $settings['reference']; ?>" type="text">
                </label>   <br>
                <label for="swift" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Swift</span>
                    <input style="width:45%;" name="swift" id="swift" value="<?php echo $settings['swift']; ?>" type="text">
                </label>   <br>
                <label for="iban" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">IBAN</span>
                    <input style="width:45%;" name="iban" id="iban" value="<?php echo $settings['iban']; ?>" type="text">
                </label>   <br>
                <label for="vat" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Org. nr.</span>
                    <input style="width:45%;" name="vat" id="vat" value="<?php echo $settings['vat_reg_no']; ?>" type="text">
                </label>
                <br>
                <label for="giro" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Bank/Post-gironummer</span>
                    <input style="width:45%;" name="giro" id="giro" value="<?php echo $settings['giro']; ?>" type="text">
                </label>   <br>
            </div>
            <div style="width:400px; float: left; text-align: center;">
                <label for="address" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Adress</span>
                    <textarea style="width:45%;" rows="6" name="address" id="address"><?php echo $settings['address']; ?></textarea>
                </label>   <br>
                <label for="tel" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Telefon</span>
                    <input style="width:45%;" name="tel" id="tel" value="<?php echo $settings['tel']; ?>" type="text">
                </label>   <br>
                <label for="fax" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Fax</span>
                    <input style="width:45%;" name="fax" id="fax" value="<?php echo $settings['fax']; ?>" type="text">
                </label>   <br>
                <label for="homepage" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Hemsida</span>
                    <input style="width:45%;" name="homepage" id="homepage" value="<?php echo $settings['homepage']; ?>" type="text">
                </label>   <br>
                <label for="email" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Email</span>
                    <input style="width:45%;" name="email" id="email" value="<?php echo $settings['email']; ?>" type="text">
                </label>   <br>
                <label for="company_hq" style="display:block; width: 300px;">
                    <span style="float:left; width:45%;">Säte</span>
                    <input style="width:45%;" name="company_hq" id="company_hq" value="<?php echo $settings['company_hq']; ?>" type="text">
                </label>
                <br>
            </div>
            <div style="clear:both;"></div><p></p>

            <input name="logo_file" id="logo_file" value="<?php echo $settings['logo_file']; ?>" type="hidden">
            <img id="billingImage" src="<?php echo $settings['logo_file']; ?>"><br>
            <a href="javascript:void(0);" onclick="openKCFinder('logo_file', 'billing', 'billingImage');">Klicka här för att välja annan bild för faktura/följesedel</a>

            <button name="save_settings" type="submit" style="margin-left: 200px;"><img src="../img/page_save.png" alt=""> Spara &nbsp;</button>
        </form>
        <br /><br />
        
        

        <div style="clear:both;"></div>
    </div>
</div>
<?php
if (isset($_GET['saved']) && isset($_GET['mismatch'])) {
    print '<script type="text/javascript">$(document).ready(function() { alert("Varning, fakturanummret verkar inte följa en nummerserie. Ändringarna sparade."); window.location = window.location.pathname; });</script>';
} else if (isset($_GET['saved'])) {
    print '<script type="text/javascript">$(document).ready(function() { alert("Basinställningarna för fakturor är nu sparade."); window.location = window.location.pathname; });</script>';
}
?>

</body>
</html>
