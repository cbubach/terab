<?php
    include("db.php");
    ini_set("memory_limit","150M");

    if(!isset($_GET['printMode']))
    {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
    <title><?php if(isset($_GET['offert'])) {print "TERAB Offert"; } else { print "TERAB Prislista"; } ?></title>
    <script type="text/javascript" src="../JS/jquery.min.js"></script>
    <script type="text/javascript" src="../JS/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery_code.js"></script>
    <script type="text/javascript" src="quickpager.jquery.js"></script>
    <link rel="stylesheet" href="../jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="../style.css" type="text/css" />
    <script type="text/javascript">
    checked=false;
    function checkedAll (frm1) {
	var aa= document.getElementById('frm1');
	 if (checked == false)
          {
           checked = true
          }
        else
          {
          checked = false
          }
	for (var i =0; i < aa.elements.length; i++)
	{
	 aa.elements[i].checked = checked;
	}
      }
    </script>
</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="/terab/images/blank.gif" width="0" height="80">
                        <img src="/terab/images/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="/terab/system.php?page=start">Startsidan</a> | <a href="/terab/system.php?function_file=login&function=logout">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

        <br />
        <h2 style="margin-left: 200px;">Skapa prislista/offert</h2>
        <br />

	<form method="get" action="offert.php">
            <table style="width:850px; padding-top:0px; padding-left:50px; margin:0px; font-size: 14px;">
              <tr>
                <td class="white">
                    <strong> Sök på kundnamn: </strong>
                </td>
                <td class="white">
                    <input type="text" name="custName0" id="custName0" value="<?php print isset($_GET['custName0']) ? $_GET['custName0'] : ''; ?>" style="width:170px; height:20px;" />
                    <input type="hidden" name="custID" id="custID" value="<?php print isset($_GET['custID']) ? $_GET['custID'] : ''; ?>" />
                </td>
                <td>
                    <input type="checkbox" name="offert" value="checked"<?php if(isset($_GET['offert'])) {print " ".$_GET['offert'].'="'.$_GET['offert'].'"'; } ?> /> Krysssa i för offert istället för prislista <br />
                    <input type="checkbox" name="bilder" value="checked"<?php if(isset($_GET['bilder'])) {print " ".$_GET['bilder'].'="'.$_GET['bilder'].'"'; } ?> /> Krysssa i för att dölja bilder
                </td>
                <td class="white">
                    <button name="save" type="submit"><img src="../img/page_white_edit.png" alt="" /> Visa för vald kund</button>
                </td>
              </tr>
            </table>
        </form> <br /><br />

        <div style="padding-left: 50px;">
                        <form method="get" action="offert.php" id="frm1">
        
<?php
    }   // end if ! $_GET['printMode']


            if (isset($_GET['printMode']))
            {
                ob_start();
?>
<style type="text/css">
h2 { font-family: sans-serif; }
table.tablesorter {
    background-color: #CDCDCD;
    font-family: arial;
    font-size: 8pt;
    margin: 10px 0 15px;
    text-align: left;
    width: 100%;
}
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #E6EEEE;
    border: 1px solid #FFFFFF;
    font-size: 8pt;
    padding: 4px;
}
table.tablesorter thead tr .header {
    background-image: url("bg.gif");
    background-position: right center;
    background-repeat: no-repeat;
    cursor: pointer;
}
table.tablesorter tbody td {
    background-color: #FFFFFF;
    color: #3D3D3D;
    padding: 4px;
    vertical-align: top;
}
table.tablesorter tbody td a {
    color: blue;
    text-decoration: underline;
}
table.tablesorter tbody tr.odd td {
    background-color: #F0F0F6;
}
table.tablesorter thead tr .headerSortUp {
    background-image: url("asc.gif");
}
table.tablesorter thead tr .headerSortDown {
    background-image: url("desc.gif");
}
table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
    background-color: #8DBDD8;
}
</style>
<?php
                print '<img src="banner_terab.jpg" alt="" /><br />'."\n";
                include("../mpdf51/mpdf.php");
                $mpdf = new mPDF('','', 0, '', 5, 5, 5, 5, 0, 0, 'P');
            }

            if (isset($_GET['custID']))
            {
                $sql = "SELECT * FROM price WHERE cust_id='{$_GET['custID']}'";
                $res = mysql_query($sql) or die(mysql_error());
                if (mysql_num_rows($res) > 0)
                {
                    if (isset($_GET['offert']))
                        print "<h2 style=\"margin-left: 40px;\">Offert</h2>\n";
                    else
                        print "<h2 style=\"margin-left: 40px;\">Prislista</h2>\n";
                        
                    if (!isset($_GET['printMode']))
                    {
                        print "Eventuell fritext:<br />\n";
                        print '<textarea name="fritext" rows="5" cols="50"></textarea><br />'."\n";
                        print 'Valuta: <input type="text" name="valuta" value="SEK" /> Kurs: <input type="text" name="kurs" value="1" /><br />';
                    }
                    else if (!empty($_GET['fritext']))
                    {
                        print stripslashes(nl2br($_GET['fritext']))."<br /><br />";
                    }
                    if (isset($_GET['printMode']))
                    {
                        print '<table class="tablesorter" align="center" style="width:90%; font-size: 14px;">'."\n";
                    }
                    else
                    {
                        print '<table class="tablesorter" style="width:850px; font-size: 14px;">'."\n";
                    }
                    print "<thead>\n";
                    print "  <tr>\n";
                    //print "    <th>Art. nr:</th>\n";
                    if (!isset($_GET['bilder']))
                        print "    <th>Bild</th>\n";
                    print "    <th>Namn</th>\n";
                    print "    <th>Pris {$_GET['valuta']}</th>\n";
                    if (!isset($_GET['printMode']))
                         print "    <th>Dölj produkt<br /><input type=\"checkbox\" name=\"checkall\" onclick=\"checkedAll();\"> Kryssa i alla</th>\n";
                    print "  </tr>\n";
                    print "</thead>\n";
                    print "<tbody>\n";
                    
                    $i = 0;
                    $print_array = array();  // store all printed data to this array for abc char sorting
                    while ($row = mysql_fetch_array($res))
                    {
                        $i++;
                        $res25 = mysql_query("SELECT * FROM art WHERE id='{$row['art_id']}'");
                        if (mysql_num_rows($res25) > 0)
                        {
                            $aRow = mysql_fetch_assoc($res25);
                            $hide = 0;
                            if (isset($_GET['hidden']))
                            {
                                foreach($_GET['hidden'] as $hidden)
                                {
                                    if ($hidden == $aRow['id'])
                                    {
                                        $hide = 1;
                                    }
                                }
                            }
                            if ($hide == 0)
                            {
                                $print_array[$i - 1] = "<!-- {$aRow['art_name']} -->\n";
                                $print_array[$i - 1] .= "  <tr>\n";
                                //print "    <td>{$aRow['art_no']}</td>\n";
                                if (($aRow['img'] != "") && (!isset($_GET['bilder'])) )
                                   $print_array[$i - 1] .= "    <td><img src=\"{$aRow['img']}\" style=\"width: 150px;\" alt=\"\" /></td>\n";
                                else if (!isset($_GET['bilder']))
                                    $print_array[$i - 1] .= "    <td></td>\n";
                                $print_array[$i - 1] .= "    <td>{$aRow['art_name']}</td>\n";
                                if (isset($_GET['kurs']) && !empty($_GET['kurs']))
                                    $print_array[$i - 1] .= "    <td>".number_format(($row['price'] / str_replace ( ',' , '.' , $_GET['kurs'] )), 2, ',', ' ')."</td>\n";
                                else
                                    $print_array[$i - 1] .= "    <td>".number_format($row['price'], 2, ',', ' ')."</td>\n";
                                if (!isset($_GET['printMode']))
                                    $print_array[$i - 1] .= '    <td><input type="checkbox" name="hidden[]" value="'.$aRow['id'].'" /></td>'."\n";
                                $print_array[$i - 1] .= "  </tr>\n";
                            }
                        }
                    }
                    sort($print_array);
                    foreach ($print_array as $key => $val)
                    {
                        print $val."\n";
                    }
                    print "</tbody>\n";
                    print "</table>\n\n";

                    if(!isset($_GET['printMode']))
                    {
                       ?>
                       <br /><br />
                            <table style="width:850px; padding-top:0px; padding-left:50px; margin:0px; font-size: 14px;">
                             <tr>
                               <td class="white">
                                   <input type="hidden" name="custName0" id="custName0" value="<?php print $_GET['custName0']; ?>" style="width:170px; height:20px;" />
                                   <input type="hidden" name="custID" id="custID" value="<?php print $_GET['custID']; ?>" />
                               <?php
                                  if(isset($_GET['offert'])) {
                                    print '                                   <input type="hidden" name="offert" value="checked" />'."\n";
                                  }
                                  if(isset($_GET['bilder'])) {
                                    print '                                   <input type="hidden" name="bilder" value="checked" />'."\n";
                                  }
                               ?>
                                   <button name="printMode" type="submit"><img src="../img/printer.png" alt="" /> Skriv ut produkter</button>
                               </td>
                             </tr>
                           </table>
                       </form>
                       <?php
                    }
                }
                else
                {
                    print "Inga anpassade priser för den valda kunden hittades!";
                }
            }

        ?>

        </div>
<?php
    if(!isset($_GET['printMode']))
    {
        print "</body>\n</html>";
    }
    else
    {
        $main_body = ob_get_contents();
        ob_end_clean();
        $mpdf->WriteHTML($main_body);
        $mpdf->Output();
        exit;
    }
?>
