<?php

/**
 * AJAX endpoint for billing related stuff
 */

include("db.php");
  
if (isset($_GET['cmd']) && ($_GET['cmd'] == "addBill" || $_GET['cmd'] == "editBill"))
{
    $waybill_no = isset($_GET['waybill_no']) ? intval($_GET['waybill_no']) : 0;
    $cust_id    = $_GET['billCustId'];
    $bill_no    = isset($_GET['bill_number']) ? intval($_GET['bill_number']) : 0;
    $settingsId = 0;

    if ($_GET['cmd'] == "addBill") {
        $newest_bill_no = isset($_GET['newestBillId']) ? intval($_GET['newestBillId']) : 0;
        $db_newest      = intval(mysql_result(mysql_query("SELECT `bill_number` + 1 FROM `bill_settings` WHERE `id`=0"), 0));
        $bill_no        = ($bill_no == $newest_bill_no) ? $db_newest : $bill_no;
    } else {
        $result = mysql_query("SELECT settings_id FROM `bill` WHERE id='{$_GET['billId']}'") or die(mysql_error());
        
        if (mysql_num_rows($result) > 0) {
            $settingsId = (int)mysql_result($result, 0);
        }
    }

    $status = 0;
    
    if ($bill_no == 0) {
        $status = 1;  // hides the bill, but why? soft delete perhaps
    } else if ($_GET['creditBill'] == 1) {
        $status = 2;
    }
    
    $item_id    = $_GET['art_ID'];
    $ref        = $_GET['reference'];
    $artName    = $_GET['artName'];
    $artAmount  = $_GET['artAmount'];
    $artUnit    = $_GET['artUnit'];
    $artVAT     = $_GET['artVAT'];
    $price      = $_GET['artPrice'];
    $currency   = $_GET['currency'];
    $iban       = $_GET['iban'];
    $due        = (empty($_GET['due']) ? 0 : intval($_GET['due']));
    $msg        = $_GET['msg'];
    $date       = $_GET['billDate'];

	$settingsRes = mysql_query("SELECT * FROM bill_settings WHERE id='{$settingsId}'") or die(mysql_error());
    $billSetting = mysql_fetch_array($settingsRes);  
    
    if (
        $iban !== $billSetting['iban'] ||
        $due !== $billSetting['due'] ||
        $msg !== $billSetting['msg']
    ) {
        if ($settingsId == 0) {
            mysql_query("INSERT INTO `bill_settings` (`due`, `iban`, `message`) VALUES('{$due}', '{$iban}', '{$msg}')") or die(mysql_error());
            $settingsId = (int)mysql_insert_id();
        } else {
            mysql_query("UPDATE `bill_settings` SET `due` = '{$due}', `iban` = '{$iban}', `message` = '{$msg}' WHERE `id` = '{$settingsId}'") or die(mysql_error()); 
        }
    }
    
    if ($_GET['cmd'] == "editBill") {
        mysql_query("DELETE FROM bill WHERE id=".$_GET['billId']) or die(mysql_error());
        mysql_query("DELETE FROM bill_item WHERE bill_id=".$_GET['billId']) or die(mysql_error());
    }

    mysql_query("INSERT INTO `bill`
           (`cust_id`, `date`, `active`, `status`, `settings_id`, `waybill_id`, `order_id`, `ref_no`)
           VALUES (".$cust_id.", '".$date."', 1, ".$status.", $settingsId, ".$waybill_no.", ".$bill_no.", '".$ref."')") or die(mysql_error());

    $bill_id = (int)mysql_insert_id();

    if ($_GET['cmd'] == "addBill") {
        mysql_query("UPDATE `bill_settings` SET `bill_number` = '".$bill_no."' WHERE id=0") or die(mysql_error());
    }
    
    for ($i = 0; $i < count($artName); $i++) {
        if (!empty($artName[$i])) {
            $price[$i] = (float)str_replace(" ", "", str_replace(",", ".", $price[$i]));
            $sum = $price[$i] * $artAmount[$i];

            if (!empty($item_id[$i])) {
                $art_no = mysql_result(mysql_query("SELECT `art_no` FROM `art` WHERE `id` = '".$item_id[$i]."'"), 0);
                $itemId = $item_id[$i];
            } else {
                $art_no = " ";
                $itemId = 0;
            }
            mysql_query("INSERT INTO `bill_item`
                       (`bill_id`, `item_id`, `art_no`, `name`, `amount`, `unit`, `VAT`, `sum`, `currency`, `price`)
                       VALUES (".$bill_id.", ".$itemId.", '".$art_no."', '".$artName[$i]."', ".$artAmount[$i].",
                               '".$artUnit[$i]."', ".$artVAT[$i].",".$sum.", '".$currency."', ".$price[$i].")") or die(mysql_error());
        }
    }

    print $bill_id;
}