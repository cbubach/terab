<?php
/*   fraktsedel.php
 *
 *   Tar emot GET data för att generera en giltlig fraktsedel
 *   med korrekt godsinformation från DB:n
 *                    Christoffer Bubach, 2011
 */
@mysql_connect("127.0.0.1", "root", "mysqlpass") or die("Could not connect: " . mysql_error());
mysql_select_db("terab") or die("Could not connect: " . mysql_error());
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
ini_set("memory_limit","150M");
date_default_timezone_set("Europe/Stockholm");

include("mpdf51/mpdf.php");

//print "<pre>";
//print_r($_GET);
//exit;

$mpdf=new mPDF('','', 0, '', 5, 5, 5, 5, 0, 0, 'P');

$mpdf->SetImportUse();   // allow pdf imports


if(isset($_GET['fSenderPays']))
    $fSenderPays = '<span class="bold">X</span>';
else
    $fSenderPays = "";

if(isset($_GET['fRePays']))
    $fRePays = '<span class="bold">X</span>';
else
    $fRePays = "";

if(isset($_GET['fOtherPays']))
    $fOtherPays = '<span class="bold">X</span>';
else
    $fOtherPays = "";
    
if(isset($_GET['fAviPhone']))
    $fAviPhone = '<span class="bold">X</span>';
else
    $fAviPhone = "";
    
if(isset($_GET['fAviFax']))
    $fAviFax = '<span class="bold">X</span>';
else
    $fAviFax = "";
    
    
$fProdList = "";

$maxValue = 28;

for ($i = 0; $i < $maxValue; $i++)
{
    if (isset($_GET['fArtAmount'.$i]))
    {
        $arr = imagettfbbox(14 , 45 , "verdana.ttf" , $_GET['fArtName'.$i] );
        if ( $arr[2] > 350 )
        {
            $maxValue--;
        }
        $fProdList .= '       <tr>';
        $fProdList .= '         <td width="200px">';
        $fProdList .= '             <span class="bold_big">'.$_GET['fArtMark'.$i].'</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">'.$_GET['fArtAmount'.$i].'</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">'.$_GET['fArtType'.$i].'</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="300px">';
        $fProdList .= '             <span class="bold_big">'.$_GET['fArtName'.$i].'</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">'.$_GET['fArtNo'.$i].'</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">'.str_replace ( '.' , ',' , $_GET['fArtWeight'.$i] ).'</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">'.$_GET['fArtVolume'.$i].'</span>';
        $fProdList .= '         </td>';
        $fProdList .= '       </tr>';
    }
    else
    {
        $fProdList .= '       <tr>';
        $fProdList .= '         <td width="200px">';
        $fProdList .= '             <span class="bold_big">&#160;</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">&#160;</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">&#160;</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="300px">';
        $fProdList .= '             <span class="bold_big">&#160;</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">&#160;</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">&#160;</span>';
        $fProdList .= '         </td>';
        $fProdList .= '         <td width="50px">';
        $fProdList .= '             <span class="bold_big">&#160;</span>';
        $fProdList .= '         </td>';
        $fProdList .= '       </tr>';
    }
}



$css = '
<style type="text/css">
 body
 {
   font-family: verdana, helvetica, sans-serif;
   font-size: 12px;
 }
 table
 {
   border-collapse: collapse;
 }
 td
 {
   vertical-align: top;
   padding: 2px;
 }
 .small
 {
   font-size: 10px;
 }
 .x-small
 {
   font-size: 8px;
 }
 .border
 {
   border: 1px solid #000;
 }
 .border-l
 {
   border-left: 1px solid #000;
 }
 .border-r
 {
   border-right: 1px solid #000;
 }
 .border-b
 {
   border-bottom: 1px solid #000;
 }
 .border-t
 {
   border-top: 1px solid #000;
 }
 .bold_big
 {
   font-weight: bold;
   font-size: 14px;
 }
 .bold_bigger
 {
   font-weight: bold;
   font-size: 16px;
 }
 .bold
 {
   font-weight: bold;
 }
 .barcode
 {
   padding: 1.5mm;
   margin: 0;
   vertical-align: top;
   color: #000000;
 }
</style> ';


$main_body = '<table style="width:100%; padding: 0px; margin:0px;">
  <tr>
    <td width="10%" style="text-align: right; padding-right:10px;">
      892
    </td>
    <td width="45%">
      Detta uppdrag utförs i enlighet med transportföretagets<br />
      vid varje tidpunk gällande ansvarsbestämmelser
    </td>
    <td width="20%">
      <span class="bold_big">FRAKTSEDEL</span>
    </td>
    <td width="25%">
      <table><tr><td style="border:0px;" width="20%">
      <span class="small">| Transportföretag,-slag</span>
      </td><td style="border:0px;" width="5%">
      <span class="small">| Sida</span>
      </td></tr></table>
    </td>
  </tr>
  <!-- första stora rutan -->
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" class="border-t border-l border-r">
      <table><tr><td>  <span class="small">Godsavsändare. postadress</span> </td><td> <span class="small"> | GAN/Kundnummer</span> </td></tr>
      <tr><td> <span class="bold_big">'.$_GET['aName'].'</span> </td><td> <span class="bold_big">'.$_GET['aGAN'].'</span> </td></tr></table>
    </td>
    <td width="20%" class="border">
      <span class="small">Utskrivningsdatum</span><br />
      <span class="bold">'.$_GET['fDate'].'</span>
    </td>
    <td width="20%" class="border">
      <span class="small">Fraktsedel nr</span><br />
      <span class="bold">'.$_GET['fNo'].'</span>
    </td>
  </tr>
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" class="border-l border-r">
      <span class="bold_big">'.$_GET['aAddress'].'</span>
    </td>
    <td width="20%" class="border-b border-r" colspan="2">
      <span class="small">Godsavsändarens referenser</span><br />
      <span class="bold">'.$_GET['aRef'].'</span>
    </td>
  </tr>
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" class="border-l border-r border-b">
      <table><tr><td> <span class="small">Avsändningsort/-station (inkl postnr) </span> </td><td> <span class="small"> | Godsavsändarens telefon/epost</span><br />
      <tr><td> <span class="bold_big">'.$_GET['aZip'].' '.$_GET['aCity'].'</span> </td><td> <span class="bold">'.$_GET['aPhone'].'</span> </td></tr></table>
    </td>
    <td width="45%" class="border-b border-r" colspan="2">
      <span class="small">Godsmottagarens referenser</span><br />
      <span class="bold">'.$_GET['mRef'].'</span>
    </td>
  </tr>
  <!-- andra stora rutan -->
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" class="border-t border-l border-r">
      <span class="small">Godsmottagare, lev. adress &#160; &#160; &#160; GAN/Kundnummer</span> <br />
       &#160; &#160; &#160;  &#160; &#160; &#160; &#160; &#160; &#160;  &#160; &#160; &#160;
       &#160; &#160; &#160;  &#160; &#160; &#160; &#160; &#160; &#160;  &#160; &#160; &#160;
       <span class="bold_big">'.$_GET['mGAN'].'</span>
    </td>
    <td width="45%" class="border-b border-r" colspan="2">
      <span class="small">Särskilda transportinstruktioner (värme, kyla e d) &#160; &#160; &#160; &#160; &#160;</span><span class="bold small">Kod</span><br />
      <span class="bold"></span>
    </td>
  </tr>
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" class="border-l border-r">
      <span class="bold_bigger">'.$_GET['mName'].'</span> <br />
      <span class="bold_bigger">'.$_GET['mAddress'].'</span>
    </td>
    <td width="45%" class="border-b border-r" colspan="2">
      <table><tr><td width="20%" class="border-r">
        <span class="small">Efterkrav, kr</span><br />
        <span class="small">&#160;</span>
      </td><td width="40%">
        <span class="small">Gironr</span><br />
      </td><td width="40%">
        <span class="small">Efterkravsreferens</span><br />
      </td></tr></table>
    </td>
  </tr>
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" class="border-l border-r border-b">
      <span class="bold_bigger">'.$_GET['mZip'].' '.$_GET['mCity'].'</span>
    </td>
    <td width="45%" class="border-b border-r" colspan="2">
      <table><tr><td width="20%">
        <span class="small bold">Fraktbetalning</span>
        <table><tr><td> <table><tr><td style="width:15px; height:15px; border: 1px solid #888;">'.$fSenderPays.'</td></tr></table> </td>
          <td>1</td><td><span style="font-size:8px;line-height:8px;">Avsändaren<br /> &#160; betalar</span></td>
        </tr></table>
      </td><td width="40%" class="border-r">
        <span class="small">inkl (kod)</span><br />
      </td><td width="40%">
        <span class="small">Transportavtals nr</span><br />
      </td></tr></table>
    </td>
  </tr>
   <!-- tredje "raden" -->
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" style="border: 2px solid #000; border-right: 4px solid #000; border-left:4px solid #000;">
      <span class="small">Bestämmelseort/-station (inkl postnr)</span> <br />
      <span class="bold">'.$_GET['mZip'].' '.$_GET['mCity'].'</span>
    </td>
    <td width="45%" class="border-b border-r" colspan="2">
      <table><tr><td width="20%">
        <table><tr><td> <table><tr><td style="width:15px; height:15px; border: 1px solid #888;">'.$fRePays.'</td></tr></table> </td>
          <td>3</td><td><span style="font-size:8px;line-height:8px;">Mottagaren<br /> &#160; betalar</span></td>
        </tr></table>
      </td><td width="40%" class="border-l">
        <table><tr><td> <table><tr><td style="width:15px; height:15px; border: 1px solid #888;">'.$fOtherPays.'</td></tr></table> </td>
          <td>4</td><td><span style="font-size:8px;line-height:8px;">Annan<br /> &#160; fraktbetalare</span></td>
        </tr></table>
      </td><td width="40%">
        <span class="small">GAN/Kundnummer</span><br />
        <span class="bold">'.$_GET['fTransNo'].'</span>
      </td></tr></table>
    </td>
  </tr>
   <!-- fjärde "raden" -->
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="45%" class="border">
      <table width="100%">
        <tr>
          <td>
              <span class="small">Leveransanvisning (efterkrav o d)&#160;&#160;&#160;&#160;&#160;</span><br /><br />
              <span class="bold">'.$_GET['fDelInfo1'].'</span><br />
              <span class="bold">'.$_GET['fDelInfo2'].'</span><br /><br /><br />
          </td>
          <td>
             <table>
             <tr>
               <td>
                 <table style="margin-top: 7px;"><tr><td style="width:15px; height:15px; border: 1px solid #888;">'.$fAviPhone.'</td></tr></table>
                 <table><tr><td style="width:15px; height:15px; border: 1px solid #888;">'.$fAviFax.'</td></tr></table>
               </td>
               <td>
                   <span style="font-size:8px;line-height:8px;">Aviseras</span><br />
                   <span class="bold small">Telefon</span> <br />
                   <span class="bold small">Fax</span>
               </td>
             </tr>
             </table>
          </td>
        </tr>
        <tr>
          <td class="border-t" width="100%" colspan="2">
            <table width="100%">
             <tr>
               <td width="25%" class="border-r">
                 <span class="bold x-small">Ant godk EUR-pallar</span><br />
                 <span class="bold">'.$_GET['fNoOfEUR'].'</span>
               </td>
               <td width="37%" class="border-r">
                 <span class="x-small">Godsmottagarens reg-nr</span><br />
                 <span class="bold">'.$_GET['mRegNo'].'</span>
               </td>
               <td width="38%">
                 <span class="x-small">Godsavsändarens pallreg-nr</span><br />
                 <span class="bold">'.$_GET['aRegNo'].'</span>
               </td>
             </tr>
             <tr>
               <td width="100%" colspan="3" class="border-t">
                 <span class="small">Lasttillbehör</span>
               </td>
             </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="45%" class="border-b border-r" colspan="2">
      <table width="100%">
       <tr>
         <td>
            <span class="small">Kodfält</span><br />
            <span class="bold_big">1</span> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;
            <span class="bold_big">2</span> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;
            <span class="bold_big">3</span><br />
            <span class="bold_big">4</span> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;
            <span class="bold_big">5</span> &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160;
            <span class="bold_big">6</span>
         </td>
       </tr>
       <tr>
         <td class="border-t" width="100%" colspan="2">
          <table>
           <tr>
             <td>
               <span class="small">Avsändarens<br />GAN/Kundnr<br />(streckkod)</span>
             </td>
             <td>
               <barcode code="'.$_GET['aGAN'].'" type="C39" class="barcode" />
             </td>
           </tr>
           <tr>
             <td>
               <span class="small">Fraktsedel<br />nummer<br />(streckkod)</span>
             </td>
             <td>
               <barcode code="'.str_replace(" ", "", $_GET['fNo']).'" type="C39" class="barcode" />
             </td>
           </tr>
          </table>
         </td>
       </tr>
      </table>
    </td>
  </tr>
  <!--  MAIN CONTENT!! -->
  <tr>
    <td width="10%">
      &#160;
    </td>
    <td width="90%" colspan="3" class="border">
      <table width="100%">
       <tr>
         <td width="200px" class="border-r">
             <span class="small">Godsmärkning/Vagnsnummer</span>
         </td>
         <td width="50px" class="border-r">
             <span class="small">Kolliantal</span>
         </td>
         <td width="50px" class="border-r">
             <span class="small">Kollislag</span>
         </td>
         <td width="300px" class="border-r">
             <span class="small">Varuslag (om i container/flak även, art, ägare, nr, längd)</span>
         </td>
         <td width="50px" class="border-r">
             <span class="small">Varunummer (transport)</span>
         </td>
         <td width="50px" class="border-r">
             <span class="small">Bruttovikt, kg</span>
         </td>
         <td width="50px">
             <span class="small">Volym, m<sup>3</sup></span>
         </td>
       </tr>
'.$fProdList.'
      </table>

      <!-- FOOTER!! -->
      <table width="100%">
       <tr>
         <td width="150px"><span class="small bold">Totalt antal kollin</span><br /><span class="bold_bigger">'.$_GET['fKAmount'].'</span></td>
         <td width="150px"><span class="small bold">Total bruttovikt</span><br /><span class="bold_bigger">'.str_replace ( '.' , ',' , $_GET['fWeight']).'</span></td>
         <td width="50px"><span class="small bold">Kod</span><br /><span class="bold_bigger">'.$_GET['fCode'].'</span></td>
         <td width="200px"><span class="small bold">Kubik, flakm, ant, kg</span><br /><span class="bold_bigger">'.$_GET['fM3'].'</span></td>
         <td width="100px"><span class="small bold">Varunummer</span></td>
         <td width="100px"><span class="small bold">Kontrolltal</span></td>
       </tr>
      </table>
    </td>
  </tr>';
  
$a_footer = '  <tr>
   <td width="10%" style="vertical-align: bottom;">
     <span class="bold_big">A</span><br />
     <span class="small">Avsändarens<br />blad</span>
   </td>
   <td width="100px" colspan="3" class="border-t border-r border-l">
      <table width="100%">
       <tr>
         <td width="300px" class="border-r">
           &#160;<br />&#160;<br />&#160;<br />&#160;<br />&#160;
         </td>
         <td width="200px" class="border-r">
           <table width="100%"><tr><td>
           <img src="img/up_arrow.gif" alt="" /><td><span class="bold">Ange kod och värde</span><br />
           <span>V = Volym<br />F = Flakmeter<br />S = Styck</span></td></tr></table>
         </td>
         <td width="300px">
           <span class="small">Sändningen mottagen för befodran (ev stämpel)<br />(datum, transportföretag)</span>
         </td>
       </tr>
      </table>
   </td>
</table>';

$ti_footer = '  <tr>
   <td width="10%" style="vertical-align: bottom;">
     <span class="bold_big">Ti</span><br />
     <span class="small">Transport-<br />instruktion</span>
   </td>
   <td width="100px" colspan="3" class="border-t border-r border-l">
      <table width="100%">
       <tr>
         <td width="300px" class="border-r">
           (Ej för genomskrivning)<br />&#160;<br />&#160;<br />&#160;<br />&#160;
         </td>
         <td width="200px" class="border-r">
           <table width="100%"><tr><td>
           <img src="img/up_arrow.gif" alt="" /><td><span class="bold">Ange kod och värde</span><br />
           <span>V = Volym<br />F = Flakmeter<br />S = Styck</span></td></tr></table>
         </td>
         <td width="300px">
           <span class="small">Sändningen mottagen för befodran (ev stämpel)<br />(datum, transportföretag)</span>
         </td>
       </tr>
      </table>
   </td>
</table>';

$tu_footer = '  <tr>
   <td width="10%" style="vertical-align: bottom;">
     <span class="bold_big">Tu</span><br />
     <span class="small">Transport-<br />företagets</span>
   </td>
   <td width="100px" colspan="3" class="border-t border-r border-l">
      <table width="100%">
       <tr>
         <td width="400px" class="border-r">
           <table width="100%"><tr><td class="border-b" style="margin-bottom: 5px;" colspan="3">
             <span class="x-small bold">Viktigt! Synlig skada eller minskning anmäles omedelbart</span>
             <span class="x-small">till transportföretaget och noteras i blad Tu. Godsutlämnaren bestyrker noteringen med sin
             signatur och ev bilnummer. Skada som inte är synlig vid mottagandet anmäles till transportföretaget snarast,
             dock senast inom sju (7) dagar.<br />&#160;</span>
           </td></tr><tr><td>
             <span class="small bold">Sändningen<br />kvitteras</span>
           </td><td width="50px">
             <span class="small">Datum<br />&#160;</span>
           </td><td width="250px">
             <span class="small">Godsmottagarens (ombudets) namnteckning<br />&#160;</span>
           </td></tr></table>
         </td>
         <td width="400px">
           <table width="100%"><tr><td width="120px">
             <span class="small">Aviseras (namn)</span> <br />&#160;
           </td><td width="50px">
             <span class="small">Datum</span>
           </td><td>
             <span class="small">Klockan</span>
           </td><td>
             <span class="small">Sign</span>
           </td></tr><tr><td colspan="4" width="400px" class="border-t">
             <span class="small">Sändningen mottagen för befodran (ev stämpel)<br />(datum, transportföretag)</span> <br />&#160;
           </td></tr></table>
         </td>
       </tr>
      </table>
   </td>
</table>';

$m_footer = '  <tr>
   <td width="10%" style="vertical-align: bottom;">
     <span class="bold_big">M</span><br />
     <span class="small">Mottagarens<br />blad</span>
   </td>
   <td width="100px" colspan="3" class="border-t border-r border-l">
      <table width="100%">
       <tr>
         <td width="400px" class="border-r">
           <table width="400px"><tr><td class="border-b" style="margin-bottom: 5px;" colspan="3">
             <span class="small">Transportföretagets kvitto på</span>
             <span class="small bold">kontant</span>
             <span class="small">mottaget belopp inkl ev efterkrav<br />&#160;<br />&#160;</span>
           </td></tr><tr><td>
             <span class="small bold">Sändningen<br />kvitteras</span>
           </td><td width="50px">
             <span class="small">Datum<br />&#160;</span>
           </td><td width="250px">
             <span class="small">Godsmottagarens (ombudets) namnteckning<br />&#160;</span>
           </td></tr></table>
         </td>
         <td width="400px">
           <table width="100%"><tr><td width="120px">
             <span class="small">Aviseras (namn)</span> <br />&#160;
           </td><td width="50px">
             <span class="small">Datum</span>
           </td><td>
             <span class="small">Klockan</span>
           </td><td>
             <span class="small">Sign</span>
           </td></tr><tr><td colspan="4" width="400px" class="border-t">
             <span class="small">Sändningen mottagen för befodran (ev stämpel)<br />(datum, transportföretag)</span> <br />&#160;
           </td></tr></table>
         </td>
       </tr>
      </table>
   </td>
</table>';

//print $css.$main_body.$ti_footer; exit;


$mpdf->WriteHTML($css.$main_body.$ti_footer);

$mpdf->AddPage();
$mpdf->WriteHTML($css.$main_body.$tu_footer);

$mpdf->AddPage();
$mpdf->WriteHTML($css.$main_body.$m_footer);

$mpdf->AddPage();
$mpdf->WriteHTML($css.$main_body.$a_footer);

if(isset($_GET['extrapdf']) && $_GET['extrapdf'] != "")
{
  $mpdf->AddPages() ;
  $mpdf->SetSourceFile('pdfer/'.$_GET['extrapdf']);
  $import1 = $mpdf->ImportPage(1) ;
  $mpdf->UseTemplate($import1,0,0,0,0) ;
}

$mpdf->Output();
exit;