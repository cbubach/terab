<?php

//-------------------------------------------
//
//    TERAB booking
//
//      File that handles all AJAX and
//      form requests.
//
//
//-------------------------------------------
@mysql_connect("127.0.0.1", "root", "mysqlpass") or die("Could not connect: " . mysql_error());
mysql_select_db("terab") or die("Could not connect: " . mysql_error());
mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
date_default_timezone_set("Europe/Stockholm");
//--------------------------------------
// code to handle swedish + date()
function veckodag($arg1) {
  $dag = array( 'Sön', 'Mån', 'Tis', 'Ons', 'Tor', 'Fre', 'Lör');
  return $dag[$arg1];
}//-------------------------------------

/*  function make_chksum($number)
 *
 *   Generera checksum nummer 10 baserat på de
 *   nio första siffrorna i fraktsedel-nummret.
 *               Christoffer Bubach, 2011
 */
function make_chksum($number)
{
    $number2 = array();
    $number2 = str_split($number, 1);
    $sum     = 0;
    $count   = 4;

    // loop numbers and multiply each with either 2 or 1,
    // as in 212121212 (set $n2 depending on $i even/odd)
    for ($i=0; $i < 9; $i++)
    {
        $tmp = array();
        $n2 = 2;
        if (is_float($i/2))
            $n2 = 1;

        // when multiplied, split results into array and add the sum
        // with a foreach. so result 14 gets split up and added as $sum += 1+4
        // when sum 9 gets added just as $sum += 9
        $tmp = str_split($number2[$i] * $n2, 1);
        foreach ($tmp as &$value)
            $sum = $sum + $value;
    }
    // loop though result-"table" of $count, if results/sum
    // is 30, that represents $count=="0". starts on 6=4, 7=3, 8=2,
    // 9=1, 10=0, 11=9...19=1,20=0,21=9 and ends on 100 = 0.
    for ($i = 6; $i < 101; $i++)
    {
        if ($i == $sum)
            return $count;
        if ($count == 0)
            $count = 9;
        else
            $count--;
    }
}


//--------------------------------------
//  Handle all FORM and AJAX requests :)
//--------------------------------------
if (isset($_GET['cmd']))
{
    if ($_GET['cmd'] == "updatePlacement")
    {
        $id = substr($_GET['id'], 6);
        mysql_query("UPDATE event SET date='{$_GET['time']}' WHERE id='{$id}'") or die(mysql_error());
        exit;
    }
    elseif ($_GET['cmd'] == "showPrevOrders" && isset($_GET['custID']))
    {
        header('HTTP/1.1 200 OK');
        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Pragma: no-cache');
        header('Content-Type: text/html; charset=utf-8');
        if (empty($_GET['custID']) || $_GET['custID'] == 0)
        {
            print "<br /><br /><div style=\"width:100%;text-align:center;\"><h2>Välj kund först!</h2></div>\n";
            print '<br /><p style="width:100%;text-align:center;"><a class="sexybutton sexysimple sexyxxl" style="background-color: rgb(150,150,150);" href="javascript:void(0);" onclick="$(\'#showPrevOrders\').dialog(\'close\');">Stäng</a></p>'."\n";
        }
        else
        {
            $query = "SELECT * FROM event WHERE type='".$_GET['type']."'";
            if ( $_GET['custID'] != 0 || !empty($_GET['custID']) )
                $query = $query." AND cust_id='{$_GET['custID']}'";
            $query = $query." ORDER BY date DESC";
            //print "$query";
            $result = mysql_query($query) or die(mysql_error());

            $outp_buffer  = "";
            $total_amount = 0;

            if (mysql_num_rows($result) > 0)
            {
                while ($row = mysql_fetch_array($result))
                {
                    $date = (int)$row['date'];

                    if (isset($_GET['artID0']))
                        $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}' AND art_id='{$_GET['artID0']}'") or die(mysql_error());
                    else
                        $r2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}'") or die(mysql_error());
                    if (mysql_num_rows($r2) > 0) {
                        while ($row2 = mysql_fetch_array($r2))
                        {
                            $outp_buffer .= "<tr>\n";
                            $outp_buffer .= "    <td>".date('Y-m-d',$date)."</td>\n";
                            $art_name = @mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$row2['art_id']}'"), 0);
                            $outp_buffer .= '    <td><a href="javascript:void(0);" onclick="addProductFromCat('.$row2['art_id'].', \''.$art_name.'\', '.$_GET['type'].')">'.$art_name.'</a></td>'."\n";
                            $outp_buffer .= "    <td>".$row2['art_amount']."</td>\n";
                            $total_amount += $row2['art_amount'];
                            //$outp_buffer .= "    <td>".@mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['cust_id']}'"), 0)."</td>\n";
                            $outp_buffer .= "</tr>\n";
                        }
                    }
                }
                print '<div style="width:100%;text-align:center;"><a class="sexybutton sexysimple sexyxxl" style="background-color: rgb(150,150,150);" href="javascript:void(0);" onclick="$(\'#showPrevOrders\').dialog(\'close\');">Stäng</a></div>'."\n";
                print "<table id=\"prevOrdersTable\" class=\"tablesorter\">\n";
                print "<thead>\n";
                print "<tr>\n";
                print "    <th>Datum</th>\n";
                print "    <th>Artikel</th>\n";
                print "    <th>Antal (totalt <big>$total_amount</big>)</th>\n";
                //print "    <th>Kundnamn</th>\n";
                print "</tr>\n";
                print "</thead>\n";
                print "<tbody>\n";
                print $outp_buffer;
                print "</tbody>\n";
                print "</table>\n";
                print '<div style="width:100%;text-align:center;"><a class="sexybutton sexysimple sexyxxl" style="background-color: rgb(150,150,150);" href="javascript:void(0);" onclick="$(\'#showPrevOrders\').dialog(\'close\');">Stäng</a></div>'."\n";
            }
            else
            {
                print "<br /><br /><div style=\"width:100%;text-align:center;\"><h2>Inga tidigare ordrar!</h2></div>\n";
                print '<br /><p style="width:100%;text-align:center;"><a class="sexybutton sexysimple sexyxxl" style="background-color: rgb(150,150,150);" href="javascript:void(0);" onclick="$(\'#showPrevOrders\').dialog(\'close\');">Stäng</a></p>'."\n";
            }
        }
        exit;
    }
    elseif ($_GET['cmd'] == "updateOrderList" && isset($_GET['time']) && isset($_GET['type']))
    {
        header('Cache-Control: no-cache');
        header('Pragma: no-cache');
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header('Content-Type: text/html; charset=utf-8');
        $result = @mysql_query("SELECT * FROM event WHERE date='{$_GET['time']}' AND type='{$_GET['type']}'");
        if(mysql_num_rows($result) > 0)
        {
            while ($row = mysql_fetch_array($result))
            {
                print '                <li id="drop0_'.$row['id'].'" class="ui-state-highlight';
                if ($row['locked']==1)
                {
                    print " ui-state-disabled";
                }
                if ($_GET['type'] == 1)
                    print '" style="border-color: #f00; background: #faa;">'."\n";
                else
                    print "\">\n";
                print '                    <div style="float:right;">'."\n";
                print '                        <a href="javascript:void(0);" onclick="showEvent('.$row['id'].');">';

                $r1 = mysql_query("SELECT id FROM waybill WHERE name='feventID' AND value='{$row['id']}'");
                if (mysql_num_rows($r1) > 0)
                {
                    $wbID = mysql_result($r1, 0);
                    $r2 = mysql_query("SELECT * FROM waybill WHERE id='{$wbID}' AND name='noPrint' AND value='1'");
                    if (mysql_num_rows($r2) > 0)
                        print '<img src="img/page_go.png" alt="Visa" title="Visa" /></a><br />'."\n";
                    else
                        print '<img src="img/check.png" alt="Visa" title="Visa" /></a><br />'."\n";
                }
                else
                {
                    print '<img src="img/page.png" alt="Visa" title="Visa" /></a><br />'."\n";
                }

                if (!empty($row['cust_info']))
                    print '                        <img src="img/error.png" alt="" /><br />'."\n";

                print '                        <a href="javascript:void(0);" onclick="toggleLoading('.$row['id'].');"><img src="img/car.png"></a>'."\n";

                print '                    </div>'."\n";
                print '                    <div>'.mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['cust_id']}'"), 0);
                print ' <small style="font-size: 0.6em">('.mysql_result(mysql_query("SELECT del_city FROM cust WHERE id='{$row['cust_id']}'"), 0).")</small></div>\n";

                if ($row['loading']==1)
                    print '<small>BILEN</small>'."\n";
                elseif ($row['loading']==2)
                    print '<small>SLÄPET</small>'."\n";
                elseif ($row['loading']==3)
                    print '<small>TRAILER</small>'."\n";
                elseif ($row['loading']==4)
                    print '<small>HÄMTAS</small>'."\n";
                elseif ($row['loading']==5)
                    print '<small>BIL&SLÄP</small>'."\n";
                elseif ($row['loading']==6)
                    print '<small>LÄTT LASTBIL</small>'."\n";

                print '                    <div style="clear:both;"></div>'."\n";
                print "                </li>\n\n";
            }
        }
        exit;
    }
    elseif ($_GET['cmd'] == "changeDate" && !empty($_GET['new_date']))
    {
        $id = $_GET['eventID'];
        $time = strtotime($_GET['new_date']);
        mysql_query("UPDATE event SET date='{$time}' WHERE id='{$id}'") or die(mysql_error());
        header ("Location: index.php");
        exit;
    }
    elseif ($_GET['cmd'] == "changeWeek" && !empty($_GET['new_week']))
    {
        $time = strtotime($_GET['new_week']);
        header ("Location: index.php?tmstp=".$time);
        exit;
    }
    elseif ($_GET['cmd'] == "getEventIDfromCust")
    {
        $event_id = @mysql_result(mysql_query("SELECT id FROM event WHERE cust_id='{$_GET['cust_id']}' ORDER BY id DESC LIMIT 0, 1"), 0) or die("0");
        header('Content-Type: text/html; charset=utf-8');
        print intval($event_id);
        exit;
    }
    elseif ($_GET['cmd'] == "getNoteFromDate")
    {
        header('Content-Type: text/html; charset=utf-8');
        print @mysql_result(mysql_query("SELECT note FROM notes WHERE date='{$_GET['date']}'"), 0) or die("0");
        exit;
    }
    elseif ($_GET['cmd'] == "editNote")
    {
        $res = mysql_query("SELECT * FROM notes WHERE date='{$_GET['noteDate']}'");
        if (mysql_num_rows($res) > 0) {
            mysql_query("UPDATE notes SET note='{$_GET['note']}' WHERE date='{$_GET['noteDate']}'") or die(mysql_error());
        }
        else {
            mysql_query("INSERT INTO notes SET note='{$_GET['note']}', date='{$_GET['noteDate']}'");
        }
    }
    elseif ($_GET['cmd'] == "getLatestOrderNo" && ($_GET['cust_id'] != 0 && !empty($_GET['cust_id']) ) )
    {
        $order_no = @mysql_result(mysql_query("SELECT order_no FROM event WHERE cust_id='{$_GET['cust_id']}' ORDER BY id DESC LIMIT 0, 1"), 0) or die("0");
        header('Content-Type: text/html; charset=utf-8');
        print $order_no;
        exit;
    }
	elseif ($_GET['cmd'] == "setShippingMethod")
	{
	    @mysql_query("UPDATE event SET shipping='{$_GET['shipping']}' WHERE id='{$_GET['order']}'");
        header('Content-Type: text/html; charset=utf-8');
	    print "OK";
		exit;
	}
    elseif ($_GET['cmd'] == "getFno")
    {
        $fno_start = @mysql_result(mysql_query("SELECT value FROM settings WHERE name='fno'"), 0) or die("Nr.serie slut!!");
        $fno_amount = @mysql_result(mysql_query("SELECT value FROM settings WHERE name='fno_amount'"), 0) or die("Nr.serie slut!!");
        $fno = @mysql_result(mysql_query("SELECT value FROM settings WHERE name='fno_current'"), 0) or die("Nr.serie slut!!");

        header('Content-Type: text/html; charset=utf-8');
        if (($fno_start+$fno_amount > $fno) && (!empty($fno) && ($fno_start < $fno) ))
            print preg_replace("/(?<=\d)(?=(\d{3})+(?!\d))/", " ", $fno)." ".make_chksum($fno);
        else
            print "Nr.serie slut!!";
        exit;

    }
    elseif ($_GET['cmd'] == "getfSenders")
    {
        header('Content-Type: text/html; charset=utf-8');
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

        $result = mysql_query("SELECT * FROM wb_sender");
        if (mysql_num_rows($result) > 0)
        {
            while ($row = mysql_fetch_array($result))
            {
                print '            <option value="'.$row['id'].'">'.$row['name'].' ('.$row['city'].')</option>'."\n";
            }
        }
        exit;
    }
    elseif ($_GET['cmd'] == "getProdList")
    {
        header('Content-Type: text/html; charset=utf-8');
        print '<script type="text/javascript" src="JS/jquery.min.js"></script>'."\n";
        print '<script type="text/javascript" src="JS/jquery-ui.min.js"></script>'."\n";
        print '<script type="text/javascript" src="jquery_code.js"></script>'."\n";
        print '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>'."\n";
        print '<script type="text/javascript" src="jquery.gomap.min.js"></script>'."\n\n";
        print '<link rel="stylesheet" href="SexyButtons/sexybuttons.css" type="text/css" />'."\n";
        //print '<script type="text/javascript" src="JS/DYMO.Label.Framework.latest.js" charset="UTF-8"></script>'."\n";
        print '<link rel="stylesheet" href="style.css" type="text/css" />'."\n";
        print '<link rel="stylesheet" href="jquery-ui.css" type="text/css" />'."\n\n";
        //print '<script>'."\n";
	//print '$(document).ready(function() {'."\n";
	//print '    $( "#accordion" ).accordion();'."\n";
	//print '});'."\n";
	//print '</script> '."\n\n";

        $result = mysql_query("SELECT DISTINCT(cat) FROM prod_cat WHERE prod_id IN (SELECT prod_id FROM art WHERE type='{$_GET['type']}')") or die(mysql_error());
        if(mysql_num_rows($result) > 0)
        {
            print '<div id="accordion">'."\n";

            while ($row = mysql_fetch_array($result))
            {
                print '    <h3><a href="#">'.$row['cat'].'</a></h3>'."\n";
                print '    <div>'."\n";
                $res2 = mysql_query("SELECT * FROM art WHERE id IN (SELECT prod_id FROM prod_cat WHERE cat='{$row['cat']}')");
                if ( mysql_num_rows($res2) > 0 ) {
                    while ( $row2 = mysql_fetch_array($res2) ) {
                        print '<a href="javascript:void(0);" style="font-size:1.2em;font-weight:bold;line-height:1.6em;" ';
                        print 'onclick="addProductFromCat('.$row2['id'].', \''.$row2['art_name'].'\', '.$_GET['type'].');">'.$row2['art_name'].' ('.$row2['art_no'].')</a><br />'."\n";
                    }
                }
                print '    </div>'."\n";
            }
            print '</div>'."\n";
        }
        print '<br /><p style="width:100%;text-align:center;"><a class="sexybutton sexysimple sexyxxl" style="background-color: rgb(150,150,150);" href="javascript:void(0);" onclick="$(\'#showAddArtCat\').dialog(\'close\');">Stäng</a></p>'."\n";
        exit;
    }
    elseif ($_GET['cmd'] == "getWayBillData")
    {
        $id = @mysql_result(mysql_query("SELECT id FROM waybill WHERE name='feventID' AND value='{$_GET['id']}'"), 0);
        $result = mysql_query("SELECT * FROM waybill WHERE id='{$id}'") or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
            header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
            header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header ("Pragma: no-cache"); // HTTP/1.0
            header ("Content-Type: application/json; charset=utf-8");

            print "[{";
            $count = 0;
            while ($row = mysql_fetch_array($result))
            {
                if ($count != 0)
                    print ",";
                $count++;
                print '"'.$row['name'].'": "'.$row['value'].'"';
            }
            print "}]";
        }
        exit;
    }
    elseif ($_GET['cmd'] == "removeWB")
    {
        if (!empty($_GET['id']))
        {
            mysql_query("DELETE FROM wb_sender WHERE id='{$_GET['id']}'");
        }
        exit;
    }
    elseif ($_GET['cmd'] == "getfSenderData")
    {
        $result = mysql_query("SELECT * FROM wb_sender WHERE id='{$_GET['id']}'") or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
            header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
            header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header ("Pragma: no-cache"); // HTTP/1.0
            header ("Content-Type: application/json; charset=utf-8");

            print "[";

            while ($row = mysql_fetch_array($result))
            {
                print '{"id": "'.$row['id'].'", "gan": "'.$row['gan'].'", "name": "'.$row['name'].'", "address": "'.$row['address'].'", ';
                print '"zip": "'.$row['zip'].'", "city": "'.$row['city'].'", "phone": "'.$row['phone_fax'].'", "ref": "'.$row['ref'].'"}';
            }
            print "]";
        }
        exit;
    }
    elseif ($_GET['cmd'] == "setfSender")
    {
        if ($_GET['id'] == "0")
        {
            mysql_query("INSERT INTO wb_sender SET gan='{$_GET['aGAN']}', name='{$_GET['aName']}', address='{$_GET['aAddress']}', zip='{$_GET['aZip']}', city='{$_GET['aCity']}', phone_fax='{$_GET['aPhone']}', ref='{$_GET['aRef']}'");
            exit;
        }
        else
        {
            mysql_query("UPDATE wb_sender SET gan='{$_GET['aGAN']}', name='{$_GET['aName']}', address='{$_GET['aAddress']}', zip='{$_GET['aZip']}', city='{$_GET['aCity']}', phone_fax='{$_GET['aPhone']}', ref='{$_GET['aRef']}' WHERE id='{$_GET['id']}'") or die(mysql_error());
            exit;
        }
    }
    elseif ($_GET['cmd'] == "getCustData")
    {
        $result = mysql_query("SELECT * FROM cust WHERE id='{$_GET['cust_id']}'") or die(mysql_error());
        if (mysql_num_rows($result) > 0)
        {
            header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
            header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
            header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header ("Pragma: no-cache"); // HTTP/1.0
            header ("Content-Type: application/json; charset=utf-8");

            print "[";

            while ($row = mysql_fetch_array($result))
            {
                print '{"id": "'.$row['id'].'", "name": "'.$row['name'].'", "del_address": "'.$row['del_address'].'", "del_zip": "'.$row['del_zip'].'", "del_city": "'.$row['del_city'].'", ';
                print '"bill_name": "'.$row['bill_name'].'", "bill_address": "'.$row['bill_address'].'", "bill_zip": "'.$row['bill_zip'].'", "bill_box": "'.$row['bill_box'].'", ';
                print '"bill_city": "'.$row['bill_city'].'", "phone": "'.$row['phone'].'", "mobile_phone": "'.$row['mobile_phone'].'", "contact": "'.$row['contact'].'", "contact_email": "'.$row['contact_email'].'", ';
                print '"fax": "'.$row['fax'].'", "vat_reg_no": "'.$row['vat_reg_no'].'", "giro_no": "'.$row['giro_no'].'", "type": "'.$row['type'].'", "alt_name": "'.$row['alt_name'].'", "del_country": "'.$row['del_country'].'", ';
                print '"att": "'.$row['att'].'", "bill_country": "'.$row['bill_country'].'", "contact_email1": "'.$row['contact_email1'].'", "contact2": "'.$row['contact2'].'", "mob2": "'.$row['mob2'].'", ';
                print '"contact_email2": "'.$row['contact_email2'].'", "schenker_no": "'.$row['schenker_no'].'", "dhl_no": "'.$row['dhl_no'].'", "other_no": "'.$row['other_no'].'", ';
                print '"f_delinfo1": "'.$row['f_delinfo1'].'", "f_delinfo2": "'.$row['f_delinfo2'].'"}';
            }
            print "]";
        }
        exit;
    }
    elseif ($_GET['cmd'] == "getEventData")
    {
        $aResults = array();

        $result = mysql_query("SELECT * FROM event WHERE id='{$_GET['event_id']}'");
        if (mysql_num_rows($result) > 0)
        {
            while ($row = mysql_fetch_array($result))
            {
                $custName = mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['cust_id']}'"), 0);
                $aResults[] = array("custID"=>$row['cust_id'], "name"=>$custName, "orderNo"=>$row['order_no'], "orderCreator"=>$row['order_creator'], "custInfo"=>$row['cust_info'], "prefDate"=>$row['pref_date']);
            }
        }

        header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
        header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header ("Pragma: no-cache"); // HTTP/1.0
        header ("Content-Type: application/json; charset=utf-8");

        echo "[";
        $arr = array();
        for ( $i = 0; $i < count($aResults); $i++ )
        {
            if(!empty($aResults[$i]['prefDate']))
                $aResults[$i]['prefDate'] = date('Y-m-d',$aResults[$i]['prefDate']);
            $arr[]  = "{\"custID\": \"".$aResults[$i]['custID']."\", \"name\": \"".$aResults[$i]['name']."\", \"orderNo\": \"".$aResults[$i]['orderNo']."\", \"orderCreator\": \"".$aResults[$i]['orderCreator']."\", \"custInfo\": \"".mysql_real_escape_string($aResults[$i]['custInfo'])."\", \"prefDate\": \"".$aResults[$i]['prefDate']."\"}";
        }
        echo implode(", ", $arr);
        echo "]";
        exit;
    }
    elseif ($_GET['cmd'] == "getArtFromEventID")
    {
        $aResults = array();

        $cust_id = mysql_result(mysql_query("SELECT cust_id FROM event WHERE id='{$_GET['event_id']}'"), 0);

        $result = mysql_query("SELECT * FROM event_art WHERE event_id='{$_GET['event_id']}'");
        if (mysql_num_rows($result) > 0)
        {
            while ($row = mysql_fetch_array($result))
            {
                $count  = mysql_result(mysql_query("SELECT COUNT(price) FROM price WHERE cust_id='$cust_id' AND art_id='{$row['art_id']}'"),0);
                $weight = mysql_result(mysql_query("SELECT weight FROM art WHERE id='{$row['art_id']}'"),0);
                if ($count >0)
                    $price = mysql_result(mysql_query("SELECT price FROM price WHERE cust_id='$cust_id' AND art_id='{$row['art_id']}' LIMIT 0,1"), 0);
                else
                    $price = mysql_result(mysql_query("SELECT price FROM price WHERE cust_id='0' AND art_id='{$row['art_id']}' LIMIT 0,1"), 0);

                $artName = mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$row['art_id']}'"), 0);
                $aResults[] = array("id"=>$row['art_id'], "name"=>htmlspecialchars($artName), "amount"=>$row['art_amount'], "enhet"=>$row['enhet'], "price"=>$price, "weight"=>$weight);
            }
        }


        header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
        header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header ("Pragma: no-cache"); // HTTP/1.0
        header ("Content-Type: application/json; charset=utf-8");

        echo "[";
        $arr = array();
        for ( $i = 0; $i < count($aResults); $i++ )
        {
            $arr[] = "{\"id\": \"".$aResults[$i]['id']."\", \"name\": \"".$aResults[$i]['name']."\", \"amount\": \"".$aResults[$i]['amount']."\", \"enhet\": \"".$aResults[$i]['enhet']."\", \"price\": \"".$aResults[$i]['price']."\", \"weight\": \"".$aResults[$i]['weight']."\"}";
        }
        echo implode(", ", $arr);
        echo "]";
        exit;
    }
    elseif ($_GET['cmd'] == "removeEvent")
    {
        if (!empty($_GET['id']))
        {
            mysql_query("DELETE FROM event_art WHERE event_id='{$_GET['id']}'");
            mysql_query("DELETE FROM event WHERE id='{$_GET['id']}'");
            
            // delete connected waybill data
            $result = mysql_query("SELECT id FROM waybill WHERE name='feventID' AND value='{$_GET['id']}' ORDER BY id DESC") or die("1-".mysql_error());
            if (mysql_num_rows($result) != 0)
            {
                $waybill_no = mysql_result($result, 0);
                @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}'");
            }
        }
        header ("Location: index.php");
        exit;
    }
    elseif ($_GET['cmd'] == "toggleLockEvent")
    {
        if (!empty($_GET['id']))
        {
            $locked = mysql_result(mysql_query("SELECT locked FROM event WHERE id='{$_GET['id']}'"), 0);
            if ($locked==1) {
                $locked = 0;
            }
            else {
                $locked = 1;
                $recurring = mysql_result(mysql_query("SELECT recurring FROM event WHERE id='{$_GET['id']}'"), 0);
                if ($recurring == 1) {
                    $query  = "INSERT INTO event (cust_id,date,pref_date,create_date,order_no,cust_info,order_creator,locked,type,loading,recurring) ";
                    $query .= "SELECT cust_id,date,pref_date,create_date,order_no,cust_info,order_creator,locked,type,loading,recurring FROM event WHERE id='{$_GET['id']}'";
                    mysql_query($query) or die(mysql_error());
                    $new_id = mysql_insert_id();
                    $old_date = mysql_result(mysql_query("SELECT date FROM event WHERE id='{$_GET['id']}'"), 0);
                    $new_date = strtotime('+7 day', $old_date);
                    mysql_query("UPDATE event SET date='{$new_date}' WHERE id='{$new_id}'");
                    mysql_query("INSERT INTO event_art (event_id, art_id, art_amount, enhet) (SELECT '{$new_id}', art_id, art_amount, enhet FROM event_art WHERE event_id='{$_GET['id']}')");
                    mysql_query("UPDATE event SET recurring='0' WHERE id='{$_GET['id']}'");
                }
            }
            mysql_query("UPDATE event SET locked='$locked' WHERE id='{$_GET['id']}'");
        }
        header ("Location: index.php");
        exit;
    }
    elseif ($_GET['cmd'] == "changeLoading")
    {
        if (!empty($_GET['id']))
        {
            mysql_query("UPDATE event SET loading='{$_GET['type']}' WHERE id='{$_GET['id']}'");
        }
        header ("Location: index.php");
        exit;
    }
    elseif ($_GET['cmd'] == "showEvent")
    {
        header('Content-Type: text/html; charset=utf-8');
        print '<script type="text/javascript" src="JS/jquery.min.js"></script>'."\n";
        print '<script type="text/javascript" src="JS/jquery-ui.min.js"></script>'."\n";
        print '<script type="text/javascript" src="jquery_code.js"></script>'."\n";
        print '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>'."\n";
        print '<script type="text/javascript" src="jquery.gomap.min.js"></script>'."\n\n";
        print '<link rel="stylesheet" href="SexyButtons/sexybuttons.css" type="text/css" />'."\n";
        //print '<script type="text/javascript" src="JS/DYMO.Label.Framework.latest.js" charset="UTF-8"></script>'."\n";
        print '<link rel="stylesheet" href="style.css" type="text/css" />'."\n";
        print '<link rel="stylesheet" href="jquery-ui.css" type="text/css" />'."\n\n";

        $result = mysql_query("SELECT * FROM event WHERE id='{$_GET['id']}'") or die(mysql_error());
        if(mysql_num_rows($result) > 0)
        {
            while ($row = mysql_fetch_array($result))
            {
                $packLink = "packlista";
                if ($row['type']==1)
                    $packLink = "hämtorder";

                if (isset($_GET['pack']))
                {
                    print "<style>body {font-family: sans-serif;}</style>\n";
                    if ($row['type']==1)
                        print "<h1>Hämtorder</h1>";
                    else
                        print "<h1>Packlista</h1>\n";
                }
                $hjName = mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['cust_id']}'"),0);
                $hjPhone = mysql_result(mysql_query("SELECT phone FROM cust WHERE id='{$row['cust_id']}'"),0);
                $hjCity = mysql_result(mysql_query("SELECT del_city FROM cust WHERE id='{$row['cust_id']}'"),0);
                $hjAdress = mysql_result(mysql_query("SELECT del_address FROM cust WHERE id='{$row['cust_id']}'"),0);
                $hjZip = mysql_result(mysql_query("SELECT del_zip FROM cust WHERE id='{$row['cust_id']}'"),0);
				$shippingMethod = $row['shipping'];
                print "<a href=\"/terab/order/admin/cust.php?showCust=".$row['cust_id']."\"><h3 style=\"margin-bottom: 0px;\">".$hjName." (klicka för att ändra kundinfo)</a></h3>\n";
                print "(tel: ".$hjPhone.")<br /><br />\n";
                print $hjAdress.'<br />'.$hjZip.", <b>$hjCity</b><br /><br />\n";

                if ( !isset($_GET['print']) )
                {
                    //if (!isset($_GET['pack']))
                    //{
                        if (!empty($row['pref_date']))
                            print "<strong>Önskat leveransdatum:</strong> ".date('Y-m-d',(int)$row['pref_date'])."<br />\n";

                        if (!empty($row['order_no']))
                            print "<strong>Ordernummer:</strong> ".$row['order_no']."<br />\n";

                        if (!empty($row['create_date']))
                            print "<strong>Order inlagd:</strong> ".date('Y-m-d',$row['create_date'])."<br />\n";

                        if (!empty($row['order_creator']))
                            print "<strong>Order lagd av:</strong> ".$row['order_creator']."<br />";

                        if (!empty($row['cust_info'])) {
						    if (isset($_GET['pack']))
                                print "<h2>";
                            print "<strong>Kund-information:</strong><br /> ".$row['cust_info']."<br />\n";
							if (isset($_GET['pack']))
                                print "</h2>";
						}
                    //}

                    print "<br /><strong style=\"font-size: large;\">Artiklar:</strong><br />\n";
                    if (isset($_GET['pack']))
                    {
                        print '<table style="width:500px;">'."\n";
                    }

                    $arrAmount = array();
                    $arrName   = array();
                    $result2 = mysql_query("SELECT * FROM event_art WHERE event_id='{$row['id']}'") or die(mysql_error());
                    if(mysql_num_rows($result2) != 0)
                    {
                        while ($row2 = mysql_fetch_array($result2))
                        {
                            if (isset($_GET['pack']))
                            {
                                print "<tr><td style=\"border: solid 1px black; padding:5px;\">";
                            }
                            print $row2['art_amount']." ".$row2['enhet']." ";
                            $arrAmount[] = $row2['art_amount'];
                            $result3 = mysql_query("SELECT COUNT(art_name) FROM art WHERE id='{$row2['art_id']}'");
                            if (mysql_result($result3, 0) > 0)
                            {
                                if (isset($_GET['pack']))
                                {
                                    print "</td><td style=\"border: solid 1px black; padding:5px;\">\n";
                                }
                                $name = mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$row2['art_id']}'"), 0);
                                print $name;
                                $arrName[] = $name;
                            }
                            if (isset($_GET['pack']))
                            {
                                print "</td></tr>";
                            }
                            else
                            {
                                print "<br />\n";
                            }
                        }
                    }
                    if (!isset($_GET['pack']))
                    {
                        print '<br /><br />Redigering:<br /><a class="sexybutton" href="javascript:void(0);" onclick="editEvent('.$_GET['id'].', '.$row['type'].');"><span><span>Redigera</span></span></a>'."\n";
                        print '<a class="sexybutton" href="javascript:void(0);" onclick="changeDate('.$_GET['id'].');"><span><span>Byt datum</span></span></a>'."\n";
                        print '<a class="sexybutton" href="javascript:void(0);" onclick="removeEvent('.$_GET['id'].');"><span><span>Radera bokning</span></span></a>'."\n";

                        if ($row['locked']==1)
                        {
                            print '<a class="sexybutton" href="javascript:void(0);" onclick="toggleLockEvent('.$_GET['id'].');"><span><span>Avmarkera som klar</span></span></a>'."\n";
                        }
                        else
                        {
                            print '<a class="sexybutton" href="javascript:void(0);" onclick="toggleLockEvent('.$_GET['id'].');"><span><span>Markera som klar</span></span></a>'."\n";
                        }

                        print '<br /><br />Utskrifter:<br /><a class="sexybutton" href="http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'&print" target="_blank"><span><span>Skriv ut karta</span></span></a>'."\n";
                        print '<a class="sexybutton" href="http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'&pack" target="_blank"><span><span>Skriv ut '.$packLink.'</span></span></a>'."\n";
                        $r1 = mysql_query("SELECT id FROM waybill WHERE name='feventID' AND value='{$_GET['id']}'");
                        if (mysql_num_rows($r1) > 0)
                        {
                            $wbID = mysql_result($r1, 0);
                            $r2 = mysql_query("SELECT * FROM waybill WHERE id='{$wbID}' AND name='noPrint' AND value='1'");
                            if (mysql_num_rows($r2) > 0) {
                                print '<a class="sexybutton sexysimple sexyred" href="javascript:void(0);" onclick="prePrintCargoList('.$_GET['id'].', \''.$shippingMethod.'\');"><img src="img/add.png" alt="" /> Skriv ut fraktsedel</a>'."\n";
                                if (isset($_SESSION['admin'])) {
                                    print "<br>Spara fraktsedel för att aktivera faktureringsknapp\n";
                                }
                            } else {
                                print '<a class="sexybutton sexysimple sexygreen" href="javascript:void(0);" onclick="prePrintCargoList('.$_GET['id'].', \''.$shippingMethod.'\');"><img src="img/book_previous.png" alt="" />Skriv ut fraktsedel</a>'."\n";
                                if (isset($_SESSION['admin'])) {
                                    $r3 = mysql_query("SELECT id FROM bill WHERE waybill_id='{$wbID}' LIMIT 1");
                                    if (mysql_num_rows($r3) > 0) {
                                        $billId = mysql_result($r3, 0);
                                        print '<br><a class="sexybutton sexysimple sexygreen" href="javascript:void(0);" onclick="billRedirect('.$billId.');">Skriv ut faktura</a>'."\n";
                                    } else {
                                        $date = mysql_result(mysql_query("SELECT value FROM waybill WHERE id='{$wbID}' AND name='fDate'"),0);
                                        print '<br><a class="sexybutton" href="javascript:void(0);" onclick="showBillingPopup('.$row['type'].','.$wbID.',\''.$row['order_no'].'\',\''.$date.'\');"><span><span>Skapa faktura</span></span></a>'."\n";
                                    }
                                }
                            }
                        }
                        else
                            print '<a class="sexybutton sexysimple sexyred" href="javascript:void(0);" onclick="prePrintCargoList('.$_GET['id'].', \''.$shippingMethod.'\');"><img src="img/add.png" alt="" /> Skriv ut fraktsedel</a>'."\n";
                        print '<br /><p style="width:100%;text-align:center;"><a class="sexybutton sexysimple sexyxxl" style="background-color: rgb(150,150,150);" href="javascript:void(0);" onclick="$(\'#showEvent\').dialog(\'close\');">Stäng</a></p>'."\n";
                    }
                }
                if (isset($_GET['pack']))
                {
                    print "</table>\n\n";

                    if ($row['type']==1)
                        $allString = "'Hämtning ";
                    else
                        $allString = "'Leverans ";

                    $allString .= ' '.date("d/m Y", $row['date']).'(utskr. '.date("d/m Y").')\n';
                    $allString .= "$hjName ($hjCity)".'\n';
                    $iCount    = 0;

                    foreach ($arrAmount as $value)
                    {
                        $allString .= $value." ".$arrName[$iCount].'\n';
                        $iCount++;
                    }
                    $allString .= "'";

                    print '<br /><br /><a class="sexybutton noprint" href="javascript:window.print()"><span><span>Skriv ut A4</span></span></a>'."\n";
                    print '<a class="sexybutton noprint" href="javascript:printCustomLabel('.$allString.');"><span><span>Skriv ut etikett</span></span></a>'."\n";
                    print '<a class="sexybutton noprint" href="javascript:window.close()"><span><span>Stäng detta fönster!</span></span></a><br />'."\n";
                }
                if ( isset($_GET['print']) )
                {
                    $result3 = mysql_query("SELECT * FROM cust WHERE id='{$row['cust_id']}'") or die(mysql_error());
                    if(mysql_num_rows($result3) != 0)
                    {
                        while ($row3 = mysql_fetch_array($result3))
                        {
                            print $row3['del_address'].'<br />'.$row3['del_zip'].' '.$row3['del_city']."<br /><br />\n";
                            print '<br /><a class="sexybutton noprint" href="javascript:window.print()"><span><span>Skriv ut</span></span></a><a class="sexybutton noprint" href="javascript:window.close()"><span><span>Stäng detta fönster!</span></span></a><br /><br />'."\n";
                            print '<div id="googleMap" style="width:600px; height:600px;"></div>'."\n";
                            print '<script type="text/javascript">'."\n";
                            print ' $(function() {'."\n";
                            print '    $("#googleMap").goMap({'."\n";
                            print '        scaleControl: true,'."\n";
                            print '        maptype: \'ROADMAP\','."\n";
                            print '        scrollwheel: false,'."\n";
                            print '        disableDoubleClickZoom: true,'."\n";
                            print '        address: \''.$row3['del_address'].', '.$row3['del_zip'].', '.$row3['del_city'].', sverige\','."\n";
                            print '        zoom: 15'."\n";
                            print '    });'."\n";
                            print '    $.goMap.createMarker({'."\n";
                            print '            address: \''.$row3['del_address'].', '.$row3['del_zip'].', '.$row3['del_city'].', sverige\''."\n";
                            print '    });'."\n";
                            print ' });'."\n";
                            print '</script>'."\n";
                        }
                    }
                }
            }
        }
        exit;
    }
    elseif ($_GET['cmd'] == "addEvent")
    {
        //$_GET = array_map("utf8_decode", $_GET);
        if (!empty($_GET['cust_id']))
        {
            $_GET['del_date'] = strtotime($_GET['del_date']);
            if ($_GET['del_date'] != "0")
            {
               $_GET['date'] = $_GET['del_date'];
            }
            $type = $_GET['eventType'];

            $recurring = 0;
            if (isset($_GET['recurring']))
                $recurring = 1;

            $create_date = time();
            mysql_query("INSERT INTO event (cust_id, date, pref_date, create_date, order_no, order_creator, cust_info, type, recurring) VALUES
             ('{$_GET['cust_id']}', '{$_GET['date']}', '{$_GET['del_date']}', '$create_date', '{$_GET['order_no']}', '{$_GET['bestName']}', '{$_GET['cust_info']}', '$type', '$recurring')")
             or die(mysql_error());
            $event_id = mysql_insert_id();
            //print "<pre>";
            for ($i = 0; $i < $_GET['artFieldNo']; $i++)
            {
              $number = $i + 1;
              if (!empty($_GET['artID'.$number]) && !empty($_GET['art_amount'.$number]) && !empty($_GET['art_name'.$number]))
              {
                  //print_r($_GET);
                  mysql_query("INSERT INTO event_art (event_id, art_id, art_amount, enhet) VALUES ('{$event_id}', '{$_GET['artID'.$number]}', '{$_GET['art_amount'.$number]}', '{$_GET['art_enhet'.$number]}')") or die(mysql_error());
              }
            }
        }
        if ( isset($_GET['tmstp']) )
            header("Location: index.php?tmstp=".$_GET['tmstp']);
        else
            header("Location: index.php");
        exit;
    }
    elseif($_GET['cmd'] == "editEvent")
    {
       //custID, event_ID, fieldAmount, artAmount1, artName1, art_ID1, orderNo, weightE, volumeE, loading_meterE, custInfo, delDate
        if (!empty($_GET['custID']))
        {
            $_GET['delDate'] = strtotime($_GET['delDate']);

            mysql_query("UPDATE event SET cust_id='{$_GET['custID']}', pref_date='{$_GET['delDate']}', order_no='{$_GET['orderNo']}', order_creator='{$_GET['bestEditName']}', cust_info='{$_GET['custInfo']}' WHERE id='{$_GET['event_ID']}'")
             or die(mysql_error());

            mysql_query("DELETE FROM event_art WHERE event_id='{$_GET['event_ID']}'") or die(mysql_error());

            for ($i = ($_GET['fieldAmount']+1); $i > 0; $i--)
            {
                $number = $i;// + 1;
                if (!empty($_GET['art_ID'.$number]) && !empty($_GET['artAmount'.$number]) && !empty($_GET['artName'.$number]))
                {
                    mysql_query("INSERT INTO event_art (event_id, art_id, art_amount, enhet) VALUES ('{$_GET['event_ID']}', '{$_GET['art_ID'.$number]}', '{$_GET['artAmount'.$number]}', '{$_GET['artEnhet'.$number]}')") or die(mysql_error());
                    $event_id = mysql_insert_id();
                    $art_name = mysql_result(mysql_query("SELECT art_name FROM art WHERE id='{$_GET['art_ID'.$number]}'"), 0);

                    // --   waybill update
                    // ----    OMG, never again!  Merge tables or take proper project specs next time!
                    //-------------------
                    $result = mysql_query("SELECT id FROM waybill WHERE name='feventID' AND value='{$_GET['event_ID']}' ORDER BY id DESC") or die("1-".mysql_error());
                    if (mysql_num_rows($result) != 0)
                    {
                        $waybill_no = mysql_result($result, 0);

                        //  prod-ID  Godsmärk.   kolli       kollislag    namn     varunr.    tot.vikt    volym       1st - org.vikt
                        // fArtID1, fArtMark1, fArtAmount1, fArtType1, fArtName1, fArtNo1, fArtWeight1, fArtVolume1, fOrgArtWeight1

                        $res = mysql_query("SELECT value FROM `waybill` WHERE id='{$waybill_no}' AND name='fArtName{$number}'");
                        if ((mysql_num_rows($res) != 0) && mysql_result($res, 0) == $art_name)
                        {
                            $art_name   = mysql_result($res, 0);
                            $art_kolli  = @mysql_result(mysql_query("SELECT value FROM waybill WHERE id='{$waybill_no}' AND name='fArtAmount{$number}'"), 0);
                            if ( $art_kolli != $_GET['artAmount'.$number] )
                            {
                                // product name is the same for current row so just adjust amount and weight
                                if ($_GET['artEnhet'.$number] != "st")
                                {
                                    mysql_query("UPDATE waybill SET value='{$_GET['artAmount'.$number]}' WHERE id='{$waybill_no}' AND name='fArtMark{$number}'");
                                    mysql_query("UPDATE waybill SET value='' WHERE id='{$waybill_no}' AND name='fArtVolume{$number}'");
                                    $weight = @mysql_result(mysql_query("SELECT value FROM waybill WHERE id='{$waybill_no}' AND name='fOrgArtWeight{$number}'"), 0);
                                    $weight = round(($weight * $_GET['artAmount'.$number]), 1);
                                    mysql_query("UPDATE waybill SET value='$weight' WHERE id='{$waybill_no}' AND name='fArtWeight{$number}'");
                                    mysql_query("UPDATE waybill SET value='' WHERE id='{$waybill_no}' AND name='fArtAmount{$number}'");
                                    mysql_query("UPDATE waybill SET value='{$_GET['artEnhet'.$number]}' WHERE id='{$waybill_no}' AND name='fArtType{$number}'");
                                }
                                else
                                {
                                    mysql_query("UPDATE waybill SET value='st' WHERE id='{$waybill_no}' AND name='fArtType{$number}'");
                                    mysql_query("UPDATE waybill SET value='' WHERE id='{$waybill_no}' AND name='fArtMark{$number}'");
                                    mysql_query("UPDATE waybill SET value='' WHERE id='{$waybill_no}' AND name='fArtVolume{$number}'");
                                    $weight = @mysql_result(mysql_query("SELECT value FROM waybill WHERE id='{$waybill_no}' AND name='fOrgArtWeight{$number}'"), 0);
                                    $weight = round(($weight * $_GET['artAmount'.$number]), 1);
                                    mysql_query("UPDATE waybill SET value='$weight' WHERE id='{$waybill_no}' AND name='fArtWeight{$number}'");
                                    mysql_query("UPDATE waybill SET value='{$_GET['artAmount'.$number]}' WHERE id='{$waybill_no}' AND name='fArtAmount{$number}'");
                                }
                            }
                        }
                        else
                        {
                            // product name has changed, discard everything with $number in waybill and add as new prod.
                            // this is also true if it's a new product, that has been added to the order
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtID{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtMark{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtAmount{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtType{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtName{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtNo{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtWeight{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fArtVolume{$number}'");
                            @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fOrgArtWeight{$number}'");
                            
                            $weight = @mysql_result(mysql_query("SELECT weight FROM art WHERE id='{$_GET['art_ID'.$number]}'"), 0);
                            $total_weight = round(($weight * $_GET['artAmount'.$number]), 1);

                            mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtID{$number}', '{$_GET['art_ID'.$number]}')");
                            if ($_GET['artEnhet'.$number] != "st")
                            {
                                mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtMark{$number}', '{$_GET['artAmount'.$number]}')");
                                mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtAmount{$number}', '')");
                                mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtType{$number}', '{$_GET['artEnhet'.$number]}')");
                            }
                            else
                            {
                                mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtMark{$number}', '')");
                                mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtAmount{$number}', '{$_GET['artAmount'.$number]}')");
                                mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtType{$number}', 'st')");
                            }
                            mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtName{$number}', '{$_GET['artName'.$number]}')");
                            mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtNo{$number}', '')");
                            mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtWeight{$number}', '{$total_weight}')");
                            mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fArtVolume{$number}', '')");
                            mysql_query("INSERT INTO waybill (id, name, value) VALUES ('{$waybill_no}', 'fOrgArtWeight{$number}', '{$weight}')");
                        }

                        // Delete field no. saved for waybill! This was a nasty one to find.... :/
                        @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fNoArtFields'");
                        // same thing for total articles no.
                        @mysql_query("DELETE FROM waybill WHERE id='{$waybill_no}' AND name='fKAmount'");

                        // Check for customer changes!
                        // $_GET['custID']
                        //  mGAN   mName  mAddress  mZip  mCity  mRef
                        $custName = @mysql_result(mysql_query("SELECT value FROM waybill WHERE id='{$waybill_no}' AND name='mName'"), 0);
                        $res5 = mysql_query("SELECT * FROM cust WHERE id='{$_GET['custID']}'") or die(mysql_error());
                        if (mysql_num_rows($res5) > 0)
                        {
                            while ($row5 = mysql_fetch_array($res5))
                            {
                                if ($custName != $row5['name'])
                                {
                                    if ($row5['schenker_no'] != "")
                                        $mGAN = $row5['schenker_no'];
                                    if ($row5['dhl_no'] != "")
                                        $mGAN = $row5['dhl_no'];
                                    if ($row5['other_no'] != "")
                                        $mGAN = $row5['other_no'];

                                    mysql_query("UPDATE waybill SET value='{$mGAN}' WHERE id='{$waybill_no}' AND name='mGAN'");
                                    mysql_query("UPDATE waybill SET value='{$row5['name']}' WHERE id='{$waybill_no}' AND name='mName'");
                                    mysql_query("UPDATE waybill SET value='{$row5['del_address']}' WHERE id='{$waybill_no}' AND name='mAddress'");
                                    mysql_query("UPDATE waybill SET value='{$row5['del_zip']}' WHERE id='{$waybill_no}' AND name='mZip'");
                                    mysql_query("UPDATE waybill SET value='{$row5['del_city']}' WHERE id='{$waybill_no}' AND name='mCity'");
                                    mysql_query("UPDATE waybill SET value='' WHERE id='{$waybill_no}' AND name='mRef'");
                                }
                            }
                        }
                        mysql_query("UPDATE waybill SET value='{$_GET['orderNo']}' WHERE id='{$waybill_no}' AND name='mRef'");
                    }
                    // -- waybill update
                    //-------------------
                }
            }
        }
        //print "<pre>";        print_r($_GET);        exit;
        if ( isset($_GET['timestamp']) )
            header("Location: index.php?tmstp=".$_GET['timestamp']);
        else
            header("Location: index.php");
        exit;
    }
    elseif ($_GET['cmd'] == "addCust")
    {
        //$GET = array_map('utf8_decode', $_GET);  // Fix jQuery UTF-8

        $type = $_GET['custType'];
        $query  = "INSERT INTO cust ";
        $query .= "(name, del_address, del_zip, del_city, bill_name, bill_address, bill_zip, bill_box, bill_city, ";
        $query .= "phone, mobile_phone, contact, contact_email, fax, vat_reg_no, giro_no, type, ";
        $query .= "alt_name, del_country, att, bill_country, contact_email1, contact2, mob2, contact_email2, ";
        $query .= "schenker_no, dhl_no, other_no, f_delinfo1, f_delinfo2) VALUES (";
        $query .= "'{$_GET['name']}', '{$_GET['del_address']}', '{$_GET['del_zip']}', '{$_GET['del_city']}', '{$_GET['bill_name']}', ";
        $query .= "'{$_GET['bill_address']}', '{$_GET['bill_zip']}', '{$_GET['bill_box']}', '{$_GET['bill_city']}', ";
        $query .= "'{$_GET['phone']}', '{$_GET['mobile_phone']}', '{$_GET['contact']}', '{$_GET['contact_email']}', '{$_GET['fax']}', ";
        $query .= "'{$_GET['vat_reg_no']}', '{$_GET['giro_no']}', '$type', '{$_GET['alt_name']}', '{$_GET['del_country']}', '{$_GET['att']}', ";
        $query .= "'{$_GET['bill_country']}', '{$_GET['contact_email1']}', '{$_GET['contact2']}', '{$_GET['mob2']}', '{$_GET['contact_email2']}', ";
        $query .= "'{$_GET['schenker_no']}', '{$_GET['dhl_no']}', '{$_GET['other_no']}', '{$_GET['f_delinfo1']}', '{$_GET['f_delinfo2']}')";

        mysql_query($query) or die(mysql_error());
        
	$sql = "insert into customers(name, address, zip, city, contact_person, contact_phone, contact_email, contact_fax, vat_reg_no, bankgiro_no, default_report_type)
         values('".$_GET['name']."','".$_GET['del_address']."','".$_GET['del_zip']."','".$_GET['del_city']."','".$_GET['contact']."','".$_GET['phone']."','".$_GET['contact_email']."',
         '".$_GET['fax']."','".$_GET['vat_reg_no']."','".$_GET['giro_no']."', 'customer_letter')";
	mysql_query($sql);
        exit;
    }
    elseif ($_GET['cmd'] == "addArt")
    {
        $type = $_GET['artType'];
        //$GET = array_map('utf8_decode', $_GET);  // Fix jQuery UTF-8
        $_GET['weight'] = str_replace ( ',' , '.' , $_GET['weight'] );       // fix for mysql float
        $_GET['volume'] = str_replace ( ',' , '.' , $_GET['volume'] );
        mysql_query("INSERT INTO art (art_no, art_name, weight, img, type) VALUES ('{$_GET['art_no']}', '{$_GET['art_name']}', '{$_GET['weight']}', '{$_GET['art_img']}', '$type')") or die(mysql_error());
        $art_id = mysql_insert_id();
        mysql_query("INSERT INTO price (art_id, price) VALUES ('{$art_id}', '{$_GET['price']}')") or die(mysql_error());
        header ("Location: index.php");
        exit;
    }
}
else if (isset($_GET['term']))
{
    header ("Content-Type: application/json; charset=utf-8");    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
    header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header ("Pragma: no-cache"); // HTTP/1.0
    header ("Content-Type: application/json; charset=utf-8");

    $input    = strtolower(urldecode($_GET['term']));
    $len      = strlen($input);
    $limit    = isset($_GET['limit']) ? (int) $_GET['limit'] : 0;
    $aDB      = array();
    $aResults = array();

    $count = 0;

    if ($_GET['auto'] == 1)
        $result = mysql_query("SELECT * FROM cust WHERE type=0 AND name LIKE '{$input}%'") or die(mysql_error());
    elseif ($_GET['auto'] == 2)
        $result = mysql_query("SELECT * FROM cust WHERE type=1 AND name LIKE '{$input}%'") or die(mysql_error());
    elseif ($_GET['auto'] == 3)
        $result = mysql_query("SELECT * FROM art WHERE type=0 AND art_name LIKE '{$input}%'") or die(mysql_error());
    elseif ($_GET['auto'] == 4)
        $result = mysql_query("SELECT * FROM art WHERE type=1 AND art_name LIKE '{$input}%'") or die(mysql_error());

    if(mysql_num_rows($result) > 0)
    {
        print "[";
        $count = 0;
        while ($row = mysql_fetch_array($result))
        {
            if ($count != 0)
                print ",";
            $count++;

            if ($_GET['auto'] == 1 || $_GET['auto'] == 2) {
                print '{"id": "'.$row['id'].'","label": "'.htmlspecialchars($row['name'])." (".$row['del_city'].")".'"}';
            } else if ($_GET['auto'] == 3 || $_GET['auto'] == 4) {
                $prod_cat = mysql_result(mysql_query("SELECT COUNT(*) FROM `prod_cat` WHERE `cat` LIKE '%krag%' AND `prod_id`='{$row['id']}'"),0);
                print '{"id": "'.$row['id'].'", "label": "'.htmlspecialchars($row['art_name']).'", "cat_id": "'.$prod_cat.'"}';
            }

        }
        print "]";
    }
    exit;
}