    <!--
          Show dialog containing booking information
     -->
    <div id="showEvent" title="Visa bokning" style="display: none;">
    </div>

    <!--
        Show dialog for creating new bill
    -->
    <div id="addBill" title="Skapa ny faktura" style="display: none;">
    </div>

    <!--
          Show dialog for changing date
     -->
    <div id="changeDate" title="Ändra datum" style="display: none;">
        <form action="index.php" method="get">
        <p>
          <input type="hidden" name="cmd" value="changeDate" />
          <input type="hidden" id="eventID" name="eventID" value="0" />

          <input type="text" style="width:150px" id="new_date" name="new_date" value="" />
          <input type="submit" name="newDate" value="Flytta bokning" />
        </p>
        </form>
    </div>
	
    <!--
          Show dialog for shipping settings
     -->
    <div id="shippingSetting" title="Välj fraktsätt" style="display: none;">
        <form action="index.php" method="get">
        <p>
          <input type="hidden" name="cmd" value="pickShipping" />
          <input type="hidden" id="shipping_eventID" name="shipping_eventID" value="0" />

          <label><input type="radio" name="shippingMethodName" value="TERAB" /> TERAB</label><br /><br />
		  <label><input type="radio" name="shippingMethodName" value="Hämtat själv" /> Hämtat själv</label><br /><br />
          <label><input type="radio" name="shippingMethodName" id="otherShipping" value="Annat" /> Annat fraktbolag</label><br />
          <input type="text" style="width:150px" id="otherShippingText" name="otherShippingText" value="" /> Ange vilket
        </p>
		<a onclick="checkShippingForm();" href="javascript:void(0);" class="sexybutton sexysimple">Spara fraktsätt</a>
        </form>
    </div>
    
    <!--
          Show dialog for changing week
     -->
    <div id="changeWeek" title="Ändra vecka" style="display: none;">
        <form action="index.php" method="get">
        <p>
          <input type="hidden" name="cmd" value="changeWeek" />

          <input type="text" style="width:150px" id="new_week" name="new_week" value="" />
          <input type="submit" name="newWeek" value="Visa vecka" />
        </p>
        </form>
    </div>

    
    <!--
          Show dialog for editing notes
     -->
    <div id="editNote" title="Ändra dagens noteringar" style="display: none;">
        <form action="index.php" method="get">
        <p>
          <input type="hidden" name="cmd" value="editNote" />
          <input type="hidden" id="noteDate" name="noteDate" value="" />

          <textarea style="width:600px;height:320px;" id="note" name="note"></textarea><br /><br />
          <div style="width:100%; text-align:center;"><button type="submit" name="newNote"><img alt="" src="/TERAB/order/img/page_save.png"> Spara noteringar</button></div>
        </p>
        </form>
    </div>


    <!--
          Show dialog containing products after category
     -->
    <div id="showAddArtCat" title="Lägg till produkt" style="display: none;">
    </div>


    <!--
          Show dialog containing previous orders
     -->
    <div id="showPrevOrders" title="Tidigare ordrar" style="display: none;">
    </div>


    <!--
          Show dialog for changing sender in waybill
     -->
    <div id="fHandleSender" title="Ändra avsändare" style="display: none;">
      <form method="get" action="" onsubmit="return false;">
        <p>
          <div id="fSenderList"></div><br />

          <input type="hidden" id="fhSenderID" name="fhSenderID" value="0" />

          <label for="fhGAN" style="display:block;">
              <span style="float:left; width:45%;">GAN</span>
              <input style="width:45%;" type="text" name="fhGAN" id="fhGAN" value="" />
          </label>
          <label for="fhName" style="display:block;">
              <span style="float:left; width:45%;">Avsändare</span>
              <input style="width:45%;" type="text" name="fhName" id="fhName" value="" />
          </label>
          <label for="fhAddress" style="display:block;">
              <span style="float:left; width:45%;">Adress</span>
              <input style="width:45%;" type="text" name="fhAddress" id="fhAddress" value="" />
          </label>
          <label for="fhZip" style="display:block;">
              <span style="float:left; width:45%;">Postnummer</span>
              <input style="width:45%;" type="text" name="fhZip" id="fhZip" value="" />
          </label>
          <label for="fhCity" style="display:block;">
              <span style="float:left; width:45%;">Stad</span>
              <input style="width:45%;" type="text" name="fhCity" id="fhCity" value="" />
          </label>
          <label for="fhPhone" style="display:block;">
              <span style="float:left; width:45%;">Telefon / Fax</span>
              <input style="width:45%;" type="text" name="fhPhone" id="fhPhone" value="" />
          </label>
          <label for="fhRef" style="display:block;">
              <span style="float:left; width:45%;">Referenser</span>
              <input style="width:45%;" type="text" name="fhRef" id="fhRef" value="" />
          </label>
          <br /><br />
          <a class="sexybutton" href="javascript:void(0);" onclick="fhDelSender();"><span><span>Radera avsändare</span></span></a> &#160;
          <a class="sexybutton" href="javascript:void(0);" onclick="fhSelectSender();"><span><span>Välj avsändare</span></span></a>
        </p>
      </form>
    </div>


    <!--
        Show dialog for adding an article
     -->
    <div id="addArt" title="Skapa ny artikel" style="display: none;">
        <form method="get" action="../index.php" onsubmit="postFormAJAX(this); return false;">
          <p>
	    <input type="hidden" name="cmd" value="addArt" />
	    <input type="hidden" id="artType" name="artType" value="0" />

            Artikelns pris är standardvärde, kan justeras för enskilda kunder.
            <table style="width:100%; padding-top:0px;">
            <tr><td class="white" colspan="2">

            <a href="javascript:void(0);" onclick="openKCFinder('art_img');">Klicka här för att välja produktbild</a>
            <div id="art_img_img"><img src="/TERAB/order/img/error.png" alt="" /></div>
	    <input type="hidden" name="art_img" id="art_img" value="" />

	    </td></tr><tr><td class="white">

	    Artikelnummer: </td><td class="white">
	    <input type="text" name="art_no" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 20px;">

	    Artikelnamn: </td><td class="white">
	    <input type="text" name="art_name" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 40px;">

	    Vikt (kg): </td><td class="white">
	    <input type="text" name="weight" value="" />

	    </td></tr><tr><td class="white" style="padding-left: 30px;">
	    
	    Artikelpris: </td><td class="white">
	    <input type="text" name="price" value="" />

            </td></tr></table><br />

	    <input type="submit" style="width:150px" id="save_art" value="Spara" />
          </p>
        </form>
    </div>
    


    <!--
          Show dialog for adding a new booking
      -->
    <div id="addEvent" title="Skapa ny bokning" style="display: none;">
        <form method="get" id="addEventForm" action="index.php">
	  <p>
	    <input type="hidden" name="cmd" value="addEvent" />
	    <input type="hidden" id="PUG" value="0" />
<?php
  if(isset($_GET['tmstp'])) {
      print '	    <input type="hidden" id="tmstp" name="tmstp" value="'.$_GET['tmstp'].'" />'."\n";
  }
?>
	    <input type="hidden" id="eventType" name="eventType" value="0" />
	    <input type="hidden" id="date" name="date" value="0" />
	    <input type="hidden" id="artFieldNo" name="artFieldNo" value="1" />
	    <input type="hidden" id="cust_id" name="cust_id" value="0" />

            <center><b>Beställning lagd utav:</b> &#160;
            <input type="text" name="bestName" id="bestName" /></center>

            <table style="width:100%; padding:0px; margin:0px;"><tr><td class="white">

            Kund: <br />
            <div id="custField"></div>

	    </td><td class="white">

            <div id="newCust"></div>

	    </td></tr><tr><td class="white">

            <input type="hidden" id="artID1" name="artID1" value="" />

            <table style="width:250px; padding:0px; margin:0px;"><tr><td class="white" style="padding:0px;margin:0px;">
	    Antal: <br />
	    <input type="text" style="width:30px" id="art_amount1" name="art_amount1" value="" />
            </td><td class="white">
	    Enhet: <br />
	    <input type="text" style="width:30px" id="art_enhet1" name="art_enhet1" value="st" />
            </td><td class="white">
	    Artikel: <br />
	    <div id="artField"></div>
	    </td></tr></table>

	    <div id="moreArticleFields2" style="margin-left: 1px; margin-top:0px;"> </div>
	    <div id="moreArtLink"></div>

	    </td><td class="white">

            <div id="newArtField"></div>

	    </td></tr><tr><td class="white">

            <br />
            Ordernummer: <br />
            <input type="text" style="width:200px" id="order_no" name="order_no" value="" />

            </td><td class="white"> <!-- <br />
            <a href="javascript:void(0);" onclick="getLatestOrderNo();">
              <img src="img/book_link.png" class="vmiddle" alt="Hämta ordernummer" title="Hämta ordernummer" />
              Hämta senaste ordernummret
            </a> -->
            <label><input type="checkbox" name="recurring" id="recurring" value="yes" /> Återkommande order veckovis</label>
            </td></tr><tr><td class="white" colspan="2">

            Extra information: <br />
            <input type="text" style="width:380px" id="cust_info" name="cust_info" value="" />

            </td></tr><tr><td class="white">

	    Önskat leveransdatum: <br />
            <input type="text" style="width:200px" id="del_date" name="del_date" value="" />
            </td><td class="white">
            <a class="sexybutton" href="javascript:void(0);" onclick="editDate();">
            <span><span>
              <img src="img/calendar_edit.png" class="vmiddle" alt="Ändra datum" title="Ändra datum" />
              Ändra datum
            </span></span>
            </a>
            </td></tr></table><br />

            <button name="save_booking" id="save_booking" style="width:150px" type="submit"><img src="img/page_save.png" alt=""> Spara</button>
          </p>
	</form>
    </div>


    <!--
         Show dialog for adding a new customer
      -->
    <div id="addCust" title="Skapa ny kund" style="display: none;">
	<form method="get" action="index.php" onsubmit="postFormAJAX(this); return false;">
          <p>
            <input type="hidden" name="cmd" value="addCust" />
            <input type="hidden" id="custType" name="custType" value="0" />

            <div id="custTabs">
                <ul>
                    <li><a href="#Ctabs-1">Grundläggande uppgifter</a></li>
                    <li><a href="#Ctabs-2">Fakturauppgifter</a></li>
                    <li><a href="#Ctabs-3">Transportnummer</a></li>
                    <li><a href="#Ctabs-4">Leveransanvisning</a></li>
                </ul>
                <div id="Ctabs-1">
                    <table style="width:100%; padding-top:0px; margin:0px;">

                      <tr>
                        <td class="white">
        	          <div id="add_kundnamn">Kundnamn:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="name" value="" tabindex="1" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Telefonnummer:
                        </td>
                        <td class="white">
                          <input type="text" name="phone" value="" tabindex="8" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
        	            <div id="alt_kundnamn">Alt. kundnamn:</div>
                        </td>
                        <td class="white">
                            <input type="text" name="alt_name" value="" tabindex="2" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Kontaktperson:
                        </td>
                        <td class="white">
                          <input type="text" name="contact" value="" tabindex="9" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_address">Leverans-adress:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_address" value="" tabindex="3" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Mobiltelefon-nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="mobile_phone" value="" tabindex="10" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_zip">Leverans-postnummer:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_zip" value="" tabindex="4" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                        </td>
                        <td class="white">
                          Kontakt e-post:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email1" value="" tabindex="11" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          <div id="add_city">Leverans-stad:</div>
                        </td>
                        <td class="white">
                          <input type="text" name="del_city" value="" tabindex="5" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Alt kontaktperson:
                        </td>
                        <td class="white">
                          <input type="text" name="contact2" value="" tabindex="12" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Leverans land:
                        </td>
                        <td class="white">
                          <input type="text" name="del_country" value="" tabindex="6" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Alt. mobiltelefon-nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="mob2" value="" tabindex="13" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fax:
                        </td>
                        <td class="white">
                          <input type="text" name="fax" value="" tabindex="7" />
                        </td>
                        <td class="white">
                          Alt. kontakt e-post:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email2" value="" tabindex="14" />
                        </td>
                      </tr>

                    </table>

                </div>
                <div id="Ctabs-2">

                    <table style="width:100%; padding-top:0px; margin:0px;">

                      <tr>
                        <td class="white">
                          Fakturerings-kund:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_name" value="" tabindex="15" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Email:
                        </td>
                        <td class="white">
                          <input type="text" name="contact_email" value="" tabindex="20" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Att:
                        </td>
                        <td class="white">
                          <input type="text" name="att" value="" tabindex="16" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Org nummer:
                        </td>
                        <td class="white">
                          <input type="text" name="vat_reg_no" value="" tabindex="21" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-adress:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_address" value="" tabindex="17" onchange="this.value = toPascalCase(this.value);" />
                        </td>
                        <td class="white">
                          Bank/Plusgiro:
                        </td>
                        <td class="white">
                          <input type="text" name="giro_no" value="" tabindex="22" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-postnummer:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_zip" value="" tabindex="18" onchange="if(this.value.substr(3, 1) != ' ') { this.value = this.value.substr(0, 3) + ' ' + this.value.substr(3); }" />
                        </td>
                        <td class="white">
                          Fakturerings-box:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_box" value="" tabindex="23" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Fakturerings-stad:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_city" value="" tabindex="19" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                        <td class="white">
                          Fakturerings-land:
                        </td>
                        <td class="white">
                          <input type="text" name="bill_country" value="" tabindex="24" onchange="this.value = this.value.toUpperCase();" />
                        </td>
                      </tr>

                    </table>
                </div>
                <div id="Ctabs-3">

                    <table style="width:70%; padding-top:0px; margin:0px;">
                      <tr>
                        <td class="white">
                          Schenker:
                        </td>
                        <td class="white">
                          <input type="text" name="schenker_no" value="" tabindex="25" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          DHL:
                        </td>
                        <td class="white">
                          <input type="text" name="dhl_no" value="" tabindex="26" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Annan:
                        </td>
                        <td class="white">
                          <input type="text" name="other_no" value="" tabindex="27" />
                        </td>
                      </tr>

                    </table>

                </div>
                <div id="Ctabs-4">
                   <p>Leveransanvisningar att visas på fraktsedeln.</p>
                   <table style="width:70%; padding-top:0px; margin:0px;">
                      <tr>
                        <td class="white">
                          Rad 1:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo1" id="f_delinfo1" value="" tabindex="28" />
                        </td>
                      </tr>

                      <tr>
                        <td class="white">
                          Rad 2:
                        </td>
                        <td class="white">
                          <input type="text" name="f_delinfo2" id="f_delinfo2" value="" tabindex="29" />
                        </td>
                      </tr>

                    </table>

                </div>
            </div>

	    <br /><button type="submit" style="width:150px" id="save_cust" name="save_cust"><img alt="" src="img/page_save.png"> Spara</button>
          </p>
	</form>
    </div>


    <!--
          Show dialog for editing a booking
      -->
    <div id="editEvent" title="Ändra bokning" style="display: none;">
        <form method="get" action="index.php" onsubmit="return check_booker('bestEditName');">
	  <p>
	    <input type="hidden" name="cmd" value="editEvent" />
<?php
  if(isset($_GET['tmstp'])) {
      print '	    <input type="hidden" id="timestamp" name="timestamp" value="'.$_GET['tmstp'].'" />';
  }
?>
	    <input type="hidden" id="fieldAmount" name="fieldAmount" value="1" />
	    <input type="hidden" id="event_ID" name="event_ID" value="0" />
            <input type="hidden" id="art_type" name="art_type" value="0" />
            <input type="hidden" id="custID" name="custID" value="0" />

            <center><b>Beställning lagd utav:</b> &#160;
            <input type="text" name="bestEditName" id="bestEditName" value="" /></center>
            

            <table style="width:100%; padding:0px; margin:0px;"><tr><td class="white" colspan="2">
            
            Kund: <br />
            <div id="custEditField"></div>

	    </td></tr><tr><td class="white" colspan="2">

            <table style="width:350px; padding:0px; margin:0px;"><tr><td class="white" style="padding:0px;margin:0px;">
	    Antal: <br />
	    <input type="text" style="width:40px" id="artAmount1" name="artAmount1" value="" />
            </td><td class="white">
	    Enhet: <br />
	    <input type="text" style="width:40px" id="artEnhet1" name="artEnhet1" value="" />
            </td><td class="white">
	    Artikel: <br />

	    <div id="art_input"> </div>

	    </td></tr></table>
            <input type="hidden" id="art_ID1" name="art_ID1" value="" />
	    <div id="moreArtFields2" style="margin-left: 2px;"> </div>

	    <div id="moreLink"> </div>

	    </td></tr><tr><td class="white" colspan="2">

            <br />
            Ordernummer: <br />
            <input type="text" style="width:200px" id="orderNo" name="orderNo" value="" />

            </td></tr><tr><td class="white" colspan="2">

            Extra information: <br />
            <input type="text" style="width:380px" id="custInfo" name="custInfo" value="" />

            </td></tr><tr><td class="white" colspan="2">

	    Önskat leveransdatum: <br />
            <input type="text" style="width:200px" id="delDate" name="delDate" value="" />

            </td></tr></table><br />

	    <input type="submit" style="width:150px" id="saveEdit" value="Spara" />
          </p>
	</form>
    </div>
    
    <!--
         Dialog för fraktsedel
      -->
    <div id="printCargoList" title="Skriv ut fraktsedel" style="display: none;">
	<form method="get" id="cargoListForm" action="fraktsedel.php" target="_blank" onsubmit="saveWayBill(0);">
	  <p>
            <input type="hidden" name="feventID" id="feventID" value="0" />
            <table style="width:100%; padding-top:0px; margin:0px;">
              <tr>
                <td class="white" style="vertical-align: top;">

                    <fieldset>
                      <legend>Avsändarinformation</legend>
                      <label for="aGAN" style="display:block;">
                          <span style="float:left; width:45%;">GAN/kundnr</span>
                          <input style="width:45%; height: 12px;" type="text" name="aGAN" id="aGAN" value="" />
                      </label>
                      <label for="aName" style="display:block;">
                          <span style="float:left; width:45%;">Avsändare</span>
                          <input style="width:45%; height: 12px;" type="text" name="aName" id="aName" value="" />
                      </label>
                      <label for="aAddress" style="display:block;">
                          <span style="float:left; width:45%;">Adress</span>
                          <input style="width:45%; height: 12px;" type="text" name="aAddress" id="aAddress" value="" />
                      </label>
                      <label for="aZip" style="display:block;">
                          <span style="float:left; width:45%;">Postnummer</span>
                          <input style="width:45%; height: 12px;" type="text" name="aZip" id="aZip" value="" />
                      </label>
                      <label for="aCity" style="display:block;">
                          <span style="float:left; width:45%;">Ort</span>
                          <input style="width:45%; height: 12px;" type="text" name="aCity" id="aCity" value="" />
                      </label>
                      <label for="aPhone" style="display:block;">
                          <span style="float:left; width:45%;">Telefon / Fax</span>
                          <input style="width:45%; height: 12px;" type="text" name="aPhone" id="aPhone" value="" />
                      </label>
                      <label for="aRef" style="display:block;">
                          <span style="float:left; width:45%;">Referenser</span>
                          <input style="width:45%; height: 12px;" type="text" name="aRef" id="aRef" value="" />
                      </label>
                      <a class="sexybutton" href="javascript:void(0);" onclick="fHandleSender();"><span><span>Välj/Hantera avsändare</span></span></a>
	            </fieldset>

                </td>
                <td class="white" style="vertical-align: top;">

                    <fieldset>
                      <legend>Mottagarinformation</legend>
                      <label for="mGAN" style="display:block;">
                          <span style="float:left; width:45%;">GAN/kundnr</span>
                          <input style="width:45%;" type="text" name="mGAN" id="mGAN" value="" />
                      </label>
                      <label for="mName" style="display:block;">
                          <span style="float:left; width:45%;">Mottagare</span>
                          <input style="width:45%;" type="text" name="mName" id="mName" value="" />
                      </label>
                      <label for="mAddress" style="display:block;">
                          <span style="float:left; width:45%;">Adress</span>
                          <input style="width:45%;" type="text" name="mAddress" id="mAddress" value="" />
                      </label>
                      <label for="mZip" style="display:block;">
                          <span style="float:left; width:45%;">Postnummer</span>
                          <input style="width:45%;" type="text" name="mZip" id="mZip" value="" />
                      </label>
                      <label for="mCity" style="display:block;">
                          <span style="float:left; width:45%;">Ort</span>
                          <input style="width:45%;" type="text" name="mCity" id="mCity" value="" />
                      </label>
                      <label for="mRef" style="display:block;">
                          <span style="float:left; width:45%;">Referenser</span>
                          <input style="width:45%;" type="text" name="mRef" id="mRef" value="" />
                      </label>
	            </fieldset>

                    <a class="sexybutton" href="javascript:void(0);" onclick="fSwitchSender();" style="margin: 5px;"><span><span><img src="img/arrow_refresh.png" style="border:0px;" alt="" /> Växla mottagare/avsändare</span></span></a>

                </td>
                <td class="white" style="vertical-align: top; width: 100px;">

                    <fieldset>
                      <legend>Datum & Frakts.nr.</legend>
                      <label for="fDate" style="display:block;">
                          <span>Utskriftsdatum</span> <br />
                          <input style="width:100px; height: 12px;" type="text" name="fDate" id="fDate" value="" />
                      </label>
                      <label for="fNo" style="display:block;">
                          <span>Fraktsedelnr</span> <br />
                          <input style="width:100px; height: 12px;" type="text" readonly="readonly" name="fNo" id="fNo" value="" />
                      </label>
	            </fieldset>
                     
                    <fieldset>
                      <legend>Fraktbetalning</legend>
                      <input type="checkbox" name="fSenderPays" id="fSenderPays" value="true" />
                      <label for="fSenderPays">Avsändaren</label><br />

                      <input type="checkbox" name="fRePays" id="fRePays" value="true" />
                      <label for="fRePays">Mottagaren</label><br />

                      <input type="checkbox" name="fOtherPays" id="fOtherPays" value="true" />
                      <label for="fOtherPays">Annan</label><br />

                      <label for="fTransNo" style="display:block;">
                          <span>Transp. avtals.nr</span><br />
                          <input style="width:100px; height: 12px;" type="text" name="fTransNo" id="fTransNo" value="" />
                      </label>
	            </fieldset>
	            
                </td>
                <td class="white" style="vertical-align: top; width: 80px;">

                    <fieldset>
                      <legend>Lev.anvisning</legend>
                      <input type="text" style="width:90%;" name="fDelInfo1" id="fDelInfo1" value="" /> <br />
                      <input type="text" style="width:90%;" name="fDelInfo2" id="fDelInfo2" value="" />
	            </fieldset>

                    <fieldset>
                      <legend>Fraktsedel-kod</legend>
                      <label for="fCode" style="display:block;">
                          <span>Ange F/V/S</span> <br />
                          <input style="width:90%; height: 12px;" type="text" name="fCode" id="fCode" value="" />
                      </label>
	            </fieldset>
	            
                    <fieldset>
                      <legend>Avisering</legend>
                      <input type="checkbox" name="fAviPhone" id="fAviPhone" value="true" />
                      <label for="fAviPhone">Telefon</label><br />
                      <input type="checkbox" name="fAviFax" id="fAviFax" value="true" />
                      <label for="fAviFax">Fax</label>
	            </fieldset>
	            
                </td>
              </tr>
              <tr>
                <td class="white" style="vertical-align: top;" colspan="3">

                    <div id="fCargoListDiv"></div>
                    
                    <div style="width:100%; text-align: center;">
                    <?php

                      $result = mysql_query("SELECT * FROM pdfer") or die(mysql_error());
                      if (mysql_num_rows($result) > 0)
                      {
                          print '<select name="extrapdf" style="width:300px"><option value="">- Ingen PDF -</option>';
                          while ($row = mysql_fetch_array($result))
                          {
                              print '<option value="dokument'.$row['id'].'.pdf">'.$row['name'].'</option>';
                          }
                          print "</select>\n";
                      }
                      else
                      {
                          print "            Inga extra PDF:er än!\n";
                      }
                    ?>
                    </div>

                </td>
                <td class="white" style="vertical-align: top; width: 80px;">

                    <fieldset>
                      <legend>Övrigt</legend>
                      <label for="fNoOfEUR" style="display: block;">
                          <span>Godk. EUR pall</span> <br />
                          <input style="width:90%; height: 12px;" type="text" name="fNoOfEUR" id="fNoOfEUR" value="" />
                      </label>
                      <label for="mRegNo" style="display: block;">
                          <span>Mottag. reg-nr</span> <br />
                          <input style="width:90%; height: 12px;" type="text" name="mRegNo" id="mRegNo" value="" />
                      </label>
                      <label for="aRegNo" style="display: block;">
                          <span>Avsänd. reg-nr</span> <br />
                          <input style="width:90%; height: 12px;" type="text" name="aRegNo" id="aRegNo" value="" />
                      </label>
	            </fieldset>
	            
	            <br />
                    <button name="fPrint" type="submit" style="width: 100%;"><img src="img/printer.png" alt="" /> Skriv ut</button> <br />
                    <button name="fSave" type="button" style="width: 100%; margin-top:5px;" onclick="saveWayBill(1);"><img src="img/page_save.png" alt="" /> Spara</button>
                </td>
              </tr>
            </table>
          </p>
	</form>
    </div>
