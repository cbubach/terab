<?php
    session_start();
    include("db.php");

    if (!isset($_SESSION['loggedin']))
    {
        header("Location: http://ad01/new/index.php");   // not logged in
        exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2012 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=iso-8859-1" />
    <title>TERAB administration - Pallkonton</title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery_code.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px; font-family: font-size: 14px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="img/blank.gif" width="0" height="80">
                        <img src="img/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="start.php">Startsidan</a> | <a href="index.php?logout=1">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<h1 style="text-align:center;">Pallkonton</h1>

<a href="?account=add" style="margin-left:20px;">Skapa pallkonto f�r ny kund och l�gg till pall (+)</a><br />
<a href="?account=remove" style="margin-left:20px;">Skapa pallkonto f�r ny kund och ta ut pall (-)</a><br />
<a href="?account=reactivate" style="margin-left:20px;">�teraktivera dold kund</a><br />

<p>
<table border="0" cellspacing="0" cellpadding="5" style="width: 100%;">
  <tr>
    <td valign="top">

      <table border="0" cellspacing="0" cellpadding="5" style="width: 450px;">
          <tr bgcolor="#C0C0C0">
              <td><b>Kund</b></td>
              <td><b>Saldo</b></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
          </tr>
<?php
    $bgcolor="#FFFFFF";
    $result = mysql_query("SELECT accounts.customer_id, cust.name AS customer_name, cust.del_city AS
     customer_city, SUM(accounts.no_of_items) AS no_of_items FROM accounts, cust
      WHERE accounts.customer_id = cust.id AND accounts.hidden = 0 GROUP BY accounts.customer_id") or die(mysql_error());
    if (mysql_num_rows($result) > 0)
    {
        while($row = mysql_fetch_assoc($result))
        {
            $bg = "";
            if($bg) { $bgcolor="#EEEEEE"; $bg = false; } else { $bg = true; $bgcolor="#FFFFFF"; }
            $periodID = mysql_result(mysql_query("SELECT id FROM periods WHERE current_period='Y'"), 0);
?>
          <tr bgcolor="<?= $bgcolor ?>">
              <td><a href="?account_show=<?= $row['customer_id'] ?>"><?= $row['customer_name'] ?></a>, <?= $row['customer_city'] ?></td>
              <td><?= $row['no_of_items'] ?></td>
              <td><a href="?account_add&customer_id=<?= $row['customer_id'] ?>">S�tt in (+)</a></td>
              <td><a href="?account_remove&customer_id=<?= $row['customer_id'] ?>">Ta ut (-)</a></td>
          </tr>
<?php
        }
    }
    else {
        print "          <tr><td colspan=\"4\">Det finns inga konton</td></tr>\n";
    }
?>
      </table>
    </td>
    <td valign="top">
<?php
    if (isset($_GET['account_show']))
        $cust_name = mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$_GET['account_show']}'"), 0);
    else
        $cust_name = "";
?>
      <h2>Kontohistorik <?php echo $cust_name; ?></h2>
      <table border="0" cellspacing="0" cellpadding="5" style="width: 450px;">
          <tr bgcolor="#C0C0C0">
              <td><b>Antal</b></td>
              <td><b>Datum</b></td>
              <td><b>Transport�r</b></td>
              <td><b>Noteringar</b></td>
          </tr>
<?php
    if (isset($_GET['account_show']))
    {
        $res = mysql_query("SELECT * FROM accounts WHERE customer_id='{$_GET['account_show']}' ORDER BY date DESC");
        if (mysql_num_rows($res) > 0) {
            while($row = mysql_fetch_assoc($res))
            {
                print "          <tr>\n";
                print "              <td>".$row['no_of_items']."</td>\n";
                print "              <td>".$row['date']."</td>\n";
                print "              <td>".$row['transporter']."</td>\n";
                print "              <td>".$row['notes']."</td>\n";
                print "          </tr>\n";
            }
        }
    }
?>
      </table>

    </td>
  </tr>
</table>


</body>
</html>
