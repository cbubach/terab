<?php
    session_start();
    include("db.php");

    if (!isset($_SESSION['loggedin']))
    {
        header("Location: http://ad01/new/index.php");   // not logged in
        exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=iso-8859-1" />
    <title>TERAB administration - Skapa order</title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery_code.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px; font-family: font-size: 14px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="img/blank.gif" width="0" height="80">
                        <img src="img/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="start.php">Startsidan</a> | <a href="index.php?logout=1">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<h1 style="text-align:center;">Skapa order</h1>

<form action="cmd.php" method="get" style="margin:auto; width: 500px;">
<input type="hidden" name="cmd" value="createOrder">
<input type="hidden" name="customer_id" id="customer_id" value="">
<table border="0" cellpadding="10" cellspacing="1" style="text-align:left;">
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Datum:</b>
        </td>
        <td>
            <input type="text" name="order_date" id="order_date" size="10" value="<?php echo date("Y-m-d"); ?>">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Ange kund:</b>
        </td>
        <td>
            <input type="text" name="customer_name" id="customer_name" size="35">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Attesteras:</b>
        </td>
        <td>
            <input type="text" name="att">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Fraktsedelnummer:</b>
        </td>
        <td>
            <input type="text" name="waybill_no">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Extra fraktsedelnummer:<br>(ett per rad)</b>
        </td>
        <td>
            <textarea cols="30" rows="5" name="extra_waybills"></textarea>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Antal:</b>
        </td>
        <td>
            <input type="text" name="total_items" size="5">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>�vriga noteringar:</b>
        </td>
        <td>
            <textarea cols="30" rows="5" name="notes"></textarea>
        </td>
    </tr>
    <tr>
        <td colspan="2"><br /><button name="saveOrder" type="submit"><img src="http://ad01/order/img/page_save.png" alt=""> Skapa order</button></td>
    </tr>
</table>
</form>


</body>
</html>
