<?php
    session_start();

    @mysql_connect("127.0.0.1", "root", "TeRab20120") or die("Could not connect: " . mysql_error());
    mysql_select_db("terab") or die("Could not connect: " . mysql_error());
    date_default_timezone_set("Europe/Stockholm");
    include("functions.php");

    if (!isset($_SESSION['loggedin']))
    {
        header("Location: http://ad01/new/index.php");   // already logged in
        exit;
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2011 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=iso-8859-1" />
    <title>TERAB administration - Meny</title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery_code.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="img/blank.gif" width="0" height="80">
                        <img src="img/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="start.php">Startsidan</a> | <a href="index.php?logout=1">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<center><!-- I know.. -->
<table border="0" cellpadding="10" cellspacing="0" style="font-family: sans-serif;font-size:14px;">
    <tr>
        <td valign="top">
            <table border="0" width="100%" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
                <tr>
                    <td><a href="create_order.php"><img src="images/icons/big/add.gif" border="0"></a></td>
                    <td><a href="create_order.php">L�gg till ny order</a></td>
                </tr>
                <tr>
                    <td><a href="list_orders.php?sort"><img src="images/icons/big/sort.gif" border="0"></a></td>
                    <td><a href="list_orders.php?sort">Sortera order</a></td>
                </tr>
                </tr>
                <tr>
                    <td><a href="accounts.php"><img src="images/icons/big/sort.gif" border="0"></a></td>
                    <td><a href="accounts.php">Pallkonton</a></td>
                </tr>
<?php if (!isset($_SESSION['admin'])) {  ?>
                <tr>
                    <td><a href="http://ad01/order/"><img src="images/icons/big/period.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/">Bokningsprogrammet</a></td>
                </tr>
<?php } ?>
            </table>
<?    if(isset($_SESSION['admin'])) { ?>
            <p>
            <table border="0" width="100%" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
                <tr>
                    <td><a href="list_orders.php"><img src="images/icons/big/edit.gif" border="0"></a></td>
                    <td><a href="list_orders.php">Redigera order</a></td>
                </tr>
                <tr>
                    <td><a href="reports.php"><img src="images/icons/big/query.gif" border="0"></a></td>
                    <td><a href="reports.php">Visa rapporter</a></td>
                </tr>
            </table>
<?    } ?>
        </td>
<?    if(isset($_SESSION['admin'])) { ?>
        <td valign="top">
            <table border="0" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
                <tr>
                    <td><a href="http://ad01/order/admin/cust.php"><img src="images/icons/big/customer.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/cust.php">Hantera kunder</a></td>
                </tr>
                <tr>
                    <td><a href="http://ad01/order/admin/products.php"><img src="images/icons/big/product.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/products.php">Hantera produkter</a></td>
                <tr>
                    <td><a href="http://ad01/order/admin/prices.php"><img src="images/icons/big/price.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/prices.php">Hantera priser</a></td>
                </tr>
                </tr>
            </table><p>
            <table border="0" width="100%" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
                <tr>
                    <td><a href="report_settings.php"><img src="images/icons/big/reports.gif" border="0"></a></td>
                    <td><a href="report_settings.php">Hantera rapporter</td>
                </tr>
                <tr>
                    <td><a href="http://ad01/order/admin/fno.php"><img src="images/icons/big/edit.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/fno.php">Fraktsedelnummer</a></td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table border="0" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
                <tr>
                    <td><a href="http://ad01/order/"><img src="images/icons/big/period.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/">Bokningsprogrammet</a></td>
                </tr>
                <tr>
                    <td><a href="http://ad01/order/admin/orderhistorik.php"><img src="images/icons/big/query.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/orderhistorik.php">Orderhistorik</a></td>
                </tr>
            </table><p>
            <table border="0" width="100%" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
                <tr>
                    <td><a href="http://ad01/order/admin/offert.php"><img src="images/icons/big/add.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/offert.php">Skapa offert/prislista</a></td>
                </tr>
                <tr>
                    <td><a href="http://ad01/order/admin/notes.php"><img src="images/icons/big/edit.gif" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/notes.php">Anteckningar</a></td>
                </tr>
                <tr>
                    <td><a href="http://ad01/order/admin/pdf.php"><img src="images/icons/big/pdf_icon.png" border="0"></a></td>
                    <td><a href="http://ad01/order/admin/pdf.php">Till�ggs PDF:er</a></td>
                </tr>
            </table>
        </td>
<?    } ?>
    </tr>
</table> </center>


</body>
</html>
