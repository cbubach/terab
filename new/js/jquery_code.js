//-------------------------------
//  TERAB booking, jQuery code
//   by Christoffer Bubach 2012
//-------------------------------
  $(document).ready(function() {

      // support for swedish week- and day-names
      jQuery(function($){
        $.datepicker.regional['sv'] = {
          closeText: 'St�ng',
          prevText: '&laquo;F�rra',
          nextText: 'N�sta&raquo;',
          currentText: 'Idag',
          monthNames: ['Januari','Februari','Mars','April','Maj','Juni',
          'Juli','Augusti','September','Oktober','November','December'],
          monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
          'Jul','Aug','Sep','Okt','Nov','Dec'],
          dayNamesShort: ['S�n','M�n','Tis','Ons','Tor','Fre','L�r'],
          dayNames: ['S�ndag','M�ndag','Tisdag','Onsdag','Torsdag','Fredag','L�rdag'],
          dayNamesMin: ['S�','M�','Ti','On','To','Fr','L�'],
          weekHeader: 'Ve',
          dateFormat: 'yy-mm-dd',
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ''};
        $.datepicker.setDefaults($.datepicker.regional['sv']);
      });


      // init auto-complete
      //----------------------
      $( "#customer_name" ).autocomplete({
        source: "../order/index.php?auto=2",
        minLength: 1,
        select: function( event, ui ) {
          $("#customer_id").val(ui.item.id);
        }
      });

      // init datepicker
      //------------------
      $( "#order_date" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      // init datepicker
      //------------------
      $( "#sort_date" ).datepicker({
        showWeek: true,
        dateFormat: 'yy-mm-dd'
      });
      
      //populate product listing at page load
      if ($("#ProductListing").length) {
          getProductTable('list', $("#order_id").val(), 0, 0);
      }

  });  // end of JS code that needs to run onload :)





  function postFormAJAX (form)
  {
    var query = $(form).serialize();
    var url = form.action + '?' + query;
    $.get(url, function (data) {
      $('#editCust').dialog('close');
      $('#editArt').dialog('close');
      window.location.reload();
    });
  }
  

  function toPascalCase(str)
  {
    var arr = str.split(/\s|_/);
    for(var i=0,l=arr.length; i<l; i++) {
        arr[i] = arr[i].substr(0,1).toUpperCase() +  arr[i].substr(1);
                 //(arr[i].length > 1 ? arr[i].substr(1).toLowerCase() : "");
    }
    return arr.join(" ");
  }


  /* loop all inputs and add everything */
  function addProductsToOrder(orderID) {
    $("#addProducts :input").each(function() {
	    if (this.value != "") { 
			var id = this.id.substr(8); // remove "addItem_" from ID
            getProductTable ('add', orderID, id, $('#addItem_'+id).val());
			$('#addItem_'+id).val('');
		}
	});
  }


  /* recalculate total amount of products */
  function reCalcAmount(element) {
	var amount = element.value;
	var itemId = element.id.substr(5); // item_
	for (var i = 0; i < productsID.length; i++) {
		if (productsID[i] == itemId) {
		   $('#sorted_amount').html(parseInt($('#sorted_amount').html()) - parseInt(productsAmount[i])); // remove old amount
		   productsAmount[i] = amount;   // replace it
		   $('#sorted_amount').html(parseInt($('#sorted_amount').html()) + parseInt(productsAmount[i])); // add new
		}
	}
  }


  /*
   *   getProductTable()
   *     gets data on all products added to the order, both in db and one page
   *     arrays (declared below).  Builds the HTML table and updates it on the
   *     page, as well as deleting or adding a product to the order with AJAX.
   *
   *             op =  list/add/del   for list, only order_id required
   *       order_id =  id of the order edited
   *        prod_id =  on add/del, the order ID, otherwise 0.
   *         amount =  on add/del, the amount of products, otherwise 0.
   */
  var productsID = new Array();      // temporary storage of what's visible
  var productsAmount = new Array();  // on page. until user saves or leaves.
  var productsName = new Array();    // table reloads from these values at each action.

  function getProductTable (op, order_id, prod_id, amount)
  {
      $.getJSON('cmd.php?cmd=getProductList&id='+order_id, function (data) {
          var newHtml = "";
		  var length = data.length;
		  var count = 0;
		  if (productsID.length === 0) {
			  $.each(data, function() {
				  productsID.push(data[count].productsID);
				  productsName.push(data[count].productsName);
				  productsAmount.push(data[count].productsAmount);
				  $('#sorted_amount').html(parseInt($('#sorted_amount').html()) + parseInt(data[count].productsAmount));
				  count++;
			  });
		  }
          if (op == "add") {
		      var count = 0;
			  var found = 0;
			  $.each(productsID, function() {
			      if (productsID[count] == prod_id) {
				      found = 1;
					  return false;
				  }
				  count++;
			  });
			  if (found == 1) {
				  productsAmount[count] = parseInt(productsAmount[count]) + parseInt(amount);
				  $('#sorted_amount').html(parseInt($('#sorted_amount').html()) + parseInt(amount));
			  } else {
				  productsID.push(prod_id);
				  productsAmount.push(amount);
				  $.get('cmd.php?cmd=getProductName&id='+prod_id, function (response) {
					  productsName.push(response);
					  $('#prodName'+productsID[productsName.length-1]).html(response);
					  $('#sorted_amount').html(parseInt($('#sorted_amount').html()) + parseInt(amount));
				  });
			  }
          }
		  var bgcolor = "#FFFFFF";
		  for (var i = 0; i < productsID.length; i++) {
			  if (op == "del" && prod_id == productsID[i]) {
			      $('#sorted_amount').html(parseInt($('#sorted_amount').html()) - parseInt(productsAmount[i]));
				  productsID.splice(i, 1);
				  productsAmount.splice(i, 1);
				  productsName.splice(i, 1);
				  i--;  // removed one from array so start next loop on same count
			  }
			  else {
				  if (bgcolor == "#EFEFEF")
					  bgcolor = "#FFFFFF";
				  else
					  bgcolor = "#EFEFEF";
				  newHtml += '<tr bgcolor="' + bgcolor + '"><td><span id="prodName' + productsID[i] + '">' + productsName[i] + '</span></td><td>' +
				             '<input type="text" size="5" name="item_' + productsID[i] + '" id="item_' + productsID[i] +'" value="' + productsAmount[i] + '" onchange="reCalcAmount(this);">' +
                             ' <input type="button" id="delete_item_' + productsID[i] +'" value="Ta bort" onclick="getProductTable(\'del\','+order_id+',this.id.slice(12),0);"></td></tr>';
			  }
		  }
		  $("#ProductListing tr").slice(1,-1).remove();
		  $("#ProductListing tr").slice(0,1).after(newHtml);
      });
  }


  function deleteOrder() {
	  $('<div title="Varning">�r du s�ker p� att du vill ta bort ordern?</div>').dialog({
		  modal: true,
		  buttons: {
			  "Ja, forts�tt": function() {
				  $( this ).dialog( "close" );
				  $("form#saveOrderChanges").attr("action", "cmd.php?cmd=deleteOrder");
				  $("form#saveOrderChanges")[0].submit();
			  },
			  "Nej, g� tillbaka": function() {
				  $( this ).dialog( "close" );
				  return false;
			  }
		  }
	  });
  }
  
    function saveOrder() {
	  if (parseInt($('#sorted_amount').html()) != parseInt($("#total_items").val()) ) {
		  $('<div title="Varning">Sorterat antal st�mmer inte med totalt antal!</div>').dialog({
			  modal: true,
			  buttons: {
				  "OK": function() {
					  $( this ).dialog( "close" );
				  }
			  }
		  });
	  } else {
	      $("form#saveOrderChanges")[0].submit();
	  }
  }