<?php
    session_start();
    include("db.php");
    include("functions.php");

    if (!isset($_SESSION['loggedin']))
    {
        header("Location: http://ad01/new/index.php");   // already logged in
        exit;
    }
    if (isset($_GET['sort']))
        $title = "Sortera";
    else
        $title = "Redigera";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2012 by
              Christoffer Bubach
      -->
    <meta http-equiv="Content-type" content="text/html;charset=iso-8859-1" />
    <title>TERAB administration - <?= $title; ?> order</title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery_code.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="img/blank.gif" width="0" height="80">
                        <img src="img/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="start.php">Startsidan</a> | <a href="index.php?logout=1">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<center><h1><?= $title;?> order</h1></center>
<form id="saveOrderChanges" method="post" action="cmd.php?cmd=saveOrder">
<input type="hidden" name="order_id" id="order_id" value="<?= $_GET['order_id']; ?>" />
<?php
    $result = mysql_query("SELECT * FROM `order` WHERE id='{$_GET['order_id']}'");
    if (mysql_num_rows($result)!=0)
    {
        while($row = mysql_fetch_assoc($result))
        {
?>
<table border="0" style="margin:auto;">
<tr><td valign="top">
<table border="0" cellpadding="10" cellspacing="1" style="">
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Kund:</b>
        </td>
        <td>
		    <input type="hidden" name="cust_id" id="cust_id" value="<?= $row['customer_id']; ?>" />
            <?= mysql_result(mysql_query("SELECT name FROM cust WHERE id='{$row['customer_id']}'"),0) ?>, <?= mysql_result(mysql_query("SELECT del_city FROM cust WHERE id='{$row['customer_id']}'"),0) ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Attesteras:</b>
        </td>
        <td>
            <input type="text" name="att" value="<?= $row['att'] ?>">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Fraktsedelnummer:</b>
        </td>
        <td>
            <input type="text" name="waybill_no" value="<?= $row['waybill_no'] ?>">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Extra fraktsedelnummer: (ett per rad)</b>
        </td>
        <td>
            <textarea cols="20" rows="3" name="extra_waybills"><?= $row['extra_waybills'] ?></textarea>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Orderdatum:</b>
        </td>
        <td>
            <?= $row['order_date'] ?>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Sorteringsdatum:</b>
        </td>
        <td>
            <input type="text" name="sort_date" id="sort_date" size="10" value="<?php if($row['sort_date'] != "0000-00-00") echo $row['sort_date']; else print date('Y-m-d'); ?>">
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>�vriga noteringar:</b>
        </td>
        <td>
            <textarea cols="20" rows="3" name="notes"><?= $row['notes'] ?></textarea>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Kundbrev / fakturaunderlag<br>noteringar:</b>
        </td>
        <td>
            <textarea cols="20" rows="3" name="reports_text"><?= $row['reports_text'] ?></textarea>
        </td>
    </tr>
    <tr>
        <td bgcolor="#C0C0C0">
            <b>Totalt antal:</b>
        </td>
        <td>
            <input type="text" id="total_items" name="total_items" size="5" value="<?= $row['total_items'] ?>">
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table id="ProductListing" style="width: 100%; margin: 0px; padding: 0px;" cellspacing="1px" cellpadding="2px">
                <tr>
                    <td bgcolor="#C0C0C0">
                        <b>Produkt:</b>
					</td>
					<td bgcolor="#C0C0C0">
					    <b>Antal:</b>
					</td>
					<td>
					</td>
				</tr>
                <!-- jQuery populated -->
                <tr>
                    <td bgcolor="#C0C0C0">
					    <b>Sorterat antal hittills:</b>
					</td>
					<td bgcolor="#C0C0C0">
					    <b id="sorted_amount">0</b>
					</td>
				</tr>
			</table>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <p>
             <button name="save_order" onclick="saveOrder(); return false;" type="submit"><img src="http://ad01/order/img/page_save.png" alt=""> Spara order</button>
             <input type="button" name="delete_order_button" value="Ta bort order" onclick="deleteOrder();">
        </td>
    </tr>
</table>
</form>
</td>
<td width="20"></td>
<td valign="top">
<table border="0" cellspacing="1" width="400px" id="addProducts">
    <tr>
        <td bgcolor="#C0C0C0" height="35">
            &nbsp;<b>Produkt:</b>
        </td>
        <td bgcolor="#C0C0C0" height="35" width="150">
            &nbsp;<b>Antal:</b>
        </td>
    </tr>
<!--
    <tr>
        <td colspan="2" align="center">
            <input type="button" name="add_items_button" value="L�gg till produkter" onclick="add_items(this.form);">
        </td>
    </tr>
-->
<?php
            $res1 = mysql_query("SELECT * FROM `art` WHERE type='1' ORDER BY sort");
            if (mysql_num_rows($res1)!=0)
            {
                while($art = mysql_fetch_assoc($res1))
                {
                    if($bg)
                        $bgcolor = "#EFEFEF";
                    else
                        $bgcolor = "#FFFFFF";
                    $bg = !$bg;
?>
    <tr bgcolor="<?= $bgcolor ?>">
        <td>
            <?= $art['art_name'] ?>
        </td>
        <td>
            <input type="text" size="5" id="addItem_<?= $art['id'] ?>">
			<a href="javascript:void(0);" onclick="addProductsToOrder('<?= $_GET['order_id']; ?>');" tabindex="5000"><img alt="L�gg till" class="vmiddle" src="img/add.png"> L�gg till</a>
        </td>
    </tr>
<?php
                }
            }
            else {
                print "Inga produkter hittade!";
            }
?>
<!--
    <tr>
        <td colspan="2" align="center">
            <input type="button" name="add_items_button" value="L�gg till produkter" onclick="add_items(this.form);">
        </td>
    </tr>
-->    
</table>
</td></tr>
</table>
<?php
        }
    }
    else {
        print "<b>Ingen order hittades!</b>";
    }
?>



</body>
</html>
