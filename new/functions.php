<?php
/*
    Helper functions to map customers and products from 
    the old system to the new.
*/

function getNewCustomerId($id)
{
    $sql_query =  "SELECT t1.id FROM `cust` AS t1, `customers` AS t2";
    $sql_query .= " WHERE t1.name = t2.name";
    $sql_query .= " AND t1.del_address = t2.address";
    $sql_query .= " AND t1.del_zip = t2.zip";
    $sql_query .= " AND t1.del_city = t2.city";
    $sql_query .= " AND t1.contact = t2.contact_person";
    $sql_query .= " AND t1.phone = t2.contact_phone";
    $sql_query .= " AND t1.contact_email = t2.contact_email";
    $sql_query .= " AND t1.fax = t2.contact_fax";
    $sql_query .= " AND t1.vat_reg_no = t2.vat_reg_no";
    $sql_query .= " AND t1.giro_no = t2.bankgiro_no";
    $sql_query .= " AND t2.id = '".$id."' AND t1.type=1";
    
    $value = @mysql_result(mysql_query($sql_query), 0) or ("0");
    if ($value == 0)
    {
        $sql_query =  "SELECT t1.id FROM `cust` AS t1, `customers` AS t2";
        $sql_query .= " WHERE t1.name = t2.name";
        $sql_query .= " AND t2.id = '".$id."' AND t1.type=1";
        $value = @mysql_result(mysql_query($sql_query), 0) or ("0");
    }
    return $value;

}

function getNewProductId($id)
{
    $sql_query  = "SELECT t1.id FROM `art` AS t1, `products` AS t2";
    $sql_query .= " WHERE t1.art_name LIKE CONCAT('%',t2.name,'%')";
    $sql_query .= " AND t2.id ='{$id}' AND type='1' LIMIT 0,1";
    
    $value = @mysql_result(mysql_query($sql_query), 0) or (die("0"));
    return $value;
}


?>