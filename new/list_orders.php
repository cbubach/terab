<?php
    session_start();
    include("db.php");
    include("functions.php");

    if (!isset($_SESSION['loggedin']))
    {
        header("Location: http://ad01/new/index.php");   // not logged in
        exit;
    }
    if (isset($_GET['sort']))
        $title = "Sortera";
    else
        $title = "Lista";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!--
          TERAB booking
            developed for TERAB 2012 by
              Christoffer Bubach, weboo.se
      -->
    <meta http-equiv="Content-type" content="text/html;charset=iso-8859-1" />
    <title>TERAB administration - <?php echo $title; ?> ordrar</title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery_code.js"></script>
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />

</head>

<body style="margin:0px; padding:0px;">


<table border="0" width="100%" cellspacing="0" style="margin:0px; padding:0px;">
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin:0px; padding:0px;">
                <tr>
                    <td align="center">
                        <img src="img/blank.gif" width="0" height="80">
                        <img src="img/logo.gif" valign="top">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top" bgcolor="#FFFCD9" align="center">
            <table border="0" width="100%" cellspacing="0" cellpadding="5" style="margin:0px; padding:0px; font: 8pt Verdana;">
                <tr>
                    <td align="center" valign="top">
                        <span class="text8">
                            <a href="start.php">Startsidan</a> | <a href="index.php?logout=1">Logga ut</a>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<h1 style="text-align:center;"><?php echo $title; ?> ordrar</h1>
<p>

<?php
    if (isset($_GET['sort']))
        $result = mysql_query("SELECT * FROM `order` WHERE customer_letter_sent = 'N' AND sort_date='0000-00-00' ORDER BY sort_date");
    else
        $result = mysql_query("SELECT * FROM `order` WHERE customer_letter_sent = 'N' ORDER BY sort_date");
    if (mysql_num_rows($result)!=0)
    {
?>
<table border="0" cellspacing="0" cellpadding="5" style="width:100%;font-size:14px;">
    <tr bgcolor="#C0C0C0">
<!--        <td><img src="images/icons/small/customer_letter_sent.gif" alt="Kundbrev/Egenfaktura skickad" title="Kundbrev/Egenfaktura skickad"></td> -->
        <td><b>Kund</b></td>
        <td><b>Fraktsedelnr</b></td>
        <td><b>Antal</b></td>
        <td><b>Orderdatum</b></td>
        <td><b>Sorteringsdatum</b></td>
    </tr>
<?php
        while($row = mysql_fetch_assoc($result))
        {
            $bg = "";
            $sort_url = "";
            if ($title == "Sortera")
                $sort_url = "&sort";
            if($row['sort_date'] == '0000-00-00' && $title == "Lista")
            {
                $bgcolor = "#FF7766";
                $sort_url = "&sort";
            }
            elseif($bg)
                $bgcolor = "#EEEEEE";
            else
                $bgcolor = "#FFFFFF";

            $bg = !$bg;
            $cust_id = $row['customer_id'];
            $result2 = mysql_query("SELECT * FROM cust WHERE id='{$cust_id}'");
            $row2 = mysql_fetch_assoc($result2);
?>
    <tr bgcolor="<?= $bgcolor ?>" onmouseover="this.style.background='#E8E8E8'; this.style.cursor='pointer';" onmouseout="this.style.background='<?= $bgcolor ?>'; this.style.cursor='default';">
<!--        <td><input type="checkbox" name="customer_letter_sent" value="<?= $row['id'] ?>" onclick="save_customer_letter_sent_state(this);"<? if($row['customer_letter_sent']=='Y') print " checked"?>></td> -->
        <td onclick="window.location='edit_order.php?order_id=<?= $row['id'].$sort_url ?>'"><?= $row2['name'] ?></td>
        <td onclick="window.location='edit_order.php?order_id=<?= $row['id'].$sort_url ?>'"><?= $row['waybill_no'] ?></td>
        <td onclick="window.location='edit_order.php?order_id=<?= $row['id'].$sort_url ?>'"><?= $row['total_items'] ?></td>
        <td onclick="window.location='edit_order.php?order_id=<?= $row['id'].$sort_url ?>'"><?=  $row['order_date'] ?></td>
        <td onclick="window.location='edit_order.php?order_id=<?= $row['id'].$sort_url ?>'"><? if($row['sort_date']!="0000-00-00") print $row['sort_date']; else print "EJ SORTERAD"; ?></td>
    </tr>
<?php
        }
    }
    else {
?>
    Det finns inga ordrar
<?php
    }
?>

</body>
</html>
