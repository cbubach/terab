<?php
	$login_pages = array(
		"login_form",
		"login_not_inlogged",
		"error",
		"error_dialog"
	);
	
	$admin_pages = array(
		"order_edit_list",
		"order_edit_form",
		"reports",
		"reports_customer_letter",
		"reports_customer_letter_print",
		"reports_customer_letter_custom_list",
		"reports_customer_letter_simple",
		"reports_customer_letter_custom",
		"reports_customer_letter_custom_simple",
		"reports_customer_letter_custom_invoice_foundation",
		"reports_customer_letter_invoice_foundation",
		"reports_customer_letter_account_balance",
		"reports_customer_private_receipt",
		"products_old_system",
		"reports_period_statistics",
		"manage_customers",
		"customer_create_form",
		"customer_edit_form",
		"manage_prices",
		"manage_periods",
		"manage_reports"
	);
	
	$user_pages = array(
		"blank",
		"login_form",
		"login_not_inlogged",
		"error",
		"error_dialog",
		"start",
		"order_create_form",
		"order_sort_list",
		"order_sort_form",
		"account_list",
		"account_add_form",
		"account_remove_form"
	);
	
	$admin_functions = array(
		"editOrder",
		"deleteOrder",
		"saveCustomerLetterSentState",
		"showCustomerDefaultReport",
		"printCustomerDefaultReportJSON",
		"saveCustomerDefaultReport",
		"createCustomer",
		"editCustomer",
		"deleteCustomer",
		"printPriceJSON",
		"savePrice",
		"setCurrentPeriod"
	);
	
	$user_functions = array(
		"login",
		"logout",
		"createOrder",
		"checkIfWaybillNoExistsJSON",
		"getCustomerContactPersonJSON",
		"autoSuggestCustomerJSON",
		"sortOrder",
		"addToAccount",
		"removeFromAccount"
	);
	
	global $login_pages, $admin_pages, $user_pages, $admin_functions, $user_functions;
	
	function check_usertype_page_permissions($page)
	{
		if(isset($_SESSION['admin']))
		{
                  /*
			if(in_array($page, $GLOBALS['admin_pages']))
			{
				return true;
			}

			if(in_array($page, $GLOBALS['user_pages']))
			{
				return true;
			}

			return false;
                        */
                        return true;
		}
		else
		{
			if(in_array($page, $GLOBALS['user_pages']))
			{
				return true;
			}
			
			return false;
		}
	}
	
	function check_usertype_function_permissions($function)
	{
		if($_SESSION['admin'])
		{
                  /*
			if(in_array($function, $GLOBALS['admin_functions']))
			{
				return true;
			}

			if(in_array($function, $GLOBALS['user_functions']))
			{
				return true;
			}

			return false;
			*/
                        return true;
		}
		else
		{
			if(in_array($function, $GLOBALS['user_functions']))
			{
				return true;
			}
			
			return false;
		}
	}
?>