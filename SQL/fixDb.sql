ALTER TABLE terab.account ENGINE=InnoDB;
ALTER TABLE terab.account CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.accounts ENGINE=InnoDB;
ALTER TABLE terab.accounts CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.art ENGINE=InnoDB;
ALTER TABLE terab.art CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.bill ENGINE=InnoDB;
ALTER TABLE terab.bill CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.bill_item ENGINE=InnoDB;
ALTER TABLE terab.bill_item CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.bill_message ENGINE=InnoDB;
ALTER TABLE terab.bill_message CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.bill_settings ENGINE=InnoDB;
ALTER TABLE terab.bill_settings CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.cust ENGINE=InnoDB;
ALTER TABLE terab.cust CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.customers ENGINE=InnoDB;
ALTER TABLE terab.customers CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.event ENGINE=InnoDB;
ALTER TABLE terab.event CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.event_art ENGINE=InnoDB;
ALTER TABLE terab.event_art CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.notes ENGINE=InnoDB;
ALTER TABLE terab.notes CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.order ENGINE=InnoDB;
ALTER TABLE terab.order CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.order_data ENGINE=InnoDB;
ALTER TABLE terab.order_data CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.orders ENGINE=InnoDB;
ALTER TABLE terab.orders CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.orders_data ENGINE=InnoDB;
ALTER TABLE terab.orders_data CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.pdfer ENGINE=InnoDB;
ALTER TABLE terab.pdfer CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.periods ENGINE=InnoDB;
ALTER TABLE terab.periods CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.price ENGINE=InnoDB;
ALTER TABLE terab.price CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.prices ENGINE=InnoDB;
ALTER TABLE terab.prices CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.`p_rices-whatisthis` ENGINE=InnoDB;
ALTER TABLE terab.`p_rices-whatisthis` CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.prod_cat ENGINE=InnoDB;
ALTER TABLE terab.prod_cat CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.products ENGINE=InnoDB;
ALTER TABLE terab.products CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.settings ENGINE=InnoDB;
ALTER TABLE terab.settings CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.users ENGINE=InnoDB;
ALTER TABLE terab.users CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.waybill ENGINE=InnoDB;
ALTER TABLE terab.waybill CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE terab.wb_sender ENGINE=InnoDB;
ALTER TABLE terab.wb_sender CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER DATABASE terab COLLATE utf8_unicode_ci;
