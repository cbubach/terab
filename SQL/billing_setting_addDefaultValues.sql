UPDATE `bill_settings` SET `iban` = 'SE5780000836830047202411' WHERE `iban` = 0;

UPDATE `bill_settings` SET `due` = 30 WHERE `due` = 0;

UPDATE `bill_settings` SET `message` = "För fakturafrågor kontakta ekonomi@terab.se
Dröjsmålsränta 15%
Under rådande omständigheter skriver vi inga restorders på artiklar
som inte kan full levereras, alla order ses som slutlevererade" WHERE `message` = "";