ALTER TABLE bill_settings DROP COLUMN `message_id`;
ALTER TABLE bill_settings ADD COLUMN `message` varchar(500);
DROP TABLE bill_message;