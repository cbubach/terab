<?php
	// display a page
	function display_page($page)
	{
		// disable the problems with . / \ in the page parameter
		if(strpos($page, ".", 0)>-1 || strpos($page, "/", 0)>-1 || strpos($page, "\\", 0)>-1)
		{
			logger("page parameter contains an illegal character (".$page.") | user: ".$_SESSION['user_name']);
			header("Location: system.php?function_file=login&function=logout".($_GET['dialog']=="true"?"&dialog=true":""));
			exit();
		}
		
		// make "start" the default page
		if(!isset($page)) $page = "start";
		
		// check user type permissions
		if(!check_usertype_page_permissions($page))
		{
			logger("user (".$_SESSION['user_name'].") didn't had the permission to view page (".$page.") because of the user type");
			header("Location: system.php?function_file=login&function=logout".($_GET['dialog']=="true"?"&dialog=true":""));
			exit();
		}
		
		$file = $GLOBALS['page_dir'].$page.".php";
		
		// if file does not exist, include the "error" page instead
		if(!file_exists($file))
		{
			logger("file doesn't exist: ".$file);
			$file = $GLOBALS['page_dir']."error.php";
		}
		
		if(isset($_GET['dialog']) && $_GET['dialog']=="true")
		{
			include($GLOBALS['root_dir']."gui_dialog.php");
		}
		else
		{
			include($GLOBALS['root_dir']."gui.php");
		}
	}
	
	// run a function
	function run_function($args)
	{

		// disable the problems with . / \ in the function_file parameter
		if(strpos($args['function_file'], ".", 0)>-1 || strpos($args['function_file'], "/", 0)>-1 || strpos($args['function_file'], "\\", 0)>-1)
		{
			logger("function_file parameter contains an illegal character (".$args['function_file'].") | user: ".$_SESSION['user_name']);
			header("Location: system.php?page=error".($args['dialog']=="true"?"&dialog=true":""));
			exit();
		}
		
		// check user type permissions
		if(!check_usertype_function_permissions($args['function']))
		{
			logger("user (".$_SESSION['user_name'].") didn't had the permission to run function (".$args['function'].") because of the user type");
			header("Location: system.php?page=error".($args['dialog']=="true"?"&dialog=true":""));
			exit();
		}
		
		// put together the path for the file to be included and check if it exist
		$file = $GLOBALS['function_dir'].$args['function_file'].".php";
		if(file_exists($file))
		{
			include($file);
		}
		else
		{
			logger("function file does not exist: " . $file);
			$redirect = "page=error".($args['dialog']=="true"?"&dialog=true":"");
		}
		
		// it has to be a function defined public (name starts with public_) that is called
		if(function_exists("public_".$args['function'])) 
		{
			$function="public_".$args['function'];
			$redirect = $function($args);
		}
		else
		{
			logger("function does not exist: " . $args['function']);
			$redirect = "page=error";
		}
		
		// notice the three "=" which understands if there is position 0 or if it's false
		if(strpos($redirect, "external:")===0)
		{
			$redirect = substr($redirect, 9);
		}
		else
		{
			$redirect = "system.php?".$redirect;
		}
		
		return $redirect;
	}
	
	// write a message with the current date and time into the error log file
	function logger($message)
	{
		error_log(date("ymd H:i:s - ") . $message . "\r\n", $GLOBALS['log_type'], $GLOBALS['log_destination']);
	}
?>