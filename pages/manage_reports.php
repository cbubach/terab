<?php
function javascript() { ?>
<script type="text/javascript" src="libs/MochiKit/Base.js"></script>
<script type="text/javascript" src="libs/MochiKit/Async.js"></script>
<script type="text/javascript">
<!--
function choose_customer(customer_id)
{
	changeReportData(customer_id);
}

function changeReportData(customer_id)
{
	var url = "system.php";
	var arguments = {
		'function_file': 'reports',
		'function': 'printCustomerDefaultReportJSON',
		'customer_id': customer_id
	}

	var d = loadJSONDoc(url, arguments);
	
	var gotReportData = function (data)
	{
		var report_types = document.getElementsByName('report_type');
		for(var i=0;i<report_types.length;i++)
		{
			if(data.report_type == report_types.item(i).value)
			{
				report_types.item(i).checked = true;
			}
		}
		
		document.forms[0].save_button.disabled = false;
	};
	var fetchFailed = function (err)
	{
  		alert(err);
	};
	d.addCallbacks(gotReportData, fetchFailed);
}

function submitForm(form)
{
	return true;
}
-->
</script>
<?php
}

function body_onload()
{

}

function body()
{ 
	$customer_result = mysql_query("select id, name, city from customers order by name");
?>
<span class="header">Hantera rapporter</span>
<p>
<form action="system.php" method="post" onsubmit="return submitForm(this);">
<input type="hidden" name="function_file" value="reports">
<input type="hidden" name="function" value="saveCustomerDefaultReport">
<table border="0" cellspacing="10">
	<tr>
		<td width="200" valign="top">
			<table border="0" width="100%" cellpadding="3" cellspacing="0">
				<tr bgcolor="#C0C0C0">
					<td>
						<b>Kunder:</b>
					</td>
				</tr>
				<tr>
					<td>
						<select name="customer_id" size="22" onclick="choose_customer(this.value)">
<?php	while($row = mysql_fetch_assoc($customer_result))
	{ ?>
						<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
<?php	} ?>
					</td>
				</tr>
			</table>
		</td>
		<td width="250" valign="top">
			<table border="0" width="100%" cellpadding="3" cellspacing="0">
				<tr bgcolor="#C0C0C0">
					<td>
						<b>Standard-rapport:</b>
					</td>
				</tr>
				<tr>
					<td>
						<input type="radio" name="report_type" value="customer_letter"> Kundbrev<br>
						<input type="radio" name="report_type" value="customer_letter_simple"> Kundbrev - Enkelt<br>
						<input type="radio" name="report_type" value="customer_letter_invoice_foundation"> Egenfaktura
					</td>
				</tr>
			</table>
		</td>
		<td width="190" valign="top">
			<table border="0" width="100%" cellpadding="3" cellspacing="0">
				<tr bgcolor="#C0C0C0">
					<td>
						<b>Genomför förändringar:</b>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="save_button" value="Spara ändringarna" disabled>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?php } ?>
