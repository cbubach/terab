<?php
function javascript()
{ ?>
<script type="text/javascript">
<!--
var products = Array();
<?php	$products_result = mysql_query("select id, name, category from products order by sort_index");
	$i = 0;
	while($product_row = mysql_fetch_assoc($products_result))
	{
		print "products[".$i."] = Array('".$product_row['id']."', '".$product_row['category']."');\n";
		$i++;
	} ?>

function add_items(form)
{	
	for(var i=0;i<products.length;i++)
	{
		var items_field = document.getElementsByName('items[<?php echo $_GET['order_id']; ?>][products]['+products[i]+']');
		if(items_field.length > 0)
		{
			var items = Number(items_field.item(0).value);
			if (isNaN(items) || items < 1)
			{
 				alert("Ett av \"Antal\"-fälten innehåller ett inkorrekt värde");
				return;
			}
		}
		
		var new_items_field = document.getElementsByName('add_items[<?php echo $_GET['order_id']; ?>][products]['+products[i]+']');
		if(new_items_field.length > 0 && new_items_field.item(0).value != '')
		{
			var items = Number(new_items_field.item(0).value);
			if (isNaN(items) || items < 1)
			{
 				alert("Ett av den nya \"Antal\"-fälten innehåller ett inkorrekt värde");
				return;
			}
		}
	}
	
	form.submit_type.value = "add_items";
	
	form.submit();
}

function delete_items(button)
{
	var form = button.form;
	
	// get product_id of item to delete
	var re = /delete_items_button\[(\d+)\]/
	var item_index = re.exec(button.name)[1];
	
	form.delete_product_id.value = item_index;
	
	form.submit_type.value = "delete_items";
	
	form.submit();
}

function edit_finished(form)
{
	var total_items = Number(form.total_items.value);
	if (isNaN(total_items))
	{
		alert("Värdet i \"Totalt antal\" måste vara ett heltal större än 0");
		return;
	}
	
	var sorted_items = 0;
	for(var i=0;i<products.length;i++)
	{
		var items_field = document.getElementsByName('items[<?php echo $_GET['order_id']; ?>][products]['+products[i][0]+']');
		if(items_field.length > 0)
		{
                    //-----

                    var count_elements = i - 1;
                    //alert(count_elements);

                    var elementCheck = document.getElementById('priceSetting'+count_elements);
                    if (elementCheck != null)
                    {
                        if (document.getElementById('priceSetting'+count_elements).innerHTML == "=")
                        {
 	                    var priceValue = document.getElementById('items[<?php echo $_GET['order_id']; ?>][price]['+count_elements+']').value;
 		            var itemsNo =  document.getElementById('items[<?php echo $_GET['order_id']; ?>][products]['+count_elements+']').value;
 		            var newPrice = priceValue / itemsNo;
 		            changePriceSetting(count_elements);
                            document.getElementById('items[<?php echo $_GET['order_id']; ?>][price]['+count_elements+']').value = newPrice;
                            //alert('priceValue: '+priceValue+'\nitemsNo: '+itemsNo+'\nnewPrice: '+newPrice);
                        }
                    }

                    //-----
			var items = Number(items_field.item(0).value);
			if(isNaN(items) || items < 1)
			{
 				alert("Ett av \"Antal\"-fälten innehåller ett inkorrekt värde");
				return;
			}
			if(products[i][1]!='')
			{
				sorted_items += items;
			}
		}
	}
	
	if (sorted_items != total_items)
	{
		alert("Antalet sorterade pallar stämmer ej överens med angivet \"Totalt antal\"\nDu har angivit: "+sorted_items);
		return;
	}
	
	form.submit_type.value = "edit_finished";
	
	form.submit();
}

function delete_order()
{
	if(confirm("Vill du verkligen ta bort aktuell order?"))
	{
		document.location = "system.php?function_file=order&function=deleteOrder&order_id=<?php echo $_GET['order_id']; ?>";
	}
}

function changePriceSetting(number)
{
    if (document.getElementById('priceSetting'+number).innerHTML == "x")
    {
        document.getElementById('priceSetting'+number).innerHTML = "=";
    }
    else
    {
        document.getElementById('priceSetting'+number).innerHTML = "x";
    }
}

-->
</script>
<?php
}

function body_onload()
{

}

function body()
{
	$products = array();
	$products_result = mysql_query("select id, name, category, edit_order_set_price from products order by sort_index");
	while($product_row = mysql_fetch_assoc($products_result))
	{
		$products[$product_row['id']] = array(
			'name'					=> $product_row['name'],
			'category'				=> $product_row['category'],
			'edit_order_set_price'	=> $product_row['edit_order_set_price']
		);
	}

	$result = mysql_query("select orders.customer_id, orders.person_nr, orders.bank_nr, customers.name as customer_name, customers.city as customer_city, orders.att, orders.waybill_no, orders.extra_waybills, orders.total_items, orders.order_date, orders.create_date_time, orders.notes, orders.reports_text, periods.name as period_name, periods.year as period_year from orders, customers, periods where orders.id = '".$_GET['order_id']."' and orders.customer_id = customers.id and orders.period_id = periods.id");
	$row = mysql_fetch_assoc($result);
	
	if(!isset($_SESSION['edit_orders']))
		$_SESSION['edit_orders'] = array();
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]))	
		$_SESSION['edit_orders'][$_GET['order_id']] = array();
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['att']))
		$_SESSION['edit_orders'][$_GET['order_id']]['att'] = $row['att'];
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['waybill_no']))
		$_SESSION['edit_orders'][$_GET['order_id']]['waybill_no'] = $row['waybill_no'];
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['extra_waybills']))
		$_SESSION['edit_orders'][$_GET['order_id']]['extra_waybills'] = $row['extra_waybills'];
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['total_items']))
		$_SESSION['edit_orders'][$_GET['order_id']]['total_items'] = $row['total_items'];
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['sort_date']))
		$_SESSION['edit_orders'][$_GET['order_id']]['sort_date'] = date("Y-m-d");
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['notes']))
		$_SESSION['edit_orders'][$_GET['order_id']]['notes'] = $row['notes'];
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['reports_text']))
		$_SESSION['edit_orders'][$_GET['order_id']]['reports_text'] = $row['reports_text'];
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['person_nr']))
		$_SESSION['edit_orders'][$_GET['order_id']]['person_nr'] = $row['person_nr'];
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['bank_nr']))
		$_SESSION['edit_orders'][$_GET['order_id']]['bank_nr'] = $row['bank_nr'];
	
	if(!isset($_SESSION['edit_orders'][$_GET['order_id']]['products']))
	{
		$_SESSION['edit_orders'][$_GET['order_id']]['products'] = array();
		$_SESSION['edit_orders'][$_GET['order_id']]['price'] = array();

		$order_data_result = mysql_query("select product_id, value, price from order_data where order_id = '".$_GET['order_id']."'");
		while($order_data_row = mysql_fetch_assoc($order_data_result))
		{
			$_SESSION['edit_orders'][$_GET['order_id']]['products'][$order_data_row['product_id']] = $order_data_row['value'];
			$_SESSION['edit_orders'][$_GET['order_id']]['price'][$order_data_row['product_id']] = $order_data_row['price'];
		}
	}
?>
<span class="header">Redigera order</span>
 <br><br>

<a href="system.php?function_file=reports&function=showCustomerDefaultReport&waybill_no=<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['waybill_no']; ?>">Kundens standard-
rapport</a>
    <br>
<a href="system.php?page=reports_customer_letter_account_balance&waybill_no=<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['waybill_no']; ?>">Kundbrev inkl saldo</a>


<p>
<form action="system.php" method="post" accept-charset="UTF-8">
<input type="hidden" name="function_file" value="order">
<input type="hidden" name="function" value="editOrder">
<input type="hidden" name="submit_type" value="">
<input type="hidden" name="customer_id" value="<?php echo $row['customer_id']; ?>">
<input type="hidden" name="order_id" value="<?php echo $_GET['order_id']; ?>">
<input type="hidden" name="delete_product_id" value="">
<table border="0">
<tr><td valign="top">
<table border="0" cellpadding="10" cellspacing="1">
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Kund:</b>
		</td>
		<td>
			<?php echo $row['customer_name']; ?>, <?php echo $row['customer_city']; ?>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Attesteras:</b>
		</td>
		<td>
			<input type="text" name="att" value="<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['att']; ?>">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Person-nr (privatleverantör):</b>
		</td>
		<td>
			<input type="text" name="person_nr" value="<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['person_nr']; ?>">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Bank-nr (privatleverantör):</b>
		</td>
		<td>
			<input type="text" name="bank_nr" value="<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['bank_nr']; ?>">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Fraktsedelnummer:</b>
		</td>
		<td>
			<input type="text" name="waybill_no" value="<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['waybill_no']; ?>">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Extra fraktsedelnummer: (ett per rad)</b>
		</td>
		<td>
			<textarea cols="20" rows="3" name="extra_waybills"><?php echo $_SESSION['edit_orders'][$_GET['order_id']]['extra_waybills']; ?></textarea>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Period:</b>
		</td>
		<td>
			<?= $row['period_name'] ?>, <?= $row['period_year'] ?>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Orderdatum:</b>
		</td>
		<td>
			<?= $row['order_date'] ?>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Sorteringsdatum:</b>
		</td>
		<td>
			<input type="text" name="sort_date" size="8" value="<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['sort_date']; ?>">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Övriga noteringar:</b>
		</td>
		<td>
			<textarea cols="20" rows="3" name="notes"><?php echo $_SESSION['edit_orders'][$_GET['order_id']]['notes']; ?></textarea>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Kundbrev / fakturaunderlag<br>noteringar:</b>
		</td>
		<td>
			<textarea cols="20" rows="3" name="reports_text"><?php echo $_SESSION['edit_orders'][$_GET['order_id']]['reports_text']; ?></textarea>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Totalt antal:</b>
		</td>
		<td>
			<input type="text" name="total_items" size="5" value="<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['total_items']; ?>">
		</td>
	</tr>
<?php	if (sizeof($_SESSION['edit_orders'][$_GET['order_id']]['products']) > 0) { ?>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Produkt:</b>
		</td>
		<td bgcolor="#C0C0C0">
			<b>Antal:</b>
		</td>
		<td>
			
		</td>
	</tr>
<?php	}

	$sorted_no_of_items = 0;
	foreach($_SESSION['edit_orders'][$_GET['order_id']]['products'] as $product_id=>$no_of_items)
	{
                if($products[$product_id]['edit_order_set_price']=='Y')
                {
                    $res = mysql_query("SELECT price FROM prices WHERE customer_id='{$row['customer_id']}' AND product_id='{$product_id}'") or die(mysql_error());
                    if (mysql_num_rows($res) > 0)
                    {
                       $_SESSION['edit_orders'][$_GET['order_id']]['price'][$product_id] = mysql_result($res, 0);
                    }
                }

		if($products[$product_id]['category'] != '')
			$sorted_no_of_items += $no_of_items;
				
		if($bg)
			$bgcolor = "#EFEFEF";
		else
			$bgcolor = "#FFFFFF";
		$bg = !$bg; ?>
	<tr bgcolor="<?php echo $bgcolor; ?>">
		<td>
			<?php echo $products[$product_id]['name']; ?>
		</td>
		<td>
			<input type="text" size="5" name="items[<?php echo $_GET['order_id']; ?>][products][<?php echo $product_id; ?>]" id="items[<?php echo $_GET['order_id']; ?>][products][<?php echo $product_id; ?>]" value="<?php echo $no_of_items; ?>">
<?php		if($products[$product_id]['edit_order_set_price']=='Y') { ?>
			<span id="priceSetting<?php echo $product_id; ?>" onclick="changePriceSetting(<?php echo $product_id; ?>);">x</span> <input type="text" size="5" id="items[<?php echo $_GET['order_id']; ?>][price][<?php echo $product_id; ?>]" name="items[<?php echo $_GET['order_id']; ?>][price][<?php echo $product_id; ?>]" value="<?php echo $_SESSION['edit_orders'][$_GET['order_id']]['price'][$product_id]; ?>">
<?php		} ?>
			<input type="button" name="delete_items_button[<?php echo $product_id; ?>]" value="Ta bort" onclick="delete_items(this);">
		</td>
	</tr>
<?php	}
	if (sizeof($_SESSION['edit_orders'][$_GET['order_id']]['products']) > 0) { ?>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Sorterat antal hittills:</b>
		</td>
		<td bgcolor="#C0C0C0">
			<b><?php echo $sorted_no_of_items; ?></b>
		</td>
	</tr>
<?php	} ?>
	<tr>
		<td colspan="2" align="center">
			<p>
			<input type="button" name="edit_finished_button" value="Spara redigerad order" onclick="edit_finished(this.form);"> <input type="button" name="delete_order_button" value="Ta bort order" onclick="delete_order(this.form);">
		</td>
	</tr>
</table>
</td>
<td width="20"></td>
<td valign="top">
<table border="0" cellspacing="1">
	<tr>
		<td bgcolor="#C0C0C0" height="35">
			&nbsp;<b>Produkt:</b>
		</td>
		<td bgcolor="#C0C0C0" height="35">
			&nbsp;<b>Antal:</b>
		</td>
	</tr>	
<?php	foreach($products as $product_id=>$product_data)
	{
		if(!array_key_exists($product_id, $_SESSION['edit_orders'][$_GET['order_id']]['products']))
		{ 
			if($bg)
				$bgcolor = "#EFEFEF";
			else
				$bgcolor = "#FFFFFF";
			$bg = !$bg; ?>
	<tr bgcolor="<?php echo $bgcolor; ?>">
		<td>
			<?php echo $product_data['name']; ?>
		</td>
		<td>
			<input type="text" size="5" name="add_items[<?php echo $_GET['order_id']; ?>][products][<?php echo $product_id; ?>]">
		</td>
	</tr>
<?php		}
	} ?>
	<tr>
		<td colspan="2" align="center">
			<input type="button" name="add_items_button" value="Lägg till produkter" onclick="add_items(this.form);">
		</td>
	</tr>
</table>
</td></tr>
</table>
</form>
<?php } ?>