<?php function javascript() { ?>
<script type="text/javascript">
<!--
var choosed_orders = Array();
function add_to_choosed_orders(row, order_id)
{
	var order_deleted = false;
	
	for(var i=0;i<choosed_orders.length;i++)
	{
		if(choosed_orders[i] == order_id)
		{
			// delete from choosed list
			choosed_orders.splice(i, 1);
			
			order_deleted = true;
			
			if(row.className=='has_bgcolor')
				row.style.background = "#EEEEEE";
			else
				row.style.background = "#FFFFFF";
				
			break;
		}
	}
	
	if(!order_deleted)
	{
		// add to choosed list
		choosed_orders.push(order_id);
		
		row.style.background = "#CCCCCC";
	}
}

function submit_form(form)
{
	if(choosed_orders.length > 0)
	{
		form.choosed_orders.value = choosed_orders.toString();
		
		if(form.customer_letter_type[0].checked)
		{
			form.page.value = "reports_customer_letter_custom";
		}
		else if(form.customer_letter_type[1].checked)
		{
			form.page.value = "reports_customer_letter_custom_simple";
		}
		else
		{
			/* form.page.value = "reports_customer_letter_custom_invoice_foundation"; */
			form.page.value = "reports_customer_letter_invoice_foundation"; 
		}
	
		form.submit();
	}
	else
	{
		alert("Du måste välja en eller flera ordrar");
	}
}
-->
</script>
<?php
}

function body_onload()
{

}

function body()
{
	$period_ids = array();
	if(strpos($_GET['period_id'], "year_")===0)
	{
		$sql = "select id, name, year from periods";
		if(substr($_GET['period_id'], 5) != "all") {
			$sql .= " where year = '".substr($_GET['period_id'], 5)."'";
		}
		$period_result = mysql_query($sql);
		while($period_row = mysql_fetch_assoc($period_result))
		{
			array_push($period_ids, $period_row['id']);
		}
		
		$period_name = "";
		if(substr($_GET['period_id'], 5) != "all") {
			$period_year = substr($_GET['period_id'], 5);
		} else {
			$period_year = "Alla år";
		}
	}
	else
	{
		$period_result = mysql_query("select id, name, year from periods where id = '".$_GET['period_id']."'");
		$period_row = mysql_fetch_assoc($period_result);
		
		array_push($period_ids, $period_row['id']);
		
		$period_name = $period_row['name'];
		$period_year = $period_row['year'];
	}
	
	$sql = "select id, customer_id, waybill_no, total_items, order_date, period_id, sort_date, customer_letter_sent from orders where customer_id = '".$_GET['customer_id']."' and sort_date != '0000-00-00' and period_id in (";
	$first = true;
	foreach($period_ids as $period_id)
	{
		if(!$first)
			$sql .= ",";
			
		$sql .= $period_id;
	
		$first = false;
	}
	$sql .= ") order by order_date";
	$result = mysql_query($sql);
	
	$customer_result = mysql_query("select name, city, default_report_type from customers where id = '".$_GET['customer_id']."'");
	$customer_row = mysql_fetch_assoc($customer_result);
?>
<form action="system.php" method="get">
<input type="hidden" name="page" value="reports_customer_letter_custom">
<input type="hidden" name="function" value="">
<input type="hidden" name="customer_id" value="<?= $_GET['customer_id'] ?>">
<input type="hidden" name="period_id" value="<?= $_GET['period_id'] ?>">
<input type="hidden" name="choosed_orders" value="">

<span class="header">Anpassa Kundbrev/Egenfaktura</span>
<p>
Vald period: <b><?= $period_name." ".$period_year; ?></b>
<p>
Valt företag: <b><?= $customer_row['name'] ?>, <?= $customer_row['city'] ?></b>
<p>
Välj typ:
<?php if ($customer_row['default_report_type'] == "customer_letter") { ?>
    <input type="radio" name="customer_letter_type" value="customer_letter" checked> <b>Vanligt</b>
    <input type="radio" name="customer_letter_type" value="customer_letter_simple"> <b>Enkelt</b> 
    <input type="radio" name="customer_letter_type" value="invoice_foundation"> <b>Egenfaktura</b>
<?php } else if ($customer_row['default_report_type'] == "customer_letter_simple") { ?>
    <input type="radio" name="customer_letter_type" value="customer_letter"> <b>Vanligt</b>
    <input type="radio" name="customer_letter_type" value="customer_letter_simple" checked> <b>Enkelt</b> 
    <input type="radio" name="customer_letter_type" value="invoice_foundation"> <b>Egenfaktura</b>
<?php } else { ?>
    <input type="radio" name="customer_letter_type" value="customer_letter"> <b>Vanligt</b>
    <input type="radio" name="customer_letter_type" value="customer_letter_simple"> <b>Enkelt</b> 
    <input type="radio" name="customer_letter_type" value="invoice_foundation" checked> <b>Egenfaktura</b>
<?php } ?>
<p>
<b>Välj ordrar:</b>
<p>
<table border="0" cellspacing="0" cellpadding="5">
	<tr bgcolor="#C0C0C0">
		<td><img src="images/icons/small/customer_letter_sent.gif" alt="Kundbrev/Egenfaktura skickad" title="Kundbrev/Egenfaktura skickad"></td>
		<td><b>Orderdatum</b></td>
		<td><b>Sorteringsdatum</b></td>
		<td><b>Fraktsedel</b></td>
		<td><b>Totalt Antal</b></td>
	</tr>
<?php		while($row = mysql_fetch_assoc($result))
		{
			if($bg) { $bgcolor="#EEEEEE"; } else { $bgcolor="#FFFFFF"; } $bg = !$bg; ?>
	<tr <?php if(!$bg) print "class=\"has_bgcolor\" "; ?>bgcolor="<?php echo $bgcolor; ?>" onclick="add_to_choosed_orders(this, <?php echo $row['id']; ?>);" onmouseover="this.style.cursor='pointer';" onmouseout="this.style.cursor='default';">
		<td><input type="checkbox"<?php if($row['customer_letter_sent']=='Y') print " checked"; ?> disabled="true"><td><?php echo $row['order_date']; ?></td><td><?php echo $row['sort_date']; ?></td><td><?php echo $row['waybill_no']; ?></td><td><?php echo $row['total_items']; ?></td>
	</tr>
<?php		}
		if(mysql_num_rows($result)==0)
		{ ?>
	<tr bgcolor="<?php echo $bgcolor; ?>">
		<td colspan="12">Det finns ings fraktsedlar för vald sökning</td>
	</tr>
<?php		} ?>
</table>
<p>
<input type="button" value="Visa anpassat kundbrev/egenfaktura" onclick="submit_form(this.form)">
</form>
<?php } ?>
