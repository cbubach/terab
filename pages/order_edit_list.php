<?php function javascript() { ?>
<script type="text/javascript" src="libs/MochiKit/Base.js"></script>
<script type="text/javascript" src="libs/MochiKit/Async.js"></script>
<script type="text/javascript">
<!--
function save_customer_letter_sent_state(checkbox)
{
	if(!confirm("Vill du ändra \"skickad\"-status på \"Kundbrev/Egenfaktura skickad\" för angiven order?"))
	{
		if(checkbox.checked)
			checkbox.checked = false;
		else
			checkbox.checked = true;
	
		return;
	}
		
	var args = Array();
	args['function_file'] = 'reports';
	args['function'] = 'saveCustomerLetterSentState';
	args['order_id'] = checkbox.value;

	if(checkbox.checked)
		args['state'] = 'sent';
	else
		args['state'] = 'not_sent';
		
	doSimpleXMLHttpRequest("system.php", args);
}
-->
</script>
<?php
}

function body_onload()
{

}

function body()
{
     // quick fix - Christoffer Bubach 2011
     //    se till så rapporter inte klumpas ihop med äldre ordrar
     //    från tidigare period, byt istället till aktuell period på
     //    ej "klara" ordrar.
        $curr_period = mysql_result(mysql_query("SELECT id FROM periods WHERE current_period='Y' LIMIT 0,1"),0);
        mysql_query("UPDATE orders SET period_id='$curr_period' WHERE customer_letter_sent = 'N'") or die(mysql_error());
     // --quick fix

	$result = mysql_query("select orders.id, orders.customer_id, customers.name as customer_name, orders.waybill_no, orders.total_items, orders.order_date, orders.sort_date, orders.create_date_time, orders.customer_letter_sent, periods.name as period_name from orders, customers, periods where orders.customer_id = customers.id and orders.period_id = periods.id and (periods.current_period = 'Y' or orders.customer_letter_sent = 'N') order by orders.create_date_time");
?>
<span class="header">Aktuell periods ordrar</span>
<p>
<?php 	if (is_resource($result) && mysql_num_rows($result)!=0) { ?>
<table border="0" cellspacing="0" cellpadding="5">
	<tr bgcolor="#C0C0C0">
		<td><img src="images/icons/small/customer_letter_sent.gif" alt="Kundbrev/Egenfaktura skickad" title="Kundbrev/Egenfaktura skickad"></td>
		<td><b>Kund</b></td>
		<td><b>Fraktsedelnr</b></td>
		<td><b>Antal</b></td>
		<td><b>Orderdatum</b></td>
		<td><b>Sorteringsdatum</b></td>
		<td><b>Skapad</b></td>
		<td><b>Period</b></td>
	</tr>
<?php		while($row = mysql_fetch_assoc($result))
		{
                    $bg ="";
			if($row['sort_date'] == '0000-00-00')
				$bgcolor="#FF7766";
			elseif($bg)
				$bgcolor="#EEEEEE";
			else
				$bgcolor="#FFFFFF";
			$bg = !$bg; ?>
	<tr bgcolor="<?php echo $bgcolor; ?>" onmouseover="this.style.background='#E8E8E8'; this.style.cursor='pointer';" onmouseout="this.style.background='<?php echo $bgcolor; ?>'; this.style.cursor='default';">
		<td><input type="checkbox" name="customer_letter_sent" value="<?php echo $row['id']; ?>" onclick="save_customer_letter_sent_state(this);"<?php if($row['customer_letter_sent']=='Y') print " checked"?>></td>
		<td onclick="window.location='system.php?page=order_edit_form&order_id=<?php echo $row['id'] ?>'"><?php echo $row['customer_name']; ?></td>
		<td onclick="window.location='system.php?page=order_edit_form&order_id=<?php echo $row['id'] ?>'"><?php echo $row['waybill_no']; ?></td>
		<td onclick="window.location='system.php?page=order_edit_form&order_id=<?php echo $row['id'] ?>'"><?php echo $row['total_items']; ?></td>
		<td onclick="window.location='system.php?page=order_edit_form&order_id=<?php echo $row['id'] ?>'"><?php echo  $row['order_date']; ?></td>
		<td onclick="window.location='system.php?page=order_edit_form&order_id=<?php echo $row['id'] ?>'"><?php if($row['sort_date']!="0000-00-00") print $row['sort_date']; else print "-"; ?></td>
		<td onclick="window.location='system.php?page=order_edit_form&order_id=<?php echo $row['id'] ?>'"><?php echo $row['create_date_time']; ?></td>
		<td onclick="window.location='system.php?page=order_edit_form&order_id=<?php echo $row['id'] ?>'"><?php echo $row['period_name']; ?></td>
	</tr>
<?php		}
	}
	else { ?>
Det finns inga ordrar
<?php	}
} ?>
