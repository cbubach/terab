<?php function javascript() { ?>
<script type="text/javascript">
<!--
-->
</script>
<?php 
}

function body_onload()
{

}

function body()
{
?>
<table border="0" cellpadding="10" cellspacing="0">
	<tr>
		<td valign="top">
			<table border="0" width="100%" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td><a href="system.php?page=order_create_form"><img src="images/icons/big/add.gif" border="0"></a></td>
					<td><a href="system.php?page=order_create_form">Lägg till ny order</a></td>
				</tr>
				<tr>
					<td><a href="system.php?page=order_sort_list"><img src="images/icons/big/sort.gif" border="0"></a></td>
					<td><a href="system.php?page=order_sort_list">Sortera order</a></td>
				</tr>
				<tr>
					<td><a href="system.php?page=account_list"><img src="images/icons/big/sort.gif" border="0"></a></td>
					<td><a href="system.php?page=account_list">Pallkonton</a></td>
				</tr>
<?php if (!isset($_SESSION['admin'])) {  ?>
				<tr>
					<td><a href="/TERAB/order/"><img src="images/icons/big/period.gif" border="0"></a></td>
					<td><a href="/TERAB/order/">Bokningsprogrammet</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/cust.php"><img src="images/icons/big/customer.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/cust.php">Hantera kunder</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/orderhistorik.php"><img src="images/icons/big/query.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/orderhistorik.php">Orderhistorik</a></td>
				</tr>
<?php } ?>
			</table>
<?php	if(isset($_SESSION['admin'])) { ?>
			<p>
			<table border="0" width="100%" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td><a href="system.php?page=order_edit_list"><img src="images/icons/big/edit.gif" border="0"></a></td>
					<td><a href="system.php?page=order_edit_list">Redigera order</a></td>
				</tr>
				<tr>
					<td><a href="system.php?page=reports"><img src="images/icons/big/query.gif" border="0"></a></td>
					<td><a href="system.php?page=reports">Visa rapporter</a></td>
				</tr>
			</table>
<?php	} ?>
		</td>
<?php	if(isset($_SESSION['admin'])) { ?>
		<td valign="top">
			<table border="0" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td><a href="system.php?page=manage_customers"><img src="images/icons/big/customer.gif" border="0"></a></td>
					<td><a href="system.php?page=manage_customers">Hantera kunder</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/products_old_system.php"><img src="images/icons/big/product.gif"></a></td>
					<td><a href="/TERAB/order/admin/products_old_system.php">Hantera produkter</a></td>
				</tr>
				<tr>
					<td><a href="system.php?page=manage_prices"><img src="images/icons/big/price.gif" border="0"></a></td>
					<td><a href="system.php?page=manage_prices">Hantera priser</a></td>
				</tr>
				<tr>
					<td><a href="system.php?page=manage_periods"><img src="images/icons/big/period.gif" border="0"></a></td>
					<td><a href="system.php?page=manage_periods">Hantera perioder</td>
				</tr>
				<tr>
					<td><a href="system.php?page=manage_reports"><img src="images/icons/big/reports.gif" border="0"></a></td>
					<td><a href="system.php?page=manage_reports">Hantera rapporter</td>
				</tr>
			</table>
		</td>
<!--
 Tillagt 2011 av Christoffer Bubach för administration av bokningsprogrammet

       började med att försöka köra system.php?sida och adda i security etc, men så jävla onödigt krångligt för intranät,
       skiter fan i det och kör som fristående PHP bara.  aDev's WSM är ett stort jävla fulhack ändå så. ;)
-->
		<td valign="top">
		        <h2>Bokningsprogrammet</h2>
			<table border="0" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td><a href="/TERAB/order/admin/cust.php"><img src="images/icons/big/customer.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/cust.php">Hantera kunder</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/products.php"><img src="images/icons/big/product.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/products.php">Hantera produkter</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/prices.php"><img src="images/icons/big/price.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/prices.php">Hantera priser</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/billing.php"><img src="images/icons/big/edit.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/billing.php">Fakturor</a></td>
				</tr>
			</table> <br />
			<table border="0" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td><a href="/TERAB/order/"><img src="images/icons/big/period.gif" border="0"></a></td>
					<td><a href="/TERAB/order/">Bokningsprogrammet</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/fno.php"><img src="images/icons/big/edit.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/fno.php">Fraktsedelnummer</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/orderhistorik.php"><img src="images/icons/big/query.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/orderhistorik.php">Orderhistorik</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/offert.php"><img src="images/icons/big/add.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/offert.php">Skapa offert/prislista</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/notes.php"><img src="images/icons/big/edit.gif" border="0"></a></td>
					<td><a href="/TERAB/order/admin/notes.php">Anteckningar</a></td>
				</tr>
				<tr>
					<td><a href="/TERAB/order/admin/pdf.php"><img src="images/icons/big/pdf_icon.png" border="0"></a></td>
					<td><a href="/TERAB/order/admin/pdf.php">Tilläggs PDF:er</a></td>
				</tr>
			</table>
		</td>
<!-- slut tillägg -->
<?php	} ?>
	</tr>
</table>
<?php } ?>