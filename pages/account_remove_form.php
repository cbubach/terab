<?php function javascript() { ?>
<script type"text/javascript">
var useBSNns = true;
</script>
<link rel="stylesheet" href="libs/AutoSuggest/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf8">
<script type="text/javascript" src="libs/AutoSuggest/bsn.AutoSuggest_2.1_comp.js" charset="utf8"></script>

<script type="text/javascript" src="libs/MochiKit/Base.js"></script>
<script type="text/javascript" src="libs/MochiKit/Async.js"></script>

<script type="text/javascript">
<!--
function submitForm(form)
{
	if(form.customer_id.value == "0")
	{
		alert("Du måste välja en kund");
		return;
	}
	
	if (form.waybill_no.value == "" || isNaN(Number(form.waybill_no.value)))
	{
		alert("Du måste fylla i ett fraktsedelnummer bestående av endast siffror");
		return;
	}
	
	var no_of_items = Number(form.no_of_items.value);
	if (form.no_of_items.value == "" || isNaN(no_of_items))
	{
		alert("Värdet i \"Antal\" måste vara ett heltal större än 0");
		return;
	}

	form.submit();
}

function onDocumentLoad()
{
	var options = {
		script:"system.php?function_file=customer&function=autoSuggestCustomerJSON",
		varname:"&input",
		json:true,
		shownoresults:false,
		timeout:5000,
		callback: function (obj) { document.getElementById('customer_id').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('customer_name', options);
}
-->
</script>
<?php 
}

function body_onload()
{
	print "onload=\"javascript:onDocumentLoad();\"";
}

function body()
{
	$result = mysql_query("select name as customer_name, city as customer_city from customers where id = '".$_GET['customer_id']."'");
	$row = mysql_fetch_assoc($result);
?>
<span class="header">Ta ut pall från konto</span>
<p>
<form action="system.php" method="post">
<input type="hidden" name="function_file" value="account">
<input type="hidden" name="function" value="removeFromAccount">
<input type="hidden" name="customer_id" id="customer_id" value="<?php echo $_GET['customer_id']; ?>">
<table border="0" cellpadding="10" cellspacing="1">
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Kund:</b>
		</td>
		<td>
		   <?php
		     if (isset($_GET['customer_id']))
		     {
                    ?>		
			<?php echo $row['customer_name']; ?>, <?php echo $row['customer_city']; ?>
		    <?php
		     }
                     else
                     {
                       print '<input id="customer_name" type="text" size="35" name="customer_name" autocomplete="off" />';
                     }
                    ?>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Datum:</b>
		</td>
		<td>
			<input type="text" name="date" size="8" value="<?php echo date("Y-m-d"); ?>">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Transportör:</b>
		</td>
		<td>
			<select name="transporter" size="1">
				<option value="Schenker">Schenker</option>
				<option value="DHL">DHL</option>
				<option value="Terab">Terab</option>
				<option value="Annan transport">Annan transport</option>
			</select>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Fraktsedelnummer:</b>
		</td>
		<td>
			<input type="text" name="waybill_no">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Antal:</b>
		</td>
		<td>
			<input type="text" name="no_of_items" size="5">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Övriga noteringar:</b>
		</td>
		<td>
			<input type="text" name="notes" size="25">
		</td>
	</tr>
</table>
<p>
<input type="button" value="Ta ut" onclick="submitForm(this.form);">
</form>
<?php } ?>