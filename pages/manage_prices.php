<?php
function javascript() { ?>
<script type="text/javascript" src="libs/MochiKit/Base.js"></script>
<script type="text/javascript" src="libs/MochiKit/Async.js"></script>
<script type="text/javascript">
<!--
var products = Array();
<?php	$products_result = mysql_query("select id, name from products order by sort_index");
	$i = 0;
	while($product_row = mysql_fetch_assoc($products_result))
	{
		print "products[".$i."] = '".$product_row['id']."';\n";
		$i++;
	} ?>
	
function choose_customer(customer_id)
{
	changePriceData(customer_id);
}

function changePriceData(customer_id)
{
	var url = "system.php";
	var arguments = {
		'function_file': 'price',
		'function': 'printPriceJSON',
		'customer_id': customer_id
	}

	var d = loadJSONDoc(url, arguments);
	
	var gotPriceData = function (data)
	{
		for(var i=0;i<data.prices.length;i++)
		{
			document.getElementsByName('price['+data.prices[i].product_id+']').item(0).value = data.prices[i].price;
		}

		document.forms[0].save_button.disabled = false;
	};
	var fetchFailed = function (err)
	{
  		alert(err);
	};
	d.addCallbacks(gotPriceData, fetchFailed);
}

function submitForm(form)
{
	for(var i=0;i<products.length;i++)
	{
		var price_field = document.getElementsByName('price['+products[i]+']');
		if(price_field.length > 0)
		{
			if(price_field.item(0).value != '')
			{
				var price = Number(price_field.item(0).value);
				if (price == -1 || isNaN(price) || price < 0)
				{
 					alert("Ett av \"Pris\"-fälten innehåller ett inkorrekt värde");
					return false;
				}
			}
		}
	}
	
	return true;
}
-->
</script>
<?php
}

function body_onload()
{

}

function body()
{ 
	$customer_result = mysql_query("select id, name, city from customers order by name");
	
	$product_result = mysql_query("select id, name from products order by sort_index"); ?>
<span class="header">Hantera priser</span>
<p>
<form action="system.php" method="post" onsubmit="return submitForm(this);">
<input type="hidden" name="function_file" value="price">
<input type="hidden" name="function" value="savePrice">
<table border="0" cellspacing="10">
	<tr>
		<td width="200" valign="top">
			<table border="0" width="100%" cellpadding="3" cellspacing="0">
				<tr bgcolor="#C0C0C0">
					<td>
						<b>Kunder:</b>
					</td>
				</tr>
				<tr>
					<td>
						<select name="customer_id" size="22" onclick="choose_customer(this.value)">
						<option value="0">- Standardkund -</option>						
<?php	while($row = mysql_fetch_assoc($customer_result))
	{ ?>
						<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
<?php	} ?>
					</td>
				</tr>
			</table>
		</td>
		<td width="250" valign="top">
			<table border="0" width="100%" cellpadding="3" cellspacing="0">
				<tr bgcolor="#C0C0C0">
					<td>
						<b>Produkter:</b>
					</td>
					<td>
						<b>Priser:</b>
					</td>
					<td>
					</td>
				</tr>
<?php	$bg = false;
	while($row = mysql_fetch_assoc($product_result))
	{
		if($bg)
			$bgcolor = "#EFEFEF";
		else
			$bgcolor = "#FFFFFF";
		$bg = !$bg; ?>
				<tr bgcolor="<?php echo $bgcolor; ?>">
					<td>
						<?php echo $row['name']; ?>
					</td>
					<td>
						<input type="text" size="5" name="price[<?php echo $row['id']; ?>]">
					</td>
					<td>
						SEK
					</td>
				</tr>
<?php	} ?>
			</table>
		</td>
		<td width="190" valign="top">
			<table border="0" width="100%" cellpadding="3" cellspacing="0">
				<tr bgcolor="#C0C0C0">
					<td>
						<b>Genomför förändringar:</b>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="save_button" value="Spara priserna" disabled>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?php } ?>
