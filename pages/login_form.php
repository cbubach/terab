<?php
// users must be outlogged to view this page
if(isset($_SESSION['inlogged']))
{
	header("Location: system.php?function_file=login&function=logout");
	exit();
}

function javascript() {
?>
<script>
<!--
function submitForm() 
{
	if(document.forms[0].username.value.search(/\w/)==-1) 
	{
		alert("Du måste fylla i ett användarnamn!");
		return false;
	}

	if(document.forms[0].password.value.search(/\w/)==-1) 
	{
		alert("Du måste fylla i ett lösenord!");
		return false;
	}

	return true;
}

function setFocusOnUsername()
{
	document.forms[0].username.focus();
}
-->
</script>
<?php
}

function body_onload()
{
	print "onload=\"setFocusOnUsername();\"";
}

function body()
{
	if(isset($_GET['message'])) 
	{
?>
<br>
<table border="0" cellspacing="0" cellpadding="0" width="250">
<tr><td align="center">
<fieldset>
		<?php if($_GET['message']=="failed") { ?>
<legend class="text7">Fel</legend>
	<table border="0">
		<tr>
			<td align="center">Du skrev fel anv&auml;ndarnamn<br> eller l&ouml;senord!</td>
		</tr>
	</table>
		<?php } else if($_GET['message']=="outlogged") { ?>
<legend class="text7">Utloggad</legend>
	<table border="0">
		<tr>
			<td align="center">Du &auml;r nu utloggad!</td>
		</tr>
	</table>
		<?php } ?>
</fieldset>
</td></tr>
</table>
<?php 	} ?>	
<br>
<form action="system.php" method="post" onsubmit="return submitForm();">
<input type="hidden" name="function_file" value="login">
<input type="hidden" name="function" value="login">
<table border="0" cellspacing="0" cellpadding="0" width="250">
<tr><td align="center">
<fieldset><legend class="text7">Login</legend>
<table border="0">
	<tr>
		<td>Anv&auml;ndarnamn<br><input type="text" name="username" style="width: 100px; height: 20px;" tabindex="1"></td>
		<td>L&ouml;senord<br><input type="password" name="password" style="width: 100px; height: 20px;" tabindex="2"></td>
	</tr>
	<tr>
		<td><input type="submit" value="Logga in" class="form-button" tabindex="3"></td></td>
	</tr>
</table>
</fieldset>
</td></tr>
</table>
</form>
</td></tr>
</table>
<?php } ?>
