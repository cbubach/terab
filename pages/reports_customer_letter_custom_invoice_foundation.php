<?php function javascript() { ?>
<script type="text/javascript" src="/order/JS/jquery.min.js"></script>
<script type="text/javascript">
<!--
$(document).ready(function() {
  $(window).focus(function() {
      console.log('Focus');
      $.get('/terab/system.php?reloadSession=1', function (data) {});
  });
});
-->
</script>
<script type="text/javascript" src="libs/MochiKit/Base.js"></script>
<script type="text/javascript" src="libs/MochiKit/Async.js"></script>
<script type="text/javascript">
<!--
function openWindow(url, width, height)
{
	open(url, "new_window", "toolbar=0,location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width="+width+",height="+height);
}

function save_customer_letter_sent_state(checkbox, orders)
{
	if(!confirm("Du kommer nu att ändra \"skickad\"-status på egenfakturan"))
	{
		if(checkbox.checked)
			checkbox.checked = false;
		else
			checkbox.checked = true;
	
		return;
	}
		
	for(var i=0;i<orders.length;i++)
	{
		var args = Array();
		args['function_file'] = 'reports';
		args['function'] = 'saveCustomerLetterSentState';
		args['order_id'] = orders[i];
		if(checkbox.checked)
			args['state'] = 'sent';
		else
			args['state'] = 'not_sent';
		
		doSimpleXMLHttpRequest("system.php", args);
	}
}
-->
</script>
<?php
}

function body_onload()
{

}

function body($print_mode=false)
{
	if(isset($_GET['waybill_no']))
	{
		$order_result = mysql_query("select customer_id, period_id from orders where waybill_no = '".$_GET['waybill_no']."'");
		$order_row = mysql_fetch_assoc($order_result);
		
		$customer_id = $order_row['customer_id'];
		$period_id = $order_row['period_id'];
	}
	else
	{
		$customer_id = $_GET['customer_id'];
		$period_id = $_GET['period_id'];
	}
	
	$customer_result = mysql_query("select name, city, contact_person, contact_fax, vat_reg_no, bankgiro_no from customers where id = '".$customer_id."'");
	$customer_row = mysql_fetch_assoc($customer_result);
	
	$period_result = mysql_query("select name, year from periods where id = '".$period_id."'");
	$period_row = mysql_fetch_assoc($period_result);
	
	$products = array();
	$products_result = mysql_query("select id, name, sign from products order by name");		
	while($product_row = mysql_fetch_assoc($products_result))
	{
		$products[$product_row['id']] = array('name'=>$product_row['name'], 'sign'=>$product_row['sign']);
	}
	
	$letter_data = array();
	$letter_data['orders'] = array();
	$letter_data['att'] = "";
	$letter_data['waybills'] = array();
	$letter_data['total_items'] = 0;
	$letter_data['products'] = array();
	$letter_data['prices'] = array();
	$letter_data['customer_letter_sent'] = true;
	
	$sql = "select id, att, waybill_no, extra_waybills, total_items, reports_text, customer_letter_sent from orders where";
	$orders = explode(',', $_GET['choosed_orders']);
	$first = true;
	foreach ($orders as $order_id)
	{
		if(!$first)
			$sql .= " or";
		$sql .= " id = '".$order_id."'";
		
		$first = false;
	}
	$order_result = mysql_query($sql);
	while($row = mysql_fetch_assoc($order_result))
	{
		$letter_data['orders'][] = $row['id'];
		$letter_data['att'] = $row['att']; // last orders "att" will be used
		$letter_data['waybills'][] = $row['waybill_no'];
		if($row['extra_waybills'] != '')
		{
			$extra_waybills = preg_split('/\\n/', trim($row['extra_waybills']));
			foreach($extra_waybills as $extra_waybill)
			{
				$letter_data['waybills'][] = $extra_waybill;
			}
		}
		$letter_data['total_items'] += $row['total_items'];
		if($letter_data['reports_text']!=$row['reports_text'])
			$letter_data['reports_text'] .= $row['reports_text'];
		if($row['customer_letter_sent']=='N')
			$letter_data['customer_letter_sent'] = false;
		
		$order_data_result = mysql_query("select product_id, value, price from order_data where order_id = '".$row['id']."'");
		while($row2 = mysql_fetch_assoc($order_data_result))
		{
			$letter_data['products'][$row2['product_id']] += $row2['value'];
			$letter_data['prices'][$row2['product_id']] = $row2['price'];
		}
	}
	
	if(!$print_mode) { ?>
<span class="header">Egenfaktura</span>
<p>
<b>[</b> <a href="javascript:openWindow('system.php/rapport.pdf?page=reports_customer_letter_print&customer_id=<?= $customer_id ?>&period_id=<?= $period_id ?>&dialog=true&referer=reports_customer_letter_custom_invoice_foundation&choosed_orders=<?= $_GET['choosed_orders'] ?>', 700, 480);"><img src="images/icons/medium/print.gif" border="0" align="absmiddle"></a> <a href="javascript:openWindow('system.php/rapport.pdf?page=reports_customer_letter_print&customer_id=<?= $customer_id ?>&period_id=<?= $period_id ?>&dialog=true&referer=reports_customer_letter_custom_invoice_foundation&choosed_orders=<?= $_GET['choosed_orders'] ?>', 700, 480);">Skriv ut</a> <b>]</b>
<!-- <b>[</b> <a href="javascript:openWindow('system.php?page=reports_customer_letter_print&customer_id=<?= $customer_id ?>&period_id=<?= $period_id ?>&dialog=true&referer=reports_customer_letter_custom_invoice_foundation&choosed_orders=<?= $_GET['choosed_orders'] ?>&email=true', 700, 480);"><img src="images/icons/small/customer_letter_sent.gif" alt="" /> E-posta rapport</a> <b>]</b> -->
<b>[</b> <input type="checkbox" name="customer_letter_sent_state" onclick="save_customer_letter_sent_state(this, Array('<?= implode('\',\'', $letter_data['orders']); ?>'));"<?php if($letter_data['customer_letter_sent']) print " checked"; ?>> Egenfaktura skickad <b>]</b>
<p>
<?php 	} ?>
<table border="0" width="600px" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<span class="text12">Trollhättan <?= date("Y-m-d") ?>
			<table border="0" width="600px" cellspacing="1" cellpadding="0" bgcolor="#000000">
			<tr><td>
			<table border="0" width="600px" bgcolor="#FFFFFF">
				<tr>
					<td>
						<span class="text18"><b>Egenfaktura (Er faktura till Terab AB)</b></span>
						<br>
						<span class="text16">
						Gäller som faktura för köp/reparation av<br>
						emballage enligt överenskommelse med kunden.</span>
						<br>
						<span class="text12">
						Om denna ska gälla som Er faktura, skriv under och skicka tillbaka!
						</span>
					</td>
				</tr>
			</table>
			</td></tr>
			</table>
			<br><br>
			<span class="text12">Gällande inkomna pallar/emballage från kund:</span><br>
			<table border="0" width="600px" cellspacing="0" cellpadding="0">
				<tr>
					<td width="50%">
						<span class="text12">
						<b><?= $customer_row['name'] ?>, <?= $customer_row['city'] ?></b><br>
						<b>Att: <?= $letter_data['att'] ?></b>
						</span>
					</td>
					<td width="50%">
						<span class="text12">
						Period: <b><?= $period_row['name'] ?></b>
						</span>
					</td>
				</tr>
			</table>
			<br>
			<table border="0" width="600px" cellspacing="1" cellpadding="0" bgcolor="#000000">
			<tr><td>
			<table border="0" width="600px" bgcolor="#FFFFFF" cellspacing="0" cellpadding="3">
				<tr bgcolor="#C0C0C0">
					<td align="right"><span class="text12"><b>Antal</span></b></td>
					<td>&nbsp;</td>
					<td><span class="text12"><b>Artikel</span></b></td>
					<td align="right"><span class="text12"><b>Á pris</span></b></td>
					<td align="right"><span class="text12"><b>Summa</b></span></td>
				</tr>
<?php	$total_price = 0;
 	foreach($letter_data['products'] as $product_id=>$items_of_product)
 	{
 		if($products[$product_id]['sign'] == '+')
 			$total_price += ($letter_data['prices'][$product_id] * $items_of_product);
 		else
 			$total_price -= ($letter_data['prices'][$product_id] * $items_of_product); 
 		
 		if($bg) { $bgcolor="#EEEEEE"; } else { $bgcolor="#FFFFFF"; } $bg = !$bg ?>
				<tr bgcolor="<?php echo $bgcolor; ?>">
					<td align="right"><span class="text12"><?php echo $items_of_product; ?></span></td>
					<td>&nbsp;</td>
					<td><span class="text12"><?php echo $products[$product_id]['name']; ?></span></td>
					<td align="right"><span class="text12"><?php echo $letter_data['prices'][$product_id]; ?></span></td>
					<td align="right"><span class="text12"><?php
  		if($products[$product_id]['sign'] == '+')
  			print $items_of_product * $letter_data['prices'][$product_id];
  		else
  			print (0 - ($items_of_product * $letter_data['prices'][$product_id]));
  ?></span></td>
				</tr>
 <?php	} ?>
				<tr bgcolor="#C0C0C0">
 					<td colspan="5">
 						<span class="text12"><b>Totalt antal pallar: <?php echo $letter_data['total_items']; ?> st</b></span>
 					</td>
 				</tr>
			</table>
			</td></tr>
			</table>
			<br>
			<table border="0" width="600px" cellspacing="0" cellpadding="0">
				<tr>
					<td width="55%" valign="top">
						<span class="text12"><b>Meddelande</b></span>
						<br>
						<table border="0" width="90%" cellspacing="0" cellpadding="1" bgcolor="#000000">
						<tr><td>
						<table border="0" width="100%" bgcolor="#FFFFFF" cellspacing="0" cellpadding="3">
							<tr>
								<td height="75" valign="top">
									<span class="text12">
									<?php echo $letter_data['reports_text']; ?>
									</span>
								</td>
							</tr>
						</table>
						</td></tr>
						</table>
					</td>
					<td width="45%">
						<span class="text12"><b><?php
	if($total_price>0)
	{
		print "Belopp att fakturera Terab AB";
	}
	elseif($total_price<0)
	{
		print "Terab Fakturerar Er";
		
		// make the total price as a postive number
		$total_price = (-1 * ($total_price));
	} ?></b></span>
						<br>
						<table border="0" width="270px" cellspacing="1" cellpadding="3" bgcolor="#000000">
							<tr>
								<td bgcolor="#FFFFFF">
									<span class="text12">Netto:</span>
								</td>
								<td  bgcolor="#FFFFFF" align="right">
									<span class="text12"><b><?php echo number_format($total_price, 2, ',', ' '); ?></b></span>
								</td>
							</tr>
							<tr>
								<td bgcolor="#FFFFFF">
									<span class="text12">Moms:</span>
								</td>
								<td  bgcolor="#FFFFFF" align="right">
									<span class="text12"><b><?php echo number_format(round($total_price * 0.25, 2), 2, ',', ' '); ?></b></span>
								</td>
							</tr>
							<tr>
								<td bgcolor="#FFFFFF">
									<span class="text12">Totalt:</span>
								</td>
								<td  bgcolor="#FFFFFF" align="right">
									<span class="text12"><b><?php echo number_format(round($total_price * 1.25, 2), 2, ',', ' '); ?></b></span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table border="0" width="600px" cellspacing="1" cellpadding="0" bgcolor="#000000">
			<tr><td>
			<table border="0" width="600px" bgcolor="#FFFFFF" cellspacing="0" cellpadding="3">
				<tr>
					<td>
						<b>Inkommande fraktsedelnummer:</b><br>
						<table border="0" width="80%" cellspacing="0" align="center">
							<tr>
								<td width="33%"><?php echo $letter_data['waybills'][0]; ?></td>
								<td width="33%"><?php echo $letter_data['waybills'][3]; ?></td>
								<td width="33%"><?php echo $letter_data['waybills'][6]; ?></td>
							</tr>
							<tr>
								<td><?php echo $letter_data['waybills'][1]; ?></td>
								<td><?php echo $letter_data['waybills'][4]; ?></td>
								<td><?php echo $letter_data['waybills'][7]; ?></td>
							</tr>
							<tr>
								<td><?php echo $letter_data['waybills'][2]; ?></td>
								<td><?php echo $letter_data['waybills'][5]; ?></td>
								<td><?php echo $letter_data['waybills'][8]; ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</td></tr>
			</table>
			<br><br>
			<table border="0" width="600px" cellspacing="0" cellpadding="3">
				<tr>
					<td width="75%" valign="top">
						<table border="0" width="100%">
							<tr>
								<td width="50%">
									<b>Moms reg. nr:</b>
								</td>
								<td width="50%">
									<b>Bankgiro nr:</b>
								</td>
							</tr>
							<tr>
								<?php if($customer_row['vat_reg_no']!='') { ?>
								<td height="30" valign="bottom"><?php echo $customer_row['vat_reg_no']; ?></td>
								<?php } else { ?>
								<td height="30">&nbsp;</td>
								<?php } ?>
								<?php if($customer_row['bankgiro_no']!='') { ?>
								<td height="30" valign="bottom"><?php echo $customer_row['bankgiro_no']; ?></td>
								<?php } else { ?>
								<td height="30">&nbsp;</td>
								<?php } ?>
							</tr>
							<tr>
								<td width="50%">
									_______________________
								</td>
								<td width="50%">
									_______________________
								</td>
							</tr>
							<tr>
								<td height="60">&nbsp;</td>
								<td height="60">&nbsp;</td>
							</tr>
							<tr>
								<td width="50%">
									<b>Ort/datum:</b>
								</td>
								<td width="50%">
									<b>Underskrift:</b>
								</td>
							</tr>
							<tr>
								<td height="30">&nbsp;</td>
								<td height="30">&nbsp;</td>
							</tr>
							<tr>
								<td width="50%">
									_______________________
								</td>
								<td width="50%">
									_______________________
								</td>
							</tr>
							<tr>
								<td height="30">&nbsp;</td>
								<td height="30">&nbsp;</td>
							</tr>
							<tr>
								<td>
									<img src="images/terab_logo_small.gif"><br>
									<img src="images/trollhattan_logo_small.gif">
								</td>
								<td>
									<img src="images/cpl_logo_small.png"><br>
								</td>
							</tr>
						</table>
						<span class="text12">
						
						</span>
						<br><br>
					</td>
					<td width="25%" valign="top">
						<span class="text12">
						<b>Telefon</b><br>
						0520-482200
						<br><br>
						<b>Organisationsnr</b><br>
						556296-8908
						<br><br>
						<b>Faktureringsadress</b><br>
                        Terab AB<br>
						Grafitvägen 3<br>
						461 38 Trollhättan<br><br>                        
						ekonomi@terab.se
						<br><br>
						<b>Besöksadress/Lager</b><br>
						Terab AB<br>
						Grafitvägen 3<br>
						461 38 Trollhättan
					</td>
				</tr>
			</table>
		</td>	
	</tr>
</table>
<?php } ?>
