<?php function javascript() { ?>
<script type"text/javascript">
var useBSNns = true;
</script>
<link rel="stylesheet" href="libs/AutoSuggest/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf8">
<script type="text/javascript" src="libs/AutoSuggest/bsn.AutoSuggest_2.1_comp.js" charset="utf8"></script>

<script type="text/javascript" src="libs/MochiKit/Base.js"></script>
<script type="text/javascript" src="libs/MochiKit/Async.js"></script>
<script type="text/javascript">
<!--
function checkIfWaybillNoExists(waybill_no, redirect_url)
{
	var url = "system.php";
	var arguments = {
		'function_file': 'order',
		'function': 'checkIfWaybillNoExistsJSON',
		'waybill_no': waybill_no
	}
	
	var d = loadJSONDoc(url, arguments);
	
	var gotData = function (data)
	{
		if(data['exists'] != "true")
		{
			alert("Det angivna fraktsedelnumret finns inte");
			return false;
		}
		else if(data['counter']>1)
		{
			alert("Det finns fler än en order med det angiva fraktsedelnumret");
			return false;
		}

		window.location = redirect_url;
		
	};
	var fetchFailed = function (err)
	{
  		alert(err);
  		return false;
	};
	d.addCallbacks(gotData, fetchFailed);
}

function customer_default_report()
{
	var waybill_no = Number(document.getElementsByName("waybill_no")[0].value);
	
	if (isNaN(waybill_no) || waybill_no == "")
	{
		var customer_id = document.getElementById("customer_id").value;
	
		var period_id = document.getElementsByName("period_id")[0].options[document.getElementsByName("period_id")[0].selectedIndex].value;
	
		if (customer_id == "")
		{
			alert("Du måste välja en kund eller ett fraktsedelnummer för att kunna visa kundens standard-rapport");
			return;
		}
	
		if (period_id.substr(0, 5) == 'year_')
		{
			alert("Du kan inte visa en standard-rapport för ett helt år");
			return;
		}
		
		window.location = "system.php?function_file=reports&function=showCustomerDefaultReport&customer_id="+customer_id+"&period_id="+period_id;
	}
	else
	{
		var url = "system.php?function_file=reports&function=showCustomerDefaultReport&waybill_no="+waybill_no;
		checkIfWaybillNoExists(waybill_no, url);
	}
}

function customer_letter()
{
	var waybill_no = Number(document.getElementsByName("waybill_no")[0].value);
	
	if (isNaN(waybill_no) || waybill_no == "")
	{
		var customer_id = document.getElementById("customer_id").value;
		
		var period_id = document.getElementsByName("period_id")[0].options[document.getElementsByName("period_id")[0].selectedIndex].value;
	
		if (customer_id == "")
		{
			alert("Du måste välja en kund eller ett fraktsedelnummer för att kunna visa kundbrev");
			return;
		}
	
		if (period_id.substr(0, 5) == 'year_')
		{
			alert("Du kan inte visa ett kundbrev för ett helt år");
			return;
		}
		
		window.location = "system.php?page=reports_customer_letter&customer_id="+customer_id+"&period_id="+period_id;
	}
	else
	{
		var url = "system.php?page=reports_customer_letter&waybill_no="+waybill_no;
		checkIfWaybillNoExists(waybill_no, url);
	}
}

function customer_letter_account_balance()
{
	var waybill_no = Number(document.getElementsByName("waybill_no")[0].value);
	
	if (isNaN(waybill_no) || waybill_no == "")
	{
		var customer_id = document.getElementById("customer_id").value;
		
		var period_id = document.getElementsByName("period_id")[0].options[document.getElementsByName("period_id")[0].selectedIndex].value;
	
		if (customer_id == "")
		{
			alert("Du måste välja en kund eller ett fraktsedelnummer för att kunna visa kundbrev");
			return;
		}
	
		if (period_id.substr(0, 5) == 'year_')
		{
			alert("Du kan inte visa ett kundbrev för ett helt år");
			return;
		}
		
		window.location = "system.php?page=reports_customer_letter_account_balance&customer_id="+customer_id+"&period_id="+period_id;
	}
	else
	{
		var url = "system.php?page=reports_customer_letter_account_balance&waybill_no="+waybill_no;
		checkIfWaybillNoExists(waybill_no, url);
	}
}

function customer_letter_simple()
{
	var waybill_no = Number(document.getElementsByName("waybill_no")[0].value);
	
	if (isNaN(waybill_no) || waybill_no == "")
	{
		var customer_id = document.getElementById("customer_id").value;
		
		var period_id = document.getElementsByName("period_id")[0].options[document.getElementsByName("period_id")[0].selectedIndex].value;
	
		if (customer_id == "")
		{
			alert("Du måste välja en kund eller ett fraktsedelnummer för att kunna visa kundbrev");
			return;
		}
	
		if (period_id.substr(0, 5) == 'year_')
		{
			alert("Du kan inte visa ett kundbrev för ett helt år");
			return;
		}
		
		window.location = "system.php?page=reports_customer_letter_simple&customer_id="+customer_id+"&period_id="+period_id;
	}
	else
	{
		var url = "system.php?page=reports_customer_letter_simple&waybill_no="+waybill_no;
		checkIfWaybillNoExists(waybill_no, url);
	}
}

function customer_letter_custom()
{
	var customer_id = document.getElementById("customer_id").value;
	
	var period_id = document.getElementsByName("period_id")[0].options[document.getElementsByName("period_id")[0].selectedIndex].value;
	
	if (customer_id == "")
	{
		alert("Du måste välja en kund för att kunna visa ett anpassat kundbrev/egenfaktura");
		return;
	}
		
	var url = "system.php?page=reports_customer_letter_custom_list&customer_id="+customer_id+"&period_id="+period_id;
	
	window.location = url;
}

function customer_letter_invoice_foundation()
{
	var waybill_no = Number(document.getElementsByName("waybill_no")[0].value);
	
	if (isNaN(waybill_no) || waybill_no == "")
	{
		var customer_id = document.getElementById("customer_id").value;
		
		var period_id = document.getElementsByName("period_id")[0].options[document.getElementsByName("period_id")[0].selectedIndex].value;
	
		if (customer_id == "")
		{
			alert("Du måste välja en kund eller ett fraktsedelnummer för att kunna visa egenfaktura");
			return;
		}
	
		if (period_id.substr(0, 5) == 'year_')
		{
			alert("Du kan inte visa en egenfaktura för ett helt år");
			return;
		}
		
		window.location = "system.php?page=reports_customer_letter_invoice_foundation&customer_id="+customer_id+"&period_id="+period_id;
	}
	else
	{
		var url = "system.php?page=reports_customer_letter_invoice_foundation&waybill_no="+waybill_no;
		checkIfWaybillNoExists(waybill_no, url);
	}
}

function reports_customer_private_receipt()
{
	var waybill_no = Number(document.getElementsByName("waybill_no")[0].value);
	
	if (isNaN(waybill_no) || waybill_no == "")
	{
		alert("På privatkunder kan du enbart välja från fraksedelnummret");
		return;
	}
	else
	{
		var url = "system.php?page=reports_customer_private_receipt&waybill_no="+waybill_no;
		checkIfWaybillNoExists(waybill_no, url);
	}
}

function period_statistics()
{
	var customer_id = document.getElementById("customer_id").value;
	
	var period_id = document.getElementsByName("period_id")[0].options[document.getElementsByName("period_id")[0].selectedIndex].value;
	
	if (customer_id == "")
	{
		customer_id = "0";
	}
	
	var url = "system.php?page=reports_period_statistics&customer_id="+customer_id+"&period_id="+period_id;
	
	window.location = url;
}

function onDocumentLoad()
{
	var options = {
		script:"system.php?function_file=customer&function=autoSuggestCustomerJSON",
		varname:"&input",
		json:true,
		shownoresults:false,
		timeout:5000,
		callback: function (obj) { document.getElementById('customer_id').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('customer_name', options);
}
-->
</script>
<?php
}

function body_onload()
{
	print "onload=\"javascript:onDocumentLoad();\"";
}

function body()
{
	$customer_result = mysql_query("select id, name, city from customers order by name");
?>
<form>
<input type="hidden" name="customer_id" id="customer_id">
<span class="header">Visa rapporter</span>
<p>
<table border="0" cellpadding="10" cellspacing="0">
	<tr>
		<td valign="top">
			<table border="0" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td><img src="images/icons/medium/customer.gif" border="0"></a></td>
					<td>
						<b>Välj kund</b><br>
						<input type="text" name="customer_name" id="customer_name">
						<!--
						<select name="customer_id" size="1">
						<option value="all_customers">-Alla kunder-</option>
<?php	while($row = mysql_fetch_assoc($customer_result)) { ?>
						<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>, <?php echo $row['city']; ?></option>
<?php	} ?>
						</select>-->
					</td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/period.gif" border="0"></a></td>
					<td>
						<b>Välj period</b><br>
						<select name="period_id" size="1">
						<option value="year_all">-Alla år-</option>
<?php	$year = "";
	
	$period_result = mysql_query("select id, year, name, current_period from periods order by id");
	while($period_row = mysql_fetch_assoc($period_result))
	{ 
		if($period_row['year'] != $year)
		{
			$year = $period_row['year']; ?>
						<option value="year_<?php echo $period_row['year']; ?>"><?php echo $period_row['year']; ?></option>
<?php		} ?>
						<option value="<?php echo $period_row['id']; ?>"<?php if($period_row['current_period']=='Y') print " selected"; ?>><?php echo $period_row['name']; ?></option>
<?php	} ?>
						</select>
					</td>
				</tr>
			</table>
			<p>
			<table border="0" width="100%" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td width="32"><img src="images/icons/medium/query.gif" border="0"></a></td>
					<td>
						<b>Fraktsedelnummer</b><br>
						<input type="text" name="waybill_no" size="20">
					</td>
				</tr>
			</table>
		</td>
		<td valign="top">
			<table border="0" width="220" cellpadding="10" bgcolor="lightgrey" style="border: 2px dashed;">
				<tr>
					<td><img src="images/icons/medium/customer_letter.gif"></td>
					<td><a href="javascript:customer_default_report();">Kundens standard-<br>rapport</a></td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/customer_letter.gif"></td>
					<td><a href="javascript:customer_letter();">Kundbrev</a></td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/customer_letter.gif"></td>
					<td><a href="javascript:customer_letter_simple();">Kundbrev - Enkelt</a></td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/customer_letter.gif"></td>
					<td><a href="javascript:customer_letter_account_balance();">Kundbrev inkl saldo</a></td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/invoice_foundation.gif"></td>
					<td><a href="javascript:customer_letter_invoice_foundation();">Egenfaktura</a></td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/invoice_foundation.gif"></td>
					<td><a href="javascript:reports_customer_private_receipt();">Kvitto, privatkund</a></td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/customer_letter_custom.gif"></td>
					<td><a href="javascript:customer_letter_custom();">Anpassa Kundbrev/ Egenfaktura</a></td>
				</tr>
				<tr>
					<td><img src="images/icons/medium/period_statistics.gif"></td>
					<td><a href="javascript:period_statistics();">Periodstatistik</a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php } ?>
