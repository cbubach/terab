<?php function javascript() { ?>
<script type"text/javascript">
var useBSNns = true;
</script>
<link rel="stylesheet" href="libs/AutoSuggest/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf8">
<script type="text/javascript" src="libs/AutoSuggest/bsn.AutoSuggest_2.1_comp.js" charset="utf8"></script>

<script type="text/javascript" src="libs/MochiKit/Base.js"></script>
<script type="text/javascript" src="libs/MochiKit/Async.js"></script>

<script type="text/javascript">
<!--
function submitForm(form)
{
	if(form.customer_id.value == "0")
	{
		alert("Du måste välja en kund");
		return;
	}
	
	if (form.waybill_no.value == "" || isNaN(Number(form.waybill_no.value)))
	{
		alert("Du måste fylla i ett fraktsedelnummer bestående av endast siffror");
		return;
	}
	
	var total_items = Number(form.total_items.value);
	if (form.total_items.value == "" || isNaN(total_items))
	{
		alert("Värdet i \"Antal\" måste vara ett heltal större än 0");
		return;
	}
	
	var url = "system.php";
	var arguments = {
		'function_file': 'order',
		'function': 'checkIfWaybillNoExistsJSON',
		'waybill_no': form.waybill_no.value
	}
	
	var d = loadJSONDoc(url, arguments);
	
	var gotData = function (data)
	{
		if(data['exists'] == "true")
		{
			alert("Det angivna fraktsedelnumret finns redan");
			return;
		}

		form.submit();
		
	};
	var fetchFailed = function (err)
	{
  		alert(err.getText());
  		return;
	};
	d.addCallbacks(gotData, fetchFailed);
}

function getCustomerContactPerson(form)
{
	var url = "system.php";
	var arguments = {
		'function_file': 'customer',
		'function': 'getCustomerContactPersonJSON',
		'customer_id': form.customer_id.value
	}
	
	var d = loadJSONDoc(url, arguments);
	
	var gotData = function (data)
	{
		form.att.value = data['contact_person'];
		
	};
	var fetchFailed = function (err)
	{
  		return;
	};
	d.addCallbacks(gotData, fetchFailed);
}

function onDocumentLoad()
{
	var options = {
		script:"system.php?function_file=customer&function=autoSuggestCustomerJSON",
		varname:"&input",
		json:true,
		shownoresults:false,
		timeout:5000,
		callback: function (obj) { document.getElementById('customer_id').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('customer_name', options);
}
-->
</script>
<?php
}

function body_onload()
{
	print "onload=\"javascript:onDocumentLoad();\"";
}

function body()
{
	$result = mysql_query("select id, name, city from customers order by name");
?>
<span class="header">Lägg till ny order</span>
<p>
<form action="system.php" method="post">
<input type="hidden" name="function_file" value="order">
<input type="hidden" name="function" value="createOrder">
<input type="hidden" name="customer_id" id="customer_id" value="">
<table border="0" cellpadding="10" cellspacing="1">
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Datum:</b>
		</td>
		<td>
			<input type="text" name="order_date" size="8" value="<?= date("Y-m-d") ?>">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Ange kund:</b>
		</td>
		<td>
			<input type="text" name="customer_name" id="customer_name" size="35" onblur="getCustomerContactPerson(this.form);">
		</td>
	</tr>
<!--	<tr>
		<td bgcolor="#C0C0C0">
			<b>Välj kund:</b>
		</td>
		<td>
			<select name="customer_id" size="1" onchange="getCustomerContactPerson(this.form);">
			<option value="0">-Välj kund-</option>
<?php	$sql = "select id, name, city from customers order by name";
	$result = mysql_query($sql);
	while($row = mysql_fetch_assoc($result))
	{
		print "<option value=\"".$row['id']."\">".$row['name'].", ".$row['city']."</option>\n";
	} ?>
		</td>
	</tr>-->
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Attesteras:</b>
		</td>
		<td>
			<input type="text" name="att">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Fraktsedelnummer:</b>
		</td>
		<td>
			<input type="text" name="waybill_no">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Extra fraktsedelnummer:<br>(ett per rad)</b>
		</td>
		<td>
			<textarea cols="20" rows="3" name="extra_waybills"></textarea>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Antal:</b>
		</td>
		<td>
			<input type="text" name="total_items" size="5">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Övriga noteringar:</b>
		</td>
		<td>
			<textarea cols="20" rows="3" name="notes"></textarea>
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Person-nummer (privatleverantör):</b>
		</td>
		<td>
			<input type="text" name="person_nr" id="person_nr" size="35">
		</td>
	</tr>
	<tr>
		<td bgcolor="#C0C0C0">
			<b>Banknummer (privatleverantör):</b>
		</td>
		<td>
			<input type="text" name="bank_nr" id="bank_nr" size="35">
		</td>
	</tr>	
</table>
<p>
<input type="button" value="Skapa order" onclick="submitForm(this.form);">
</form>
<?php } ?>
